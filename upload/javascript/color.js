/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

function RGBFromhex() {
	
}

function hexFromRGB(r, g, b) {
		var hex = [
			r.toString(16),
			g.toString(16),
			b.toString(16)
		];
		$.each(hex, function (nr, val) {
			if (val.length == 1) {
				hex[nr] = '0' + val;
			}
		});
		return hex.join('').toUpperCase();
	}
	
function refreshSwatch() {
		var red = $("#red").slider("value")
			,green = $("#green").slider("value")
			,blue = $("#blue").slider("value")
			,hex = hexFromRGB(red, green, blue);
		$("#swatch").css("background-color", "#" + hex);
		$("#colorhex").val("#"+hex);
}

$(function() {
		$("#red, #green, #blue").slider({
			orientation: 'horizontal',
			range: "min",
			max: 255,
			value: 140,
			slide: refreshSwatch,
			change: refreshSwatch
		});
		
		$("#red").slider("value", 140);
		$("#green").slider("value", 140);
		$("#blue").slider("value", 140);
		
		refreshSwatch();
});
		