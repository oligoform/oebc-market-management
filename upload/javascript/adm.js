/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

function bringtofront(div) {
	
	var zmax = 0;
	
	$('#content').children().each(
	function() {
		var cur = $(this).css( 'z-index');
		if((zmax < cur) && (zmax != 'auto')) {
			zmax = cur;
		}
	});
	if((zmax != 0) && (zmax != 'auto')) {
		$(div).css( 'z-index', zmax+1 );
	} else {
		$(div).css( 'z-index', '999' );
	}
}

function cWin(div) {
	bringtofront(div);
	$(div).toggle('fast');
	return true;	
}

var spawns = 0;

var spwx = 20;
var spwy = 10;

var vdesk = 1;

function spawngeneric(src, title, height, width) {
	if(!title) {
		title = "New Window";
	}
	if(!height) {
		height = 250;
	}
	if(!width) {
		width = 920;
	}
	//clone template and append new identifier class
	$('#generictpl').clone().appendTo('#content').addClass('spawn'+spawns);
	//$('.spawn'+spawns).addClass('window');
	$('.spawn'+spawns).find('iframe').attr('src', src);
	$('.spawn'+spawns).draggable({ opacity: 0.7, stack: ".winstack", refreshPositions: true, iframeFix: true });
	$('.spawn'+spawns).resizable({ minWidth: 200, minHeight:200, ghost: true  });
	
	$('.spawn'+spawns).height(height);
	$('.spawn'+spawns).width(width);
	
	//Todo: Fix
	$('.spawn'+spawns).children(".title").html(title);
	
	$('.spawn'+spawns).removeAttr('id');
		
	$('.spawn'+spawns).toggle();

	$('.spawn'+spawns).addClass("desk"+vdesk);

	$('.spawn'+spawns).css("margin-left", spwx);
	$('.spawn'+spawns).css("margin-top", spwy);

	if(spwx > 150) {
		spwx = 20;	
	} else {
		spwx = spwx + 10;
	}
	
	if(spwy > 150) {
		spwy = 10;	
	} else {
		spwy = spwy + 10;
	}

	bringtofront('.spawn'+spawns);

	// new spawn added
	spawns++;
}

function unselectAll() {

	var inFocus = 0;

	$('.desktop').find('.vmenu').hide();

	$('.desktop').children().each(
    
	function(){
		
		if($(this).hasClass("focus")) {
			inFocus = 1;
		}
		
	});
	
	if(!inFocus) {
		
		$('.desktop').children().each(
    
		function(){
		
			if($(this).hasClass("selected")) {
				$(this).removeClass("selected") 			// typ
	
			}

		});
		
		if($("body").children("#fileinfo")) {
			$("#fileinfo").hide();	
		}
	
	}
		
	//alert(selectMe);
}

function selectItem(selectMe) {

	$('.desktop').children().each(
    
	function(){
			
		if($(this).hasClass("selected")) {
			
			$(this).removeClass("selected");			// typ
			
		}
		if($(this).hasClass("focus")) {	
			
			$(selectMe).addClass("selected");

		}
	});
}

function loadContext() {

		$('.file').bind('contextmenu',function(e){
			var $cmenu = $('.desktop').find('.vmenu');
			$('<div class="overlay"></div>').css({left : '0px', top : '0px',position: 'absolute', width: '100%', height: '100%' }).click(function() {
				$(this).remove();
				$cmenu.hide();
			}).bind('contextmenu' , function(){return false;}).appendTo(document.body);
				$('.desktop').find('.vmenu').css({ left: e.pageX, top: e.pageY }).show();
				bringtofront($('.desktop').find('.vmenu'));
				$('.desktop').find('.vmenu').show();
				return false;
		});

		$('.vmenu .first_li').live('click',function() {
			if( $(this).children().size() == 1 ) {
				$('.vmenu').hide();
				$('.overlay').hide();
			}
		});
	
		$(".first_li").hover(function () {
			$(this).css({backgroundColor : '#E0EDFE' , cursor : 'pointer'});
			if ( $(this).children().size() >0 )
					$(this).find('.inner_li').show();	
					$(this).css({cursor : 'default'});
			}, 
			function () {
				$(this).css('background-color' , '#fff' );
				$(this).find('.inner_li').hide();
		});	
}

function viewFile() {
	
}

function downloadFile() {
	var file = $(".desktop").find(".selected").find(".path").val();
	window.open( "/components/download.php?file=" + escape(file));
}

function deleteFile() {
	var file = $(".desktop").find(".selected").find(".path").val();
	if(file != "/desktop") {
	$.post("/components/ajax.php", {	
					'mode'	   			: 	'removefile',
					'file'				: 	file
	},
	function(){
		window.location.reload()
	});
	} else {
		alert("Sie können das Desktop nicht löschen!");	
	}
}

function renameFile() {
	
}

// Keycombo for ctrl + key
// $.ctrl(key, function(s) { do something });

$.ctrl = function(k, c, a) {
	$(document).keydown(function(e) {
		if(!a) {
			a = [];	
		}
		if(e.keyCode == k.charCodeAt(0) && e.ctrlKey) 
		{
			c.apply(this, a);
			return false;	
		}
		});
};

$.ctrl('E', function(s) {
    
	//alert(s);
	if(vdesk == 1) {
		vdesk = 5;
	}
	vdesk--;
	$(".vdesk").html(vdesk);
	
	$(".desk1").hide("fast");
	$(".desk2").hide("fast");
	$(".desk3").hide("fast");
	$(".desk4").hide("fast");
	
	$(".desk"+vdesk).show("fast");
	
	
}, ["Desktop changed"]);

$.ctrl('D', function(s) {
    
	//alert(s);
	if(vdesk == 4) {
		vdesk = 0;
	}
	vdesk++;
	$(".vdesk").html(vdesk);
	
	$(".desk1").hide("fast");
	$(".desk2").hide("fast");
	$(".desk3").hide("fast");
	$(".desk4").hide("fast");
	
	$(".desk"+vdesk).show("fast");
	
	
}, ["Desktop changed"]);

