<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

	session_start();


	ini_set("display_errors", "on");
	
	require dirname(__FILE__)."/../classes/db.class.php";
	require dirname(__FILE__)."/../classes/auth.class.php";
	require dirname(__FILE__)."/../classes/subsystem.class.php";

	$db 	= new dbal;
	$auth 	= new auth($db);	
	$files 	= new subsystem($db);

	// Check Login

	$uid = $auth->get_uid();
	
	if(!$uid) {
		echo "no uid!";
		exit(1);
	}

	if(ini_get('zlib.output_compression'))
	{
		ini_set('zlib.output_compression', 'off');
	}

	// Prevent abuse
	$file = str_replace(array("/", ".."), "", $_REQUEST["file"]);
	
	$outfile = realpath(dirname(__FILE__)."/../filesystem/".$file);

	//do download
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false);
	header("Content-type: application/force-download");
	header("Content-Disposition: attachment; filename=\"".$file."\";" );
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($outfile));
	readfile($outfile);
	exit();

?>