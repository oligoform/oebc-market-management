<!-- files -->
{section name=deskfiles loop=$deskfiles}
{if $deskfiles[deskfiles].is_dir != ""}
<div class="file folder">
{if $deskfiles[deskfiles].8 == ""}
<img src="/images/system/folder.png" width="50" height="50" alt="$deskfiles[deskfiles].filename" />
{else}
<img src="/images/system/folder_full.png" width="50" height="50" alt="$deskfiles[deskfiles].filename" />
{/if}
{else}
<div class="file">
{if $deskfiles[deskfiles].2 == "txt"}
<img src="/images/system/file_txt.png" width="50" height="50" alt="$deskfiles[deskfiles].0" />
{elseif $deskfiles[deskfiles].2 == "pdf"}
<img src="/images/system/file_pdf.png" width="50" height="50" alt="$deskfiles[deskfiles].0" />
{elseif $deskfiles[deskfiles].2 == "csv"}
<img src="/images/system/file_csv.png" width="50" height="50" alt="$deskfiles[deskfiles].0" />
{elseif $deskfiles[deskfiles].2 == "xml"}
<img src="/images/system/file_xml.png" width="50" height="50" alt="$deskfiles[deskfiles].0" />
{else}
<img src="/images/system/file.png" width="50" height="50" alt="$deskfiles[deskfiles].0" />
{/if}
{/if}
<span style="white-space:nowrap;">{$deskfiles[deskfiles].1}</span>

<input type="hidden" value="{$deskfiles[deskfiles].filename}" class="filename"/>
<input type="hidden" value="{$deskfiles[deskfiles].filesize}" class="filesize"/>
<input type="hidden" value="{$deskfiles[deskfiles].filedate}" class="filedate"/>
<input type="hidden" value="{$deskfiles[deskfiles].path}" class="path"/>
<input type="hidden" value="{$deskfiles[deskfiles].is_dir}" class="is_dir"/>

</div>
<!-- files -->
{/section}
<!-- context menu -->
<div class="vmenu">
 		<div class="first_li"><span><a href="#" onclick="viewFile();">Ansehen</a></span></div>
 		<div class="first_li"><span><a href="#" onclick="downloadFile();">Download</a></span></div>
		<div class="first_li"><span><a href="#" onclick="deleteFile();">Löschen</a></span></div>
        <div class="first_li"><span><a href="#" onclick="renameFile();">Umbenennen</a></span></div>
</div>
<!-- / context menu -->
<input type="hidden" value="{$currentfs}" class="currentfs" />
