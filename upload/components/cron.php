<?

/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

ini_set("display_errors", "on");

require "../classes/subsystem.class.php";
require "../classes/db.class.php";

echo "running...";

$db 	= new dbal;
$files 	= new subsystem($db);

$now 		= time();

$cron_005 	= $files->getOpt("cron_005");
$cron_015 	= $files->getOpt("cron_015");
$cron_030 	= $files->getOpt("cron_030");
$cron_1 	= $files->getOpt("cron_1");	
$cron_6 	= $files->getOpt("cron_6");	
$cron_12 	= $files->getOpt("cron_12");	
$cron_24 	= $files->getOpt("cron_24");	

$path = realpath(dirname(__FILE__).'/../cron');

$nowtime= time() - $now;
// für alle 5min
if ($now >= $cron_005) {
	$nextexec = $now + ($nowtime+300);
	$files->setOpt("cron_005", $nextexec);
	$jobs = $files->dirListExt($path.'/m05/');
	
	foreach($jobs as $job) {
	
//		echo $job."\n";
		
		$ch = @curl_init($_SERVER['HTTP_HOST']."/cron/m05/".$job); 
		@curl_setopt($ch,CURLOPT_HEADER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$out = @curl_exec($ch);

	}
}

$nowtime= time() - $now;
// für alle 15min
if ($now >= $cron_015) {
	$nextexec = $now + ($nowtime+900);
	$files->setOpt("cron_015", $nextexec);
	$jobs = $files->dirListExt($path.'/m15/');
	
	foreach($jobs as $job) {
		
//		echo $job."\n";
		
		$ch = @curl_init($_SERVER['HTTP_HOST']."/cron/m15/".$job); 
		@curl_setopt($ch,CURLOPT_HEADER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$out = @curl_exec($ch);

	}	
	
}

$nowtime= time() - $now;
// für alle 30min
if ($now >= $cron_030) {
	$nextexec = $now + ($nowtime+1800);
	$files->setOpt("cron_030", $nextexec);
	$jobs = $files->dirListExt($path.'/m30/');
	
	foreach($jobs as $job) {

//		echo $job."\n";

		$ch = @curl_init($_SERVER['HTTP_HOST']."/cron/m30/".$job); 
		@curl_setopt($ch,CURLOPT_HEADER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$out = @curl_exec($ch);

	}	
}

// für alle 1h
if ($now >= $cron_1) {
	$nextexec = $now + ($nowtime+3600);
	$files->setOpt("cron_1", $nextexec);
	$jobs = $files->dirListExt($path.'/h01/');
	
	foreach($jobs as $job) {

//		echo $job."\n";
				
		$ch = @curl_init($_SERVER['HTTP_HOST']."/cron/h01/".$job); 
		@curl_setopt($ch,CURLOPT_HEADER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$out = @curl_exec($ch);

	}		
}

$nowtime= time() - $now;
// für alle 6h
if ($now >= $cron_6) {
	$nextexec = $now + ($nowtime+(3600*6));
	$files->setOpt("cron_6", $nextexec);
	$jobs = $files->dirListExt($path.'/h06/');
	
	foreach($jobs as $job) {
		
//		echo $job."\n";
		
		$ch = @curl_init($_SERVER['HTTP_HOST']."/cron/h06/".$job); 
		@curl_setopt($ch,CURLOPT_HEADER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$out = @curl_exec($ch);

	}
}

$nowtime= time() - $now;
// für alle 12h
if ($now >= $cron_12) {
	$nextexec = $now + ($nowtime+(3600*12));
	$files->setOpt("cron_12", $nextexec);
	$jobs = $files->dirListExt($path.'/h12/');
	
	foreach($jobs as $job) {
		
//		echo $job."\n";
		
		$ch = @curl_init($_SERVER['HTTP_HOST']."/cron/h12/".$job); 
		@curl_setopt($ch,CURLOPT_HEADER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$out = @curl_exec($ch);

	}	
}

$nowtime= time() - $now;
// für alle 24h
if ($now >= $cron_24) {
	$nextexec = $now + ($nowtime+(3600*24));
	$files->setOpt("cron_24", $nextexec);
	$jobs = $files->dirListExt($path.'/h24/');
	
	foreach($jobs as $job) {
		
		$ch = @curl_init($_SERVER['HTTP_HOST']."/cron/h24/".$job); 
		@curl_setopt($ch,CURLOPT_HEADER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		@curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$out = @curl_exec($ch);

	}		
}

echo "done!";

?>