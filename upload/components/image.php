<? 
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/
 
	if($_REQUEST["size"] == "full") {
		header("Content-Type: image/jpeg");
	} else {
		header("Content-Type: image/png");
	}
	
	//ini_set("display_errors", "off");
	
	$i = str_replace("..", "", $_REQUEST["img"]);
	if(!preg_match('/^\/([-_\w]+\/)*[-_\w]+\.(gif|jpg|png)$/iD', $i)) 
	{
		exit(0);
	}
	
	//$im = imagecreatefromjpeg($image);
	
	list($width, $height) = getimagesize($image);
	
	$percent = 1;
	
	if($_REQUEST["h"] && $_REQUEST["w"]) {
		$maxheight = $_REQUEST["h"];
		$maxwidth  = $_REQUEST["w"];		
	} else {
		$maxheight = 75;
		$maxwidth  = 100;
	}
	
	if($_REQUEST["size"] == "full") {
		
		if($height > $maxheight) {
			$percent = $maxheight / $height;
		}

		$new_width = $width * $percent;
		$new_height = $height * $percent;

		$image_p = imagecreatetruecolor($new_width, $new_height);	
		$im = imagecreatefromstring(file_get_contents($image));
		imagecopyresampled($image_p, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	
		$white = imagecolorallocate($image_p,255,255,255);
		$black = imagecolorallocate($image_p,0,0,0);
	
		list($null, $path, $path2, $filename) = split("/", $_REQUEST["img"]);
	
		imagejpeg($image_p,'',100);
		imagedestroy($im);
		imagedestroy($image_p);
	
	} else {
		
		$hoehe  = $height; 
		$breite = $width;
		
		if ($hoehe >= $breite) {
  	 	 $hoehe1  = $maxheight;
  	 	 $prozent = ($hoehe1 / $hoehe);
  		 $breite1 = ($prozent * $breite);

  		 if ($breite1 > $maxwidth) {
  		  $breite1 = $maxwidth;
  		  $prozent = ($breite1 / $breite);
  		  $hoehe1  = ($prozent * $hoehe);
  		 }
  		}
  		else {
  		 $breite1 = $maxwidth;
  		 $prozent = ($breite1 / $breite);
  		 $hoehe1  = ($prozent * $hoehe);

  		 if ($hoehe1 > $maxheight) {
  		  $hoehe1  = $maxheight;
	  	  $prozent = ($hoehe1 / $hoehe);
	  	  $breite1 = ($prozent * $breite);
	  	 }
	  	}
	
		$nn = $hoehe1 - $maxheight;
		$nn = ($nn / 2) - $nn;

		$image_p = imagecreatetruecolor($maxwidth,$maxheight) or die('Kann keinen neuen GD-Bild-Stream erzeugen');	
	
		if($_REQUEST["bg"] == "trans")  
		{
			imagesavealpha($image_p, true);
			imagealphablending($image_p, false);

			$trans_color = imagecolorallocatealpha($image_p, 0, 0, 0, 127);
  			imagefill($image_p, 0, 0, $trans_color);
		}
	
		$im = imagecreatefromstring(file_get_contents($image));
		imagecopyresampled($image_p, $im, 0, $nn, 0, 0, $breite1, $hoehe1, $width, $height);
			
		imagepng($image_p);
		imagedestroy($im);
		imagedestroy($image_p);	
	}

?>