<?

/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


session_start();

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Datum in der Vergangenheit

//disable notcies about timezones on OSX
date_default_timezone_set(date_default_timezone_get());

ini_set("error_reporting", "E_ALL  & ~E_NOTICE");
ini_set("display_errors", "on");
//ini_set('default_socket_timeout', 500);  

define('isOEBC', TRUE);

require dirname(__FILE__)."/../classes/smarty/Smarty.class.php";

require dirname(__FILE__)."/../classes/db.class.php";
require dirname(__FILE__)."/../classes/auth.class.php";
require dirname(__FILE__)."/../classes/subsystem.class.php";
require dirname(__FILE__)."/../classes/oxid.class.php";

// Merge GET & POST
$method = $_SERVER['REQUEST_METHOD'];
$request = split("/", substr(@$_SERVER['PATH_INFO'], 1));

if($method == 'POST')
{
	$_REQUEST = $_POST;
}

// Init
$db 	= new dbal;
$auth 	= new auth($db);
$files 	= new subsystem($db);
$template = new smarty;

$template->template_dir = dirname(__FILE__).'/../templates/';
$template->compile_dir  = dirname(__FILE__).'/../tmp/';

// Check Login

$uid = $auth->get_uid();
if(!$uid)
{
	echo "Please log in!";	
}
if($_REQUEST["thisapp"]) {
	
	$template->template_dir = dirname(__FILE__).'/../apps/';
	
	// Module laden/checken
	$appslist 	= $files->getApps(dirname(__FILE__).'/../apps/');
	$app 	 	= $_REQUEST["thisapp"];

	$app = ereg_replace("[^A-Za-z0-9]", "", $app);

	$valid_app = in_array($app, $appslist);

	if($valid_app) {
		// Module laden/checken
		$modlist 	= $files->dirList(dirname(__FILE__).'/../apps/'.$app."/ajax/");
		$mode 	 	= $_REQUEST["mode"];

		$mode = ereg_replace("[^A-Za-z0-9]", "", $mode);

		$valid_mode = in_array($mode, $modlist);

		if($valid_mode) {
			include(dirname(__FILE__).'/../apps/'.$app."/ajax/".$mode.'.php');	
		} else {
			echo "ERROR! Module is missing! $mode";	
		}

	} else {
		echo "ERROR! App is missing! $app";	
	}
	
} elseif($_REQUEST["mode"]) {
	
	// Module laden/checken
	$modlist 	= $files->dirList(dirname(__FILE__).'/modules/');
	$mode 	 	= $_REQUEST["mode"];

	$mode = ereg_replace("[^A-Za-z0-9]", "", $mode);

	$valid_mode = in_array($mode, $modlist);

	if($valid_mode) {
		include(dirname(__FILE__).'/modules/'.$mode.'.php');	
	} else {
		echo "ERROR! Module is missing! $mode";	
	}
}

?>