<?
class eBayFetchToken 
{
	var $_sid;
	var $_call = "FetchToken";

	private function _getRequestBody()
	{
		$requestXmlBody =
		
		'<?xml version="1.0" encoding="utf-8"?> 
			<FetchTokenRequest xmlns="urn:ebay:apis:eBLBaseComponents"> 
   			<SessionID>'.$this->_sid.'</SessionID> 
		</FetchTokenRequest>'; 
		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody());	
	}
		
}
?>