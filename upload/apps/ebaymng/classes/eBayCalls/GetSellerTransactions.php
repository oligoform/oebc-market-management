<?
class eBayGetSellerTransactions
{	
	
	private $_call = 'GetSellerTransactions';
	var $_dur = "3";

	private function _getRequestBody($nc)
	{
		
		
		$apiValues = $nc->_keys[$nc->_environment];

		$requestXmlBody =
		
		'<?xml version="1.0" encoding="utf-8"?>
			<GetSellerTransactionsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
				<RequesterCredentials>
    				<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  				</RequesterCredentials>
 				<ErrorLanguage>de_DE</ErrorLanguage>
  				<Version>'.$nc->_eBayApiVersion.'</Version>';
    			
		$requestXmlBody .=			
				'<DetailLevel>ReturnAll</DetailLevel>
				<Pagination>
	  			<NumberOfDays>'.$this->_dur.'</NumberOfDays>
				<EntriesPerPage>200</EntriesPerPage>
				<PageNumber>'.$this->_page.'</PageNumber>
    			</Pagination>';
				
  		$requestXmlBody .=		
				'</GetSellerTransactionsRequest>';
		

	//var_dump($requestXmlBody);

		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
}

?>