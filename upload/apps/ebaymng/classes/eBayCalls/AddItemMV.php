<?

class eBayAddItemMV
{	

	var $_itemStartPrice = array();
	var $_itemBuyItNowPrice;
	var $_itemTitle;
	var $_itemDescription;
	var $_categoryId;
	var $_categoryId2;
	var $_quantity = array();
	var $_payPalEmailAddress;
	var $_currency	;
	var $_site;
	var $_listingDuration;
	var $_galleryType;
	var $_picnum;
	var $_listType;
	var $_schedule;
	
	var $_shipping;
	var $_shipping2;
	var $_freeShipping;
	
	var $_variants;
	
	var $_p = array();

	private $_call = 'AddFixedPriceItem';

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];

		$db 	= new dbal;	
		$files 	= new subsystem($db);


		$requestXmlBody = 
		
		'<?xml version="1.0" encoding="UTF-8"?>
			<AddFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
				<RequesterCredentials>
					<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
				</RequesterCredentials>
			<DetailLevel>ReturnAll</DetailLevel>
			<ErrorLanguage>de_DE</ErrorLanguage>
			<Version>'.$nc->_eBayApiVersion.'</Version>
			<Item>';

		$picurl = "";
		
		if($this->_picnum > 0) {	
			
			//<GalleryURL>''</GalleryURL>
			
			$requestXmlBody .=
			'<PictureDetails>
			 <PhotoDisplay>None</PhotoDisplay>
			 <GalleryType>'.$this->_galleryType.'</GalleryType>';
			
			
			for($i = 1; $i <= $this->_picnum;$i++)
			{	
				$ul = new eBayEPSUpload;		
						
				$file = $this->_p[$i];
				if($file != "") {
		
					$picurl = $ul->uploadImage($file);
				
					$requestXmlBody .=
					'<PictureURL>'.$picurl.'</PictureURL>';
				
				}
				
			}
			$requestXmlBody .=				
			'</PictureDetails>';
		
		}
		
		if($this->_shopcat) {
			$requestXmlBody .= "<Storefront>  
					      		<StoreCategoryID>".$this->_shopcat."</StoreCategoryID>";
		
			if($this->_shopcat2) {	  
    	  		$requestXmlBody .=	"<StoreCategory2ID>".$this->_shopcat2."</StoreCategory2ID>";
			}
		
	    	$requestXmlBody .="</Storefront>";  
		}
		
		if($this->_subtitel) {
			$requestXmlBody .= "<SubTitle>".$this->_subtitel."</SubTitle>";
		}
		
		//$null = "im Shop!";	
				
		//$this->_listingDuration = "Days_3";	
		
				//$null = "im Shop!";	
				
		//$this->_listingDuration = "Days_3";		
		
		$tpl = new smarty;
		$tpl->compile_dir  = dirname(__FILE__).'/../../../../tmp/';
		
		// register the resource name "var"
		$tpl->register_resource("var", array("var_template",
                                       "var_timestamp",
                                       "var_secure",
                                       "var_trusted"));


		function var_template ($tpl_name, &$tpl_source, &$smarty_obj)
		{
			global $files;
   
    		$tpl_source = $files->getOpt("template");
    		return empty($tpl_source) ? false : true;
		}

		function var_timestamp($tpl_name, &$tpl_timestamp, &$smarty_obj)
		{
    		//echo "timestamp called!";
			
			$time = $smarty_obj->get_template_vars($tpl_name.'_time');
    		return !empty($time) ? $time : time();
		}

		function var_secure($tpl_name, &$smarty_obj)
		{
    		return true;
		}

		function var_trusted($tpl_name, &$smarty_obj)
		{
		} 
		
		if (get_magic_quotes_gpc()) {
			$desc = stripslashes($this->_itemDescription);
		} else {
			$desc = $this->_itemDescription;
		}
		
		$tpl->clear_cache("var:dummy");
		
		$tpl->assign("auctiondetails","<!--start_desc_oebc-->".$desc."<!--end_desc_oebc-->");
		
		$tpl->assign("articlepicture",$picurl);
		
		$tpl->assign("auctiontitle","<!--start_title_oebc-->".$this->_itemTitle."<!--end_title_oebc-->");	
		
			
		$tpldisplay = $tpl->fetch("var:dummy");
						
				
		$requestXmlBody .=
		
		'		<Country>DE</Country>
				<Currency>'.$this->_currency.'</Currency>
				<Description><![CDATA['.$tpldisplay.']]></Description>
				<DispatchTimeMax>1</DispatchTimeMax>
				<ListingDuration>'.$this->_listingDuration.'</ListingDuration>
				<Location><![CDATA['.$this->_location.']]></Location>
				<ConditionID>'.$this->_conditionid.'</ConditionID>';	
		
		if($this->_MoneyXferAccepted) {	
			$requestXmlBody .=
				'<PaymentMethods>MoneyXferAccepted</PaymentMethods>';
		}
		if($this->_COD) {	
			$requestXmlBody .=
				'<PaymentMethods>COD</PaymentMethods>';
		}
		/*if($this->_Other) {	
			$requestXmlBody .=
				'<PaymentMethods>Other</PaymentMethods>';
		}*/
		if($this->_PaymentSeeDescription) {	
			$requestXmlBody .=
				'<PaymentMethods>PaymentSeeDescription</PaymentMethods>';
		}
		if($this->_Paypal) {	
			$requestXmlBody .=
				'<PaymentMethods>PayPal</PaymentMethods>
				<PayPalEmailAddress>'.$this->_payPalEmailAddress.'</PayPalEmailAddress>';
		}
    	if(!$this->_PaymentSeeDescription 
			&& !$this->_Other 
			&& !$this->_COD 
			&& !$this->_MoneyXferAccepted 
			&& !$this->_Paypal
			) {	
			$requestXmlBody .=
				'<PaymentMethods>None</PaymentMethods>';
		}
				
		$requestXmlBody .=		
				'<PrimaryCategory>
					<CategoryID>'.$this->_categoryId.'</CategoryID>
				</PrimaryCategory>
				<RegionID>0</RegionID>';
				
		if($this->_categoryId2 != "") {
			$requestXmlBody .=		
				'<SecondaryCategory>
					<CategoryID>'.$this->_categoryId2.'</CategoryID>
				</SecondaryCategory>';
		}
		
		$returns = $files->getOpt("RefundOption");
		
		/*
		
		<ReturnPolicy>
 			<RefundOption>Exchange</RefundOption>
 			<Refund>Exchange</Refund>
 			<ReturnsWithinOption>Days_30</ReturnsWithinOption>
 			<ReturnsWithin>30 Days</ReturnsWithin>
 			<ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>
 			<ReturnsAccepted>Returns Accepted</ReturnsAccepted>
 			<Description>Detailed explanation of seller's policy here</Description>
 			<ShippingCostPaidByOption>Buyer</ShippingCostPaidByOption>
			<ShippingCostPaidBy>Buyer</ShippingCostPaidBy>
		</ReturnPolicy>

		*/
		
		if($returns == "") {
			$requestXmlBody .=		
				'<ReturnPolicy>
					<ReturnsAcceptedOption>ReturnsNotAccepted</ReturnsAcceptedOption>
				</ReturnPolicy>';
		} else {
			$requestXmlBody .=		
				'<ReturnPolicy>
					<ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>
					<ReturnsWithinOption>'.$files->getOpt("ReturnsWithinOption").'</ReturnsWithinOption>
					<ShippingCostPaidByOption>'.$files->getOpt("ShippingCostPaidByOption").'</ShippingCostPaidByOption>
					<Description><![CDATA['.$files->getOpt("ReturnDescription").']]></Description>
				</ReturnPolicy>';
		}
	
		/*
		
			$requestXmlBody .= '
		      <NameValueList> 
        		<Name>Marke</Name> 
        		<Value></Value> 
		      </NameValueList> 
		';
		
		*/
		
		// Item specifics hack
	
		$requestXmlBody .=	'
    	<ItemSpecifics>';
		
		/*for($i = 0;$i < count($this->_variantsnames); $i++) {		
			
			// finde Größe in string
			if(strstr($this->_variantsnames[$i], 'öße')) {
				
      		$requestXmlBody .=	'
	  			<NameValueList>
        			<Name>Schuhgröße</Name>';
        			
					for($i2 = 0;$i2 < count($this->_variants[$i]); $i2++) {
						//if($this->_quantity[$i2] > 0) {	
							$requestXmlBody .=	'<Value>EUR '.$this->_variants[$i][$i2].'</Value>';
						//}
					}
					
      		$requestXmlBody .=	'
				</NameValueList>
      			';
			}
		}*/
		
		$requestXmlBody .= '
			  <NameValueList> 
        		<Name>Stil</Name> 
        		<Value>Modern</Value> 
		      </NameValueList> 
		      <NameValueList> 
        		<Name>Marke</Name> 
        		<Value>Schuhe</Value> 
		      </NameValueList> 
		';
				
		$requestXmlBody .=	'
		</ItemSpecifics>';

		// Item specifics hack
	
		$requestXmlBody .=	'
		<Variations>
    	<VariationSpecificsSet>	';
		
		for($i = 0;$i < count($this->_variantsnames); $i++) {		
	
			if(strstr($this->_variantsnames[$i], 'öße')) {
				$this->_variantsnames[$i] = "Schuhgröße";
			}
	
      		$requestXmlBody .=	'
	  			<NameValueList>
        			<Name>'.$this->_variantsnames[$i].'</Name>';
        			
					for($i2 = 0;$i2 < count($this->_variants[$i]); $i2++) {
						//if($this->_quantity[$i2] > 0) {	
							$requestXmlBody .=	'<Value>'.$this->_variants[$i][$i2].'</Value>';
						//}
					}
					
      		$requestXmlBody .=	'
				</NameValueList>
      			';
				
		}
		
		$requestXmlBody .=	'
		</VariationSpecificsSet>';
				
		//var_dump($this->_itemStartPrice);		
						
		for($i = 0;$i <= count($this->_itemStartPrice); $i++) {
						
			if($this->_quantity[$i] > 0) {	
    			
				$requestXmlBody .=
				'<Variation>
	      		<StartPrice>'.$this->_itemStartPrice[$i].'</StartPrice>
				<Quantity>'.$this->_quantity[$i].'</Quantity>
      			<VariationSpecifics>';
        	
				for($i2 = 0; $i2 < count($this->_avariants[$i]); $i2++) 
				{
					if(strstr($this->_variantsnames[$i2], 'öße')) {
						$this->_variantsnames[$i2] = "Schuhgröße";
					}
	
					$requestXmlBody .=
					'<NameValueList>
          			<Name>'.$this->_variantsnames[$i2].'</Name>
          			<Value>'.$this->_avariants[$i][$i2].'</Value>
        			</NameValueList>';
				
				}
			
				$requestXmlBody .=
				'</VariationSpecifics>
				</Variation>
				';
			}
			
		}
		
		$requestXmlBody .=	
		'</Variations>'; 
		
		//var_dump(	$this->_avariants);
		//exit();	
		
		/*
		
		Todo:
		
		<Storefront>  
      		<StoreCategoryID>1</StoreCategoryID>  
      		<StoreCategory2ID>20</StoreCategory2ID>  
	    </Storefront>  
		*/
		
		$requestXmlBody .=
		
		'		<ShippingDetails>
					<ShippingServiceOptions>
						<ShippingService>'.$this->_delivery.'</ShippingService>
						<ShippingServicePriority>1</ShippingServicePriority>
						<ShippingServiceCost currencyID="EUR">'.$this->_shipping.'</ShippingServiceCost>
						<ShippingServiceAdditionalCost currencyID="EUR">'.$this->_shipping2.'</ShippingServiceAdditionalCost>
						<FreeShipping>'.$this->_freeShipping.'</FreeShipping>
					</ShippingServiceOptions>
				</ShippingDetails>
				<Site>'.$this->_site.'</Site>
				<Title><![CDATA['.$this->_itemTitle.']]></Title>
			</Item>
			</AddFixedPriceItemRequest>';
			
			return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
}
?>