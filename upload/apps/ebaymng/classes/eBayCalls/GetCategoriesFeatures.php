<?
class eBayGetCategoryFeatures
{	

	private $_call = 'GetCategoryFeatures';

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];

		$requestXmlBody = '
			<?xml version="1.0" encoding="utf-8"?>
				<GetCategoryFeaturesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  				<RequesterCredentials>
    			<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  				</RequesterCredentials>
				<FeatureID>VariationsEnabled</FeatureID>
  				<ViewAllNodes>true</ViewAllNodes>
				<DetailLevel>ReturnAll</DetailLevel>
			</GetCategoryFeaturesRequest>
			';

		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}
?>