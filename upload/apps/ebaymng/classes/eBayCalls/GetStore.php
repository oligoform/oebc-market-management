<?
class eBayGetStore
{	

	private $_call = 'GetStore';
	//var 	$_site;

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];

		$requestXmlBody = '
			<?xml version="1.0" encoding="utf-8"?>
			<GetStoreRequest xmlns="urn:ebay:apis:eBLBaseComponents">
				<RequesterCredentials> 
    				<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
				</RequesterCredentials> 
				<ErrorLanguage>de_DE</ErrorLanguage>
				<Version>'.$nc->_eBayApiVersion.'</Version>
			</GetStoreRequest>
		';
		//echo "Body:".$requestXmlBody;
		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}
?>