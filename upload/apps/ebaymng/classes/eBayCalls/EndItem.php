<?
class eBayRelistItem
{	

	private $_call = 'RelistItem';
	var 	$_itemid;

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];

		$requestXmlBody = '
			<?xml version="1.0" encoding="utf-8"?>
			<RelistItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  			<RequesterCredentials>
   			<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  			</RequesterCredentials>
  				<ErrorLanguage>de_DE</ErrorLanguage>
  				<Version>'.$nc->_eBayApiVersion.'</Version>
  				<Item>
    				<ItemID>'.$this->_itemid.'</ItemID>
				</Item>
			</RelistItemRequest>
		';
		//echo "Body:".$requestXmlBody;
		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}
?>