<?

class eBayAddItem
{	

	var $_itemStartPrice;
	var $_itemBuyItNowPrice;
	var $_itemTitle;
	var $_itemDescription;
	var $_categoryId;
	var $_categoryId2;
	var $_quantity;
	var $_payPalEmailAddress;
	var $_currency	;
	var $_site;
	var $_listingDuration;
	var $_galleryType;
	var $_picnum;
	var $_listType;
	var $_schedule;
	
	var $_shipping;
	var $_shipping2;
	var $_freeShipping;
	
	var $_p = array();

	private $_call = 'AddItem';

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];

		$db 	= new dbal;		
		$files 	= new subsystem($db);

		$requestXmlBody = 
		
		'<?xml version="1.0" encoding="UTF-8"?>
			<AddItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
				<RequesterCredentials>
					<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
				</RequesterCredentials>
			<DetailLevel>ReturnAll</DetailLevel>
			<ErrorLanguage>de_DE</ErrorLanguage>
			<Version>'.$nc->_eBayApiVersion.'</Version>
			<Item>';
			
		if(($this->_itemBuyItNowPrice > 0) && ($this->_listType == "Chinese")) {	
				$requestXmlBody .= 
				
				'<BuyItNowPrice currencyID="'.$this->_currency.'">'.$this->_itemBuyItNowPrice.'</BuyItNowPrice>
				<Quantity>'.$this->_quantity.'</Quantity>';
		}
		
		$picurl = "";
		
		if($this->_picnum > 0) {	
			
			//<GalleryURL>''</GalleryURL>
			
			$requestXmlBody .=
			'<PictureDetails>
			 <PhotoDisplay>None</PhotoDisplay>
			 <GalleryType>'.$this->_galleryType.'</GalleryType>';
			
			for($i = 1; $i <= $this->_picnum;$i++)
			{	
				$ul = new eBayEPSUpload;
				
				//$file = 
				
				$file = $this->_p[$i];
	
				if($file != "") {
					$picurl = $ul->uploadImage($file);
				
					$requestXmlBody .=
					'<PictureURL>'.$picurl.'</PictureURL>';
				
				}
			}
			$requestXmlBody .=				
			'</PictureDetails>';
		
		}
		
		//$null = "im Shop!";	
				
		//$this->_listingDuration = "Days_3";		
		
		$tpl = new smarty;
		$tpl->compile_dir  = dirname(__FILE__).'/../../../../tmp/';
		
		// register the resource name "var"
		$tpl->register_resource("var", array("var_template",
                                       "var_timestamp",
                                       "var_secure",
                                       "var_trusted"));

		function var_template ($tpl_name, &$tpl_source, &$smarty_obj)
		{
			global $files;
   
    		$tpl_source = $files->getOpt("template");
    		return empty($tpl_source) ? false : true;
		}

		function var_timestamp($tpl_name, &$tpl_timestamp, &$smarty_obj)
		{
    		$time = $smarty_obj->get_template_vars($tpl_name.'_time');
    		return !empty($time) ? $time : time();
		}

		function var_secure($tpl_name, &$smarty_obj)
		{
    		return true;
		}

		function var_trusted($tpl_name, &$smarty_obj)
		{
		} 
		
		if (get_magic_quotes_gpc()) {
			$desc = stripslashes($this->_itemDescription);
		} else {
			$desc = $this->_itemDescription;
		}
				
	
		$tpl->clear_cache("var:dummy");
		
				
		$tpl->assign("auctiondetails","<!--start_desc_oebc-->".$desc."<!--end_desc_oebc-->");
		
		$tpl->assign("articlepicture",$picurl);
		
		$tpl->assign("auctiontitle","<!--start_title_oebc-->".$this->_itemTitle."<!--end_title_oebc-->");	
		
										
		$requestXmlBody .=
		
		'		<Country>'.$files->getOpt("eland").'</Country>
				<Currency>'.$this->_currency.'</Currency>
				<Description><![CDATA['.$tpl->fetch("var:dbtmpl").']]></Description>
				<DispatchTimeMax>'.$files->getOpt("DispatchTimeMax").'</DispatchTimeMax>
				<ListingDuration>'.$this->_listingDuration.'</ListingDuration>
				<ListingType>'.$this->_listType.'</ListingType>
				<Location><![CDATA['.$this->_location.']]></Location>
				<ConditionID>'.$this->_conditionid.'</ConditionID>';	
		

		if($this->_MoneyXferAccepted) {	
			$requestXmlBody .=
				'<PaymentMethods>MoneyXferAccepted</PaymentMethods>';
		}
		if($this->_COD) {	
			$requestXmlBody .=
				'<PaymentMethods>COD</PaymentMethods>';
		}
		/*if($this->_Other) {	
			$requestXmlBody .=
				'<PaymentMethods>Other</PaymentMethods>';
		}*/
		if($this->_PaymentSeeDescription) {	
			$requestXmlBody .=
				'<PaymentMethods>PaymentSeeDescription</PaymentMethods>';
		}
		if($this->_Paypal) {	
			$requestXmlBody .=
				'<PaymentMethods>PayPal</PaymentMethods>
				<PayPalEmailAddress>'.$this->_payPalEmailAddress.'</PayPalEmailAddress>';
		}
		
    	if(!$this->_PaymentSeeDescription 
			&& !$this->_Other 
			&& !$this->_COD 
			&& !$this->_MoneyXferAccepted 
			&& !$this->_Paypal
			) {	
			$requestXmlBody .=
				'<PaymentMethods>None</PaymentMethods>';
		}

		
		
				
		$requestXmlBody .=		
				'<PrimaryCategory>
					<CategoryID>'.$this->_categoryId.'</CategoryID>
				</PrimaryCategory>
				<RegionID>0</RegionID>';
				
		if($this->_categoryId2 != "") {
			$requestXmlBody .=		
				'<SecondaryCategory>
					<CategoryID>'.$this->_categoryId2.'</CategoryID>
				</SecondaryCategory>';
		}
		
		$returns = $files->getOpt("RefundOption");
		
		/*
		
		<ReturnPolicy>
 			<RefundOption>Exchange</RefundOption>
 			<Refund>Exchange</Refund>
 			<ReturnsWithinOption>Days_30</ReturnsWithinOption>
 			<ReturnsWithin>30 Days</ReturnsWithin>
 			<ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>
 			<ReturnsAccepted>Returns Accepted</ReturnsAccepted>
 			<Description>Detailed explanation of seller's policy here</Description>
 			<ShippingCostPaidByOption>Buyer</ShippingCostPaidByOption>
			<ShippingCostPaidBy>Buyer</ShippingCostPaidBy>
		</ReturnPolicy>

		*/
		
		if($returns == "") {
			$requestXmlBody .=		
				'<ReturnPolicy>
					<ReturnsAcceptedOption>ReturnsNotAccepted</ReturnsAcceptedOption>
				</ReturnPolicy>';
		} else {
			$requestXmlBody .=		
				'<ReturnPolicy>
					<ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>
					<ReturnsWithinOption>'.$files->getOpt("ReturnsWithinOption").'</ReturnsWithinOption>
					<ShippingCostPaidByOption>'.$files->getOpt("ShippingCostPaidByOption").'</ShippingCostPaidByOption>
					<Description><![CDATA['.$files->getOpt("ReturnDescription").']]></Description>
				</ReturnPolicy>';
		}
		
		if($this->_listType == "Chinese") {			
			$requestXmlBody .=	'<StartPrice>'.$this->_itemStartPrice.'</StartPrice>';
		} else {
			$requestXmlBody .=	'<StartPrice>'.$this->_itemBuyItNowPrice.'</StartPrice>';
		}
		
		if($this->_schedule != "") {			
			$requestXmlBody .=	'<ScheduleTime>'.$this->_schedule.'</ScheduleTime>';
		} 
		
		if($this->_shopcat) {
			$requestXmlBody .= "<Storefront>  
					      		<StoreCategoryID>".$this->_shopcat."</StoreCategoryID>";
		
			if($this->_shopcat2) {	  
    	  		$requestXmlBody .=	"<StoreCategory2ID>".$this->_shopcat2."</StoreCategory2ID>";
			}
		
	    	$requestXmlBody .="</Storefront>";  
		}
		
		if($this->_subtitel) {
			$requestXmlBody .= "<SubTitle>".$this->_subtitel."</SubTitle>";
		}
		
		if (get_magic_quotes_gpc()) {
			$tit = stripslashes($this->_itemTitle);
		} else {
			$tit = $this->_itemTitle;
		}
		

		
		$requestXmlBody .=
		
		'		<ShippingDetails>
					<ShippingServiceOptions>
						<ShippingService>'.$this->_delivery.'</ShippingService>
						<ShippingServicePriority>1</ShippingServicePriority>
						<ShippingServiceCost currencyID="'.$this->_currency.'">'.$this->_shipping.'</ShippingServiceCost>
						<ShippingServiceAdditionalCost currencyID="'.$this->_currency.'">'.$this->_shipping2.'</ShippingServiceAdditionalCost>
						<FreeShipping>'.$this->_freeShipping.'</FreeShipping>
					</ShippingServiceOptions>
				</ShippingDetails>
				<Site>'.$this->_site.'</Site>
				<Title><![CDATA['.$tit.']]></Title>
			</Item>
			</AddItemRequest>';
		//echo $requestXmlBody;
			return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
}

/******************* Multivariant Listing ********************/

?>