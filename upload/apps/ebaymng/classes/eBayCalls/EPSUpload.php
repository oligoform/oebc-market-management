<?

class eBayEPSUpload
{
	var $requestToken;
	var $serverUrl;
	var $compatLevel = false;
	var $call;
    var $boundary;


	var $_siteId;	// default: Germany
	var $_environment;   // toggle between sandbox and production
	var $_eBayApiVersion;
	var $_keys;
	
	public function __construct() {
		
		
		$db 	= new dbal;
		$files 	= new subsystem($db);
		
		require(dirname(__FILE__)."/../eBay.inc.php");
		
		$this->_siteId 			= $_siteId;
		$this->_environment		= $_environment;
		$this->_eBayApiVersion 	= $_eBayApiVersion;
		$this->_keys 			= $_keys;

	}
	
	/**	sendHttpRequest
		Sends a HTTP request to the server for this session
		Input:	$requestBody
		Output:	The HTTP Response as a String
	*/
	public function sendHttpRequest($requestBody)
	{     
		$apiValues = $this->_keys[$this->_environment];
	   
        $headers = array (
            'Content-Type: multipart/form-data; boundary=' . $this->boundary,
            'Content-Length: ' . strlen($requestBody),
			'X-EBAY-API-COMPATIBILITY-LEVEL: ' . $this->_eBayApiVersion,
			'X-EBAY-API-DEV-NAME: ' . $apiValues['DEVID'],
			'X-EBAY-API-APP-NAME: ' . $apiValues['AppID'],
			'X-EBAY-API-CERT-NAME: ' . $apiValues['CertID'],
			'X-EBAY-API-CALL-NAME: ' . $this->call,
			'X-EBAY-API-SITEID: ' . $this->_siteId,
        );
		
		//initialize a CURL session - need CURL library enabled
		$connection = curl_init();
		
		//var_dump($headers);
		
		curl_setopt($connection, CURLOPT_URL, $apiValues['ServerUrl']);
        curl_setopt($connection, CURLOPT_TIMEOUT, 360 );
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($connection, CURLOPT_POST, 1);
		curl_setopt($connection, CURLOPT_POSTFIELDS, $requestBody);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($connection, CURLOPT_FAILONERROR, 0 );
        curl_setopt($connection, CURLOPT_FOLLOWLOCATION, 1 );
        //curl_setopt($connection, CURLOPT_HEADER, 1 );           // Uncomment these for debugging
        //curl_setopt($connection, CURLOPT_VERBOSE, true);        // Display communication with serve
        curl_setopt($connection, CURLOPT_USERAGENT, 'ebatns;xmlstyle;1.0' );
        curl_setopt($connection, CURLOPT_HTTP_VERSION, 1 );       // HTTP version must be 1.0
		$response = curl_exec($connection);
		
        
        if ( !$response ) {
            print "curl error " . curl_errno($connection ) . "\n";
        }
		curl_close($connection);
		return $response;
		
    } // function sendHttpRequest
	
	function uploadImage($url)
	{  
	
	$apiValues = $this->_keys[$this->_environment];
	
	// siteID needed in request - US=0, UK=3, DE=77...
    $call    = 'UploadSiteHostedPictures';   // the call being made:
    
    $picNameIn = 'Auktions Beschreibung';
	
	$multiPartImageData = file_get_contents($url);
	
    ///Build the request XML request which is first part of multi-part POST
    $xmlReq = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
    $xmlReq .= '<' . $call . 'Request xmlns="urn:ebay:apis:eBLBaseComponents">' . "\n";
    $xmlReq .= "<Version>".$this->_eBayApiVersion."</Version>\n";
    $xmlReq .= "<PictureName>$picNameIn</PictureName>\n";    
    $xmlReq .= "<RequesterCredentials><eBayAuthToken>".$apiValues['UserToken']."</eBayAuthToken></RequesterCredentials>\n";
    $xmlReq .= '</' . $call . 'Request>';

    $boundary = "MIME_boundary";
    $CRLF = "\r\n";
    
    // The complete POST consists of an XML request plus the binary image separated by boundaries
	
    $firstPart   = '';
    $firstPart  .= "--" . $boundary . $CRLF;
    $firstPart  .= 'Content-Disposition: form-data; name="XML Payload"' . $CRLF;
    $firstPart  .= 'Content-Type: text/xml;charset=utf-8' . $CRLF . $CRLF;
    $firstPart  .= $xmlReq;
    $firstPart  .= $CRLF;
    
    $secondPart .= "--" . $boundary . $CRLF;
    $secondPart .= 'Content-Disposition: form-data; name="dummy"; filename="dummy"' . $CRLF;
    $secondPart .= "Content-Transfer-Encoding: binary" . $CRLF;
    $secondPart .= "Content-Type: application/octet-stream" . $CRLF . $CRLF;
    $secondPart .= $multiPartImageData;
    $secondPart .= $CRLF;
    $secondPart .= "--" . $boundary . "--" . $CRLF;
    
    $fullPost = $firstPart . $secondPart;
    
    // Create a new eBay session (defined below) 
    	
	$this->boundary = $boundary;
	$this->call = $call;

    $respXmlStr = $this->sendHttpRequest($fullPost);   // send multi-part request and get string XML response
  	
    if(stristr($respXmlStr, 'HTTP 404') || $respXmlStr == '')
        die('<P>Fehler bei der Anfrage an eBay, bitte wenden Sie sich an den Support.');
        
    $responseDoc = new DomDocument();
	$responseDoc->loadXML($respXmlStr);
	
	//get any error nodes
	$errors = $responseDoc->getElementsByTagName('Errors');
		
	//echo $errors;
	$urlNode = $responseDoc->getElementsByTagName("FullURL");
		
	return $urlNode->item(0)->nodeValue;
		
 
	}
	
}  // class eBaySession

?>