<?

class eBayGetCategoriesTree
{	

	private $_call = 'GetCategories';
	var $_site;
	var $_vonly;

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];


		$requestXmlBody = '
		<?xml version="1.0" encoding="utf-8"?>
			<GetCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
			  	<RequesterCredentials>
    				<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  				</RequesterCredentials>
  			<CategorySiteID>'.$this->_site.'</CategorySiteID>';
  		
		if($this->_vonly != 1) {
			$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
		}
		$requestXmlBody .= '</GetCategoriesRequest>';
	

		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}

?>