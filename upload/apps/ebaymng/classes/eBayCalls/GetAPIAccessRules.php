<?
class eBayGetApiAccessRules
{	

	private $_call = 'GetApiAccessRules';
	var 	$_itemid;

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];
	
		$requestXmlBody =
		'<?xml version="1.0" encoding="utf-8"?>
			<GetApiAccessRulesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  			<RequesterCredentials>
    			<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  			</RequesterCredentials>
			<Version>'.$nc->_eBayApiVersion.'</Version>
  			<EndingReason>NotAvailable</EndingReason>
			</GetApiAccessRulesRequest>';
		//echo "Body:".$requestXmlBody;
		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}
?>