<?
class eBayEndItem
{	

	private $_call = 'EndItem';
	var 	$_itemid;

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];
	
		$requestXmlBody =
		'<?xml version="1.0" encoding="utf-8"?>
			<EndItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  			<RequesterCredentials>
    			<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  			</RequesterCredentials>
			<Version>'.$nc->_eBayApiVersion.'</Version>
  			<ItemID>'.$this->_itemid.'</ItemID>
  			<EndingReason>NotAvailable</EndingReason>
			</EndItemRequest>';
		//echo "Body:".$requestXmlBody;
		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}
?>