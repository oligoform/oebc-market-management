<?
class eBayGetSessionID 
{
	
	
	private $_call = 'GetSessionID';
	
	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];
		
		$requestXmlBody = 
		'<?xml version="1.0" encoding="utf-8"?>
	 	 <GetSessionIDRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  			<ErrorLanguage>de_DE</ErrorLanguage>
  			<Version>'.$nc->_eBayApiVersion.'</Version>
			<RuName>'.$apiValues['RuName'].'</RuName>
	 	</GetSessionIDRequest>';
				
		return $requestXmlBody;
	}


	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
}
?>