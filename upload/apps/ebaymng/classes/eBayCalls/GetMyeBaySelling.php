<?
class eBayGetMyeBaySelling
{	
	
	private $_call = 'GetMyeBaySelling';
	var $_scall = 'ActiveList';
	var $_dur = 14;
	var $_entries = 80;

	private function _getRequestBody($nc)
	{
		
		
		$apiValues = $nc->_keys[$nc->_environment];

		$requestXmlBody =
		
		'<?xml version="1.0" encoding="utf-8"?>
			<GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">
				<RequesterCredentials>
    				<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  				</RequesterCredentials>
 				<ErrorLanguage>de_DE</ErrorLanguage>
  				<Version>'.$nc->_eBayApiVersion.'</Version>
  				<'.$this->_scall.'>';
    			
		$requestXmlBody .=			
				'<DetailLevel>ReturnAll</DetailLevel>
				<IncludeNotes>true</IncludeNotes>
				<Pagination>
	  			<DurationInDays>'.$this->_dur.'</DurationInDays>
      						<EntriesPerPage>'.$this->_entries.'</EntriesPerPage>
      			<PageNumber>'.$this->_page.'</PageNumber>
    			</Pagination>';
				
  		$requestXmlBody .=		
				'</'.$this->_scall.'>
			</GetMyeBaySellingRequest>';
		

		//var_dump($requestXmlBody);

		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
}
?>