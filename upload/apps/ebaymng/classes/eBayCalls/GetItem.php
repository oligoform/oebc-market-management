<?
class eBayGetItem
{	

	private $_call = 'GetItem';
	var 	$_itemid;

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];

		$requestXmlBody = '
			<?xml version="1.0" encoding="utf-8"?>
			<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  			<RequesterCredentials>
   				<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
  			</RequesterCredentials>
  			<ErrorLanguage>de_DE</ErrorLanguage>
  			<Version>'.$nc->_eBayApiVersion.'</Version>
    		<ItemID>'.$this->_itemid.'</ItemID>
			<DetailLevel>ReturnAll</DetailLevel>
			</GetItemRequest>
		';
		//echo "Body:".$requestXmlBody;
		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}
?>