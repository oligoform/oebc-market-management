<?
class eBayGeteBayDetails
{	

	private $_call = 'GeteBayDetails';
	var 	$_site;

	private function _getRequestBody($nc)
	{
		$apiValues = $nc->_keys[$nc->_environment];

		$requestXmlBody = '
			
			<?xml version="1.0" encoding="utf-8"?> 
				<GeteBayDetailsRequest xmlns="urn:ebay:apis:eBLBaseComponents"> 
  				<RequesterCredentials> 
    				<eBayAuthToken>'.$apiValues['UserToken'].'</eBayAuthToken>
				</RequesterCredentials> 
  				<DetailName>'.$this->_scall.'</DetailName> 
			</GeteBayDetailsRequest>';

		return $requestXmlBody;
	}

	public function callEbay()
	{
		$nc = new _callEbay;
		return $nc->callEbay($this->_call, $this->_getRequestBody($nc));
	}
	
	
}
?>