<?

/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


class _callEbay
{
	var $_siteId;	// default: Germany
	var $_environment;   // toggle between sandbox and production
	var $_eBayApiVersion;
	var $_keys;
	
	public function __construct() {
		
		$db 	= new dbal;
		$files 	= new subsystem($db);
		
		require(dirname(__FILE__)."/eBay.inc.php");
		
		$this->_siteId 			= $_siteId;
		$this->_environment		= $_environment;
		$this->_eBayApiVersion 	= $_eBayApiVersion;
		$this->_keys 			= $_keys;

	}
	
	public function callEbay($call, $requestBody)
	{
		$apiValues = $this->_keys[$this->_environment];

		$connection = curl_init();
		curl_setopt($connection, CURLOPT_URL, $apiValues['ServerUrl']);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);

		$headers = array (
			'X-EBAY-API-COMPATIBILITY-LEVEL: ' . $this->_eBayApiVersion,
			'X-EBAY-API-DEV-NAME: ' . $apiValues['DEVID'],
			'X-EBAY-API-APP-NAME: ' . $apiValues['AppID'],
			'X-EBAY-API-CERT-NAME: ' . $apiValues['CertID'],
			'X-EBAY-API-CALL-NAME: ' . $call,
			'X-EBAY-API-SITEID: ' . $this->_siteId,
		);

		curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($connection, CURLOPT_POST, 1);
		
		//echo $requestBody;

		curl_setopt($connection, CURLOPT_POSTFIELDS, $requestBody);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		$responseXml = curl_exec($connection);
		curl_close($connection);
		
	//	echo $responseXml;
		return $responseXml;
	}
	
}

require(dirname(__FILE__)."/eBayCalls/GetSessionID.php");

require(dirname(__FILE__)."/eBayCalls/FetchToken.php");

require(dirname(__FILE__)."/eBayCalls/EPSUpload.php");

require(dirname(__FILE__)."/eBayCalls/GetMyeBaySelling.php");

require(dirname(__FILE__)."/eBayCalls/AddItem.php");

require(dirname(__FILE__)."/eBayCalls/AddItemMV.php");

require(dirname(__FILE__)."/eBayCalls/GetCategoriesTree.php");

require(dirname(__FILE__)."/eBayCalls/GetCategoriesFeatures.php");

require(dirname(__FILE__)."/eBayCalls/GeteBayDetails.php");

require(dirname(__FILE__)."/eBayCalls/GetStore.php");

require(dirname(__FILE__)."/eBayCalls/RelistItem.php");

require(dirname(__FILE__)."/eBayCalls/GetItem.php");

require(dirname(__FILE__)."/eBayCalls/GetSellerTransactions.php");

require(dirname(__FILE__)."/eBayCalls/EndItem.php");

require(dirname(__FILE__)."/eBayCalls/GetAPIAccessRules.php");

?>