<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

$_siteId = $files->getOpt("land");					// default: Germany
$_environment = 'production';   	// toggle between sandbox and production
$_eBayApiVersion = 729;
$_keys = array(
		'production' => array(
			'DEVID' 	=> '75a64f76-d912-44b9-bd98-6f9c00ab89ad',
			'AppID' 	=> 'PBTMedia-3d0a-4d74-9c4b-dbdb65e6f7bc',
			'CertID' 	=> 'de132c52-14ab-41b0-810d-81b6419a691e',
			'UserToken'	=> $files->getOpt("token"),
			'ServerUrl' => 'https://api.ebay.com/ws/api.dll',
			'RuName' 	=> "PBT_Media-PBTMedia-3d0a-4-cfqbvrglt"
			),
		'sandbox' => array(
			'DEVID' 	=> '75a64f76-d912-44b9-bd98-6f9c00ab89ad',
			'AppID' 	=> 'PBTMedia-79d4-4a12-9fa6-e531ab01aaaa',
			'CertID' 	=> 'e666b0cf-f22f-42c5-b381-c05a622b8824',
			'UserToken'	=> $files->getOpt("token"),
			'ServerUrl' => 'https://api.sandbox.ebay.com/ws/api.dll',
			'RuName' 	=> "PBT_Media-PBTMedia-79d4-4-hzzmhz"
		)
	);
	
if($_environment == "sandbox") {	
	$ebayurl = "cgi.sandbox.ebay.com";
} else {
	$ebayurl = "cgi.ebay.com";	
}

?>
