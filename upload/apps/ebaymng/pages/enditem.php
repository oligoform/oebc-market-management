<?

/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	/*
		Relist
	*/

		//smarty Element für Darstellung erzeugen
		$smarty = new smarty;
$smarty->compile_dir  = dirname(__FILE__).'/../tmp/';
			
		$sl = new eBayEndItem();
		
		$sl->_itemid		=	$_REQUEST["id"];
	
		$er = $sl->callEbay();
	
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($er);
		
		//echo $er;
	
		//get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');
		
		$log = "";
		
		//if there are error nodes
		if($errors->length > 0)
		{
			$log = '<B>eBay gab folgenden Fehler zurück:</B>';
			foreach ($errors as $error) {
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $error->getElementsByTagName('ErrorCode');
			$shortMsg = $error->getElementsByTagName('ShortMessage');
			$longMsg  = $error->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$log .= '<P>'.$code->item(0)->nodeValue.' : '.str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0) {
				$log .= '<BR>'.str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
			}
			}
			

		} else 	{ //no errors
		
			$linkBase = "http://$shopurl/ws/eBayISAPI.dll?ViewItem&item=";
		
			$log .= "<p><b>Auktion erfolgreich beendet!</b><br />";
			$log .= 'Auktions ID: <a href="'.$linkBase.$_REQUEST["id"].'">'.$_REQUEST["id"]."</a> <BR /></p>\n";			
			
		}	
	
		$smarty->assign("log", $log);

		//Content erzeugen und bereitstellen
		$content = $smarty->fetch('frontend/pages/result.tpl');
		$template->assign("content",$content);
		

?>