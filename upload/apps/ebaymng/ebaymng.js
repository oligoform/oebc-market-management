/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

function viewNode(p) {
	$(p).toggle();
}

function nextAuctions(p)
{
	if(p > 0) {
		
	$("#auctions").html('<center>Lade Daten...<br/><img src="/images/system/load.gif" /></center>');
	$.post("/components/ajax.php", {	
					'mode'	   			: 	'getauction',
					'page'				:	p,
					'thisapp'			:	'ebaymng'
			},
			function(resdata){
				$("#auctions").html(resdata);
				$('#auctionstab').dataTable( {
					"bJQueryUI": true,
					"bStateSave": true,
					"iDisplayLength" : 100,
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze dieser Seite",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_  Datensätzen dieser Seite",
						"sInfoEmpty": "Keine laufenden Auktionen",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );
 			});
	}
}


$(function() {

		/**********************************************************/
		

		$.post("/components/ajax.php", {	
					'mode'	   			: 	'getauction',
					'thisapp'				:	'ebaymng'
			},
			function(resdata){
				$("#auctions").html(resdata);
				$('#auctionstab').dataTable( {
					"bJQueryUI": true,
					"bStateSave": true,
					"iDisplayLength" : 100,
					"aaSorting": [[1,'asc']],			
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze dieser Seite",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_  Datensätzen dieser Seite",
						"sInfoEmpty": "Keine laufenden Auktionen",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );
 			});	

		$.post("/components/ajax.php", {	
					'mode'	   			: 	'getsold',
					'thisapp'			:	'ebaymng'
			},
			function(resdata){
				$("#auctions_sold").html(resdata);
				$('#soldtab').dataTable( {
					"bJQueryUI": true,
					"bStateSave": true,
					"iDisplayLength" : 100,
					"aaSorting": [[1,'asc']],			
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze dieser Seite",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_  Datensätzen dieser Seite",
						"sInfoEmpty": "Keine laufenden Auktionen",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );
				
 			});
		$.post("/components/ajax.php", {	
					'mode'	   			: 	'getunsold',
					'thisapp'			:	'ebaymng'
			},
			function(resdata){
				$("#auctions_unsold").html(resdata);
				$('#unsoldtab').dataTable( {
					"bJQueryUI": true,
					"bStateSave": true,
					"iDisplayLength" : 100,
					"aaSorting": [[1,'asc']],			
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze dieser Seite",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_  Datensätzen dieser Seite",
						"sInfoEmpty": "Keine laufenden Auktionen",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );
				
 			});
		$.post("/components/ajax.php", {	
					'mode'	   			: 	'getscheduled',
					'thisapp'			:	'ebaymng'

			},
			function(resdata){
				$("#auctions_scheduled").html(resdata);
				$('#scheduledtab').dataTable( {
					"bJQueryUI": true,
					"bStateSave": true,
					"iDisplayLength" : 100,
					"aaSorting": [[1,'asc']],			
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze dieser Seite",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_  Datensätzen dieser Seite",
						"sInfoEmpty": "Keine laufenden Auktionen",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );
 			});	
				
});