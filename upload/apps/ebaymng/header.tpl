{literal}
<style type="text/css">
html {
	overflow:auto !important;
	overflow-x:hidden !important;
	overflow-y:visible !important;
}
html, body {
	height:100%;
	width:100%;
	padding:0;
	margin:0;
}
.scrollable {
	height:350px;
	overflow:auto;
	margin:0;
	width:100%;	
	border: 1px solid #ccc;	
}
.scrollable2 {
	height:400px;
	overflow:auto;
	margin:0;	
	width:100%;
	border: 1px solid #ccc;
	padding:3px;		
}
#tabs2 {
	height:450px;
}
</style>

<link rel="stylesheet" type="text/css" href="/css/tables.css">

<script language="javascript" src="/apps/ebaymng/ebaymng.js"></script> 
<script language="javascript" src="/javascript/jquery.dataTables.js"></script> 
<script language="javascript">
$(function() {
{/literal}
	$(".tabs2").tabs();	
	$("#s20").val("{$s20}");
	$("#s12").val("{$s12}");
	$("#s17").val("{$ReturnsWithinOption}");
	$("#s16").val("{$RefundOption}");
	$("#s19").val("{$ShippingCostPaidByOption}");
	$("#DispatchTimeMax").val("{$DispatchTimeMax}");
	$("#bestand").val("{$bestand}");
{literal}
		
	$('#shoptab').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"bStateSave": true,
					"iDisplayLength" : 50,
					"aaSorting": [[1,'asc']],			
					"bAutoWidth": false,
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_ Datensätzen",
						"sInfoEmpty": "-Keine-",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );

});
</script> 
{/literal} 