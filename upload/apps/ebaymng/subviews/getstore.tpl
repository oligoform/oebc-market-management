<div class="ui-widget-content" style="padding:10px;"><br />
<h2>Store Eigenschaften</h2>
<p>Folgende Daten zu Ihrem Store wurden geladen:</p>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr>
    <td width="200">Store Name</td>
    <td>{$esName}</td>
  </tr>
  <tr>
    <td width="200">Beschreibung</td>
    <td>{$esDes}</td>
  </tr>
  <tr>
    <td width="200">Logo</td>
    <td>{$esLogo}</td>
  </tr>
  <tr>
    <td>Homepage</td>
    <td>{$esHome}</td>
  </tr>
  <tr>
    <td>Shoplevel</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
<script src="/javascript/cufon-yui.js" type="text/javascript"></script> 
<script src="/javascript/font.js" type="text/javascript"></script> 
<script type="text/javascript">
Cufon.replace('h2');
Cufon.replace('h3');
</script> 