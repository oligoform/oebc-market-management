<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	$smarty = new smarty;
	$smarty->template_dir = dirname(__FILE__);
	$smarty->compile_dir  = dirname(__FILE__).'/../../../tmp/';

	require dirname(__FILE__)."/../classes/eBay.inc.php";
	require(dirname(__FILE__)."/../classes/eBay.class.php");
	
//	echo "test";
	
	$sl = new eBayGetStore;
	
	$er = $sl->callEbay();
	
	//var_dump($er);

	$responseDoc = new DomDocument();
	$responseDoc->loadXML($er);
		
//	echo $er;
	
	//get any error nodes
	$errors = $responseDoc->getElementsByTagName('Errors');
		
	//echo $errors->length == 0;
	
	$esDes = "-"; 
	$esName = "-";
	$esLogo = "-";
	$esHome = "-";
		
	if($errors->length == 0)
	{
		$esDes = $responseDoc->getElementsByTagName("Description")->item(0)->nodeValue;
		$esName = $responseDoc->getElementsByTagName("Name")->item(0)->nodeValue;
		$esLogo = $responseDoc->getElementsByTagName("Logo")->item(0)->nodeValue;
		$esHome = $responseDoc->getElementsByTagName("URL")->item(0)->nodeValue;
	}
	
	$smarty->assign("esDes", $esDes);
	$smarty->assign("esName",$esName);
	$smarty->assign("esLogo",$esLogo);
	$smarty->assign("esHome",$esHome);
	
	$files->setOpt("esDes",$db->clean($esDes));
	$files->setOpt("esName",$db->clean($esName));
	$files->setOpt("esLogo",$db->clean($esLogo));
	$files->setOpt("esHome",$db->clean($esHome));
	
	$cats = $responseDoc->getElementsByTagName('CustomCategory');
	
	/*
	
	    <CategoryID>1</CategoryID>
        <Name>Sonstige</Name>
        <Order>0</Order>
		
	*/
	
	$sql =  "TRUNCATE TABLE `oebc_shopcats`";  
	$db->query_exec($sql);
	
	foreach($cats as $cat) {
		
		
		$sql = "INSERT INTO `oebc_shopcats` (
				`eid` ,
				`name` ,
				`parent` ) VALUES ("
								."\"".$cat->getElementsByTagName("CategoryID")->item(0)->nodeValue."\","
								."\"".$cat->getElementsByTagName("Name")->item(0)->nodeValue."\","
								."\"0\")";	
		//echo $sql;
		$db->query_exec($sql);
		
		$ccats = $responseDoc->getElementsByTagName('ChildCategory');
		
		foreach($ccats as $ccat) {
			
				$sql = "INSERT INTO `oebc_shopcats` (
				`eid` ,
				`name` ,
				`parent` ) VALUES ("
								."\"".$ccat->getElementsByTagName("CategoryID")->item(0)->nodeValue."\","
								."\"".$ccat->getElementsByTagName("Name")->item(0)->nodeValue."\","
								."\"".$cat->getElementsByTagName("CategoryID")->item(0)->nodeValue."\")";	
				$db->query_exec($sql);
		}

		
	}
	
	//Content erzeugen und bereitstellen
	$content = $smarty->fetch('getstore.tpl');
	$template->assign("content",$content);

?>