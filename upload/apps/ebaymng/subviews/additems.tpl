 {if $renew}
<div class="ui-widget">
  <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
    <p> <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"> </span> <strong>Achtung:</strong> Die lokale Kopie des Kategoriebaums is nicht mehr aktuell. Bitte updaten Sie ihn, bevor Sie versuchen einen Artikel einzustellen, dies kann einige Minuten dauern. Es aknn einige Minuten dauern, bis der Updateprozess erfolgreich abgeschlossen ist, halten Sie den Ladevorgang auf keinen Fall an oder berechen ihn ab.
      Um die Datenbank jetzt zu aktualisieren, <a href="/?app=ebaymng&subview=renew" style="color:#FF0">klicken Sie hier</a>. </p>
  </div>
</div>
{/if}
  {if $mv}
<div class="ui-widget">
  <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Hinweis:</strong> Sie sind dabei ein Multivarianten Artikel einzustellen. Bitte beachten Sie die gesonderten Einstellungen für Varianten.</p>
  </div>
</div>
{/if}
<form action="/index.php" method="post" id="additemForm" name="additemForm">
  <input type="hidden" value="ebaymng" name="app" id="app" />
  <input type="hidden" value="additems" name="subview" id="subview" />
  <input type="hidden" value="{$article.OXID}" name="artnr" />
  {if $mv}
  <input type="hidden" value="sellmv" name="action" />
  {else}
  <input type="hidden" value="sell" name="action" />
  {/if}
  <div class="tabs2" style="border:0; margin:0;">
  <ul>
    <li><a href="#art">1. Artikel Daten</a></li>
    <li><a href="#cat">2. Kategorie, Plattform und Startzeit</a></li>
    <li><a href="#4sl">3  Verkaufs- und Versand </a></li>
    <li><a href="#pic">4. Bilder und Gallery</a></li>
    {if $mv}
    <li><a href="#var">5. Varianten</a></li>
    {/if}
    <li><a href="#addnow">Jetzt Einstellen!</a></li>
  </ul>
  <div id="art">
    <table width="800" border="0" cellpadding="0" cellspacing="10">
      <tr>
        <td width="150">Titel</td>
        <td><div class="input">
            <input name="Title" type="text" id="Title" value="{$article.OXTITLE}" class="word_count" style="width:550px;" />
            <span class="counter"></span> </div></td>
      </tr>
      <tr>
        <td width="150">Untertitel</td>
        <td><div class="input">
            <input name="SubTitle" type="text" id="SubTitle" value="{$SubTitle}" class="word_count"  style="width:550px;" />
            <span class="counter"></span> </div></td>
      </tr>
      <tr>
        <td width="150" valign="top">Beschreibung</td>
        <td><textarea name="Description" cols="70" rows="22" id="Description">{$article.OXLONGDESC}</textarea></td>
      </tr>
    </table>
  </div >
  <div id="cat">
    <table width="800" border="0" cellpadding="0" cellspacing="10">
      <tr>
        <td width="150" valign="top">Kategorie</td>
        <td><input name="catterm" type="text" id="catterm" style="width:360px" size="58" onkeydown="catselect();" value="{$cat}" title="Suchbegriff eingeben..."/>
          <div id="catsearch" style="display:none; height:155px; overflow:auto;"></div>
          <input type="hidden" id="CategoryID" name="CategoryID" value="{$catid}" /></td>
      </tr>
      <tr>
        <td width="150" valign="top">Kategorie 2</td>
        <td><input name="catterm2" type="text" id="catterm2" style="width:360px" size="58" onkeydown="catselect2();" value="{$cat2}" title="Suchbegriff eingeben..."/>
          <div id="catsearch2" style="display:none; height:155px; overflow:auto;"></div>
          <input type="hidden" id="CategoryID2" name="CategoryID2" value="{$catid2}" /></td>
      </tr>
      <tr>
        <td width="150">Startzeit</td>
        <td><input name="starttime" type="text" id="starttime" size="8" maxlength="8" readonly="readonly" />
          um <strong>
          <select name="starth" id="starth">
            <option value="00" selected="selected">00</option>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
          </select>
          <select name="startm" id="startm">
            <option value="00" selected="selected">00</option>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
            <option value="24">24</option>
            <option value="25">25</option>
            <option value="26">26</option>
            <option value="27">27</option>
            <option value="28">28</option>
            <option value="29">29</option>
            <option value="30">30</option>
            <option value="31">31</option>
            <option value="32">32</option>
            <option value="33">33</option>
            <option value="34">34</option>
            <option value="35">35</option>
            <option value="36">36</option>
            <option value="37">37</option>
            <option value="38">38</option>
            <option value="39">39</option>
            <option value="40">40</option>
            <option value="41">41</option>
            <option value="42">42</option>
            <option value="43">43</option>
            <option value="44">44</option>
            <option value="45">45</option>
            <option value="46">46</option>
            <option value="47">47</option>
            <option value="48">48</option>
            <option value="49">49</option>
            <option value="50">50</option>
            <option value="51">51</option>
            <option value="52">52</option>
            <option value="53">53</option>
            <option value="54">54</option>
            <option value="55">55</option>
            <option value="56">56</option>
            <option value="57">57</option>
            <option value="58">58</option>
            <option value="59">59</option>
          </select>
          </strong> Uhr </td>
      </tr>
      <tr>
        <td width="150">Land</td>
        <td><select name="Site" id="Site">
            <option value="Germany">Deutschland</option>
          </select></td>
      </tr>
      <tr>
        <td width="150">Dauer</td>
        <td><select name="ListingDuration" id="ListingDuration">
            <option value="Days_1">1 Tag</option>
            <option value="Days_3" selected="selected">3 Tage</option>
            <option value="Days_7">7 Tage</option>
            <option value="Days_14">14 Tage</option>
            <option value="Days_30">30 Tage</option>
            <option value="GTC">Bis auf Widerruf</option>
          </select></td>
      </tr>
      <tr>
        <td width="150">eBay Shop Kategorie</td>
        <td><select name="shopcat" id="shopcat">
            <option value="">- keine -</option>
            
			{section name=shopcats loop=$shopcats}     
           	{if $shopcats[shopcats].parent == 0}        
			
            <option value="{$shopcats[shopcats].eid}">{$shopcats[shopcats].name}</option>
            
            	{section name=shopcats2 loop=$shopcats}
                	{if $shopcats[shopcats2].parent == $shopcats[shopcats].eid}            
            		
            <option value="{$shopcats[shopcats2].eid}">- {$shopcats[shopcats2].name}</option>
            
                	{/if}
            	{/section}
             {/if}
            {/section}
            
          </select></td>
      </tr>
      <tr>
        <td width="150">eBay Shop Kategorie 2</td>
        <td><select name="shopcat2" id="shopcat2">
            <option value="">- keine -</option>
            
			{section name=shopcats loop=$shopcats}     
           	{if $shopcats[shopcats].parent == 0}        
			
            <option value="{$shopcats[shopcats].eid}">{$shopcats[shopcats].name}</option>
         
            	{section name=shopcats2 loop=$shopcats}
                	{if $shopcats[shopcats2].parent == $shopcats[shopcats].eid}            
            		
            <option value="{$shopcats[shopcats2].eid}">- {$shopcats[shopcats2].name}</option>
            
                	{/if}
            	{/section}
             {/if}
            {/section}
            
          </select></td>
      </tr>
    </table>
  </div>
  <div id="4sl">
    <table border="0" cellpadding="0" cellspacing="10">
      <tr>
        <td width="150">Artikelzustand</td>
        <td><select name="ConditionID" id="ConditionID">
            <option value="1000">Neu</option>
            <option value="3000">Gebraucht</option>
          </select></td>
      </tr>
      {if !$mv}
      <tr>
        <td width="150">Stückzahl</td>
        <td> {if $bestand == 1}
          <label for="Quantity"></label>
          <select name="Quantity" id="Quantity">
            
      	{section name=foo loop=$article.OXSTOCK}
    		
            <option value="{$smarty.section.foo.iteration}">{$smarty.section.foo.iteration}</option>
            
		{/section}
     
          </select>
          {else}
          <input name="Quantity" type="text" id="Quantity" value="1" size="20" />
          {/if}
          </td>
      </tr>
      <tr>
        <td width="150">Nur Sofortkauf</td>
        <td><input name="FixedPriceListing" type="checkbox" id="FixedPriceListing" value="1" checked="checked" /></td>
      </tr>
      <tr>
        <td width="150">Auktionspreis ab</td>
        <td><input name="StartPrice" type="text" id="StartPrice" value="{if $article.StartPrice}{$article.StartPrice}{else}1.00{/if}" size="20" /></td>
      </tr>
      {/if}
      <tr>
        <td width="150">Sofortkaufpreis</td>
        <td><input name="BuyItNowPrice" type="text" id="BuyItNowPrice" value="{math equation="y-x" x=4 y=$children[0].OXPRICE}" size="20" /></td>
      </tr>
      <tr>
        <td width="150">Zahlungsarten</td>
        <td><input name="s6" type="checkbox" id="checkbox7" value="1" {if $s6}checked="checked"{/if} />
          Paypal
          <input name="s7" type="checkbox" id="checkbox8" value="1" {if $s7}checked="checked"{/if} />
          Überweisung
          <input name="s8" type="checkbox" id="checkbox9" value="1" {if $s8}checked="checked"{/if} />
          Nachnahme 
          <!-- <input name="s9" type="checkbox" id="checkbox10" value="1" {if $s9}checked="checked"{/if} />
          Andere-->
          
          <input name="s10" type="checkbox" id="checkbox11" value="1" {if $s10}checked="checked"{/if} />
          Siehe Beschreibung </td>
      </tr>
      <tr>
        <td width="150">Paypal E-Mail</td>
        <td><input name="PayPalEmailAddress" type="text" id="PayPalEmailAddress" value="{$pp_email}" /></td>
      </tr>
      <tr>
        <td width="150">Währung</td>
        <td><select name="Currency" id="Currency">
            <option value="EUR">EUR</option>
            <option value="USD">US $</option>
          </select></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Versandkostenfrei</td>
        <td><input name="freeShipping" type="checkbox" id="freeShipping" /></td>
      </tr>
      <tr>
        <td>Versandkosten</td>
        <td><input name="shipping" type="text" id="shipping" value="{if $shipping}{$shipping}{else}4.90{/if}" size="20" /></td>
      </tr>
      <tr>
        <td>Zusätzliche Versandkosten <br /></td>
        <td><input name="shipping2" type="text" id="shipping2" value="{if $shipping2}{$shipping2}{else}0.00{/if}" size="20" />
          pro weiterem Artikel</td>
      </tr>
      <tr>
        <td>Versandtart</td>
        <td><label for="delivery"></label>
          <select name="delivery" id="delivery">
            <option value="DE_DeutschePostBrief">Deutsche Post Brief</option>
            <option value="DE_DPBuecherWarensendung">Deutsche Post Bücher-/Warensendung</option>
            <option value="DE_DHLPackchen">DHL Päckchen</option>
            <option value="DE_DHLPaket">DHL Paket</option>
            <option value="DE_HermesPaket">Hermes Paket</option>
            <option value="DE_IloxxTransport">iloxx Transport</option>
            <option value="DE_Paket" selected="selected">Paketversand</option>
            <option value="DE_Express">Expressversand</option>
            <option value="DE_SpecialDelivery">Sonderversand</option>
            <option value="DE_Einschreiben">Einschreiben (inkl. aller Gebühren)</option>
            <option value="DE_Nachname">Nachnahme (inkl. aller Gebühren)</option>
            <option value="DE_SonstigeDomestic">Sonstige</option>
            <option value="DE_DeutschePostBriefInternational">Deutsche Post Brief International</option>
            <option value="DE_DHLPackchenInternational">DHL Päckchen International</option>
            <option value="DE_DHLPaketInternational">DHL Paket International</option>
            <option value="DE_HermesPaketInternational">Hermes Paket International</option>
            <option value="DE_IloxxTransportInternational">iloxx Transport International</option>
            <option value="DE_PaketInternational">Paketversand International</option>
            <option value="DE_ExpressInternational">Expressversand International</option>
            <option value="DE_SonstigeInternational ">Sonstige International</option>
          </select></td>
      </tr>
    </table>
  </div>
  <div id="pic">
    <table width="100%"  border="0" cellpadding="0" cellspacing="10">
      <tr>
        <td width="150">Gallery Typ</td>
        <td><select name="GalleryType" id="GalleryType">
            <option value="None">Keiner</option>
            <option value="Gallery">Gallerie</option>
            <option value="Plus">Gallerie Plus</option>
          </select></td>
      </tr>
      <tr>
        <td width="150">Bilder</td>
        <td><!-- needs some improvemnts PictureC -->
          
          <div class="ppics"> 
         	{section name=ppics loop=$ppics}
            <div style="display:inline; height:180px; width:180px; margin:20px;">
              <img src="{$ppics[ppics]}" />
                <input name="pic{$smarty.section.ppics.iteration}" type="checkbox" value="1" />
              <input name="urlpic{$smarty.section.ppics.iteration}" type="hidden" value="{$ppics[ppics]}" />
            </div>
            {/section}
          </div>
          <input name="piccount" type="hidden" value="{$ppicsc}" />
		</td>
      </tr>
      <tr>
        <td width="150">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div>
  {if $mv}
  <div id="var">
    <table  border="0" cellpadding="0" cellspacing="10">
      <tr>
        <td>Varianten Name(n)</td>
        <td>{$article.OXVARNAME}</td>
      </tr>
      <tr>
        <td width="150" valign="top">Stückzahlen pro Artikel</td>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            {section name=children loop=$children}
            <tr>
              <td width="200">{$children[children].OXVARSELECT} </td>
              <td> {if $children[children].OXSTOCK > 0}
                <select name="iq{$smarty.section.children.iteration}" id="Quantity_{$smarty.section.children.iteration}" style="width: 60px";>
                  <option value="0">0</option>

      	{section name=foo loop=$children[children].OXSTOCK}             

                  <option value="{$smarty.section.foo.iteration}"> {$smarty.section.foo.iteration} </option>
                    
		{/section}

                </select>
                {else}&nbsp;&nbsp;<span style="color:#F00;">0</span>
                <input type="hidden"  name="iq{$smarty.section.children.iteration}" id="Quantity_{$smarty.section.children.iteration}" value="0" />
                {/if}</td>
              <td width="200"><input type="text" value="{$children[children].OXPRICE}" name="ip{$smarty.section.children.iteration}" /></td>
              <td width="100"><input type="hidden" value="{$children[children].OXVARSELECT}" name="iv{$smarty.section.children.iteration}" />
                <input type="hidden" value="{$children[children].OXID}" name="ia{$smarty.section.children.iteration}" /></td>
            </tr>
            {/section}
          </table>
          <input type="hidden" name="v" value="{$article.OXVARCOUNT}" />
          <input type="hidden" name="parent" value="{$article.OXPARENTID}" /></td>
      </tr>
    </table>
  </div>
  {/if}
 <div id="addnow">
 <p align="center"><img src="../../../images/system/warning_sign.png" width="200" height="179" /></p>
 <p align="center"><strong><em>Mit einem Klick auf den Button stellen Sie den Artikel bei eBay ein!</em></strong></p>
 <p align="center">
   <!--<button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="preview" onclick="previewItem();">Preview ansehen</button>
    &nbsp;-->
    <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="anbieten"
onclick="submitItem();">Auktion starten</button>
</p>
  <!--  <p>
  <p>* Stückzahlverkäufe sind nur bei Festpreisangeboten möglich. Für auktionen wird diese Option ignoriert.</p>
  <p>** Bei nur Sofortkauf Aktionen wird beträgt die Dauer mindestens 3 Tage, sollte Ihr gewählter Wert dies unterschreiten, setzt das System die Dauer auf 3 Tage hoch.</p>
  <p>*** Frei lassen wenn kein Sofortkauf gewünscht wird</p>-->
  </div>
</form>
