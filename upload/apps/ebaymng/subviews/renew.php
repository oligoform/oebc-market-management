<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}

	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->template_dir = dirname(__FILE__);
	$smarty->compile_dir  = dirname(__FILE__).'/../../../tmp/';

	require dirname(__FILE__)."/../classes/eBay.inc.php";
	require(dirname(__FILE__)."/../classes/eBay.class.php");
	
	set_time_limit(0);
	
	if($_REQUEST["step"] == "") {
	 $smarty->assign("step1", 1);
	 $template->assign("step1", 1);
	}
	if($_REQUEST["step"] == 2) {
	$ct = new eBayGetCategoriesTree;	
	$ct->_vonly = 1;
	
	$ct->_site = $files->getOpt("land");
	
	$cat_xml = $ct->callEbay();
	
	$responseDoc = new DomDocument();
	$responseDoc->loadXML($cat_xml);
		
	//get any error nodes
	$errors = $responseDoc->getElementsByTagName('Errors');

	/*if($errors->length != 0) {
		var_dump($errors);
		die();
	} */

	//if there are error nodes
	if($errors->length == 0) {
			
			$version = $responseDoc->getElementsByTagName("CategoryVersion")->item(0)->nodeValue;
			
			if($version != $files->getOpt("cat_version"))
			{
				$ct->_vonly = 0;
	
				$cat_xml = $ct->callEbay();
				$responseDoc = new DomDocument();
				$responseDoc->loadXML($cat_xml);
				
				error_log($cat_xml, 3, "/tmp/cats.xml");
				
				$files->setOpt("cat_version",$db->clean($version));
				$sql = "TRUNCATE TABLE `oebc_categories`";
				$db->query_exec($sql);
				
				$responses = $responseDoc->getElementsByTagName("Category");
				
				foreach ($responses as $response) {
					
					$catNames  = $response->getElementsByTagName("CategoryName");
					$catName   = $catNames->item(0)->nodeValue;
	
					$catLevels  = $response->getElementsByTagName("CategoryLevel");
					$catLevel   = $catLevels->item(0)->nodeValue;
					
					$catParents  = $response->getElementsByTagName("CategoryParentID");
					$catParent   = $catParents->item(0)->nodeValue;
					
					$catIds  = $response->getElementsByTagName("CategoryID");
					$catId   = $catIds->item(0)->nodeValue;
					
					$catBOEs  = $response->getElementsByTagName("BestOfferEnabled");
					$catBOE   = $catBOEs->item(0)->nodeValue;
					
					if ($catBOE == "true") {
						$catBOE = 1;	
					} else {
						$catBOE = 0;
					}
					
					$catOrpas  = $response->getElementsByTagName("ORPA");
					$catOrpa   = $catOrpas->item(0)->nodeValue;
					
					$catLeafs  = $response->getElementsByTagName("LeafCategory");
					$catLeaf   = $catLeafs->item(0)->nodeValue;
					
					if ($catLeaf == "true") {
						$catLeaf = 1;	
					} else {
						$catLeaf = 0;
					}
					
					$catExpireds  = $response->getElementsByTagName("Expired");
					$catExpired   = $catExpireds->item(0)->nodeValue;
					
					if ($catExpired == "true") {
						$catExpired = 1;	
					} else {
						$catExpired = 0;
					}
					
					$catVirtuals  = $response->getElementsByTagName("Virtual");
					$catVirtual   = $catVirtuals->item(0)->nodeValue;
					
					if ($catVirtual == "true") {
						$catVirtual = 1;	
					} else {
						$catVirtual = 0;
					}
					
					$sql = "INSERT INTO `oebc_categories` (`ID`, `cid`, `name`, `parent`, `level`, `bestoffer`, `leaf`, `expired`, `virtual`) 
							VALUES (NULL, '$catId', '".$db->clean($catName)."', '$catParent', '$catLevel', '$catBOE', '$catLeaf', '$catExpired', '$catVirtual')";
					//echo $sql;
					$db->query_exec($sql);
					
				}
			}
	}
	 	$smarty->assign("step2", 1);
	 	$template->assign("step2", 1);
	} 
	if($_REQUEST["step"] == 3) {
	 	$smarty->assign("step3", 1);
	 	$template->assign("step3", 1);
	}
	if($_REQUEST["step"] == 4) {
		
		/*$sql = "SELECT * FROM oebc_categories WHERE level = 1";
		$toplevel = $db->query_array($sql);
		//var_dump($toplevel);
		foreach($toplevel as $ccat)
		{
	 		// Kat Eigenschaften*/
			$ct = new eBayGetCategoryFeatures;
			
			$cat_xml = $ct->callEbay();
			
			$responseDoc = new DomDocument();
			$responseDoc->loadXML($cat_xml);
			
			//var_dump($cat_xml);
			//exit(0);
		
			//get any error nodes
			$errors = $responseDoc->getElementsByTagName('Errors');

			//if there are error nodes
			if($errors->length == 0) {
				
				$responses = $responseDoc->getElementsByTagName("Category");
				
				foreach ($responses as $response) {
					$v  = $response->getElementsByTagName("VariationsEnabled")->item(0)->nodeValue;
					$c  = $response->getElementsByTagName("CategoryID")->item(0)->nodeValue;
					if($v) {
					
						$sql = "UPDATE `oebc_categories` SET `variants` = '2' WHERE `cid` = $c  LIMIT 1";
						$db->query_exec($sql);
						
						$sql = "UPDATE `oebc_categories` SET `variants` = '2' WHERE `parent` = $c";
						$db->query_exec($sql);
						
						// for all children in level2
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 2 AND level = 1";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 2
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '2' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 2 AND level = 2";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 3
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '2' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 2 AND level = 3";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 4
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '2' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 2 AND level = 4";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 5
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '2' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "UPDATE `oebc_categories` SET `variants` = '1' WHERE `variants` = '2'";
						$db->query_exec($sql);
						
						
					} else {
						$sql = "UPDATE `oebc_categories` SET `variants` = '3' WHERE `cid` = $c  LIMIT 1";
						$db->query_exec($sql);	
						
						$sql = "UPDATE `oebc_categories` SET `variants` = '3' WHERE `parent` = $c";
						$db->query_exec($sql);
						
						// for all children in level2
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 3 AND level = 1";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 2
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '3' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 3 AND level = 2";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 3
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '3' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 3 AND level = 3";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 4
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '3' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "SELECT * FROM oebc_categories WHERE variants = 3 AND level = 4";
						$c2s = $db->query_array($sql);
						
						// set for all children in level 5
						foreach ($c2s as $c2) {
							$sql = "UPDATE `oebc_categories` SET `variants` = '3' WHERE `parent` = ".$c2["cid"];
							$db->query_exec($sql);
						}
						
						$sql = "UPDATE `oebc_categories` SET `variants` = '0' WHERE `variants` = '3'";
						$db->query_exec($sql);
					}
					
					
				}
				
				
				
				
				
			} else {
				var_dump($cat_xml);
				die("An error occured");	
			}
		//}
		
		$smarty->assign("step4", 1);
	 	$template->assign("step4", 1);
	}
	if($_REQUEST["step"] == 5) {
	 	$smarty->assign("step5", 1);
	 	$template->assign("step5", 1);
	}
	
	//Content erzeugen und bereitstellen
	$content = $smarty->fetch('renew.tpl');
	$template->assign("content",$content);

?>