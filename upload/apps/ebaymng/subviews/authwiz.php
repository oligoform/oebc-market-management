<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/



	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}

	/*
		Authorize Wizard
	*/

	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->template_dir = dirname(__FILE__);
	$smarty->compile_dir  = dirname(__FILE__).'/../../../tmp/';

	require dirname(__FILE__)."/../classes/eBay.inc.php";
	require(dirname(__FILE__)."/../classes/eBay.class.php");
	
	// prevent undefined index notices for this
	if(empty($_REQUEST["step"])) {
		$_REQUEST["step"] = "";
	}
		
	if($_REQUEST["step"] == "1") {

		$apiValues = $_keys[$_environment];

		$sl = new eBayGetSessionID;
			
		$eres = $sl->callEbay();
		//var_dump($eres);	
		
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($eres);
	
	
		$errors = $responseDoc->getElementsByTagName('Errors');
	
		if($errors->length == 0)
		{ //no errors
	
		$sid = $responseDoc->getElementsByTagName('SessionID')->item(0)->nodeValue;

		if($_environment == "sandbox") {
			$url = "https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn&RuName=".$apiValues['RuName']."&SessID=$sid";
		} else {
			$url = "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&RuName=".$apiValues['RuName']."&SessID=$sid";
		}  
		
		$files->setOpt("sid",$db->clean($sid));
		
		header("Location: $url");
		exit(0);
	
		} else {
			$smarty->assign("err", 1);
		}
	}
	
	if($_REQUEST["step"] == "2") {
		
		$sid = $files->getOpt("sid");
		
		$apiValues = $_keys[$_environment];

		$sl = new eBayFetchToken;
		
		$sl->_sid = $sid;
		//echo $sid;
			
		$eres = $sl->callEbay();
		//var_dump($eres);	
		
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($eres);
		
		$errors = $responseDoc->getElementsByTagName('Errors');
	
		//var_dump($eres);
	
		if($errors->length == 0)
		{ //no errors
			$token = $responseDoc->getElementsByTagName('eBayAuthToken')->item(0)->nodeValue;
			$valid = $responseDoc->getElementsByTagName('HardExpirationTime')->item(0)->nodeValue;
			
			$smarty->assign("suc", 1);
			$smarty->assign("HardExpirationTime", $valid);
			
			$files->setOpt("token",$db->clean($token));
			$files->setOpt("valid",$db->clean($valid));
			
			//exit(0);	
		} else {
			$smarty->assign("err", 1);

		}
		
				
	}
	
	
	

	//Content erzeugen und bereitstellen
	$content = $smarty->fetch('authwiz.tpl');
	$template->assign("content",$content);

?>