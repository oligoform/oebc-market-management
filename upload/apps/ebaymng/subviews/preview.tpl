<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>PREVIEW</title>
<style type="text/css">
{literal}
body {
	padding: 0px; 
	margin: 0px;
	overflow-x: hidden;
}
#preview_warning {
	background-color:#CCC; 
	border-bottom: 1px solid #666; 
	width:100%;
	color: #F00;
	height:25px;
	text-align:center;
	margin-bottom:30px;
	padding: 5px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
}
{/literal}
</style>
</head>
<body>
<div id="preview_warning"> ACHTUNG! Dies ist nur ein  Preview!<span style="float:left;"><input type="button" value="Zurück" onclick="javascript:history.go(-1);" /></span></div>
{$preview}
</body>
</html>
