<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/



		if(!$uid || !defined('isOEBC'))
		{
			header("Location: http://". $_SERVER['SERVER_NAME']."/");
			exit(0);
			die('Direct access not premitted');
		}

		$tpl = new smarty;
		$smarty->compile_dir  = dirname(__FILE__).'/../tmp/';
		
		// register the resource name "var"
		$tpl->register_resource("var", array("var_template",
                                       "var_timestamp",
                                       "var_secure",
                                       "var_trusted"));


		function var_template ($tpl_name, &$tpl_source, &$smarty_obj)
		{
			global $files;
   
    		$tpl_source = $files->getOpt("template");
    		return empty($tpl_source) ? false : true;
		}

		function var_timestamp($tpl_name, &$tpl_timestamp, &$smarty_obj)
		{
    		//echo "timestamp called!";
			
			$time = $smarty_obj->get_template_vars($tpl_name.'_time');
    		return !empty($time) ? $time : time();
		}

		function var_secure($tpl_name, &$smarty_obj)
		{
    		return true;
		}

		function var_trusted($tpl_name, &$smarty_obj)
		{
		} 
		
		if (get_magic_quotes_gpc()) {
			$desc = stripslashes($_REQUEST["Description"]);
		} else {
			$desc = $_REQUEST["Description"];
		}
		
		$tpl->assign("auctiondetails",$desc);
		
		if($_REQUEST["OXPIC1"] != "") {
			$tpl->assign("articlepicture", $files->getOpt("url").'/out/pictures/master/1/'.$_REQUEST["OXPIC1"]);
		}
		
		$tpl->assign("auctiontitle",$_REQUEST["Title"]);

					
		$tpldisplay = $tpl->fetch("var:dummy");
		
		// DEBUG		
			
		$template->assign("preview", $tpldisplay );

		$template->display('frontend/pages/preview.tpl');
		exit(0); 		

?>