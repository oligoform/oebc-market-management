<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/



	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}

	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->template_dir = dirname(__FILE__);
	$smarty->compile_dir  = dirname(__FILE__).'/../../../tmp/';

	ini_set("error_reporting", "E_ALL  & ~E_NOTICE");

	require(dirname(__FILE__)."/../classes/eBay.inc.php");
	require(dirname(__FILE__)."/../classes/eBay.class.php");
	
	// prevent some undefined index notices 
	if(empty($_REQUEST["mv"])) {
		$_REQUEST["mv"] = "";
	}
	if(empty($_REQUEST["action"])) {
		$_REQUEST["action"] = "";
	}

	/********* eBay Functions *******************/
	if($_REQUEST["action"] == "sell") {
		//echo $_REQUEST["artnr"];
		$ox = new oxid($db);
		$art = $ox->getArticle($_REQUEST["artnr"]);
		
		$sl = new eBayAddItem();
		
		$sl->_itemTitle			=	$_REQUEST["Title"];
		$sl->_itemDescription	=	$_REQUEST["Description"];
		$sl->_itemBuyItNowPrice	=	str_replace(",",".",$_REQUEST["BuyItNowPrice"]);
		$sl->_itemStartPrice	=	str_replace(",",".",$_REQUEST["StartPrice"]);	//$art["OXTITLE"];
		$sl->_categoryId		=	$_REQUEST["CategoryID"];	//$art["OXTITLE"];
		$sl->_categoryId2		=	$_REQUEST["CategoryID2"];	//$art["OXTITLE"];

		
		$sl->_quantity				=	$_REQUEST["Quantity"];
		//$sl->_template				=	$_REQUEST["Template"];
		$sl->_auction				=	$_REQUEST["Auction"];
		$sl->_startPrice			=	str_replace(",",".",$_REQUEST["StartPrice"]); // Doppelt??
		$sl->_payPal				=	$_REQUEST["PayPal"];
		$sl->_payPalEmailAddress	=	$_REQUEST["PayPalEmailAddress"];
		$sl->_currency				=	$_REQUEST["Currency"];
		$sl->_site					=	$_REQUEST["Site"];
		$sl->_listingDuration		=	$_REQUEST["ListingDuration"];
		
		$sl->_location				=	$files->getOpt("ort");
		
		$sl->_shipping				=	$_REQUEST["shipping"];
		$sl->_shipping2				=	$_REQUEST["shipping2"];
		$sl->_delivery				=   $_REQUEST["delivery"];
		
		$sl->_conditionid			=   $_REQUEST["ConditionID"];
		
		if($_REQUEST["freeShipping"] == 1) { 
			$sl->_freeShipping		=	"true";
		} else {
			$sl->_freeShipping		=	"false";
		}
		
		$sl->_galleryType			=	$_REQUEST["GalleryType"];
		$sl->_picnum				=	$_REQUEST["piccount"];
		
		if($_REQUEST["pic1"]) {
			$sl->_p[1]					=	$_REQUEST["urlpic1"];
		}
		if($_REQUEST["pic2"]) {
			$sl->_p[2]					=	$_REQUEST["urlpic2"];
		}
		if($_REQUEST["pic3"]) {
			$sl->_p[3]					=	$_REQUEST["urlpic3"];
		}
		if($_REQUEST["pic4"]) {
			$sl->_p[4]					=	$_REQUEST["urlpic4"];
		}
		if($_REQUEST["pic5"]) {
			$sl->_p[5]					=	$_REQUEST["urlpic5"];
		}
		if($_REQUEST["pic6"]) {
			$sl->_p[6]					=	$_REQUEST["urlpic6"];
		}
		if($_REQUEST["pic7"]) {
			$sl->_p[7]					=	$_REQUEST["urlpic7"];
		}
		if($_REQUEST["pic8"]) {
			$sl->_p[8]					=	$_REQUEST["urlpic8"];
		}
		if($_REQUEST["pic9"]) {
			$sl->_p[9]					=	$_REQUEST["urlpic9"];
		}
		if($_REQUEST["pic10"]) {
			$sl->_p[10]					=	$_REQUEST["urlpic10"];
		}
		if($_REQUEST["pic11"]) {
			$sl->_p[11]					=	$_REQUEST["urlpic11"];
		}
		if($_REQUEST["pic12"]) {
			$sl->_p[12]					=	$_REQUEST["urlpic12"];
		}
	
		
		$sl->_Paypal				=   $_REQUEST["s6"];
		$sl->_MoneyXferAccepted		=   $_REQUEST["s7"];	//MoneyXferAcceptedInCheckout
		$sl->_COD					=   $_REQUEST["s8"];	
		$sl->_Other					=   $_REQUEST["s9"];
		$sl->_PaymentSeeDescription	=   $_REQUEST["s10"];
		
		$sl->_shopcat	=   $_REQUEST["shopcat"];
		$sl->_shopcat2	=   $_REQUEST["shopcat2"];
		$sl->_subtitel 	=   $_REQUEST["SubTitle"];
		
		//var_dump($art);
	if(	$_REQUEST["FixedPriceListing"] == 1 ) {
		$sl->_listType = "FixedPriceItem";
	} else {
		$sl->_listType = "Chinese";
	}
	
		//scheudle = YYYY-MM-DDTHH:MM:SS.SSSZ
		if($_REQUEST["starttime"]) {
			$schedtime = $_REQUEST["starttime"]."T".$_REQUEST["starth"].":".$_REQUEST["startm"].":00.000Z";
			if($_REQUEST["starttime"] != date("Y-j-n")) {
			 	$sl->_schedule = 	$schedtime;
			} elseif(($_REQUEST["starth"] >= date("G")) && ($_REQUEST["startm"] >= date("i"))) {
				$sl->_schedule =	$schedtime;
			}
		}
	
		$er = $sl->callEbay();
	
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($er);
		
		//echo $er;
	
		//get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');
		
		$log = "";
		
		//if there are error nodes
		if($errors->length > 0)
		{
			$log = '<B>eBay gab folgenden Fehler zurück:</B>';
			foreach ($errors as $error) {
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $error->getElementsByTagName('ErrorCode');
			$shortMsg = $error->getElementsByTagName('ShortMessage');
			$longMsg  = $error->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$log .= '<P>'.$code->item(0)->nodeValue.' : '.str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0) {
				$log .= '<BR>'.str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
			}
			}
			

		} 
		

		$acks = $responseDoc->getElementsByTagName("Ack");
		$ack   = $acks->item(0)->nodeValue;
		
		if(($errors->length == 0) || ($ack == "Warning"))
		{ //no errors
		
			
		
			//get results nodes
			$responses = $responseDoc->getElementsByTagName("AddItemResponse");
			foreach ($responses as $response) {
	
				if ($ack == "Warning")
				{
					$log .= '<p style="color:red">Keine Panik! Diese Mitteilung ist legendlich eine Warnung, Ihre Angebot hat trotzdem erfolgreich den Marktplatz ereicht.<br /></p>';	
				}
		
				$log .= '<p><strong>eBay gab folgenden Angaben zum Angebot zurück:</strong><br /></p>';//"Ack = $ack <BR />\n";   // Success if successful

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = $endTimes->item(0)->nodeValue;
				$log .= "<p>Auktions Ende: ".str_replace("T", " ", $endTime)." <BR />\n";

				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				$log .=  "Auktions ID: $itemID <BR /></p>\n";

				$linkBase = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item=";

				$feeNodes = $responseDoc->getElementsByTagName('Fee');
				
				$lf = 0;
				
				foreach($feeNodes as $feeNode) {
					$feeNames = $feeNode->getElementsByTagName("Name");
					if ($feeNames->item(0)) {
						$feeName = $feeNames->item(0)->nodeValue;
						$fees = $feeNode->getElementsByTagName('Fee');  // get Fee amount nested in Fee
						$fee = $fees->item(0)->nodeValue;
						if ($fee > 0.0) {
							if ($feeName == 'ListingFee') {
								$log .= "<b>Gebühren: ".sprintf("%01.2f", $fee)."</b> <br />\n";
								
								$lf = $fee;
								
							} else {
								$log .= "$feeName: ".sprintf("%01.2f", $fee)." <br />\n";
							}
						}
					}
				}
				
				$log .= "<br />Ihre Auktion wurde erfolgreich bei eBay eingestellt!<br /></p>";
				
				$sql = "INSERT INTO `oebc_offers` (eid, oid, fees, stock, sold, last) VALUES ($itemID, '".$db->clean($_REQUEST["artnr"])."', '$lf', ".$db->clean($_REQUEST["Quantity"]).", 0, 0)";
				$db->query_exec($sql);
				
				if($files->getOpt("bestand") == "1") 
				{
					
					correctStock($_REQUEST["artnr"], $_REQUEST["Quantity"], $_REQUEST["parent"]);

				}
				elseif($files->getOpt("bestand") == "2") 
				{
					
					//correctStock($_REQUEST["artnr"], $_REQUEST["Quantity"], $_REQUEST["parent"]);
				}
								
			}
		}
		
	
		$smarty->assign("log", $log);

		//Content erzeugen und bereitstellen
		$content = $smarty->fetch('result.tpl');
		$template->assign("content",$content);
		
		// Ausgabe
		if(!$template->titel){
			$template->titel = "OEBC";
		}

		$template->publisher = $publisher;

		$template->display('common/app.tpl');
		//$responses = $responseDoc->getElementsByTagName("GetSellerListResponse");
		
		//header("Location: http://". $_SERVER['SERVER_NAME']."/index.php?mode=adm#tabs-2");
		exit(0);
	}
	
	if($_REQUEST["action"] == "sellmv") {
		
				//echo $_REQUEST["artnr"];
		$ox = new oxid($db);
		$art = $ox->getArticle($_REQUEST["artnr"]);
		
		$sl = new eBayAddItemMV();
		
		$sl->_itemTitle			=	$_REQUEST["Title"];
		$sl->_itemDescription	=	$_REQUEST["Description"];
		//$sl->_itemBuyItNowPrice	=	$_REQUEST["BuyItNowPrice"];
		//$sl->_itemStartPrice	=	$_REQUEST["StartPrice"];	//$art["OXTITLE"];
		$sl->_categoryId		=	$_REQUEST["CategoryID"];	//$art["OXTITLE"];
		$sl->_categoryId2		=	$_REQUEST["CategoryID2"];	//$art["OXTITLE"];

		//$sl->_quantity				=	$_REQUEST["Quantity"];
		//$sl->_template				=	$_REQUEST["Template"];
		$sl->_auction				=	$_REQUEST["Auction"];
		$sl->_startPrice			=	str_replace(",",".",$_REQUEST["StartPrice"]);
		$sl->_payPal				=	$_REQUEST["PayPal"];
		$sl->_payPalEmailAddress	=	$_REQUEST["PayPalEmailAddress"];
		$sl->_currency				=	$_REQUEST["Currency"];
		$sl->_site					=	$_REQUEST["Site"];
		$sl->_listingDuration		=	$_REQUEST["ListingDuration"];
		
		$sl->_location				=	$files->getOpt("ort");
		
		$sl->_shipping				=	$_REQUEST["shipping"];
		$sl->_shipping2				=	$_REQUEST["shipping2"];
		$sl->_delivery				=   $_REQUEST["delivery"];
		
		if($_REQUEST["freeShipping"] == 1) { 
			$sl->_freeShipping			=	"true";
		} else {
			$sl->_freeShipping			=	"false";
		}
		
		$sl->_conditionid				=   $_REQUEST["ConditionID"];
		
		$sl->_galleryType			=	$_REQUEST["GalleryType"];
		$sl->_picnum				=	$_REQUEST["piccount"];
		
		if($_REQUEST["pic1"]) {
			$sl->_p[1]					=	$_REQUEST["urlpic1"];
		}
		if($_REQUEST["pic2"]) {
			$sl->_p[2]					=	$_REQUEST["urlpic2"];
		}
		if($_REQUEST["pic3"]) {
			$sl->_p[3]					=	$_REQUEST["urlpic3"];
		}
		if($_REQUEST["pic4"]) {
			$sl->_p[4]					=	$_REQUEST["urlpic4"];
		}
		if($_REQUEST["pic5"]) {
			$sl->_p[5]					=	$_REQUEST["urlpic5"];
		}
		if($_REQUEST["pic6"]) {
			$sl->_p[6]					=	$_REQUEST["urlpic6"];
		}
		if($_REQUEST["pic7"]) {
			$sl->_p[7]					=	$_REQUEST["urlpic7"];
		}
		if($_REQUEST["pic8"]) {
			$sl->_p[8]					=	$_REQUEST["urlpic8"];
		}
		if($_REQUEST["pic9"]) {
			$sl->_p[9]					=	$_REQUEST["urlpic9"];
		}
		if($_REQUEST["pic10"]) {
			$sl->_p[10]					=	$_REQUEST["urlpic10"];
		}
		if($_REQUEST["pic11"]) {
			$sl->_p[11]					=	$_REQUEST["urlpic11"];
		}
		if($_REQUEST["pic12"]) {
			$sl->_p[12]					=	$_REQUEST["urlpic12"];
		}
		
		$sl->_Paypal				=   $_REQUEST["s6"];
		$sl->_MoneyXferAccepted		=   $_REQUEST["s7"];	//MoneyXferAcceptedInCheckout
		$sl->_COD					=   $_REQUEST["s8"];	
		$sl->_Other					=   $_REQUEST["s9"];
		$sl->_PaymentSeeDescription	=   $_REQUEST["s10"];	
		
		$sl->_shopcat	=   $_REQUEST["shopcat"];
		$sl->_shopcat2	=   $_REQUEST["shopcat2"];
		$sl->_subtitel 	=   $_REQUEST["SubTitle"];
				
		// Build Tree for Variants.
		
		$children = $ox->getArticleChildren($_REQUEST["artnr"], 1);

		$variants = array();
		
		foreach($children as $child) {
			
			$v = array();
			
			$v = split("\|", $child["OXVARSELECT"]);
			
			array_push($variants, $v);								
			//for($i = 0; $i != count($v); $i++) {
				
			//}
		}
		
		//$sl->_variants				=   array_unique($variants);
		
		$ml = array();
		
		$mv = count($variants);
		
		foreach($variants as $v) {
			$c = count($v);
			
			for($i = 0;$i < count($v); $i++) {
				$ml[$i][$mv] = trim($v[$i]);	
			}	
			$mv--;		
		}
		
		for($i = 0;$i < count($ml);$i++) {
			$t = array_unique($ml[$i]);
			sort($t);
			$ml[$i] = $t;
		}
		
		$children = $ox->getArticle($_REQUEST["artnr"]);
		
		$v = split("\|", $children["OXVARNAME"]);
		
		$variantnames = array();
		
		foreach($v as $variantname) {
			array_push($variantnames, trim($variantname));
		}
		
		$sl->_variants 		= $ml;
		$sl->_variantsnames = $variantnames;

		$sl->articlec		= $_REQUEST["v"];
			
		//build arries for eBay Listing		
		for($i = 1; $i <= $_REQUEST["v"];$i++) {
		
			//echo "v$i";
		
			$t1[$i] = str_replace(",",".",$_REQUEST["ip".$i]);
			$t2[$i] = $_REQUEST["iq".$i];
			
			$v = split("\|",  $_REQUEST["iv".$i]);
			
			// trim/clean array elements 
			
			$k = array();
			
			foreach($v as $a)
			{
				array_push($k,trim($a));	
			}
			
			$sl->_avariants[$i] = $k;
			
		}
		$sl->_itemStartPrice	= str_replace(",",".",$t1);
		$sl->_quantity		= $t2;
		//scheudle = YYYY-MM-DDTHH:MM:SS.SSSZ
		if($_REQUEST["starttime"]) {
			$schedtime = $_REQUEST["starttime"]."T".$_REQUEST["starth"].":".$_REQUEST["startm"].":00.000Z";
			if($_REQUEST["starttime"] != date("Y-j-n")) {
			 	$sl->_schedule = 	$schedtime;
			} elseif(($_REQUEST["starth"] >= date("G")) && ($_REQUEST["startm"] >= date("i"))) {
				$sl->_schedule =	$schedtime;
			}
		}
	
		$er = $sl->callEbay();
	
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($er);
		
		//echo $er;
	
		//get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');
		
		$log = "";
		
		//if there are error nodes
		if($errors->length > 0)
		{
			$log = '<B>eBay gab folgenden Fehler zurück:</B>';
			foreach ($errors as $error) {
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $error->getElementsByTagName('ErrorCode');
			$shortMsg = $error->getElementsByTagName('ShortMessage');
			$longMsg  = $error->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$log .= '<P>'.$code->item(0)->nodeValue.' : '.str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0) {
				$log .= '<BR>'.str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
			}
			}
			

		} 
		

		$acks = $responseDoc->getElementsByTagName("Ack");
		$ack   = $acks->item(0)->nodeValue;
		
		//echo $errors->length == 0;
		
		if(($errors->length == 0) || ($ack == "Warning"))
		{ //no errors
		
			//echo "fail!";
		
			//get results nodes
			$response = $responseDoc;
//			foreach ($responses as $response) {
	
				if ($ack == "Warning")
				{
					$log .= '<p style="color:red">Keine Panik! Diese Mitteilung ist legendlich eine Warnung, Ihre Angebot hat trotzdem erfolgreich den Marktplatz ereicht.<br /></p>';	
				}
		
				$log .= '<p><strong>eBay gab folgenden Angaben zum Angebot zurück:</strong><br /></p>';//"Ack = $ack <BR />\n";   // Success if successful

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = $endTimes->item(0)->nodeValue;
				$log .= "<p>Auktions Ende: ".str_replace("T", " ", $endTime)." <BR />\n";

				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				$log .=  "Auktions ID: $itemID <BR /></p>\n";

				$linkBase = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item=";

				$feeNodes = $responseDoc->getElementsByTagName('Fee');
				
				$lf = 0;
				
				foreach($feeNodes as $feeNode) {
					$feeNames = $feeNode->getElementsByTagName("Name");
					if ($feeNames->item(0)) {
						$feeName = $feeNames->item(0)->nodeValue;
						$fees = $feeNode->getElementsByTagName('Fee');  // get Fee amount nested in Fee
						$fee = $fees->item(0)->nodeValue;
						if ($fee > 0.0) {
							if ($feeName == 'ListingFee') {
								$log .= "<b>Gebühren: ".sprintf("%01.2f", $fee)."</b> <br />\n";
								
								$lf = $fee;
								
							} else {
								$log .= "$feeName: ".sprintf("%01.2f", $fee)." <br />\n";
							}
						}
					}
				}
				
				$log .= "<br />Ihre Auktion wurde erfolgreich bei eBay eingestellt!<br /></p>";
				
				$sql = "INSERT INTO `oebc_offers` (eid, oid, fees, stock, sold, last) VALUES ($itemID, '".$db->clean($_REQUEST["artnr"])."', $lf, '".$db->clean($_REQUEST["Quantity"])."', 0, 0)";
				$db->query_exec($sql);
				
				for($i = 0; $i != $_REQUEST["v"];$i++) {
						
					if($_REQUEST["i1"+$i] > 0) {
						$sql = "INSERT INTO `oebc_offers` (eid, oid, fees, stock, sold, last) VALUES ($itemID, '".$db->clean($_REQUEST["ia"+$i])."', $lf, '".$db->clean($_REQUEST["iq"+$i])."', 0, 0)";
						//$sql = "UPDATE `oxarticles` SET `OXSTOCK` =  '$stock' WHERE `OXID` = '".$db->clean($_REQUEST["ia"+$i])."' LIMIT 1";
						$db->query_exec($sql);	
					}
						
		
				}
				
				if($files->getOpt("bestand") == 1) 
				{
					$v = $_REQUEST["v"];
					$ia = array();
					$iq = array();
					
					for($i = 1; $i <= $_REQUEST["v"];$i++) {
						array_push($ia, $_REQUEST["ia$i"]);
						array_push($iq, $_REQUEST["iq$i"]);	
					}
					
					$ox->correctStockMv();
					
				}
								
			//}
		}
		
	
		$smarty->assign("log", $log);

		//Content erzeugen und bereitstellen
		$content = $smarty->fetch('result.tpl');
		$template->assign("content",$content);
		
		// Ausgabe
		if(!$template->titel){
			$template->titel = "OEBC";
		}

		$template->publisher = $publisher;

		$template->display('common/app.tpl');
		//$responses = $responseDoc->getElementsByTagName("GetSellerListResponse");
		
		//header("Location: http://". $_SERVER['SERVER_NAME']."/index.php?mode=adm#tabs-2");
		exit(0);
		
	}
	
	$ox = new oxid($db);
	
	if($_REQUEST["mv"] == "1") 
	{
		$smarty->assign("mv", "1");
		
		$children = $ox->getArticleChildren($_REQUEST["artnr"]);
		
		$smarty->assign("children", $children);
			
	}
	
	
	
	$art = $ox->getArticle($_REQUEST["artnr"]);
	
	$ct = new eBayGetCategoriesTree;
	
	$ct->_vonly = 1;
	
	$ct->_site = $files->getOpt("land");
	
	$cat_xml = $ct->callEbay();
	
	$responseDoc = new DomDocument();
	$responseDoc->loadXML($cat_xml);
	
	//get any error nodes
	$errors = $responseDoc->getElementsByTagName('Errors');

	//if there are error nodes
	if($errors->length == 0) {
			
			$version = $responseDoc->getElementsByTagName("CategoryVersion")->item(0)->nodeValue;
			
			if($version != $files->getOpt("cat_version"))
			{
				$smarty->assign("renew", 1);
				//echo $version;
			}
	}
	
	$sql = "SELECT * FROM `oebc_shopcats`";
	$cats = $db->query_array($sql);
	
	$smarty->assign("shopcats", $cats);
	
	$smarty->assign("s6", $files->getOpt("p_pp"));
	$smarty->assign("s7", $files->getOpt("p_ueb"));	
	$smarty->assign("s8", $files->getOpt("p_nn"));	
	$smarty->assign("s9", $files->getOpt("p_no"));
	$smarty->assign("s10", $files->getOpt("p_ot"));
	
	ini_set("error_reporting", "E_ALL");
	ini_set("display_errors", "on");
	
		
	$pics = $ox->getArtPics($_REQUEST["artnr"]);
	
	//var_dump($pics);
	
	$ppics = array();
	
	//echo realpath(dirname(__FILE__)."/../../../classes/oxidimg.class.php");
	require(dirname(__FILE__)."/../../../classes/oxidimg.class.php");
	
	foreach($pics as $pic) {
		if($pic != "") {
			
			$url = $files->getOpt("url").'/out/pictures/master/1/'.$pic;
	
			$oximg = new oxidimg;
			
			$res = $oximg->getImg($url);
			
			// put in array, no puplicates pls (as returned by getArtPics
			
			if(!in_array($res[1],$ppics)) {
				
				array_push($ppics, $res[1]);
			
			}
			
		}
	}
	
	$smarty->assign("ppics", $ppics);
	$smarty->assign("ppicsc", count($ppics));
	
	$smarty->assign("pp_email", $files->getOpt("pp_email"));
	
	$smarty->assign("article", $art);

	$smarty->assign("bestand", $files->getOpt("bestand"));
	
	$smarty->assign("mymode", "additems");

	$content = $smarty->fetch('additems.tpl');
		
	$template->assign("content",$content);

?>