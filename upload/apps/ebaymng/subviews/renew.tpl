<div class="ui-widget-content" style="padding:10px; height:100%;"><br />
<h2>Aktualisieren</h2>
{if $step1}
<p>Eine aktuelle Version des Kategoriebaums wird heruntergeladen, dieser Vorgang kann einige Minuten dauern, bitte warten Sie und schliessen Sie auf keinen Fall dieses Fenster!</p><br />
<strong>Dieser Vorgang dauert aufgrund des Umfangs des Kategoriebaums einige Zeit! Drücken auf keinen Fall auf Abbrechen oder Zurück in ihrem Browser!</strong>
<p>&nbsp;</p>
{elseif $step3}
<p>Eine aktuelle Version des Kategoriebaums wird heruntergeladen, dieser Vorgang kann einige Minuten dauern, bitte warten Sie und schliessen Sie auf keinen Fall dieses Fenster!</p><br />
<strong>Dieser Vorgang dauert aufgrund des Umfangs des Kategoriebaums einige Zeit! Drücken auf keinen Fall auf Abbrechen oder Zurück in ihrem Browser!</strong>
{elseif $step5}
<p>Eine aktuelle Version des Kategoriebaums sowie der Eigenschaften wurde erfolgreich in der Datenbank hinterlegt, sie können dieses Fenster jetzt schliessen.</p>
{/if}
</div>