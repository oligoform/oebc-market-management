<?

/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	

	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
		
	$smarty->template_dir = dirname(__FILE__);
	$smarty->compile_dir  = dirname(__FILE__).'/../../../tmp/';

	require dirname(__FILE__)."/../classes/eBay.inc.php";
	require(dirname(__FILE__)."/../classes/eBay.class.php");
			
	$sl = new eBayRelistItem();
		
	$sl->_itemid		=	$_REQUEST["id"];
	
	$er = $sl->callEbay();
	
	$responseDoc = new DomDocument();
	$responseDoc->loadXML($er);
		
		//echo $er;
	
		//get any error nodes
	$errors = $responseDoc->getElementsByTagName('Errors');
		
	$log = "";
		
		//if there are error nodes
	if($errors->length > 0)
	{
		$log = '<B>eBay gab folgenden Fehler zurück:</B>';
		foreach ($errors as $error) {
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $error->getElementsByTagName('ErrorCode');
			$shortMsg = $error->getElementsByTagName('ShortMessage');
			$longMsg  = $error->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$log .= '<P>'.$code->item(0)->nodeValue.' : '.str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0) {
				$log .= '<BR>'.str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
			}
		}
			

	} 
		

	$acks = $responseDoc->getElementsByTagName("Ack");
	$ack   = $acks->item(0)->nodeValue;
				
	if(($errors->length == 0) || ($ack == "Warning"))
	{ //no errors
		
			
		
			//get results nodes
			$responses = $responseDoc->getElementsByTagName("RelistItemResponse");
			foreach ($responses as $response) {
	
				if ($ack == "Warning")
				{
					$log .= '<p style="color:red">Keine Panik! Diese Mitteilung ist legendlich eine Warnung, Ihre Angebot hat trotzdem erfolgreich den Marktplatz ereicht.<br /></p>';	
				}
		
				$log .= '<p><strong>eBay gab folgenden Angaben zum Angebot zurück:</strong><br /></p>';//"Ack = $ack <BR />\n";   // Success if successful

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = $endTimes->item(0)->nodeValue;
				$log .= "<p>Auktions Ende: ".str_replace("T", " ", $endTime)." <BR />\n";

				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				$log .=  "Auktions ID: $itemID <BR /></p>\n";

				$linkBase = "http://$shopurl/ws/eBayISAPI.dll?ViewItem&item=";

				$feeNodes = $responseDoc->getElementsByTagName('Fee');
				
				$lf = 0;
				
				foreach($feeNodes as $feeNode) {
					$feeNames = $feeNode->getElementsByTagName("Name");
					if ($feeNames->item(0)) {
						$feeName = $feeNames->item(0)->nodeValue;
						$fees = $feeNode->getElementsByTagName('Fee');  // get Fee amount nested in Fee
						$fee = $fees->item(0)->nodeValue;
						if ($fee > 0.0) {
							if ($feeName == 'ListingFee') {
								$log .= "<b>Gebühren: ".sprintf("%01.2f", $fee)."</b> <br />\n";
								
								$lf = $fee;
								
							} else {
								$log .= "$feeName: ".sprintf("%01.2f", $fee)." <br />\n";
							}
						}
					}
				}
				
				$log .= "<br />Ihre Auktion wurde erfolgreich bei eBay wiedereingestellt! Bitte aktualisieren Sie die Auktionsansicht.<br /></p>";
				
				$sl = new eBayGetItem();
				
				
				$sl->_itemid		=	$_REQUEST["id"];
	
				$er = $sl->callEbay();
	
				$responseDoc = new DomDocument();
				$responseDoc->loadXML($er);
			
				//echo $er;
				
				$itemid = $responseDoc->getElementsByTagName("RelistedItemID")->item(0)->nodeValue;
				$quantity = $responseDoc->getElementsByTagName("Quantity")->item(0)->nodeValue;
				
				$sql = "INSERT INTO `oebc_offers` (eid, oid, fees, stock, sold, last) VALUES ($itemID, '".$itemid."', '$lf', ".$quantity.", 0, 0)";
				$db->query_exec($sql);
				
				/*if($files->getOpt("bestand")) 
				{
					
					$sql = "SELECT `OXSTOCK` FROM `oxarticles` WHERE `OXID` = '".$db->clean($_REQUEST["artnr"])."' LIMIT 1";
					$rr = $db->query($sql);
					
					$stock = $rr[0] - $_REQUEST["Quantity"];
					
					$sql = "UPDATE `oxarticles` SET `OXSTOCK` =  '$stock' WHERE `OXID` = '".$db->clean($_REQUEST["artnr"])."' LIMIT 1";
					$db->query_exec($sql);	
				}*/
								
			}
	}
	
	$smarty->assign("log", $log);

		//Content erzeugen und bereitstellen
	$content = $smarty->fetch('result.tpl');
	$template->assign("content",$content);
		
		/*// Ausgabe
		if(!$template->titel){
			$template->titel = $default_titel;
		}

		$template->publisher = $publisher;

		$template->display('frontend/common/index.tpl');*/

?>