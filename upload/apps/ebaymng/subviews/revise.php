<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/



	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}


	/*$ct = new eBayGeteBayDetails;
			
	$cat_xml = $ct->callEbay();*/
	//echo $cat_xml;

	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../tmp/';

	/********* eBay Functions *******************/
	if($_REQUEST["action"] == "revise") {
		//echo $_REQUEST["artnr"];
		$ox = new oxid($db);
		$art = $ox->getArticle($_REQUEST["artnr"]);
		
		$sl = new eBayAddItem();
		
		$sl->_itemTitle			=	$_REQUEST["Title"];
		$sl->_itemDescription	=	$_REQUEST["Description"];
		$sl->_itemBuyItNowPrice	=	str_replace(",",".",$_REQUEST["BuyItNowPrice"]);
		$sl->_itemStartPrice	=	str_replace(",",".",$_REQUEST["StartPrice"]);	//$art["OXTITLE"];
		$sl->_categoryId		=	$_REQUEST["CategoryID"];	//$art["OXTITLE"];
		
		$sl->_quantity				=	$_REQUEST["Quantity"];
		//$sl->_template				=	$_REQUEST["Template"];
		$sl->_auction				=	$_REQUEST["Auction"];
		$sl->_startPrice			=	str_replace(",",".",$_REQUEST["StartPrice"]); // Doppelt??
		$sl->_payPal				=	$_REQUEST["PayPal"];
		$sl->_payPalEmailAddress	=	$_REQUEST["PayPalEmailAddress"];
		$sl->_currency				=	$_REQUEST["Currency"];
		$sl->_site					=	$_REQUEST["Site"];
		$sl->_listingDuration		=	$_REQUEST["ListingDuration"];
		
		$sl->_location				=	$files->getOpt("ort");
		
		$sl->_shipping				=	$_REQUEST["shipping"];
		$sl->_shipping2				=	$_REQUEST["shipping2"];
		$sl->_delivery				=   $_REQUEST["delivery"];
		
		$sl->_conditionid			=   $_REQUEST["ConditionID"];
		
		if($_REQUEST["freeShipping"] == 1) { 
			$sl->_freeShipping		=	"true";
		} else {
			$sl->_freeShipping		=	"false";
		}
		
		$sl->_galleryType			=	$_REQUEST["GalleryType"];
		$sl->_picnum				=	$_REQUEST["PictureC"];
		$sl->_p[1]					=	$art["OXPIC1"];
		$sl->_p[2]					=	$art["OXPIC2"];
		$sl->_p[3]					=	$art["OXPIC3"];
		$sl->_p[4]					=	$art["OXPIC4"];
		$sl->_p[5]					=	$art["OXPIC5"];
		$sl->_p[6]					=	$art["OXPIC6"];
		$sl->_p[7]					=	$art["OXPIC7"];
		$sl->_p[8]					=	$art["OXPIC8"];
		$sl->_p[9]					=	$art["OXPIC9"];
		$sl->_p[10]					=	$art["OXPIC10"];
		$sl->_p[11]					=	$art["OXPIC11"];
		$sl->_p[12]					=	$art["OXPIC12"];
		
		$sl->_Paypal				=   $_REQUEST["s6"];
		$sl->_MoneyXferAccepted		=   $_REQUEST["s7"];	//MoneyXferAcceptedInCheckout
		$sl->_COD					=   $_REQUEST["s8"];	
		$sl->_Other					=   $_REQUEST["s9"];
		$sl->_PaymentSeeDescription	=   $_REQUEST["s10"];
		
		$sl->_shopcat	=   $_REQUEST["shopcat"];
		$sl->_shopcat2	=   $_REQUEST["shopcat2"];
		$sl->_subtitel 	=   $_REQUEST["SubTitle"];
		
		//var_dump($art);
	if(	$_REQUEST["FixedPriceListing"] == 1 ) {
		$sl->_listType = "FixedPriceItem";
	} else {
		$sl->_listType = "Chinese";
	}
	
		//scheudle = YYYY-MM-DDTHH:MM:SS.SSSZ
		if($_REQUEST["starttime"]) {
			$schedtime = $_REQUEST["starttime"]."T".$_REQUEST["starth"].":".$_REQUEST["startm"].":00.000Z";
			if($_REQUEST["starttime"] != date("Y-j-n")) {
			 	$sl->_schedule = 	$schedtime;
			} elseif(($_REQUEST["starth"] >= date("G")) && ($_REQUEST["startm"] >= date("i"))) {
				$sl->_schedule =	$schedtime;
			}
		}
	
		$er = $sl->callEbay();
	
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($er);
		
		echo $er;
	
		//get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');
		
		$log = "";
		
		//if there are error nodes
		if($errors->length > 0)
		{
			$log = '<B>eBay gab folgenden Fehler zurück:</B>';
			foreach ($errors as $error) {
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $error->getElementsByTagName('ErrorCode');
			$shortMsg = $error->getElementsByTagName('ShortMessage');
			$longMsg  = $error->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$log .= '<P>'.$code->item(0)->nodeValue.' : '.str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0) {
				$log .= '<BR>'.str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
			}
			}
			

		} 
		

		$acks = $responseDoc->getElementsByTagName("Ack");
		$ack   = $acks->item(0)->nodeValue;
		
		if(($errors->length == 0) || ($ack == "Warning"))
		{ //no errors
		
			
		
			//get results nodes
			$responses = $responseDoc->getElementsByTagName("AddItemResponse");
			foreach ($responses as $response) {
	
				if ($ack == "Warning")
				{
					$log .= '<p style="color:red">Keine Panik! Diese Mitteilung ist legendlich eine Warnung, Ihre Angebot hat trotzdem erfolgreich den Marktplatz ereicht.<br /></p>';	
				}
		
				$log .= '<p><strong>eBay gab folgenden Angaben zum Angebot zurück:</strong><br /></p>';//"Ack = $ack <BR />\n";   // Success if successful

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = $endTimes->item(0)->nodeValue;
				$log .= "<p>Auktions Ende: ".str_replace("T", " ", $endTime)." <BR />\n";

				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				$log .=  "Auktions ID: $itemID <BR /></p>\n";

				$linkBase = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item=";

				$feeNodes = $responseDoc->getElementsByTagName('Fee');
				
				$lf = 0;
				
				foreach($feeNodes as $feeNode) {
					$feeNames = $feeNode->getElementsByTagName("Name");
					if ($feeNames->item(0)) {
						$feeName = $feeNames->item(0)->nodeValue;
						$fees = $feeNode->getElementsByTagName('Fee');  // get Fee amount nested in Fee
						$fee = $fees->item(0)->nodeValue;
						if ($fee > 0.0) {
							if ($feeName == 'ListingFee') {
								$log .= "<b>Gebühren: ".sprintf("%01.2f", $fee)."</b> <br />\n";
								
								$lf = $fee;
								
							} else {
								$log .= "$feeName: ".sprintf("%01.2f", $fee)." <br />\n";
							}
						}
					}
				}
				
				$log .= "<br />Ihre Auktion wurde erfolgreich bei eBay eingestellt!<br /></p>";
				
				$sql = "INSERT INTO `oebc_offers` (eid, oid, fees, stock, sold, last) VALUES ($itemID, '".$db->clean($_REQUEST["artnr"])."', '$lf', ".$db->clean($_REQUEST["Quantity"]).", 0, 0)";
				$db->query_exec($sql);
				
				if($files->getOpt("bestand") == "1") 
				{
					
					correctStock($_REQUEST["artnr"], $_REQUEST["Quantity"], $_REQUEST["parent"]);
				}
				elseif($files->getOpt("bestand") == "2") 
				{
					
					//correctStock($_REQUEST["artnr"], $_REQUEST["Quantity"], $_REQUEST["parent"]);
				}
								
			}
		}
		
		$smarty->assign("log", $log);

		//Content erzeugen und bereitstellen
		$content = $smarty->fetch('frontend/pages/result.tpl');
		$template->assign("content",$content);
		
		// Ausgabe
		if(!$template->titel){
			$template->titel = $default_titel;
		}

		$template->publisher = $publisher;

		$template->display('frontend/common/index.tpl');
		//$responses = $responseDoc->getElementsByTagName("GetSellerListResponse");
		
		//header("Location: http://". $_SERVER['SERVER_NAME']."/index.php?mode=adm#tabs-2");
		exit(0);
	}
	
	if($_REQUEST["action"] == "revisemv") {
		
				//echo $_REQUEST["artnr"];
		$ox = new oxid($db);
		$art = $ox->getArticle($_REQUEST["artnr"]);
		
		$sl = new eBayAddItemMV();
		
		$sl->_itemTitle			=	$_REQUEST["Title"];
		$sl->_itemDescription	=	$_REQUEST["Description"];
		//$sl->_itemBuyItNowPrice	=	$_REQUEST["BuyItNowPrice"];
		//$sl->_itemStartPrice	=	$_REQUEST["StartPrice"];	//$art["OXTITLE"];
		$sl->_categoryId		=	$_REQUEST["CategoryID"];	//$art["OXTITLE"];
		
		//$sl->_quantity				=	$_REQUEST["Quantity"];
		//$sl->_template				=	$_REQUEST["Template"];
		$sl->_auction				=	$_REQUEST["Auction"];
		$sl->_startPrice			=	str_replace(",",".",$_REQUEST["StartPrice"]);
		$sl->_payPal				=	$_REQUEST["PayPal"];
		$sl->_payPalEmailAddress	=	$_REQUEST["PayPalEmailAddress"];
		$sl->_currency				=	$_REQUEST["Currency"];
		$sl->_site					=	$_REQUEST["Site"];
		$sl->_listingDuration		=	$_REQUEST["ListingDuration"];
		
		$sl->_location				=	$files->getOpt("ort");
		
		$sl->_shipping				=	$_REQUEST["shipping"];
		$sl->_shipping2				=	$_REQUEST["shipping2"];
		$sl->_delivery				=   $_REQUEST["delivery"];
		
		if($_REQUEST["freeShipping"] == 1) { 
			$sl->_freeShipping			=	"true";
		} else {
			$sl->_freeShipping			=	"false";
		}
		
		$sl->_conditionid				=   $_REQUEST["ConditionID"];
		
		$sl->_galleryType			=	$_REQUEST["GalleryType"];
		$sl->_picnum				=	$_REQUEST["PictureC"];
		$sl->_p[1]					=	$art["OXPIC1"];
		$sl->_p[2]					=	$art["OXPIC2"];
		$sl->_p[3]					=	$art["OXPIC3"];
		$sl->_p[4]					=	$art["OXPIC4"];
		$sl->_p[5]					=	$art["OXPIC5"];
		$sl->_p[6]					=	$art["OXPIC6"];
		$sl->_p[7]					=	$art["OXPIC7"];
		$sl->_p[8]					=	$art["OXPIC8"];
		$sl->_p[9]					=	$art["OXPIC9"];
		$sl->_p[10]					=	$art["OXPIC10"];
		$sl->_p[11]					=	$art["OXPIC11"];
		$sl->_p[12]					=	$art["OXPIC12"];
		
		$sl->_Paypal				=   $_REQUEST["s6"];
		$sl->_MoneyXferAccepted		=   $_REQUEST["s7"];	//MoneyXferAcceptedInCheckout
		$sl->_COD					=   $_REQUEST["s8"];	
		$sl->_Other					=   $_REQUEST["s9"];
		$sl->_PaymentSeeDescription	=   $_REQUEST["s10"];	
		
		$sl->_shopcat	=   $_REQUEST["shopcat"];
		$sl->_shopcat2	=   $_REQUEST["shopcat2"];
		$sl->_subtitel 	=   $_REQUEST["SubTitle"];
				
		// Build Tree for Variants.
		
		$children = $ox->getArticleChildren($_REQUEST["artnr"], 1);

		$variants = array();
		
		foreach($children as $child) {
			
			$v = array();
			
			$v = split("\|", $child["OXVARSELECT"]);
			
			array_push($variants, $v);								
			//for($i = 0; $i != count($v); $i++) {
				
			//}
		}
		
		//$sl->_variants				=   array_unique($variants);
		
		$ml = array();
		
		$mv = count($variants);
		
		foreach($variants as $v) {
			$c = count($v);
			
			for($i = 0;$i < count($v); $i++) {
				$ml[$i][$mv] = trim($v[$i]);	
			}	
			$mv--;		
		}
		
		for($i = 0;$i < count($ml);$i++) {
			$t = array_unique($ml[$i]);
			sort($t);
			$ml[$i] = $t;
		}
		
		$children = $ox->getArticle($_REQUEST["artnr"]);
		
		$v = split("\|", $children["OXVARNAME"]);
		
		$variantnames = array();
		
		foreach($v as $variantname) {
			array_push($variantnames, trim($variantname));
		}
		
		$sl->_variants 		= $ml;
		$sl->_variantsnames = $variantnames;

		$sl->articlec		= $_REQUEST["v"];
			
		//build arries for eBay Listing		
		for($i = 1; $i <= $_REQUEST["v"];$i++) {
		
			//echo "v$i";
		
			$t1[$i] = str_replace(",",".",$_REQUEST["ip".$i]);
			$t2[$i] = $_REQUEST["iq".$i];
			
			$v = split("\|",  $_REQUEST["iv".$i]);
			
			// trim/clean array elements 
			
			$k = array();
			
			foreach($v as $a)
			{
				array_push($k,trim($a));	
			}
			
			$sl->_avariants[$i] = $k;
			
		}
		$sl->_itemStartPrice	= str_replace(",",".",$t1);
		$sl->_quantity		= $t2;
		//scheudle = YYYY-MM-DDTHH:MM:SS.SSSZ
		if($_REQUEST["starttime"]) {
			$schedtime = $_REQUEST["starttime"]."T".$_REQUEST["starth"].":".$_REQUEST["startm"].":00.000Z";
			if($_REQUEST["starttime"] != date("Y-j-n")) {
			 	$sl->_schedule = 	$schedtime;
			} elseif(($_REQUEST["starth"] >= date("G")) && ($_REQUEST["startm"] >= date("i"))) {
				$sl->_schedule =	$schedtime;
			}
		}
	
		$er = $sl->callEbay();
	
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($er);
		
		//echo $er;
	
		//get any error nodes
		$errors = $responseDoc->getElementsByTagName('Errors');
		
		$log = "";
		
		//if there are error nodes
		if($errors->length > 0)
		{
			$log = '<B>eBay gab folgenden Fehler zurück:</B>';
			foreach ($errors as $error) {
			//display each error
			//Get error code, ShortMesaage and LongMessage
			$code     = $error->getElementsByTagName('ErrorCode');
			$shortMsg = $error->getElementsByTagName('ShortMessage');
			$longMsg  = $error->getElementsByTagName('LongMessage');
			//Display code and shortmessage
			$log .= '<P>'.$code->item(0)->nodeValue.' : '.str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg->item(0)->nodeValue));
			//if there is a long message (ie ErrorLevel=1), display it
			if(count($longMsg) > 0) {
				$log .= '<BR>'.str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg->item(0)->nodeValue));
			}
			}
			

		} 
		

		$acks = $responseDoc->getElementsByTagName("Ack");
		$ack   = $acks->item(0)->nodeValue;
		
		//echo $errors->length == 0;
		
		if(($errors->length == 0) || ($ack == "Warning"))
		{ //no errors
		
			//echo "fail!";
		
			//get results nodes
			$response = $responseDoc;
//			foreach ($responses as $response) {
	
				if ($ack == "Warning")
				{
					$log .= '<p style="color:red">Keine Panik! Diese Mitteilung ist legendlich eine Warnung, Ihre Angebot hat trotzdem erfolgreich den Marktplatz ereicht.<br /></p>';	
				}
		
				$log .= '<p><strong>eBay gab folgenden Angaben zum Angebot zurück:</strong><br /></p>';//"Ack = $ack <BR />\n";   // Success if successful

				$endTimes  = $response->getElementsByTagName("EndTime");
				$endTime   = $endTimes->item(0)->nodeValue;
				$log .= "<p>Auktions Ende: ".str_replace("T", " ", $endTime)." <BR />\n";

				$itemIDs  = $response->getElementsByTagName("ItemID");
				$itemID   = $itemIDs->item(0)->nodeValue;
				$log .=  "Auktions ID: $itemID <BR /></p>\n";

				$linkBase = "http://cgi.sandbox.ebay.com/ws/eBayISAPI.dll?ViewItem&item=";

				$feeNodes = $responseDoc->getElementsByTagName('Fee');
				
				$lf = 0;
				
				foreach($feeNodes as $feeNode) {
					$feeNames = $feeNode->getElementsByTagName("Name");
					if ($feeNames->item(0)) {
						$feeName = $feeNames->item(0)->nodeValue;
						$fees = $feeNode->getElementsByTagName('Fee');  // get Fee amount nested in Fee
						$fee = $fees->item(0)->nodeValue;
						if ($fee > 0.0) {
							if ($feeName == 'ListingFee') {
								$log .= "<b>Gebühren: ".sprintf("%01.2f", $fee)."</b> <br />\n";
								
								$lf = $fee;
								
							} else {
								$log .= "$feeName: ".sprintf("%01.2f", $fee)." <br />\n";
							}
						}
					}
				}
				
				$log .= "<br />Ihre Auktion wurde erfolgreich bei eBay eingestellt!<br /></p>";
				
				$sql = "INSERT INTO `oebc_offers` (eid, oid, fees, stock, sold, last) VALUES ($itemID, '".$db->clean($_REQUEST["artnr"])."', $lf, '".$db->clean($_REQUEST["Quantity"])."', 0, 0)";
				$db->query_exec($sql);
				
				for($i = 0; $i != $_REQUEST["v"];$i++) {
						
					if($_REQUEST["i1"+$i] > 0) {
						$sql = "INSERT INTO `oebc_offers` (eid, oid, fees, stock, sold, last) VALUES ($itemID, '".$db->clean($_REQUEST["ia"+$i])."', $lf, '".$db->clean($_REQUEST["iq"+$i])."', 0, 0)";
						//$sql = "UPDATE `oxarticles` SET `OXSTOCK` =  '$stock' WHERE `OXID` = '".$db->clean($_REQUEST["ia"+$i])."' LIMIT 1";
						$db->query_exec($sql);	
					}
						
		
				}
				
				if($files->getOpt("bestand") == 1) 
				{
					$v = $_REQUEST["v"];
					$ia = array();
					$iq = array();
					
					for($i = 1; $i <= $_REQUEST["v"];$i++) {
						array_push($ia, $_REQUEST["ia$i"]);
						array_push($iq, $_REQUEST["iq$i"]);	
					}
					
					$ox->correctStockMv();
					
				}
								
			//}
		}
		
		$log .= '<p><br /><a href="/?mode=adm#tabs-2">Zurück zum Admin</a></p>';
	
	
		$smarty->assign("log", $log);

		//Content erzeugen und bereitstellen
		$content = $smarty->fetch('frontend/pages/result.tpl');
		$template->assign("content",$content);
		
		// Ausgabe
		if(!$template->titel){
			$template->titel = $default_titel;
		}

		$template->publisher = $publisher;

		$template->display('frontend/common/index.tpl');
		//$responses = $responseDoc->getElementsByTagName("GetSellerListResponse");
		
		//header("Location: http://". $_SERVER['SERVER_NAME']."/index.php?mode=adm#tabs-2");
		exit(0);
		
	}
	
	if($_REQUEST["artid"])
	{
		$ct = new eBayGetItem;
		
		$ct->_itemid = $_REQUEST["artid"];
		
		$repsonse = $ct->callEbay();
		
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($repsonse);
		
		//echo $repsonse;
		//exit(0);
		
	
	} else {
		
		echo "No EID given!";
		exit(0);	
		
	}
	
	//get any error nodes
	$errors = $responseDoc->getElementsByTagName('Errors');
	/*if($errors->length != 0) {
		var_dump($errors);
		die();
	} */

	//if there are error nodes
	
	if($errors->length == 0) {
		
		$art["OXID"] 		= $_REQUEST["artid"];
		$art["OXTITLE"] 	= $responseDoc->getElementsByTagName("Title")->item(0)->nodeValue;
		$smarty->assign("SubTitle",$responseDoc->getElementsByTagName("SubTitle")->item(0)->nodeValue);
		$art["OXLONGDESC"] 	= $responseDoc->getElementsByTagName("Description")->item(0)->nodeValue;
		
		$smarty->assign("cat", $responseDoc->getElementsByTagName("PrimaryCategory")->item(0)->getElementsByTagName("CategoryName")->item(0)->nodeValue);//PrimaryCategory
		$smarty->assign("catid", $responseDoc->getElementsByTagName("PrimaryCategory")->item(0)->getElementsByTagName("CategoryID")->item(0)->nodeValue); 
	
		if($responseDoc->getElementsByTagName("SecondaryCategory")->item(0)) {
			$smarty->assign("cat2", $responseDoc->getElementsByTagName("SecondaryCategory")->item(0)->getElementsByTagName("CategoryName")->item(0)->nodeValue);
			$smarty->assign("catid2", $responseDoc->getElementsByTagName("SecondaryCategory")->item(0)->getElementsByTagName("CategoryID")->item(0)->nodeValue);
		}
		/*$art["starttime"] 	= $responseDoc->getElementsByTagName("Title")->item(0)->nodeValue;
		$art["starth"] 	= $responseDoc->getElementsByTagName("Title")->item(0)->nodeValue;
		$art["startm"] 	= $responseDoc->getElementsByTagName("Title")->item(0)->nodeValue;*/
		
		/* disable -> Fixed */
		
		$template->assign("Site", $responseDoc->getElementsByTagName("Site")->item(0)->nodeValue);
		$template->assign("ListingDuration", $responseDoc->getElementsByTagName("ListingDuration")->item(0)->nodeValue);
		$template->assign("ConditionID", $responseDoc->getElementsByTagName("ConditionID")->item(0)->nodeValue);
		
		// single only
		$template->assign("Quantity", $responseDoc->getElementsByTagName("Quantity")->item(0)->nodeValue);
		
		// Hier muss noch was gemacht werden:
		//$art["ListingType"] 	= $responseDoc->getElementsByTagName("Quantity")->item(0)->nodeValue;
		
		if($responseDoc->getElementsByTagName("ListingType")->item(0)->nodeValue == "FixedPriceItem") { 
			$art["StartPrice"] 			= "1.00"; //$responseDoc->getElementsByTagName("StartPrice")->item(0)->nodeValue;
			$art["OXPRICE"] 			= $responseDoc->getElementsByTagName("StartPrice")->item(0)->nodeValue;
		} else {
			$art["StartPrice"] 			= $responseDoc->getElementsByTagName("StartPrice")->item(0)->nodeValue;
			$art["OXPRICE"] 			= $responseDoc->getElementsByTagName("BuyItNowPrice")->item(0)->nodeValue;
		} 
			
		/*
		    <PaymentMethods>PayPal</PaymentMethods>
    		<PaymentMethods>MoneyXferAcceptedInCheckout</PaymentMethods>
    		<PaymentMethods>CashOnPickup</PaymentMethods>
    		<PaymentMethods>PaymentSeeDescription</PaymentMethods>
			
			$smarty->assign("s6", $files->getOpt("p_pp"));
			$smarty->assign("s7", $files->getOpt("p_ueb"));	
			$smarty->assign("s8", $files->getOpt("p_nn"));	
			$smarty->assign("s9", $files->getOpt("p_no"));
			$smarty->assign("s10", $files->getOpt("p_ot"));
		*/	
		$payMeth = $responseDoc->getElementsByTagName("PaymentMethods");	
		
		foreach($payMeth as $pm) {
	
			if($pm->nodeValue == "PayPal") 
			{
				$smarty->assign("s6",1);
			}
			if($pm->nodeValue == "MoneyXferAcceptedInCheckout") 
			{
				$smarty->assign("s7",1);
			}
			if($pm->nodeValue == "CashOnPickup") 
			{
				$smarty->assign("s8",1);
			}
			if($pm->nodeValue == "COD") 
			{
				$smarty->assign("s9",1);
			}
			if($pm->nodeValue == "PaymentSeeDescription") 
			{
				$smarty->assign("s10",1);
			}
				
		}
		
		$smarty->assign("pp_email", $responseDoc->getElementsByTagName("PayPalEmailAddress")->item(0)->nodeValue);
		$smarty->assign("freeShipping", $responseDoc->getElementsByTagName("ShippingServiceOptions")->item(0)->getElementsByTagName("FreeShipping")->item(0)->nodeValue); //ShippingServiceOptions
		$template->assign("Currency", $responseDoc->getElementsByTagName("Currency")->item(0)->nodeValue);
		$smarty->assign("shipping", $responseDoc->getElementsByTagName("ShippingServiceOptions")->item(0)->getElementsByTagName("ShippingServiceCost")->item(0)->nodeValue);
		$smarty->assign("shipping2", $responseDoc->getElementsByTagName("ShippingServiceOptions")->item(0)->getElementsByTagName("ShippingServiceAdditionalCost")->item(0)->nodeValue);
		$template->assign("delivery", $responseDoc->getElementsByTagName("ShippingServiceOptions")->item(0)->getElementsByTagName("ShippingService")->item(0)->nodeValue);
		
		if($responseDoc->getElementsByTagName("StoreCategoryID")) {
			$template->assign("shopcat",$responseDoc->getElementsByTagName("StoreCategoryID")->item(0)->nodeValue);
		}
		
		if($responseDoc->getElementsByTagName("StoreCategoryID2")) {
			$template->assign("shopcat2",$responseDoc->getElementsByTagName("StoreCategoryID2")->item(0)->nodeValue);
		}
		
		//Gallery Type -> Fixed
		//Pictures -> Fixed
		
		$vars = $responseDoc->getElementsByTagName("Variation");
			
		if($vars) {
			$smarty->assign("mv", 1);
			$smarty->assign("revise", 1);
			
			$children = array();
					
			$i = 0;		
			foreach($vars as $vc) {
				
				$children[$i]["OXPRICE"] 		= $vc->getElementsByTagName("StoreCategoryID2")->item(0)->nodeValue; 
				$children[$i]["OXSTOCK"] 		= $vc->getElementsByTagName("StoreCategoryID2")->item(0)->nodeValue;
				
				// vehicle for name/value
				//$children[$i]["OXID"] 			= $vc->getElementsByTagName("StoreCategoryID2")->item(0)->nodeValue;
				
				// vehicle for name/value
				
				$aa = array();
				
				$i2 = 0;
				
				$vn = $vc->getElementsByTagName("NameValueList");
				
				$st = ""; // Varianten
				
				foreach($vn as $vns) {
					if($i2 != 0) {
						$st .= " | ";	
					}
					
					//Build VARSELECT
					 $st	.= 	$vns->getElementsByTagName("Value")->item(0)->nodeValue;
					 $i2++;
				}
				$children[$i]["OXVARSELECT"] = $st;
				
				$q 		= $vc->getElementsByTagName("Quantity")->item(0)->nodeValue;
				$qs 	= $vc->getElementsByTagName("QuantitySold")->item(0)->nodeValue;
				
				$children[$i]["OXSTOCK"] = $q - $qs; // Anzahl - Verkauft
				$children[$i]["OXPRICE"] = $vc->getElementsByTagName("StartPrice")->item(0)->nodeValue;
				$i++;
			}
			
			$smarty->assign("children", $children);
			
		}		
			
	} else {
	
		$log = "<p><b>Fehler</b><br/>Problem beid er Kommunikation mit eBay<br /></p>";	
	
		$smarty->assign("log", $log);

		//Content erzeugen und bereitstellen
		$content = $smarty->fetch('frontend/pages/result.tpl');
		$template->assign("content",$content);
		
		// Ausgabe
		if(!$template->titel){
			$template->titel = $default_titel;
		}

		$template->publisher = $publisher;

		$template->display('frontend/common/framed.tpl');
		
		exit(0);
		
	}
	
	/*********************************************/
	
	/*$ox = new oxid($db);
	
	if($_REQUEST["mv"] == "1") 
	{
		$smarty->assign("mv", "1");
		
		$children = $ox->getArticleChildren($_REQUEST["artnr"]);
		
		$smarty->assign("children", $children);
			
	}*/
	
	$ct = new eBayGetCategoriesTree;
	
	$ct->_vonly = 1;
	
	$ct->_site = $files->getOpt("land");
	
	$cat_xml = $ct->callEbay();
	
	$responseDoc = new DomDocument();
	$responseDoc->loadXML($cat_xml);
	
	//get any error nodes
	$errors = $responseDoc->getElementsByTagName('Errors');

	//if there are error nodes
	if($errors->length == 0) {
			
			$version = $responseDoc->getElementsByTagName("CategoryVersion")->item(0)->nodeValue;
			
			if($version != $files->getOpt("cat_version"))
			{
				$smarty->assign("renew", 1);
				//echo $version;
			}
	}
	
	$sql = "SELECT * FROM `oebc_shopcats`";
	$cats = $db->query_array($sql);
	
	$smarty->assign("shopcats", $cats);
		
	$smarty->assign("article", $art);

	$smarty->assign("bestand", $files->getOpt("bestand"));
	
	$smarty->assign("mymode", "revise");

	$content = $smarty->fetch('frontend/pages/additems.tpl');
		
	$template->assign("content",$content);

?>
