{literal}
<script language="javascript" src="/javascript/jquery.validate.min.js" type="text/javascript" ></script>
<script language="javascript" src="/javascript/jquery.hint.js" type="text/javascript" ></script>
<script language="javascript" src="/javascript/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/javascript/tiny_mce/jquery.tinymce.js"></script>

<script language="javascript">
$(function(){
	$('#starttime').datepicker({
							  userLang	: 'de',
							  dateFormat: 'yy-mm-dd',
							  minDate: 0
	});
	
	$("#accordion").accordion();

	$("#additem").submit(function() {
		if($("#CategoryID").val()) {
			return true;
		} 
		alert("Bitte wählen Sie eine Kategorie!");
		return false;
	});

	$('#Description').tinymce({
			// Location of TinyMCE script
			script_url : '/javascript/tiny_mce/tiny_mce.js',
			theme : "advanced",
			language : 'de',
			relative_urls : false,
			plugins : "safari,pagebreak,style,layer,table,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,searchreplace,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras",
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 :"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_buttons3 :
			"bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,help,code,|,forecolor,backcolor,|,insertdate,inserttime,preview,|,image",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js"

		});
		
	$('.word_count').each(function(){  
	 if(length > 55) {  
	 	$(this).parent().find('.counter').css("color","red");
	 } else {
		 $(this).parent().find('.counter').css("color","gray");
	 }
     var length = $(this).val().length;  
     $(this).parent().find('.counter').html( length + " Buchstaben" );  
     $(this).keyup(function(){  
         var new_length = $(this).val().length;
		 if(new_length > 55) {  
		 	$(this).parent().find('.counter').css("color","red");
		 } else {
		 	$(this).parent().find('.counter').css("color","gray");
	 	 }
		 $(this).parent().find('.counter').html( new_length + " Buchstaben" );
		 
     });  
 	}); 
	
	$("#GalleryType").attr("disabled", "disabled");
	$("#PictureC").attr("disabled", "disabled");
	$("#elm").attr("disabled", "disabled");
	$("#FixedPriceListing").attr("disabled", "disabled");
	$("#StartPrice").attr("disabled", "disabled"); //conditional
	
	$("#starttime").attr("disabled", "disabled");
	$("#starth").attr("disabled", "disabled");
	$("#startm").attr("disabled", "disabled");
	
	{/literal}
	$("#Site").val("{$Site}");
	$("#ListingDuration").val("{$ListingDuration}");
	$("#ConditionID").val("{$ConditionID}");
	
	$("#Currency").val("{$Currency}");
	$("#delivery").val("{$delivery}");
	
	$("#shopcat").val("{$shopcat}");
	$("#shopcat2").val("{$shopcat2}");
	{literal}
	
});
  
var categoriesshow = 0;

function catselect() {
	if(($("#catterm").val()).length > 0) {
			if(categoriesshow == 0) {
				$("#catsearch").show();
				$("#catsearch").html('<center>Lade Daten...<br /><img src="/images/system/load.gif"></center>');
				categoriesshow = 1;
			}
   			$.post("/components/ajax.php", {	
					'mode'	   			: 	'categories',					
					'searchterm'	   	: 	$("#catterm").val(),
					'v'					: 	$("#v").val()
			},
			function(resdata){
				$("#catsearch").html(resdata);
 			});	
	} else {
		categoriesshow = 0;
		$("#catsearch").hide();
	}
}

var categoriesshow2 = 0;

function catselect2() {
	if(($("#catterm2").val()).length > 0) {
			if(categoriesshow2 == 0) {
				$("#catsearch2").show();
				$("#catsearch2").html('<center>Lade Daten...<br /><img src="/images/system/load.gif"></center>');
				categoriesshow2 = 1;
			}
   			$.post("/components/ajax.php", {	
					'mode'	   			: 	'categories',					
					'searchterm'	   	: 	$("#catterm2").val(),
					'c2'				:   '1',
					'v'					: 	$("#v").val()
			},
			function(resdata){
				$("#catsearch2").html(resdata);
 			});	
	} else {
		categoriesshow = 0;
		$("#catsearch2").hide();
	}
}

function setCat(name, id) {
	$("#CategoryID").val(id);
	$("#catterm").val(name);
	$("#catsearch").hide();
	categoriesshow = 0;
}  
function setCat1(name, id) {
	$("#CategoryID2").val(id);
	$("#catterm2").val(name);
	$("#catsearch2").hide();
	categoriesshow2 = 0;
}  

function previewItem() {
	$("#mode").attr('value', 'preview');
	document.forms.additemForm.setAttribute("target", "_blank");
	$("#additemForm").submit();
	return true;
}
function submitItem() {
	$("#mode").attr('value', 'additems');
	document.additemForm.setAttribute("target", "_self");
	
	if($("#CategoryID").val() == "") {
		alert("Bitte wählen Sie eine Kategorie!");
		return true;			
	} else {	
	$('.word_count').each(function(){
		if($(this).val().length < 56)
		{
				$("#additemForm").submit();
				return true;
		} else {
			alert("Titel oder Untertitel zu lang!");
			return true;
		}
	});
	}
}
</script>

{/literal}