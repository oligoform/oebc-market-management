<table id="auctionstab" width="100%" border="0" cellpadding="0" cellspacing="0"  >
<thead> 
  <tr>
    <th style="width:120px"><strong>Artikelnummer</strong></th>
    <th style="width:20px">&nbsp;</th>
    <th style="width:200px"><strong>Name</strong></th>
    <th style="width:50px"><strong>Dauer</strong></th>
    <th style="width:160px"><strong>Noch verbleibend</strong></th>
    <th style="width:50px"><strong>Anzahl</strong></th>
    <th style="width:50px"><strong>Gebot</strong></th>
    <th style="width:50px"><strong>Sofort</strong></th>
    <th style="width:50px"><strong>Format</strong></th>
    <th style="width:20px">&nbsp;</th>
  </tr>
</thead> 
<tbody>
{section name=items loop=$items}
<tr>
    <td  width="102" align="center"><a href="{$items[items].itemUrl}" target="_blank">{$items[items].itemId}</a></td>  
  	<td  width="20">{if $items[items].itemId|in_array:$bysystem}{else}<img src="/images/icons/link.png" width="16" height="16">{/if}</td>
    <td  >{$items[items].title}</td>
    <td  width="60">{if $items[items].listingTime}{$items[items].listingTime|replace:"Days_":""} Tage{else}-{/if}
    </td>
    <td  >{$items[items].timeLeft|replace:"T":""|replace:"S":" Sek."|replace:"D":" Tage "|replace:"H":" Std. "|replace:"M":" Min. "|replace:"P":""}</td>    
    <td  width="50">{$items[items].quanity}</td>
    <td  >{if $items[items].listingType == "Chinese"}{"%01.2f"|sprintf:$items[items].currentPrice}{/if}</td>
    <td  >{"%01.2f"|sprintf:$items[items].buyItNowPrice}</td>
    <td  width="35" >{if $items[items].listingType == "Chinese"}Auktion{/if}{if $items[items].listingType == "FixedPriceItem"}Fix{/if}{if $items[items].listingType == "StoresFixedPrice"}Shop{/if}</td>
    <td  width="20"><a href="/?mode=enditem&id={$items[items].itemId}"><img src="/images/icons/cross.png" width="16" height="16"></a></td>
 </tr>      
{/section}
</tbody>  
</table>
<p><center><a href="javascript:nextAuctions(1);">&lt;&lt;</a> <a href="javascript:nextAuctions({$page-1});">&lt;</a> ({$page}/{$pages}) <a href="javascript:nextAuctions({$page+1});">&gt;</a> <a href="javascript:nextAuctions({$pages});">&gt;&gt;</a></center></p>
<p><center>Insgesamt {$nitems} aktive Angebote</center></p>
