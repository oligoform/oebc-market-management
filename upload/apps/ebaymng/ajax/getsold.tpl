<table  width="100%" id="soldtab" border="0" cellpadding="0" cellspacing="0" >
<thead>
  <tr>
    <th style="width:20px">&nbsp;</th>
    <th style="width:120px"><strong>Artikelnummer</strong></th>
    <th style="width:120px"><strong>Transaktion</strong></th>
    <th style="width:180px"><strong>Name</strong></th>
    <!--<th><strong>Auktion Start</strong></th>-->
    <th style="width:180px"><strong>Käufer</strong></th>
    <th style="width:50px"><strong>Anzahl</strong></th>
    <th style="width:50px"><strong>VP</strong></th>
    <th style="width:50px"><strong>Format</strong></th>
  </tr>
 </thead><tbody> 
{section name=items4 loop=$items4}
  <tr style="border:#F00 1px solid">
    <td width="20" align="center">{if $items4[items4].SellerPaidStatus == "NotPaid"}<img src="/images/icons/coins.png" width="16" height="16">{/if}</td>
    <td width="90"><a href="{$items4[items4].itemUrl}" target="_blank">{$items4[items4].itemId}</a></td>  
    <td >{$items4[items4].TransactionID}</td>
    <td >{$items4[items4].title}</td>
    
    <!--<td >{$items4[items4].startTime}</td>-->
    <td >{$items4[items4].UserID}<br />(<a href="mailto:{$items4[items4].Email}">{$items4[items4].Email}</a>)</td>    
    <td width="50">{$items4[items4].quanity}</td>
    <td >{"%01.2f"|sprintf:$items4[items4].Price}</td>
    <td width=50>{if $items4[items4].listingType == "Chinese"}Auktion{/if}{if $items4[items4].listingType == "FixedPriceItem"}Fix Preis{/if}</td>
  </tr>
{/section}  
</tbody>
</table>
<p><center>&lt;&lt; &lt; ({$page}/{$pages}) &gt; &gt;&gt;</center></p>
<p><center>Insgesamt {$nitems4} verkaufte Artikel</center></p>
