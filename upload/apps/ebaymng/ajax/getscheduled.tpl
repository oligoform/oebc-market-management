<table  width="100%" id="scheduledtab" border="0" cellpadding="0" cellspacing="0" >
  <thead>
  <tr>
    <th style="width:120px"><strong>Artikelnummer</strong></th>
    <th style="width:250px"><strong>Name</strong></th>
    <!--<th ><strong>Auktion Start</strong></th>-->
    <th ><strong>Dauer</strong></th>
    <th ><strong>Start Zeit</strong></th>
    <th style="width:20px"><strong>Anzahl</strong></th>
    <th style="width:50px"><strong>Startpreis</strong></th>
    <th style="width:50px"><strong>Sofortkauf</strong></th>
    <th style="width:50px"><strong>Format</strong></th>
  </tr>
  </thead>
<tbody>  
{section name=items3 loop=$items3}
  <tr style="border:#F00 1px solid">
    <td  width="110" align="center"><a href="{$items3[items3].itemUrl}" target="_blank">{$items3[items3].itemId}</a></td>  
    <td  >{$items3[items3].title}</td>
    <!--<td  >{$items3[items3].startTime}</td>-->
    <td  width="60">{$items3[items3].listingTime|replace:"Days_":""} Tage</td>
    <td  >{$items3[items3].startTime|replace:"T":"  "|replace:".000Z":""}</td>    
    <td  width="50">{$items3[items3].quanity}</td>
    <td  >{if $items3[items3].listingType == "Chinese"}{"%01.2f"|sprintf:$items3[items3].currentPrice}{else}-{/if}</td>
    <td  >{"%01.2f"|sprintf:$items3[items3].buyItNowPrice}</td>
    <td  >{if $items3[items3].listingType == "Chinese"}Auktion{/if}{if $items3[items3].listingType == "FixedPriceItem"}Fix Preis{/if}</td>
  </tr>
{/section} 
</tbody> 
</table>
<p><center>&lt;&lt; &lt; ({$page}/{$pages}) &gt; &gt;&gt;</center></p>
<p><center>Insgesamt {$nitems3} schedulte Artikel</center></p>
