<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/



	//smarty class laden und die template pfaede festlegen
	$template = new smarty;

	$template->template_dir = dirname(__FILE__);
	$template->compile_dir  = dirname(__FILE__).'/../../../tmp/';

	$searchterm = $db->clean($_REQUEST["searchterm"]);

	$results = array();
	
	if($_REQUEST["v"]) {
	$sql = "SELECT * FROM oebc_categories WHERE name LIKE '%$searchterm%' 
											AND leaf = 1 
											AND virtual = 0
											AND expired = 0
											AND variants = 1
											LIMIT 0, 50";
	} else {
	$sql = "SELECT * FROM oebc_categories WHERE name LIKE '%$searchterm%' 
											AND leaf = 1 
											AND virtual = 0
											AND expired = 0
											LIMIT 0, 50";
	}
	
	//echo $sql;
	
	$res = $db->query_array($sql);
	
	$sql = "SELECT name, parent, cid FROM oebc_categories";
	
	//echo $sql;
	
	$parentsRaw = $db->query_array($sql);
	
	$parents = array();
	
	foreach($parentsRaw as $parent) {
		$parents[$parent["cid"]] = array("name" => $parent["name"],"parent" => $parent["parent"]); 
		//echo " ".$parent["cid"];
	}
	//echo "Memory used: ".number_format(memory_get_usage())." bytes";
	
	//$template->assign("parents", $parents);

	$template->assign("parents", $parents);
	
	//echo "Cat:".$parents[20068]["name"];
	
	foreach($res as $cat) {
		$cat_array = array();
		$cat_array["name"] 	= $cat["name"];
		$cat_array["parent"] 	= $cat["parent"];
		$cat_array["cid"] 	= $cat["cid"];
		$results[] = $cat_array;

	}


	$template->assign("categories", $results);
	
	if($_REQUEST["c2"]) {
		$template->assign("c2", 1);
	}
	
	$template->display('categories.tpl'); 

?>