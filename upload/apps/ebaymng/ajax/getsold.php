<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	//smarty class laden und die template pfaede festlegen
	$template = new smarty;

	$template->template_dir = dirname(__FILE__);
	$template->compile_dir  = dirname(__FILE__).'/../../../tmp/';
	
	require(dirname(__FILE__)."/../classes/eBay.inc.php");
	require(dirname(__FILE__)."/../classes/eBay.class.php");	
	
	/* INIT */
	
	$sql = "SELECT eid FROM oebc_offers ORDER BY eid";
	$result = $db->query_array_append($sql);
	$template->assign("bysystem", $result); 

	if($_REQUEST["page"] == "") {
		$page = 1;	
	}

	/*********/


	/**** SOLD LIST ****/
	
	$sl = new eBayGetMyeBaySelling();
	$sl->_page = $page;
		
	$sl->_scall = "SoldList";
		
	$eres = $sl->callEbay();
		
	$responseDoc = new DomDocument();
	$responseDoc->loadXML($eres);
	
	$responses = $responseDoc->getElementsByTagName("GetMyeBaySellingResponse");
	
	$a_items = array();
	
	foreach ($responses as $response) 
	{
		$acks = $response->getElementsByTagName("Ack");
		$ack   = $acks->item(0)->nodeValue;
		//echo "Ack = $ack <BR />\n";   // Success if successful

		$totalNumberOfEntries  = $response->getElementsByTagName("TotalNumberOfEntries");
		$totalNumberOfEntries  = $totalNumberOfEntries->item(0)->nodeValue;
		
		$template->assign("nitems4", $totalNumberOfEntries);	

		$items  = $response->getElementsByTagName("OrderTransaction");

		for($i=0; $i<$items->length; $i++) 
		{
			
			$a_items[$i]["itemId"] 			= $items->item($i)->getElementsByTagName('ItemID')->item(0)->nodeValue;
			$a_items[$i]["itemUrl"] 		= $items->item($i)->getElementsByTagName('ViewItemURL')->item(0)->nodeValue;
			$a_items[$i]["TransactionID"] 		= $items->item($i)->getElementsByTagName('TransactionID')->item(0)->nodeValue;
			$a_items[$i]["listingTime"]		= $items->item($i)->getElementsByTagName('ListingDuration')->item(0)->nodeValue;
			$a_items[$i]["UserID"]		= $items->item($i)->getElementsByTagName('UserID')->item(0)->nodeValue;
			$a_items[$i]["Email"] 		= $items->item($i)->getElementsByTagName('Email')->item(0)->nodeValue;
			
			$a_items[$i]["status"] 			= $items->item($i)->getElementsByTagName('ListingStatus')->item(0)->nodeValue;
			$a_items[$i]["title"] 			= $items->item($i)->getElementsByTagName('Title')->item(0)->nodeValue;
			$a_items[$i]["quanity"] 		= $items->item($i)->getElementsByTagName('QuantityPurchased')->item(0)->nodeValue;
			
			$a_items[$i]["SellerPaidStatus"] 		= $items->item($i)->getElementsByTagName('SellerPaidStatus')->item(0)->nodeValue;	
			$a_items[$i]["Price"] 		= $items->item($i)->getElementsByTagName('CurrentPrice')->item(0)->nodeValue;
			$a_items[$i]["listingType"] 		= $items->item($i)->getElementsByTagName('ListingType')->item(0)->nodeValue;		 		
		}
	}
	
		
	$template->assign("items4", $a_items);	

	/*********/
	
	
	if($items->length > 1) {
		$pages = ceil($totalNumberOfEntries / $items->length);
	} else {
		$pages = 1;	
	}
	$template->assign("pages", $pages);
	$template->assign("page", $page);	
		
	$template->display('getsold.tpl'); 

?>