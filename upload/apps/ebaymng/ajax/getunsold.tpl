<table  width="100%" id="unsoldtab" border="0" cellpadding="0" cellspacing="0" >
  <thead>
  <tr>
    <th style="width:120px"><strong>Artikelnummer</strong></th>
    <th>&nbsp;</th>
    <th ><strong>Name</strong></th>
    <th style="width:50px"><strong>Dauer</strong></th>
<!--<th><strong>Auktion Start</strong></th>
    <th><strong>Dauer</strong></th>
    <th><strong>Noch verbleibend</strong></th>-->
    <th style="width:50px"><strong>Anzahl</strong></th>
    <th style="width:50px"><strong>Gebot</strong></th>
    <th style="width:50px"><strong>Sofortkauf</strong></th>
    <th style="width:50px"><strong>Format</strong></th>
    <!--<th>&nbsp;</th>-->
    <th style="width:20px">&nbsp;</th>
  </tr>
  </thead>
  <tbody>
{section name=items2 loop=$items2}
  <tr>
    <td width="110" align="center"><a href="{$items2[items2].itemUrl}" target="_blank">{if $items2[items2].Relisted}<span style="color:#ccc;">{/if}{$items2[items2].itemId}{if $items2[items2].Relisted}</span>{/if}</a></td>  
    <td width="20">{if $items2[items2].itemId|in_array:$bysystem}{else}<img src="/images/icons/link.png" width="16" height="16">{/if}{if $items2[items2].Relisted}<a href='#'><img src="/images/icons/arrow_undo.png" width="16" height="16"></a>{/if}</td>
    <td width="510">{if $items2[items2].Relisted}<span style="color:#ccc;">{/if}{$items2[items2].title}{if $items2[items2].Relisted}</span>{/if}</td>
        <td width="60">{if $items2[items2].Relisted}<span style="color:#ccc;">{/if}{if $items[items].listingTime}{$items[items].listingTime|replace:"Days_":""} Tage{else}-{/if}{if $items2[items2].Relisted}</span>{/if}
    </td>
    <!--<td >{$items2[items2].startTime}</td>
    <td width="60">{$items2[items2].listingTime|replace:"Days_":""} Tage</td>
    <td >{$items2[items2].timeLeft|replace:"T":""|replace:"S":" Sek."|replace:"D":" Tage "|replace:"H":" Std. "|replace:"M":" Min. "|replace:"P":""}</td>    -->
    <td width="50">{if $items2[items2].Relisted}<span style="color:#ccc;">{/if}{$items2[items2].quanity}{if $items2[items2].Relisted}</span>{/if}</td>
    <td >{if $items2[items2].Relisted}<span style="color:#ccc;">{/if}{if $items2[items2].listingType == "Chinese"}{"%01.2f"|sprintf:$items2[items2].currentPrice}{/if}{if $items2[items2].Relisted}</span>{/if}</td>
    <td >{if $items2[items2].Relisted}<span style="color:#ccc;">{/if}{"%01.2f"|sprintf:$items2[items2].buyItNowPrice}{if $items2[items2].Relisted}</span>{/if}</td>
    <td >{if $items2[items2].Relisted}<span style="color:#ccc;">{/if}{if $items2[items2].listingType == "Chinese"}Auktion{/if}{if $items2[items2].listingType == "FixedPriceItem"}Fix Preis{/if}{if $items2[items2].listingType == "StoresFixedPrice"}Shop{/if}{if $items2[items2].Relisted}</span>{/if}</td>
<!--    <td width="20">{if !$items2[items2].Relisted}{if $items2[items2].itemId|in_array:$bysystem}<a href='#'><img src="/images/icons/cross.png" width="16" height="16"></a>{/if}{/if}</td>-->
    <td width="20">{if !$items2[items2].Relisted}{if $items2[items2].itemId|in_array:$bysystem}<a href='#' onclick="parent.spawngeneric('/?app=ebaymng&subview=relist&id={$items2[items2].itemId}', '', 210, 720) "><img src="/images/icons/cart_go.png" width="16" height="16"></a>{/if}{/if}</td>
  </tr>
{/section} 
<tbody> 
</table>
<p><center>&lt;&lt; &lt; ({$page}/{$pages}) &gt; &gt;&gt;</center></p>
<p><center>Insgesamt {$nitems2} nicht verkaufte Artikel</center></p>