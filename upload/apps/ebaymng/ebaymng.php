<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	require dirname(__FILE__)."/classes/eBay.inc.php";
	require(dirname(__FILE__)."/classes/eBay.class.php");
	
	if($_REQUEST["action"] == "settings") {

		$files->setOpt("cur",$db->clean($_REQUEST["s4"]));
		$files->setOpt("ort",$db->clean($_REQUEST["s5"]));
		$files->setOpt("p_pp",$db->clean($_REQUEST["s6"]));
		$files->setOpt("p_ueb",$db->clean($_REQUEST["s7"]));
		$files->setOpt("p_nn",$db->clean($_REQUEST["s8"]));
		$files->setOpt("p_no",$db->clean($_REQUEST["s9"]));
		$files->setOpt("p_ot",$db->clean($_REQUEST["s10"]));
		$files->setOpt("template",$db->clean($_REQUEST["s11"]));
		$files->setOpt("land",$db->clean($_REQUEST["s12"]));		
			
		/* */

		$files->setOpt("DispatchTimeMax",$db->clean($_REQUEST["DispatchTimeMax"]));
		
		$files->setOpt("pp_email",$db->clean($_REQUEST["s14"]));	
		
		$files->setOpt("RefundOption",$db->clean($_REQUEST["s16"]));
		$files->setOpt("ReturnsWithinOption",$db->clean($_REQUEST["s17"]));
		$files->setOpt("ReturnDescription",$db->clean($_REQUEST["s18"]));
		$files->setOpt("ShippingCostPaidByOption",$db->clean($_REQUEST["s19"]));
		
		$files->setOpt("eland",$db->clean($_REQUEST["s20"]));	
		
		$files->setOpt("bestand",$db->clean($_REQUEST["bestand"]));	
		
		header("Location: http://". $_SERVER['SERVER_NAME']."/?app=ebaymng#settings");
		exit(0);	
	
	}
	
	
	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../../tmp/';
	$smarty->template_dir = dirname(__FILE__).'/../';

	$ox = new oxid($db);
		
	$arts = $ox->getArticles();
	
	$smarty->assign("oxitems", $arts);
		
	$smarty->assign("s4", $files->getOpt("cur"));
	$smarty->assign("s5", $files->getOpt("ort"));
	
	$smarty->assign("s6", $files->getOpt("p_pp"));
	$smarty->assign("s7", $files->getOpt("p_ueb"));	
	$smarty->assign("s8", $files->getOpt("p_nn"));	
	$smarty->assign("s9", $files->getOpt("p_no"));
	$smarty->assign("s10", $files->getOpt("p_ot"));
	
	$smarty->assign("s11", $files->getOpt("template"));
	
	$template->assign("s12", $files->getOpt("land"));

	$smarty->assign("s13", $files->getOpt("valid"));
	
	$smarty->assign("s14", $files->getOpt("pp_email"));
	
	$smarty->assign("esDes", $files->getOpt("esDes"));
	$smarty->assign("esLogo", $files->getOpt("esLogo"));	
	$smarty->assign("esHome", $files->getOpt("esHome"));	
	$smarty->assign("esName", $files->getOpt("esName"));
	
	//
	$template->assign("ReturnsWithinOption", $files->getOpt("ReturnsWithinOption"));
	$template->assign("RefundOption", $files->getOpt("RefundOption"));
	$smarty->assign("ReturnDescription", $files->getOpt("ReturnDescription"));
	$template->assign("ShippingCostPaidByOption", $files->getOpt("ShippingCostPaidByOption"));
	
	$template->assign("DispatchTimeMax", $files->getOpt("DispatchTimeMax"));
	
	$template->assign("s20", $files->getOpt("eland"));
	
	$template->assign("bestand", $files->getOpt("bestand"));
	$smarty->assign("bestand", $files->getOpt("bestand"));
	
	$content = $smarty->fetch('ebaymng/ebaymng.tpl');
	$template->assign("content",$content);

?>