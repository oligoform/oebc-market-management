<div class="tabs2" style="border:0; margin:0; min-height:450px;">
  <ul>
    <li><a href="#tabs-10">Auktionen</a></li>
    <li><a href="#tabs-20">Nicht verkauft</a></li>
    <li><a href="#tabs-30">Verkauft</a></li>
    <li><a href="#tabs-40">Scheduled</a></li>
    <li><a href="#tabs-50">Shop Artikel</a></li>
    <li><a href="#settings">Einstellungen</a></li>
  </ul>
  <div id="tabs-10">
    <div id="auctions" class="ebayoverflow">
      <center>
        Lade Daten...<br/>
        <img src="/images/system/load.gif" />
      </center>
    </div>
  </div>
  <div id="tabs-20">
    <!--<p>
    <center>
      <form method="post" action="index.php">
        Nur Artikel der letzten
        <select name="days" id="days">
          <option value="7">7 Tage</option>
          <option value="14" selected="selected">14 Tage</option>
          <option value="30">30 Tage</option>
          <option value="60">60 Tage</option>
          <option value="0">Alle</option>
        </select>
        zeigen
        <input type="submit" name="button" id="button" value="Jetzt anzeigen!" />
        <input type="hidden" value="adm" name="mode" />
      </form>
      <br />
    </center>
    </p>-->
    <div id="auctions_unsold" class="ebayoverflow">
      <center>
        Lade Daten...<br/>
        <img src="/images/system/load.gif" />
      </center>
    </div>
  </div>
  <div id="tabs-30">
   <!-- <p>
    <center>
      <form method="post" action="/index.php#tabs-3">
        Nur Artikel der letzten
        <select name="days" id="days">
          <option value="7">7 Tage</option>
          <option value="14" selected="selected">14 Tage</option>
          <option value="30">30 Tage</option>
          <option value="60">60 Tage</option>
          <option value="0">Alle</option>
        </select>
        zeigen
        <input type="submit" name="button" id="button" value="Jetzt anzeigen!" />
        <input type="hidden" value="adm" name="mode" />
      </form>
      <br />
    </center>
    </p>-->
    <div id="auctions_sold" class="ebayoverflow">
      <center>
        Lade Daten...<br/>
        <img src="/images/system/load.gif" />
      </center>
    </div>
  </div>
  <div id="tabs-40">
    <div id="auctions_scheduled" class="ebayoverflow">
      <center>
        Lade Daten...<br/>
        <img src="/images/system/load.gif" />
      </center>
    </div>
  </div>
  <div id="tabs-50">
      <table border="0" cellpadding="0" cellspacing="0" style="width:100%;" id="shoptab">
        <thead>
        <tr>
          <th style="width:150px;"><strong>Shop ArtNr.</strong></th>
          <th style="width:200px;"><strong>Name</strong></th>
          <th style="width:250px"><strong>Kurz Beschreibung</strong></th>
          <th style="width:25px;"><strong>Lager</strong></th>
          <th style="width:35px;"><strong>Preis</strong></th>
          <th style="width:25px;">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        {section name=items loop=$oxitems}
        {if $oxitems[items].hc == 0}
        <tr >
          <td><a href="#" onclick="javascript:parent.spawngeneric('{$shopurl}/index.php?cl=search&amp;searchparam={$oxitems[items].OXARTNUM}', '', 500, 920); return true;" target="_blank">{$oxitems[items].OXARTNUM}</a></td>
          <td>{$oxitems[items].OXTITLE}</td>
          <td>{$oxitems[items].OXSHORTDESC}</td>
         <!-- <td  ></td>-->
          <td alignn="center">{if ($oxitems[items].OXSTOCK <= 0) && ($bestand != 0)}<span style="color:#F00">{$oxitems[items].OXSTOCK}</span>{else}{$oxitems[items].OXSTOCK}{/if}</td>
          <td>{$oxitems[items].OXPRICE|number_format:2:",":"."}</td>
          <td>{if ($oxitems[items].OXSTOCK <= 0) && ($bestand != 0)}<a href='javascript:alert("Artikel nicht auf Lager, Verkauf nicht möglich!");'align="center">{else}<a href="#" onclick="javascript:parent.spawngeneric('/?app=ebaymng&subview=additems&artnr={$oxitems[items].OXID}&framed=1', '', 500, 920); return true;">{/if}<img src="/images/icons/cart.png" width="16" height="16"></a></td>
        </tr>
        {else}
        <tr >
          <td><a href="#" onclick="javascript:parent.spawngeneric('{$shopurl}/index.php?cl=search&amp;searchparam={$oxitems[items].OXID4}&frame=1', '', 500, 920); return true;" >{$oxitems[items].OXARTNUM}</a></td>
          <td>{$oxitems[items].OXTITLE}</td>
          <td></td>
          <!--<td  >{$oxitems[items].hc}</td>-->
          <td >
          {if ($oxitems[items].OXSTOCK <= 0) && ($bestand != 0)}<span style="color:#F00">{$oxitems[items].OXSTOCK}</span>{else}{$oxitems[items].OXSTOCK}{/if} / 
          {if ($oxitems[items].OXVARSTOCK <= 0) && ($bestand != 0)}<span style="color:#F00">{$oxitems[items].OXVARSTOCK}</span>{else}{$oxitems[items].OXVARSTOCK}{/if}     
          </td>
          <td>{$oxitems[items].OXPRICE|number_format:2:",":"."}</td>
          <td>{if ($oxitems[items].OXVARSTOCK <= 0) && ($bestand != 0)}<a href='javascript:alert("Artikel nicht auf Lager, Verkauf nicht möglich!");'>{else} <a href="#" onclick="javascript:parent.spawngeneric('/?app=ebaymng&subview=additems&artnr={$oxitems[items].OXID}&mv=1&framed=1', '', 500, 920); return true;">{/if}<img src="/images/icons/cart_add.png" width="16" height="16"></a></td>
        </tr>
        {section name=items2 loop=$oxitems[items].mc}
        <tr>
          <td><span style="color:#999">{$oxitems[items].OXARTNUM} /</span> <a href="#" onclick="javascript:parent.spawngeneric('{$shopurl}/index.php?cl=search&amp;searchparam={$oxitems[items].mc[items2].OXARTNUM}', '', 500, 920); return true;" target="_blank">{$oxitems[items].mc[items2].OXARTNUM}</a></td>
          <td>{$oxitems[items].OXTITLE} {$oxitems[items].mc[items2].OXVARSELECT}</td>
          <!--<td  ></td>-->
          <td></td>
          <td>{if ($oxitems[items].mc[items2].OXSTOCK <= 0) && ($bestand != 0)}<span style="color:#F00">{$oxitems[items].mc[items2].OXSTOCK}</span>{else}{$oxitems[items].mc[items2].OXSTOCK}{/if}</td>
          <td>{$oxitems[items].mc[items2].OXPRICE|number_format:2:",":"."}</td>
          <td>{if ($oxitems[items].mc[items2].OXSTOCK <= 0) && ($bestand != 0)}<a href='javascript:alert("Artikel nicht auf Lager, Verkauf nicht möglich!");'>{else}<a  href="#" onclick="javascript:parent.spawngeneric('/?app=ebaymng&subview=additems&artnr={$oxitems[items].mc[items2].OXID}&ms=1', '', 500, 920); return true;">{/if}<img src="/images/icons/cart.png" width="16" height="16"></a></td>
        </tr>
        {/section}  
        {/if}
        {/section}
        </tbody>
      </table>
  </div>
  <div id="settings">
    <div class="scrollable2">
      <form action="index.php" method="post">
        <input type="hidden" name="app" value="ebaymng" />
        <input type="hidden" name="action" value="settings" />
        <h4><a href="#">eBay Template</a></h4>
        <div>
          <table width="820" border="0" cellpadding="0" cellspacing="10">
            <tr>
              <td align="left" valign="top"><strong>Template Code</strong></td>
              <td><textarea name="s11" rows="20" id="s11" style="width:710px">{$s11}</textarea></td>
            </tr>
          </table>
        </div>
        <h4><a href="#">eBay Auktions und Plattform Einstellungen</a></h4>
        <div>
          <table width="820" border="0" cellpadding="0" cellspacing="10">
            <tr>
              <td align="left" valign="top"><strong>Währung für Auktionen</strong></td>
              <td><select name="select2" id="select2">
                  <option value="EUR" {if $s4 == "EUR"}selected="selected"{/if}>EUR</option>
                  <option value="USD" {if $s4 == "USD"}selected="selected"{/if}>USD</option>
                </select></td>
            </tr>
            <tr>
              <td align="left" valign="top"><strong>Standort</strong></td>
              <td><input name="s5" type="text" id="s5" style="width:630px" value="{$s5}" /></td>
            </tr>
            <tr>
              <td align="left" valign="top"><strong>Standort (Land)</strong></td>
              <td><select name="s20" id="s20">
                  <option value="DE">Deutschland</option>
                  <option value="AT">Österreich</option>
                  <option value="CH">Schweiz</option>
                  <option value="LU">Luxemburg</option>
                  <option value="FR">Frankreich</option>
                  <option value="IT">Italien</option>
                  <option value="NL">Niederlande</option>
                  <option value="GB">Großbritanien</option>
                  <option value="ES">Spanien</option>
                  <option value="US">USA</option>
                </select>
                *</td>
            </tr>
            <tr>
              <td align="left" valign="top"><strong>Plattform</strong></td>
              <td><label for="s13"></label>
                <select name="s12" id="s12" onchange="javascript:alert('Achtung! Wenn Sie die Zielseite ändern, müssen Sie auch den Kategoriebaum neu einlesen');">
                  <option value="77">Deutschland</option>
                  <option value="16">Österreich</option>
                  <option value="0">USA</option>
                </select>
                *</td>
            </tr>
            <tr>
              <td align="left" valign="top"><strong>Zahlungsmethoden</strong></td>
              <td><input name="s6" type="checkbox" id="checkbox7" value="1" {if $s6}checked="cheked"{/if} />
                Paypal
                <input name="s7" type="checkbox" id="checkbox8" value="1" {if $s7}checked="cheked"{/if} />
                Überweisung
                <input name="s8" type="checkbox" id="checkbox9" value="1" {if $s8}checked="cheked"{/if} />
                Nachnahme 
                <!--     <input name="s9" type="checkbox" id="checkbox10" value="1" {if $s9}checked="cheked"{/if} />
          Andere-->
                
                <input name="s10" type="checkbox" id="checkbox11" value="1" {if $s10}checked="cheked"{/if} />
                Siehe Beschreibung</td>
            </tr>
            <tr>
              <td width="150" align="left" valign="top"><strong>Ihre Paypal Email</strong></td>
              <td><input name="s14" type="text" id="s14" style="width:630px" value="{$s14}" /></td>
            </tr>
            <tr>
              <td align="left" valign="top"><strong>Versand spätestens </strong></td>
              <td><label for="DispatchTimeMax"></label>
                <select name="DispatchTimeMax" id="DispatchTimeMax">
                  <option value="1">1 Tag</option>
                  <option value="2">2 Tage</option>
                  <option value="3">3 Tage</option>
                  <option value="4">4 Tage</option>
                  <option value="5">5 Tage</option>
                </select>
                nach Zahlungseingang</td>
            </tr>
            <tr>
              <td align="left" valign="top"><strong>Bestandabgleich</strong></td>
              <td><label for="bestand"></label>
                <select name="bestand" id="bestand">
                  <option value="0">nicht abgleichen</option>
                  <option value="1">abgleichen</option>
                  <option value="2">Live Abgleich</option>
                </select></td>
            </tr>
          </table>
        </div>
        <h4><a href="#">eBay Authorisierung und Konnektivität</a></h4>
        <div>
          <table width="820" border="0" cellpadding="0" cellspacing="10">
            <tr>
              <td width="150" align="left" valign="top"><strong>eBay Token</strong></td>
              <td>Token gültig bis: {$s13}</td>
            </tr>
            <tr>
              <td width="150" align="left" valign="top"></td>
              <td><a href="#" onclick="parent.spawngeneric('/?app=ebaymng&subview=authwiz', '', 210, 720)">System erneut an eBay anbinden</a></td>
            </tr>
            <tr>
              <td width="150" align="left" valign="top"></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="150" align="left" valign="top"><strong>eBay Shop Details</strong></td>
              <td><table border="0" cellspacing="4" cellpadding="0">
                  <tr>
                    <td width="150" align="left" valign="top"><em>Store Name</em></td>
                    <td>{$esName}</td>
                  </tr>
                  <tr>
                    <td width="150" align="left" valign="top"><em>Beschreibung</em></td>
                    <td>{$esDes}</td>
                  </tr>
                  <tr>
                    <td width="150" align="left" valign="top"><em>Logo</em></td>
                    <td>{if $esLogo}<img src="{$esLogo}"  />{/if}</td>
                  </tr>
                  <tr>
                    <td width="150" align="left" valign="top"><em>Homepage</em></td>
                    <td>{$esHome}</td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td width="150" align="left" valign="top">&nbsp;</td>
              <td><a href="#" onclick="parent.spawngeneric('/?app=ebaymng&subview=getstore', '', 250, 720)">eBay Store Details neu abfragen</a></td>
            </tr>
          </table>
        </div>
        <h4><a href="#">eBay Rückgabe und Widerrufsbelehrung</a></h4>
        <div>
        <table width="820" border="0" cellpadding="0" cellspacing="10">
          <tr>
            <td width="138" align="left" valign="top"><strong>Rückgaben</strong></td>
            <td width="632"><select name="s16" id="s16">
                <option>Nein</option>
                <option value="1">Ja</option>
              </select></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Rückgabe binnen</strong></td>
            <td><select name="s17" id="s17">
                <option value="Days_7">7 Tagen</option>
                <option value="Days_14">14 Tagen</option>
                <option value="Days_30">30 Tagen</option>
              </select></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Widerrufsbelehrung</strong></td>
            <td><textarea name="s18" rows="20" id="s18" style="width:710px">{$ReturnDescription}</textarea></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Rücksendekosten</strong></td>
            <td><select name="s19" id="s19">
                <option value="Buyer">trägt Käufer</option>
                <option value="Seller">trägt Verkäufer</option>
              </select></td>
          </tr>
        </table>
        <p style="margin-bottom:20px;">
          <center>
            <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="anbieten">Einstellungen speichern</button>
          </center>
        </p>
      </form>
    </div>
  </div>
</div>
</div>
<p align="center"><a href="javascript:location.reload()">Ansicht neu laden</a></p>

