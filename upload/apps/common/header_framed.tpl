<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" /> 

<title>{$title}</title>

<meta http-equiv="content-Language" content="de" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />

<meta name="description" content='' />

<meta name="language" content="de" />

<meta name="robots" content="noindex">

<meta name="publisher" content="{$publisher}" />
<meta name="copyright" content="Copyright 2009 by {$publisher}" />
<meta name="author" content="Alexander Pick - PBT Media - http://www.pbt-media.de" />

<link rel="shortcut icon" href="favicon.ico" /> 
<link href="/css/style.css" rel="stylesheet" type="text/css" />

{literal}
<style>
html, body {
	background-color:#f9f9f9;	
}
</style>
{/literal}


<script language="javascript" src="/javascript/jquery.js" type="text/javascript" ></script>
<script language="javascript" src="/javascript/jquery.ui.js" type="text/javascript" ></script>
{if $sv == ""}
{include file="$app/header.tpl"}
{else}
{include file="$app/subviews/header/$sv.tpl"}
{/if}
</head>
<body>

