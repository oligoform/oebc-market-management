{literal}
<style>
html {
	overflow:auto !important;
	overflow-x:hidden !important;
	overflow-y:visible !important;
}
html, body {
	height:100%;
	width:100%;
	padding:0;
	margin:0;
}
</style>
<script language="javascript" src="/apps/googletools/googletools.js"></script>
<script language="javascript">
$(function() {
	{/literal}
	$("#gsac").val("{$gsac}");
	$("#gscc").val("{$gscc}");
	$("#gskc").val("{$gskc}");
	{literal}
});
</script>
{/literal}
