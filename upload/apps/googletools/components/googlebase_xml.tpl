<?xml version="1.0" encoding="UTF-8" ?>

<rss version ="2.0" xmlns:g="http://base.google.com/ns/1.0">
<channel>
	<title>{$shopname}</title>
	<description>GamesManiac - Alles für Gamer und Causal Player. Spiele für alle gängigen Konsolen, Computer und Mac. Wir führen ein umfangreiches Produktsortiment und bieten blitzschnellen Versand.</description>
	<link>{$host}</link>
{section name=articles loop=$articles}
<item>
<title><![CDATA[{$articles[articles].OXTITLE}]]></title>
<g:condition>new</g:condition>
<description><![CDATA[{$articles[articles].OXLONGDESC|trim|html_entity_decode:$smarty.const.ENT_QUOTES:'utf-8'|truncate:400}]]></description>
<g:id>{$smarty.section.articles.iteration}</g:id>
{if $articles[articles].OXPIC1}
<g:image_link>{$host}/out/pictures/1/{$articles[articles].OXPIC1}</g:image_link>
{/if}
<link>{$host}/{$articles[articles].OXSEOURL}</link>
<g:ean>{$articles[articles].OXARTNUM}</g:ean>
<g:mpn>{$articles[articles].OXID}</g:mpn>
<g:price>{$articles[articles].OXPRICE}</g:price>
<g:product_type>Software &gt; Software für Videospiele</g:product_type>
<g:google_product_category>Software &gt; Software für Videospiele</g:google_product_category>
<g:quantity>{$articles[articles].OXSTOCK}</g:quantity>
<g:availability>in stock</g:availability>
<g:expiration_date>{"+30 days"|date_format:"%Y-%m-%d"}</g:expiration_date>
<g:shipping>
	<g:country>DE</g:country>
	<g:service>Ground</g:service>
	<g:price>2.90</g:price>
</g:shipping>
</item>
{/section}
</channel>
</rss>