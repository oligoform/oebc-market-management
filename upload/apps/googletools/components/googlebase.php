<?
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Datum in der Vergangenheit

//disable notcies about timezones on OSX
date_default_timezone_set(date_default_timezone_get());

ini_set("error_reporting", "E_ALL"); //  & ~E_NOTICE");
ini_set("display_errors", "1");

set_time_limit(0);
ini_set('memory_limit','-1');

require "../../../classes/subsystem.class.php";
require "../../../classes/db.class.php";
require "../../../classes/oxid.class.php";

require "../../../classes/smarty/Smarty.class.php";

$db 	= new dbal;
$files 	= new subsystem($db);

$smarty = new smarty;

$smarty->compile_dir  = dirname(__FILE__).'/../../../tmp/';
$smarty->template_dir = dirname(__FILE__);

$host = $files->getOpt("url");
$smarty->assign("host", $host);

$smarty->assign("shopname", $files->getOpt("shopname"));

$ox = new oxid($db);
$a = $ox->getSeoUrls();

// Build Pricelist

$pparentlist = array();

foreach($a as $aa) {
	if($aa["OXPARENTID"] == "")
	{
		$parentlist[$aa["OXID"]] = $aa;
	}
}

$articles = array();

foreach($a as $aa) {
		
	if($aa["OXPARENTID"] != "")
	{
		$aa["OXTITLE"] 	= $parentlist[$aa["OXPARENTID"]]["OXTITLE"];	
		$aa["OXLONGDESC"] = $parentlist[$aa["OXPARENTID"]]["OXLONGDESC"];	
		$aa["OXPIC1"] 	= $parentlist[$aa["OXPARENTID"]]["OXPIC1"];
		$aa["OXTAGS"] 	= $parentlist[$aa["OXPARENTID"]]["OXTAGS"];
		
		$aa["OXLONGDESC"] = filter_var($aa["OXLONGDESC"], FILTER_SANITIZE_STRING);
		$aa["OXLONGDESC"] = str_replace("!", ".", $aa["OXLONGDESC"]);
		
		$isd = strstr($aa["OXSTDURL"], 'cl=details');
		if($isd) {
			array_push($articles, $aa);
		}
	} 
		elseif(1) 
	{	
		$isd = strstr($aa["OXSTDURL"], 'cl=details');
		if($isd && ($aa["OXTITLE"] != "")) {
			array_push($articles, $aa);
		}
	}

}
	
$smarty->assign("articles", $articles);

$sitemap = $smarty->fetch('googlebase_xml.tpl');
echo "writing Sitemap...<br />";
$fp = fopen(dirname(__FILE__).'/../../../filesystem/google-base.xml', 'w');
fwrite($fp, $sitemap);
fclose($fp);

echo "<strong>Ergebnis des Base Export</strong><br />";
echo "Google Base Export wurde erstellt<br />";

//exit();

$host = $files->getOpt("shopftp");
$ftp_user_name = $files->getOpt("shopftpuser"); //this does not equal your Google Account username.
$ftp_user_pass = $files->getOpt("shopftppw"); //again not equal to Google Account password.
$remote_file = ".".$files->getOpt("shopftpverz")."gmerchant.xml"; //Change this to match the exact name of the file you setup in Google Base as your upload file.
$file = realpath(dirname(__FILE__).'/../../../filesystem/google-base.xml'); // This need to be the full path to the filey ou want to send to google.

// setup $host and $file variables for your setup before here...

$hostip = gethostbyname($host);
$conn_id = @ftp_connect($hostip);

// login with username and password
$login_result = @ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

// IMPORTANT!!! turn passive mode on
@ftp_pasv ( $conn_id, true );

if ((!$conn_id) || (!$login_result)) {
	echo "FTP Verbindung zu Server ist fehlgeschlagen!<br />";
	echo "$host mit Benutzernamen $ftp_user_name<br />";
	die;
} else {
	echo "Erfolgreich zu Server verbunden ($host), Benutzername $ftp_user_name<br />";
	echo "Host IP ist $hostip<br />";
}

if (!ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
		echo "Es gabe ein Problem beim Upload $file<br />";
}

echo "DONE!";

// close the connection
@ftp_close($conn_id);

?> 