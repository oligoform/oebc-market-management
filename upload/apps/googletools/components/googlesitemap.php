<?

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Datum in der Vergangenheit

//disable notcies about timezones on OSX
date_default_timezone_set(date_default_timezone_get());

ini_set("error_reporting", "E_ALL"); //  & ~E_NOTICE");
ini_set("display_errors", "1");

set_time_limit(0);
ini_set('memory_limit','-1');

require "../../../classes/subsystem.class.php";
require "../../../classes/db.class.php";
require "../../../classes/oxid.class.php";

require "../../../classes/smarty/Smarty.class.php";

$db 	= new dbal;
$files 	= new subsystem($db);
$smarty = new smarty;

$smarty->compile_dir  = dirname(__FILE__).'/../../../tmp/';
$smarty->template_dir = dirname(__FILE__);

$host = $files->getOpt("url");
$smarty->assign("host", $host);

$ox = new oxid($db);
$a = $ox->getSeoUrls();

$articles = array();
$cats = array();
$cms = array();

function pingBingSitemaps( $url_xml )
{
	$status = 0;

	$fp = @fsockopen("www.bing.com", 80);

	if (!$fp)
	{
		echo "$errstr ($errno)<br />\n";
	}
	else
	{
		$req = 'GET /webmaster/ping.aspx?siteMap=' .
		urlencode( $url_xml ) . " HTTP/1.1\r\n" .
								"Host: www.bing.com\r\n" .
								"User-Agent: Mozilla/5.0 (compatible; " .
								PHP_OS . ") PHP/" . PHP_VERSION . "\r\n" .
								"Connection: Close\r\n\r\n";
		fwrite( $fp, $req );
		while( !feof($fp) )
		{
			if( @preg_match('~^HTTP/\d\.\d (\d+)~i', fgets($fp, 128), $m) )
			{
				$status = intval( $m[1] );
				break;
			}
		}
		fclose( $fp );
	}
return( $status );
}

function pingGoogleSitemaps( $url_xml )
{
	$status = 0;
	
	//www.google.com/webmasters/tools/ping?sitemap=

	$fp = @fsockopen("www.google.com", 80);

	if (!$fp)
	{
		echo "$errstr ($errno)<br />\n";
	}
	else
	{
		$req = 'GET /webmasters/tools/ping?sitemap=' .
		urlencode( $url_xml ) . " HTTP/1.1\r\n" .
								"Host: www.google.com\r\n" .
								"User-Agent: Mozilla/5.0 (compatible; " .
								PHP_OS . ") PHP/" . PHP_VERSION . "\r\n" .
								"Connection: Close\r\n\r\n";
		fwrite( $fp, $req );
		while( !feof($fp) )
		{
			if( @preg_match('~^HTTP/\d\.\d (\d+)~i', fgets($fp, 128), $m) )
			{
				$status = intval( $m[1] );
				break;
			}
		}
		fclose( $fp );
	}
return( $status );
}

function pingAskSitemaps( $url_xml )
{
	$status = 0;
	
	//www.google.com/webmasters/tools/ping?sitemap=

	$fp = @fsockopen("www.ask.com", 80);

	if (!$fp)
	{
		echo "$errstr ($errno)<br />\n";
	}
	else
	{
		$req = 'GET http://submissions.ask.com/ping?sitemap=' .
		urlencode( $url_xml ) . " HTTP/1.1\r\n" .
								"Host: www.ask.com\r\n" .
								"User-Agent: Mozilla/5.0 (compatible; " .
								PHP_OS . ") PHP/" . PHP_VERSION . "\r\n" .
								"Connection: Close\r\n\r\n";
		fwrite( $fp, $req );
		while( !feof($fp) )
		{
			if( @preg_match('~^HTTP/\d\.\d (\d+)~i', fgets($fp, 128), $m) )
			{
				$status = intval( $m[1] );
				break;
			}
		}
		fclose( $fp );
	}
return( $status );
}

foreach($a as $aa) {
	
	$isd = strstr($aa["OXSTDURL"], 'cl=details');
	if($isd) {
		array_push($articles, $aa);
	}
	$isa = strstr($aa["OXSTDURL"], 'cl=content');
	if($isa) {
		array_push($cms, $aa);
	}
	$isc = strstr($aa["OXSTDURL"], 'cl=alist');
	if($isc) {
		array_push($cats, $aa);
	}
	//index.php?cl=alist&amp;cnid=531e1b5aef473f543a97c1670e48fdd3
}
	
$smarty->assign("articles", $articles);

$smarty->assign("article_changefreq", $files->getOpt("gsac"));
$smarty->assign("article_priority", $files->getOpt("gsar"));

$smarty->assign("cats", $cats);

$smarty->assign("cat_changefreq", $files->getOpt("gsac"));
$smarty->assign("cat_priority", $files->getOpt("gsar"));

$smarty->assign("cms", $cms);

$smarty->assign("cms_changefreq", $files->getOpt("gsac"));
$smarty->assign("cms_priority", $files->getOpt("gsar"));


$sitemap = $smarty->fetch('googlesitemap_xml.tpl');

$fp = fopen(dirname(__FILE__).'/../../../filesystem/sitemap.xml', 'w');
fwrite($fp, $sitemap);
fclose($fp);

echo "<strong>Ergebnis des Sitemap Export</strong><br />";
echo "Google Sitemap Export wurde erstellt<br />";

if($files->getOpt("shopftpactive"))
{
	
$host = $files->getOpt("shopftp");
$ftp_user_name = $files->getOpt("shopftpuser"); //this does not equal your Google Account username.
$ftp_user_pass = $files->getOpt("shopftppw"); //again not equal to Google Account password.
$remote_file = ".".$files->getOpt("shopftpverz")."sitemap.xml"; //Change this to match the exact name of the file you setup in Google Base as your upload file.
$file = realpath(dirname(__FILE__).'/../../../filesystem/sitemap.xml'); // This need to be the full path to the filey ou want to send to google.

// setup $host and $file variables for your setup before here...

$hostip = gethostbyname($host);
$conn_id = @ftp_connect($hostip);

// login with username and password
$login_result = @ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

// IMPORTANT!!! turn passive mode on
@ftp_pasv ( $conn_id, true );

if ((!$conn_id) || (!$login_result)) {
	echo "FTP Verbindung zu Server ist fehlgeschlagen!<br />";
	echo "$host mit Benutzernamen $ftp_user_name<br />";
	die;
} else {
	echo "Erfolgreich zu Server verbunden ($host), Benutzername $ftp_user_name<br />";
	echo "Host IP ist $hostip<br />";

// upload a file
if (ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
	echo "Upload erfolgreich $file<br />";
	echo "URL der Sitemap: http://".$files->getOpt("url")."/sitemap.xml<br />";
	echo "Ping an Google senden: ".pingGoogleSitemaps( "http://".$files->getOpt("url")."/sitemap.xml" )."<br />";
	echo "Ping an Bing senden: ".pingBingSitemaps( "http://".$files->getOpt("url")."/sitemap.xml" )."<br />";
	echo "Ping an Ask senden: ".pingBingSitemaps( "http://".$files->getOpt("url")."/sitemap.xml" )."<br />";
} else {
	echo "Es gabe ein Problem beim Upload $file<br />";
}

// close the connection
@ftp_close($conn_id);
}

}

?> 
