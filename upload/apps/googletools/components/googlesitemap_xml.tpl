<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url>
  <loc>{$host}/</loc>
  <priority>1.00</priority>
  <changefreq>daily</changefreq>
</url>
{section name=articles loop=$articles}
<url>
  <loc>{$host}/{$articles[articles].OXSEOURL}</loc>
  <priority>{$article_priority}</priority>
  <changefreq>{$article_changefreq}</changefreq>
</url>
{/section}
{section name=cats loop=$cats}
<url>
  <loc>{$host}/{$cats[cats].OXSEOURL}</loc>
  <priority>{$cat_priority}</priority>
  <changefreq>{$cat_changefreq}</changefreq>
</url>
{/section}
{section name=cms loop=$cms}
<url>
  <loc>{$host}/{$cms[cms].OXSEOURL}</loc>
  <priority>{$cms_priority}</priority>
  <changefreq>{$cms_changefreq}</changefreq>
</url>
{/section}
</urlset>