<?

	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
	}
	
	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../../tmp/';
	$smarty->template_dir = dirname(__FILE__);

	if($_REQUEST["action"] == "save") {
		
		/* GOOGLE Sitemaps */
		
		$files->setOpt("gsactive",$db->clean($_REQUEST["gsactive"]));
		
		$files->setOpt("gsa",$db->clean($_REQUEST["gsa"]));
		$files->setOpt("gsar",$db->clean($_REQUEST["gsar"]));
		$files->setOpt("gsac",$db->clean($_REQUEST["gsac"]));
		
		$files->setOpt("gsk",$db->clean($_REQUEST["gsk"]));
		$files->setOpt("gskr",$db->clean($_REQUEST["gskr"]));
		$files->setOpt("gksc",$db->clean($_REQUEST["gksc"]));
		
		$files->setOpt("gsc",$db->clean($_REQUEST["gsc"]));
		$files->setOpt("gscr",$db->clean($_REQUEST["gscr"]));
		$files->setOpt("gscc",$db->clean($_REQUEST["gscc"]));

		/* GOOGLE Base */
		$files->setOpt("shopftpactive",$db->clean($_REQUEST["shopftpactive"]));
		
		$files->setOpt("gmactive",$db->clean($_REQUEST["gmactive"]));
		
		$files->setOpt("gb",$db->clean($_REQUEST["gb"]));
		$files->setOpt("gp",$db->clean($_REQUEST["gp"]));

	}

	$smarty->assign("gsactive", $files->getOpt("gsactive"));
		
	$smarty->assign("gsa", $files->getOpt("gsa"));
	$smarty->assign("gsar",$files->getOpt("gsar"));
	$smarty->assign("gsac", $files->getOpt("gsac"));
		
	$smarty->assign("gsk", $files->getOpt("gsk"));
	$smarty->assign("gskr", $files->getOpt("gskr"));
	$smarty->assign("gksc", $files->getOpt("gksc"));
		
	$smarty->assign("gsc", $files->getOpt("gsc"));
	$smarty->assign("gscr", $files->getOpt("gscr"));
	$smarty->assign("gscc", $files->getOpt("gscc"));
	
	$smarty->assign("shopftpactive", $files->getOpt("shopftpactive"));

	/* GOOGLE Base */
		
	$smarty->assign("gmactive", $files->getOpt("gmactive"));
		
	$smarty->assign("gmu", $files->getOpt("gmu"));
	$smarty->assign("gmp", $files->getOpt("gmp"));
	$smarty->assign("gb", $files->getOpt("gb"));
	$smarty->assign("gp", $files->getOpt("gp"));

	/*****************************************************************/	

	$content = $smarty->fetch('googletools.tpl');
	$template->assign("content",$content);

?>