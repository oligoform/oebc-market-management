<div class="tabs2" style="border:0; margin:0; min-height:450px;">
  <ul>
    <li><a href="#tabs-GE">Google Export ausführen</a></li>
    <li><a href="#tabs-SE">Einstellungen</a></li>
  </ul>
  
  <div id="tabs-GE">
    <h4>Google Export ausführen</h4>
      <p><a href='javascript:exportCall("/apps/googletools/components/googlesitemap.php")'>Google Sitemap erstellen</a></p>
      <p><a href='javascript:exportCall("/apps/googletools/components/googlebase.php")'>Google Base Export</a></p>
      <div style="overflow:auto; height:200px; width:100%;" id="results"> </div>
  </div>
  
  <div id="tabs-SE">
    <h4>Google Base</h4>
      <form id="settings" name="settings" method="post" action="index.php">
        <input type="hidden" name="app" value="googletools" />
        <input type="hidden" name="action" value="save" />
        <table width="880" border="0" cellpadding="0" cellspacing="10">
          <tr>
            <td width="230" align="left" valign="top"><strong>Aktiv</strong></td>
            <td><input name="gmactive" type="checkbox" id="gmactive" {if $gmactive}checked="checked"{/if} /></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Varianten Vater exportieren</strong></td>
            <td><input name="gp" type="checkbox" id="gp" {if $gp}checked="checked"{/if} /></td>
          </tr>
          <!-- <tr>
      <td align="left" valign="top"><strong>Artikel Ohne Bestand</strong></td>
      <td><input name="gb" type="checkbox" id="gb"{if $gb}checked="checked"{/if} />
        </td>
    </tr>-->
        </table>
        <h4>Google Sitemaps</h4>
        <table width="800" border="0" cellpadding="0" cellspacing="10">
          <tr>
            <td align="left" valign="top"><strong>Aktiv</strong></td>
            <td><input name="gsactive" type="checkbox" id="gsactive" {if $gsactive}checked="checked"{/if} /></td>
          </tr>
          <tr>
            <td width="150" align="left" valign="top"><strong>Artikel</strong></td>
            <td><input name="gsa" type="checkbox" id="gsa" {if $gsa}checked="checked"{/if} />
              <input name="gsar" type="text" id="gsar" size="3" value="{$gsar}" />
              <select name="gsac" id="gsac">
                <option value="daily">täglich</option>
                <option value="weekly">wöchentlich</option>
                <option value="monthly">monatlich</option>
                <option value="yearly">jährlich</option>
                <option value="always">immer</option>
              </select></td>
          </tr>
          <tr>
            <td width="150" align="left" valign="top"><strong>Katgorien</strong></td>
            <td><input name="gsk" type="checkbox" id="gsk" {if $gsk}checked="checked"{/if} />
              <input name="gskr" type="text" id="gskr" size="3" value="{$gskr}" />
              <select name="gskc" id="gskc">
                <option value="daily">täglich</option>
                <option value="weekly">wöchentlich</option>
                <option value="monthly">monatlich</option>
                <option value="yearly">jährlich</option>
                <option value="always">immer</option>
              </select></td>
          </tr>
          <tr>
            <td width="150" align="left" valign="top"><strong>CMS Seiten</strong></td>
            <td><input name="gsc" type="checkbox" id="gsc"	{if $gsc}checked="checked"{/if} />
              <input name="gscr" type="text" id="gscr" size="3" value="{$gscr}" />
              <select name="gscc" id="gscc">
                <option value="daily">täglich</option>
                <option value="weekly">wöchentlich</option>
                <option value="monthly">monatlich</option>
                <option value="yearly">jährlich</option>
                <option value="always">immer</option>
              </select></td>
          </tr>

          <tr>
            <td align="left" valign="top">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Sitemap FTP Export</strong></td>
            <td><input type="checkbox" name="shopftpactive" id="shopftpactive" {if $shopftpactive}checked="checked"{/if} /></td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Sitemap URL</strong></td>
            <td><input name="sitemapurl" type="text" id="sitemapurl" style="width:300px" value="{$sitemapurl}" />
              /sitemap.xml</td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top"><strong>Hinweis</strong></td>
            <td><em>Sollten Sie kein FTP nutzen, lesen Sie bitte den Artikel <a href="http://googlewebmastercentral.blogspot.com/2008/02/cross-submissions-via-robotstxt-on.html" target="_blank">zu Crossdomain Submission von Sitemaps</a></em></td>
          </tr>
        </table>
          <div style="text-align:center !important; width: 100%;">
          <input name="submit" type="submit" id="submit" value="Einstellungen Speichern" />
        </div>
      </form>
  </div>
</div>
