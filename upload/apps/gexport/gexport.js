function execGen(p)
{	
	$("#execres").html('<center>Wird ausgeführt...<br/><img src="/images/system/load.gif" /></center>');
	$.post(p,function(resdata){
					$("#execres").html(resdata);
 				});
}

function geShow() {
	
			var myId = $("#geType").val();	
		
			$("#geloading").html('<center>Lade Daten...<br/><img src="/images/system/load.gif" /></center>');
			$(".geform").toggle();
		
			$.post("/components/ajax.php", {	
					'mode'	   			: 	'getexport',
					'thisapp'			: 	'gexport',
					'id'				: 	myId
			},
			function(xml){
				$(xml).find('formfields').each(function(){
                        var id = $(this).find('id').text();
                        var template = $(this).find('template').text();
						var name = $(this).find('name').text();
	                    var active = $(this).find('active').text();
                        var interval = $(this).find('interval').text();
                        var latest = $(this).find('latest').text();
                        var filename = $(this).find('filename').text();
						 
						$("#id").val(id);
						$("#template").val($.trim(template));
						$("#name").val(name);

						if(active == "1") {
							$("#active").attr("checked","checked");
						} else {
							$("#active").removeAttr("checked");	
						}
						$("#interval").val(interval);
						if(latest == "1") {
							$("#latest").attr("checked","checked");
						} else {
							$("#latest").removeAttr("checked");
						}
						$("#filename").val(filename);
						 
                 });
	
	 			$("#geloading").html('');
				$(".geform").toggle();


 			});
}

$(function() {
	$(".tabs2").tabs();	
});