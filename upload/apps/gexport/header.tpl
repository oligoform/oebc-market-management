{literal}
<style>
html {
	overflow:auto !important;
	overflow-x:hidden !important;
	overflow-y:visible !important;
}
html, body {
	height:100%;
	width:100%;
	padding:0;
	margin:0;
}
#execres {
	height: 360px;
	width:100%;
	overflow:auto;
}
</style>

<script type="text/javascript" src="/apps/gexport/gexport.js"></script>
{/literal}
