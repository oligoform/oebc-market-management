<form id="geexport" name="geexport" method="post" action="index.php">

<input type="hidden" name="app" value="gexport" />
<input type="hidden" name="action" value="save" />

<div id="geloading"></div>
<table width="880" border="0" cellpadding="0" cellspacing="10" class="geform">
      <tr >
        <td align="left" valign="top"><strong>Format Vorlage</strong></td>
        <td>
          <select name="geType" id="geType" onchange="javascript:geShow();">
            <option value="">Neu</option>
            {section name=exports loop=$exports}
            <option value="{$exports[exports].id}">{$exports[exports].name}</option>
            {/section}
        </select> 
          <a href="javascript:geDelete();">Löschen</a></td>
      </tr>
<tr>
        <td align="left" valign="top"><strong>Name</strong></td>
        <td><input name="name" type="text" id="name" maxlength="200" style="width:730px" /></td>
    </tr>
       <tr>
        <td width="150" align="left" valign="top"><strong>Vorlage</strong></td>
        <td><textarea name="template" rows="20" id="template" style="width:730px"></textarea></td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td></td>
      </tr>
      <tr>
        <td align="left" valign="top"><strong>Vorlage aktiv</strong></td>
        <td><input type="checkbox" name="active" id="active" value="1" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><strong>Interval</strong></td>
        <td>
          <select name="interval" id="interval">
            <option value="5">alle 5 Min</option>
            <option value="15">alle 15 Min</option>
            <option value="30">alle 30 Min</option>
            <option value="1">alle 60 Min</option>
            <option value="6">alle 6 Std</option>
            <option value="12">alle 12 Std</option>
            <option value="24">täglich</option>
          </select></td>
      </tr>
      <tr>
        <td align="left" valign="top"><strong>Nur neuste Bestellungen</strong></td>
        <td><input type="checkbox" name="latest" id="latest" value="1" />
          </td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top"><strong>Ausgabe Dateiname</strong></td>
        <td><input name="filename" type="text" id="filename" maxlength="64"  style="width:730px"></td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="center">
        <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="anbieten">Export speichern</button>&nbsp;
                <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="reset" id="anbieten">Zurücksetzen</button>
            <p style="margin-top:30px;"><a href="javascript:exportCallGen();">Export der gewählten Vorlage manuell durchführen</a></p>
        </td>
      </tr>
      </table>
      </form>
