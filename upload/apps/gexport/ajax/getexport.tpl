<?xml version="1.0"?>
<formset>
  <formfields>
    <id>{$formdata.id}</id>
    <template>
      <![CDATA[{$formdata.template}]]>
    </template>
    <name>{$formdata.name}</name>
    <active>{$formdata.active}</active>
    <interval>{$formdata.interval}</interval>
    <latest>{$formdata.latest}</latest>
    <filename>{$formdata.filename}</filename>
  </formfields>
</formset>
