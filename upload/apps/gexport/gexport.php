<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../../tmp/';
	$smarty->template_dir = dirname(__FILE__).'/../';

	// Generischer Export - Settings
	
	if($_REQUEST["action"] == "save") {
	
	
		if($_REQUEST["geType"] == "") {
			$sql = "INSERT INTO `oebc_exports` (`template` ,`name` , `active` ,`interval` , `latest` ,`filename`)
									VALUES ('".$db->clean($_REQUEST["template"])."', '".$db->clean($_REQUEST["name"])."', '".$db->clean($_REQUEST["active"])."', 
											'".$db->clean($_REQUEST["interval"])."', '".$db->clean($_REQUEST["latest"])."', '".$db->clean($_REQUEST["filename"])."')";
		} else {
		
			$sql = "UPDATE `oebc_exports` SET `template` = '".$db->clean($_REQUEST["template"])."', `name` = '".$db->clean($_REQUEST["name"])."',  `active` = '".$db->clean($_REQUEST["active"])."', 
												`interval` = '".$db->clean($_REQUEST["interval"])."', `latest` = '".$db->clean($_REQUEST["latest"])."', `filename` = '".$db->clean($_REQUEST["filename"])."' 
					WHERE `id` = ".$db->clean($_REQUEST["geType"])." LIMIT 1";
				
		}
				
		$db->query_exec($sql);							
		
		header("Location: http://". $_SERVER['SERVER_NAME']."/index.php?app=gexport");
		exit(0);
		
	}
	
	$sql = "SELECT * FROM oebc_exports";
	$result = $db->query_array($sql);
	$smarty->assign("exports", $result); 
	
	$content = $smarty->fetch('gexport/gexport.tpl');
	$template->assign("content",$content);
	
?>