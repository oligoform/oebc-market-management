{section name=p step=2 loop=$packme}
<div class="note">
<div style="border:2px solid #000; width:45%; height:100%; margin:1%; padding:2px; float:left;">
  <div style="width:98%; height:8%; margin:1%; padding:2px; float:left;">
    <h3>Bestellung {$packme[p].OXORDERNR}</h3>
  </div>
  <div style="width:45%; height:44%; margin:1%; padding:2px; float:left;"><strong>Rechnung <br>
    </strong>     
    {$packme[p].OXBILLFNAME} {$packme[p].OXBILLLNAME}<br>
    {$packme[p].OXBILLCOMPANY}<br>
    {$packme[p].OXBILLSTREET} {$packme[p].OXBILLSTREETNR}<br>
    {$packme[p].OXBILLADDINFO}<br>
    {$packme[p].OXBILLZIP} {$packme[p].OXBILLCITY}<br>
    {$packme[p].OXBILLZIP}<br>
    <br>
    Fon: {$packme[p].OXBILLFON}<br>
    Fax: {$packme[p].OXBILLFAX}</div>
  {if $packme[p].OXDELSTREET}<div style="width:45%; height:44%; margin:1%; padding:2px; float:right"><strong>Lieferung<br>
    </strong> 
    {$packme[p].OXDELFNAME} {$packme[p].OXDELLNAME}<br>
    {$packme[p].OXDELCOMPANY}<br>
    {$packme[p].OXDELSTREET} {$packme[p].OXDELSTREETNR}<br>
    {$packme[p].OXDELADDINFO}<br>
    {$packme[p].OXDELZIP} {$packme[p].OXDELCITY}<br>
    {$packme[p].OXDELZIP}<br>
    <br>
    Fon: {$packme[p].OXDELFON}<br>
    Fax: {$packme[p].OXDELFAX}</div>{/if}
  <div style="clear:both"></div>
  <div style="width:98%; height:24%; margin:1%; padding:2px; float:left;"><strong>Mitteilung:</strong> <br>
  {$packme[p].OXREMARK}</div>
  <div style="width:98%; height:45%; margin:1%; padding:2px; float:left; font-size:12px;">

    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
<strong>    ArtNr.
</strong>    </div> 
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
    <strong>Anzahl</strong> 
    </div>
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
    <strong>Variante</strong>
    </div>
    <div style="width:40%; height:20px; margin:1%; padding:2px; float:left;">
    <strong>Artikel</strong>
    </div>
    <div style="clear:both"></div>
    {section name=a loop=$packme[p].articles}

    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme[p].articles[a].OXARTNUM}
    </div> 
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme[p].articles[a].OXAMOUNT}
    </div>
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme[p].articles[a].OXSELVARIANT}
    </div>
    <div style="width:40%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme[p].articles[a].OXTITLE}
    </div>
    <div style="clear:both"></div>
    <hr />
    <div style="clear:both"></div>
    {/section}
    
   </div>
  <div style="clear:both"></div>
</div>
<!--second-->
{math equation="x + y" x=$smarty.section.p.index y=1 assign="q"}
{if $packme.$q.OXORDERNR}
<div style="border:2px solid #000; width:47%; height:100%; margin:1%; padding:2px; float:right;">
  <div style="width:98%; height:8%; margin:1%; padding:2px; float:left;">
    <h3>Bestellung {$packme.$q.OXORDERNR}</h3>
  </div>
  <div style="width:45%; height:44%; margin:1%; padding:2px; float:left;"><strong>Rechnung <br>
    </strong>     
    {$packme.$q.OXBILLFNAME} {$packme.$q.OXBILLLNAME}<br>
    {$packme.$q.OXBILLCOMPANY}<br>
    {$packme.$q.OXBILLSTREET} {$packme.$q.OXBILLSTREETNR}<br>
    {$packme.$q.OXBILLADDINFO}<br>
    {$packme.$q.OXBILLZIP} {$packme.$q.OXBILLCITY}<br>
    {$packme.$q.OXBILLZIP}<br>
    <br>
    Fon: {$packme.$q.OXBILLFON}<br>
    Fax: {$packme.$q.OXBILLFAX}</div>
  {if $packme.$q.OXDELSTREET}<div style="width:45%; height:44%; margin:1%; padding:2px; float:right"><strong>Lieferung<br>
    </strong> 
    {$packme.$q.OXDELFNAME} {$packme.$q.OXDELLNAME}<br>
    {$packme.$q.OXDELCOMPANY}<br>
    {$packme.$q.OXDELSTREET} {$packme.$q.OXDELSTREETNR}<br>
    {$packme.$q.OXDELADDINFO}<br>
    {$packme.$q.OXDELZIP} {$packme.$q.OXDELCITY}<br>
    {$packme.$q.OXDELZIP}<br>
    <br>
    Fon: {$packme.$q.OXDELFON}<br>
    Fax: {$packme.$q.OXDELFAX}</div>{/if}
  <div style="clear:both"></div>
  <div style="width:98%; height:24%; margin:1%; padding:2px; float:left;"><strong>Mitteilung: </strong><br>
  {$packme.$q.OXREMARK}</div>
  <div style="width:98%; height:48%; margin:1%; padding:2px; float:left; font-size:12px;">

    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
    <strong>ArtNr.</strong>
    </div> 
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
    <strong>Anzahl</strong> 
    </div>
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
    <strong>Variante</strong>
    </div>
    <div style="width:40%; height:20px; margin:1%; padding:2px; float:left;">
    <strong>Artikel</strong>
    </div>
    <div style="clear:both"></div>
    {section name=a loop=$packme.$q.articles}

    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme.$q.articles[a].OXARTNUM}
    </div> 
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme.$q.articles[a].OXAMOUNT}
    </div>
    <div style="width:15%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme.$q.articles[a].OXSELVARIANT}
    </div>
    <div style="width:40%; height:20px; margin:1%; padding:2px; float:left;">
		{$packme.$q.articles[a].OXTITLE}
    </div>
    <div style="clear:both"></div>
    <hr />
    <div style="clear:both"></div>
    {/section}
    
   </div>
  <div style="clear:both"></div>
</div>
</div>
{/if}
<div style="clear:both"></div>
<div class="pagebreakhere"></div>
<div style="clear:both"></div>
{/section}

