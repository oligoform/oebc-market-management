{literal}
<style type="text/css">
.drop {
	float:left;
	height: 350px;
	width:95%;
	border: 1px solid #ccc;
	overflow-y:hidden;
	overflow-y:auto;
	background-color:#FFF;
}
.myarticle, .myarticle2 {
	width:350px;;
	min-height:30px;
	margin: 5px;	
}
</style>
<script>

function submitMan() {
	
	var oxids = Array();
	
	$('#basket').children().each(
    function(){
		oxids.push($(this).find(".OXID").val());	
	});
	
	if(oxids.length < 1) {
		alert("Bitte legen Sie mindestens 1 Artikel bei bestellte Waren ab!");
		return true;	
	}

	$.post("/components/ajax.php", {	
					'mode'	   		: 	'makeorder',
					'thisapp'		:	'orders',
					'arts'			:	oxids,
					'badd'			:	$("#badd").val(),
					'sadd'			:	$("#sadd").val(),
					'sname'			:	$("#sname").val(),
					'bname'			:	$("#bname").val(),
					'sfname'		:	$("#sfname").val(),
					'bfname'		:	$("#bfname").val(),
					'bcorp'			:	$("#bcorp").val(),
					'scorp'			:	$("#scorp").val(),
					'sstr'			:	$("#sstr").val(),
					'bstr'			:	$("#bstr").val(),
					'bnr'			:	$("#bnr").val(),
					'snr'			:	$("#snr").val(),
					'szip'			:	$("#szip").val(),
					'bzip'			:	$("#bzip").val(),
					'bcity'			:	$("#bcity").val(),
					'scity'			:	$("#scity").val(),
					'scountry'		:	$("#scountry").val(),
					'bcountry'		:	$("#bcountry").val(),
					'bvatid'		:	$("#bvatid").val(),
					'pay'			:	$("#pay").is(':checked'),
					'isBill'		:	$("#isBill").is(':checked'),
					'email'			:	$("#email").val(),
					'payment'		:	$("#payments").val(),
					'delivery'		:	$("#delivery").val()
	
			},
			function(){
				
				var oid = $("#isEdit").val();
				
				if(oid != "") {
				
					var doOnEdit = $("#doOnEdit").val();
				
					if(doOnEdit == "sto") {
						$.post("/components/ajax.php", {	
							'mode'	   			: 	'stornoorder',
							'thisapp'			: 	'orders',
							'oid'				:	oid
						});
					}
					if(doOnEdit == "del") {
							$.post("/components/ajax.php", {	
								'mode'	   			: 	'deleteorder',
								'thisapp'			: 	'orders',
								'oid'				:	oid
							});	
					}
				}
				
				alert("Bestellung wurde im System abgelegt, bitte aktualisieren Sie die Bestellübersicht!");
				
				var frameWindow = document.parentWindow || document.defaultView;
    			var outerDiv = $(frameWindow.frameElement.parentNode);
			    kill = $('.genericFrame', outerDiv).get(0);
				
				$(kill).parent().remove();
				
			});	


}


var art = 0;

$(function() {
$('.myarticle').draggable({ opacity: 0.7, helper: 'clone' }); 
$('.myarticle2').draggable({ opacity: 0.7, helper: 'clone' }); 

$('#basket').droppable({ accept: ".myarticle", 
                       activeClass: 'droppable-active', 
                       hoverClass: 'droppable-hover', 
                       drop: function(e, ui) { 
						   		$('#basket').append(ui.draggable.clone().addClass("cf"+art));
						   		$('.cf'+art).removeClass("myarticle");
								$('.cf'+art).addClass("myarticle2");

								$('.myarticle2').draggable({ opacity: 0.7, helper: 'clone' }); 

								art++;
                       		}
					   });

$('#stock').droppable({ accept: ".myarticle2", 
                       activeClass: 'droppable-active', 
                       hoverClass: 'droppable-hover', 
                       drop: function(e, ui) { 
						   		ui.draggable.remove();
								$(ui.helper).remove();
                       		}
					   });
});
</script>
{/literal}