{if $thisOrder}
<input type="hidden" value="{$thisOrder.OXID}" name="isEdit" id="isEdit" />
{/if}
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td align="left" valign="top"><p><strong>Rechnung</strong></p>
      <p>
        <input type="checkbox" name="pay" id="pay" {if $thisOrder.OXSENDDATE != "0000-00-00 00:00:00"}checked{/if}>
      bereits bezahlt</p>
      <table width="95%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td width="150">Anrede</td>
          <td><select name="badd" id="badd">
            <option value="MR">Herr</option>
            <option value="MRS">Frau</option>
            <option>Firma</option>
          </select></td>
        </tr>
        <tr>
          <td width="150">Name</td>
          <td><input name="bfname" type="text" id="bfname" style="width:100%" value="{$thisOrder.OXBILLLNAME}"></td>
        </tr>
        <tr>
          <td width="150">Vorname</td>
          <td><input type="text" name="bname" id="bname" style="width:100%" value="{$thisOrder.OXBILLFNAME}"></td>
        </tr>
        <tr>
          <td width="150">Firma</td>
          <td><input type="text" name="bcorp" id="bcorp" style="width:100%" value="{$thisOrder.OXBILLCOMPANY}"></td>
        </tr>
        <tr>
          <td width="150">Strasse</td>
          <td><input type="text" name="bstr" id="bstr" style="width:85%" value="{$thisOrder.OXBILLSTREET}"> <input type="text" name="bnr" id="bnr" style="width:10%; float:right;" value="{$thisOrder.OXBILLSTREETNR}"></td>
        </tr>
        <tr>
          <td width="150">PLZ</td>
          <td><input type="text" name="bzip" id="bzip" style="width:100%" value="{$thisOrder.OXBILLZIP}"></td>
        </tr>
        <tr>
          <td width="150">Ort</td>
          <td><input type="text" name="bcity" id="bcity" style="width:100%" value="{$thisOrder.OXBILLCITY}"></td>
        </tr>
        <tr>
          <td width="150">Fon</td>
          <td><input type="text" name="bfon" id="bfon" style="width:100%" value="{$thisOrder.OXBILLFON}"></td>
        </tr>
        <tr>
          <td width="150">Fax</td>
          <td><input type="text" name="bfax" id="bfax" style="width:100%" value="{$thisOrder.OXBILLFAX}"></td>
        </tr>
        <tr>
          <td width="150">Land</td>
          <td><select name="bcountry" id="bcountry">
				{section name=c loop=$countries}
                	<option value="{$countries[c].OXISOALPHA2}">{$countries[c].OXTITLE}</option>
                {/section}
          </select></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><input type="text" name="email" id="email" style="width:100%" value="{$thisOrder.OXBILLEMAIL}"></td>
        </tr>
        <tr>
          <td>USt ID</td>
          <td><input type="text" name="bvatid" id="bvatid" style="width:100%" value="{$thisOrder.OXBILLUSTID}"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Versand</td>
          <td><select name="delivery" id="delivery">
              <option value="">-</option>          
				{section name=d loop=$delivery}
                	
            <option value="{$delivery[d].OXID}">{$delivery[d].OXTITLE}</option>
            
                {/section}
          
          </select></td>
        </tr>
        <tr>
          <td>Zahlungsart</td>
          <td><select name="payments" id="payments">
                <option value="">-</option>          
				{section name=d loop=$payments}
	            <option value="{$payments[d].OXID}">{$payments[d].OXDESC}</option>
                {/section}
          </select></td>
        </tr>
      </table>
</td>
    <td width="50%" align="left" valign="top"><p><strong>Lieferung</strong>
      </p>
      <p>
        <input name="isBill" type="checkbox" id="isBill" onClick="$('.delivery').toggle();" {if $thisOrder.OXDELCITY == ""}checked{/if}>
        wie Rechnung</p>
      <table class="delivery" width="95%" border="0" cellspacing="2" cellpadding="0"  {if $thisOrder.OXDELCITY == ""}style="display:none;"{/if}>
        <tr>
          <td width="150">Anrede</td>
          <td><select name="sadd" id="sadd">
            <option value="MR">Herr</option>
            <option value="MRS">Frau</option>
            <option>Firma</option>
          </select></td>
        </tr>
        <tr>
          <td width="150">Name</td>
          <td><input type="text" name="sname" id="sname" style="width:100%"  value="{$thisOrder.OXDELLNAME}"></td>
        </tr>
        <tr>
          <td width="150">Vorname</td>
          <td><input type="text" name="sfname" id="sfname" style="width:100%"  value="{$thisOrder.OXDELFNAME}"></td>
        </tr>
        <tr>
          <td width="150">Firma</td>
          <td><input type="text" name="scorp" id="scorp" style="width:100%"  value="{$thisOrder.OXDELCOMPANY}"></td>
        </tr>
        <tr>
          <td width="150">Strasse</td>
          <td><input type="text" name="sstr" id="sstr" style="width:85%"  value="{$thisOrder.OXDELSTREET}">
          <input type="text" name="snr" id="snr" style="width:10%;  float:right;"  value="{$thisOrder.OXDELSTREETNR}"></td>
        </tr>
        <tr>
          <td width="150">PLZ</td>
          <td><input type="text" name="szip" id="szip" style="width:100%"  value="{$thisOrder.OXDELZIP}"></td>
        </tr>
        <tr>
          <td width="150">Ort</td>
          <td><input type="text" name="scity" id="scity" style="width:100%" value="{$thisOrder.OXDELCITY}"></td>
        </tr>
        <tr>
          <td width="150">Fon</td>
          <td><input type="text" name="sfon" id="sfon" style="width:100%" value="{$thisOrder.OXDELFON}"></td>
        </tr>
        <tr>
          <td width="150">Fax</td>
          <td><input type="text" name="sfax" id="sfax" style="width:100%" value="{$thisOrder.OXDELFAX}"></td>
        </tr>
        <tr>
          <td width="150">Land</td>
          <td><select name="scountry" id="scountry">
            
				{section name=c loop=$countries}
                	
            <option value="{$countries[c].OXISOALPHA2}">{$countries[c].OXTITLE}</option>
            
                {/section}
          
          </select></td>
        </tr>
                <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
  </td>
  </tr>
  <tr>
    <td>
    <strong>Bestellte Waren</strong>
    <div id="basket" class="drop">
    {section name=oarticles loop=$thisOrder.articles}
    <div class="myarticle2">
    	<strong><a href="#" onclick="" >{$thisOrder.articles[oarticles].OXARTNUM}</a><br />
        {$thisOrder.articles[oarticles].OXTITLE}</strong><br />
    	Preis: {$thisOrder.articles[oarticles].OXPRICE|number_format:2:",":"."}
        <input type="hidden" name="OXID" class="OXID" value="{$thisOrder.articles[oarticles].OXARTID}" />     
    </div>
    {/section}
    </div>
    </td>
    <td width="50%">
    <strong>Shop Artikel</strong>
    <div id="stock" class="drop">
    {section name=articles loop=$articles}
    <div class="myarticle">
    	<strong><a href="#" onclick="" >{$articles[articles].OXARTNUM}</a><br />
        {$articles[articles].OXTITLE}</strong><br />
    	Preis: {$articles[articles].OXPRICE|number_format:2:",":"."}
        <input type="hidden" name="OXID" class="OXID" value="{$articles[articles].OXID}" />     
    </div>
    	{section name=articles2 loop=$articles[articles].mc}	
    	<div class="myarticle">
    		<strong><span style="color:#999;">{$articles[articles].OXARTNUM} / </span><a href="#" onclick="" >{$articles[articles].mc[articles2].OXARTNUM}</a><br />
            {$articles[articles].OXTITLE} {$articles[articles].mc[articles2].OXVARSELECT}</strong><br />
    		Preis: {$articles[articles].mc[articles2].OXPRICE|number_format:2:",":"."}
            <input type="hidden" name="OXID" class="OXID" value="{$articles[articles].mc[articles2].OXID}" />
    	</div> 
    	{/section} 
    {/section}
    </div>
    </td>
  </tr>
</table>
{if $thisOrder}
<p align="center">Orginal Bestellung 
  <select name="doOnEdit" id="doOnEdit">
  <option value="del">löschen</option>
  <option value="sto">stornieren</option>
  <option value="keep">beibehalten</option>
</select></p>
{/if}
        <p style="margin-bottom:20px;">
          <center>
            <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="speichern" onClick="submitMan();">Bestellung speichern</button>
          </center>
          <br />
</p>