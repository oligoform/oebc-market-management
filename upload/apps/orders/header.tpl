{literal}
<style>

html {
	overflow:auto !important;
	overflow-x:hidden !important;
	overflow-y:visible !important;
}
html, body {
	height:100%;
	width:100%;
	padding:0;
	margin:0;
}
.scrollable {
	height:350px;
	overflow:auto;
	margin:0;
	width:100%;	
	border: 1px solid #ccc;	
}
.scrollable2 {
	height:400px;
	overflow:auto;
	margin:0;	
	width:100%;
	border: 1px solid #ccc;
	padding:3px;		
}
#tabs2 {
	height:450px;
}
.xlsexport {
	height: 10px;
	width:200px;
	margin: 0 auto;	
}
.storno td {
	color:#CCC;	
}
.myorders {
	height: 100px;	
}
</style>

<link rel="stylesheet" type="text/css" href="/css/tables.css">

<script language="javascript" src="/javascript/jquery.dataTables.js"></script> 
<script language="javascript">

function packList() {
	
	if($("#pdate").val() == "") {
		alert("Datum fehlt!");
		return true;	
	}
	
	var pdate = $("#pdate").val()

	parent.spawngeneric("/?app=orders&subview=packlist&pdate=" + pdate, '', 500, 940);
}

function verset() {
	
	if($("#vdate").val() == "") {
		alert("Datum fehlt!");
		return true;	
	}
	
	var vdate = $("#vdate").val()
	$.post("/components/ajax.php", {	
			'mode'	   			: 	'csv',
			'thisapp'			: 	'orders',
			'vdate'				:	vdate
	},
	function(resdata){
		$("#vdown").html(resdata);
	});
}

function xlsExport() {
	
	if($("#mindate").val() == "") {
		alert("Min Datum fehlt!");
		return true;	
	}
	if($("#maxdate").val() == "") {
		alert("Max. Datum fehlt!");
		return true;	
	}
	
	$.post("/components/ajax.php", {	
			'mode'	   			: 	'writexls',
			'thisapp'			: 	'orders',
			'mindate'			:	$("#mindate").val(),
			'maxdate'			:	$("#maxdate").val()
	},
	function(resdata){
		$(".xlsexport").html(resdata);	
	});		
}

function setPayed(oid) {
	
	var date;
	
	if($("#pd_"+oid).html().trim() != "-") {
		date = "0";
	} else {
		date = '1';
	}
	
	$.post("/components/ajax.php", {	
			'mode'	   			: 	'setpay',
			'thisapp'			: 	'orders',
			'oid'				:	oid,
			'mdate'				:	date
	},
	function(resdata){
	
		if(date != "0") {
			$("#pi_"+oid).html('<img src="../../images/icons/coins.png" width="16" height="16">'); 
			$("#pd_"+oid).html(resdata);	
		} else {
			$("#pi_"+oid).html("");
			$("#pd_"+oid).html("-");
		}
	
	});
}

function setSent(oid) {
		var date;
	
	if($("#sd_"+oid).html().trim() != "-") {
		date = "0";
	} else {
		date = '1';
	}
	
	$.post("/components/ajax.php", {	
			'mode'	   			: 	'setpackage',
			'thisapp'			: 	'orders',
			'oid'				:	oid,
			'mdate'				:	date
	},
	function(resdata){
	
		if(date != "0") {
			$("#si_"+oid).html('<img src="../../images/icons/package.png" width="16" height="16">'); 
			$("#sd_"+oid).html(resdata);	
		} else {
			$("#si_"+oid).html("");
			$("#sd_"+oid).html("-");
		}
	
	});
}

function refundOrder(row) {
	var a = confirm("Wollen Sie diese Bestellung wirklich stornieren?");
	if(a) {	
		$("."+row).addClass("storno");
		$(".d_"+row).html("-");
		$.post("/components/ajax.php", {	
			'mode'	   			: 	'stornoorder',
			'thisapp'			: 	'orders',
			'oid'				:	row
		});
	}
}
function deleteOrder(row) {
	var a = confirm("Wollen Sie diese Bestellunf wirklich löschen?");
	if(a) {
		$("."+row).remove();
		$.post("/components/ajax.php", {	
			'mode'	   			: 	'deleteorder',
			'thisapp'			: 	'orders',
			'oid'				:	row
		});
	}
}

function orderList() {

	$.post("/components/ajax.php", {	
			'mode'	   			: 	'myorders',
			'thisapp'			: 	'orders',
			'oodate'			:	$("#oodate").val()
	},
	function(resdata){
		
	$("#myorders").html(resdata);	
			
	$('#table1').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"bStateSave": false,
					"iDisplayLength" : 50,
					"aaSorting": [[9,'asc']],			
					"bAutoWidth": false,
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_  Datensätzen",
						"sInfoEmpty": "-Keine-",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );
					
	$('#table2').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"bStateSave": false,
					"iDisplayLength" : 50,
					"aaSorting": [[9,'asc']],
					"bAutoWidth": false,
					"oLanguage": {
						"sLengthMenu": "Zeige _MENU_ Datensätze",
						"sZeroRecords": "Nothing found - sorry",
						"sInfo": "Zeige _START_ bis _END_ von _TOTAL_  Datensätzen",
						"sInfoEmpty": "-Keine-",
						"sInfoFiltered": "(filtered from _MAX_ total records)"
					}
					} );
	});
}


$(function() {
	$(".tabs2").tabs();	
	
	$('#oodate').datepicker({
							  userLang	: 'de',
							  dateFormat: 'yy-mm-dd',
	});
	
	$('#mindate').datepicker({
							  userLang	: 'de',
							  dateFormat: 'yy-mm-dd',
	});
	
	$('#maxdate').datepicker({
							  userLang	: 'de',
							  dateFormat: 'yy-mm-dd',
							  maxDate: 0
	});
	
	$('#pdate').datepicker({
							  userLang	: 'de',
							  dateFormat: 'yy-mm-dd',
							  maxDate: 0					  
	});
	
	$('#vdate').datepicker({
							  userLang	: 'de',
							  dateFormat: 'yy-mm-dd',
							  maxDate: 0					  
	});
	
	orderList();
				
});
</script>
{/literal}