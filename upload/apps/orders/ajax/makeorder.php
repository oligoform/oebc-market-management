<?
	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
/*
					'mode'	   		: 	'makeorder',
					'thisapp'		:	'orders',
					'arts'			:	oxids,
					'badd'			:	$("badd").val(),
					'sadd'			:	$("sadd").val(),
					'sname'			:	$("sname").val(),
					'bname'			:	$("bname").val(),
					'sfname'		:	$("sfname").val(),
					'bfname'		:	$("bfname").val(),
					'bcorp'			:	$("bcorp").val(),
					'scorp'			:	$("scorp").val(),
					'sstr'			:	$("sstr").val(),
					'bstr'			:	$("bstr").val(),
					'bnr'			:	$("bnr").val(),
					'snr'			:	$("snr").val(),
					'szip'			:	$("szip").val(),
					'bzip'			:	$("bzip").val(),
					'bcity'			:	$("bcity").val(),
					'scity'			:	$("scity").val(),
					'scountry'		:	$("scountry").val(),
					'bcountry'		:	$("bcountry").val(),
					'bvatid'		:	$("bvatid").val(),
					'pay'			:	$("pay").val(),
					'isBill'		:	$("isBill").val()
*/	

    function inMultiarray($elem, $array)
    {
        $top = sizeof($array) - 1;
        $bottom = 0;
        while($bottom <= $top)
        {
            if($array[$bottom] == $elem)
                return true;
            else
                if(is_array($array[$bottom]))
                    if(inMultiarray($elem, ($array[$bottom])))
                        return true;
                   
            $bottom++;
        }       
        return false;
    }
	
	$narts = array();
	
	if($_REQUEST["isBill"]) {
			$_REQUEST["sadd"] = "";
			$_REQUEST["sfname"] = "";
			$_REQUEST["sname"] = "";
			$_REQUEST["sstr"] = "";
			$_REQUEST["snr"] = "";
			$_REQUEST["scity"] = "";
			$_REQUEST["szip"] = "";
			$_REQUEST["scountry"] = "";
			$_REQUEST["scorp"] = ""; 
			$_REQUEST["sfax"] = "";
			$_REQUEST["sfon"] = "";
	}
	
	
	foreach($_REQUEST["arts"] as $art) {
		
		//echo "!?";
		
		$tmp = array();
		
		$tmp[0]	= $art;
		
		if(!inMultiarray($art, $narts)) { 	
		
			// push new
		
			$tmp[1]	= 1;
			array_push($narts,  $tmp);
		
		} else {
			
			//count up
			
			$i=0;
			while($narts[$i][0] != $art) {
				// do nothing
				$i++; 
			}
			$narts[$i][1] = $narts[$i][1] + 1;
			
		}	
	}	
						
	$ox = new oxid($db);
	$ox->createOrder(	$_REQUEST["badd"], $_REQUEST["bname"], $_REQUEST["bfname"], $_REQUEST["bstr"], $_REQUEST["bnr"], $_REQUEST["bcity"], $_REQUEST["bzip"], 
						$_REQUEST["bcountry"], $_REQUEST["bcorp"], $_REQUEST["bfon"], $_REQUEST["bfax"], $_REQUEST["sadd"], $_REQUEST["sname"], $_REQUEST["sfname"], 
						$_REQUEST["sstr"], $_REQUEST["snr"],
						$_REQUEST["scity"], $_REQUEST["szip"], $_REQUEST["scountry"], $_REQUEST["scorp"], $_REQUEST["sfon"], $_REQUEST["sfax"], $_REQUEST["email"], 
						"Via OEBC Backend", "EUR", 
						$_REQUEST["pay"], $_REQUEST["isBill"],	$_REQUEST["delivery"], $_REQUEST["payment"], $_REQUEST["bvatid"], $narts
					);	
					
	foreach($narts as $nart) {
	
		$parent = $ox->hasParent($nart[0]);
		
		//echo "parent:".$parent;
						
		$ox->correctStock($nart[0], $nart[1], $parent);				
	
	}

?>