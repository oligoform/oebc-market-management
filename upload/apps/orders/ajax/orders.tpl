    <table id="table1" width="100%" border="0" cellpadding="0" cellspacing="0"  >
      <thead>
        <tr>
          <th style="width:20px">&nbsp;</th>
          <th style="width:20px">&nbsp;</th>
          <th style="width:120px"><strong>Bestellnummer</strong></th>
          <th><strong>Name</strong></th>
          <th style="width:80px"><strong>Gesamtbetrag</strong></th>
          <th style="width:80px"><strong>Steuer</strong></th>
          <th style="width:80px"><strong>VK</strong></th>
          <th style="width:80px"><strong>Datum</strong></th>
          <th style="width:80px"><strong>Zahlung</strong></th>
          <th style="width:80px"><strong>Versand</strong></th>
          <th style="width:20px"><img src="../../images/icons/coins_add.png" width="16" height="16"></th>
          <th style="width:20px"><img src="../../images/icons/package_add.png" width="16" height="16"></th>
          <th style="width:20px">&nbsp;</th>
	      <th style="width:20px">&nbsp;</th>
          <th style="width:20px">&nbsp;</th>

        </tr>
      </thead>
      <tbody>
      
      {section name=orders loop=$orders}
      {if $orders[orders].OXSTORNO == 1}
      <tr class="storno {$orders[orders].OXID}">
      {else}
      <tr class="{$orders[orders].OXID}">
      {/if}
        <td><span id="pi_{$orders[orders].OXORDERNR}"> {if $orders[orders].OXPAID != "0000-00-00 00:00:00"}<img src="../../images/icons/coins.png" width="16" height="16">{/if} </span></td>
        <td><span id="si_{$orders[orders].OXORDERNR}"> {if $orders[orders].OXSENDDATE != "0000-00-00 00:00:00"}<img src="../../images/icons/package.png" width="16" height="16">{/if} </span></td>
        <td>{$orders[orders].OXORDERNR}</td>
        <td>{$orders[orders].OXBILLFNAME} {$orders[orders].OXBILLLNAME}</td>
        <td>{$orders[orders].OXTOTALBRUTSUM|number_format:2:",":"."} ({$orders[orders].OXTOTALNETSUM|number_format:2:",":"."})</td>
        <td>{$orders[orders].OXARTVATPRICE1|number_format:2:",":"."} ({$orders[orders].OXARTVAT1}%)</td>
        <td>{$orders[orders].OXDELCOST|number_format:2:",":"."}</td>
        <td>{$orders[orders].OXORDERDATE}</td>
        <td><span id="pd_{$orders[orders].OXORDERNR}"> {if $orders[orders].OXPAID != "0000-00-00 00:00:00"}{$orders[orders].OXPAID}{else}-{/if} </span></td>
        <td><span id="sd_{$orders[orders].OXORDERNR}"> {if $orders[orders].OXSENDDATE != "0000-00-00 00:00:00"}{$orders[orders].OXSENDDATE}{else}-{/if} </span></td>
        {if $orders[orders].OXSTORNO == 1}
        <td align="center">-</td>
        <td align="center">-</td>
        <td align="center">-</td>
        <td align="center">-</td>
		{else}
        <td class="d_{$orders[orders].OXID}"><input onclick="javascript:setPayed('{$orders[orders].OXORDERNR}')" type="checkbox" value="1" name="{$orders[orders].OXORDERNR}" {if $orders[orders].OXPAID != "0000-00-00 00:00:00"}checked{/if} ></td>
        <td class="d_{$orders[orders].OXID}"><input onclick="javascript:setSent('{$orders[orders].OXORDERNR}')"type="checkbox" value="1" name="{$orders[orders].OXORDERNR}" {if $orders[orders].OXSENDDATE != "0000-00-00 00:00:00"}checked{/if} ></td>
        <td class="d_{$orders[orders].OXID}" style="width:20px"><a href="#" onclick="javascript:parent.spawngeneric('/?app=orders&subview=manuell&edit={$orders[orders].OXID}', '', 500, 920);"><img src="../../images/icons/pencil.png" width="16" height="16"></a></td>
        <td class="d_{$orders[orders].OXID}" style="width:20px"><a href="#" onclick="javascript:refundOrder('{$orders[orders].OXID}');"><img src="../../images/icons/money_delete.png" width="16" height="16"></a></td>
      	{/if}
        <td style="width:20px"><a href="#" onclick="javascript:deleteOrder('{$orders[orders].OXID}');"><img src="../../images/icons/cross.png" width="16" height="16"></a></td>
      </tr>
      {/section}
      </tbody>
      
    </table>