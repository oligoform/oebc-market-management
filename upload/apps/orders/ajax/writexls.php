<?

	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	$ox = new oxid($db);    
    
	require_once dirname(__FILE__)."/../classes/writeexcel/class.writeexcel_workbook.inc.php";
	require_once dirname(__FILE__)."/../classes/writeexcel/class.writeexcel_worksheet.inc.php";

	$myfile = 'steuer_export_'.time().'.xls';

	$fname = dirname(__FILE__).'/../../../filesystem/'.$myfile;
	$workbook = &new writeexcel_workbook($fname);
	$worksheet = &$workbook->addworksheet();
	
	// header
	$format =& $workbook->addformat(array(
                                        'fg_color' => 42,
                                        'pattern'  => 1,
                                     ));							 
	$sumary =& $workbook->addformat(array(
                                        'fg_color' => 46,
                                        'pattern'  => 1,
                                     ));										 

									 
	$worksheet->set_column(0, 10, 30);								 
	
	$worksheet->write(0,0,  "Datum", $format);
	$worksheet->write(0,1,  "Käufer", $format);
	$worksheet->write(0,2,  "ArtNr.", $format);
	$worksheet->write(0,3,  "Artikel", $format);
	$worksheet->write(0,4,  "Variante", $format);	
	$worksheet->write(0,5,  "Netto", $format);
	$worksheet->write(0,6,  "Brutto", $format);
	$worksheet->write(0,7,  "Steuranteil", $format);	
	$worksheet->write(0,8,  "Netto Gesamt", $format);
	$worksheet->write(0,9,  "Brutto Gesamt", $format);
	
	$line = 1;
	
	$orders = $ox->getOrders(0, $_REQUEST["mindate"], $_REQUEST["maxdate"]); 
	
	//echo $_REQUEST["mindate"];
	//echo $_REQUEST["maxdate"];
		
	foreach($orders as $order)
	{
		
		$worksheet->write($line,0,  $order["OXORDERDATE"], $sumary);
		$worksheet->write($line,1,  $order["OXBILLCOMPANY"].", ".$order["OXBILLFNAME"].", ".$order["OXBILLLNAME"], $sumary);
		$worksheet->write($line,2,  "", $sumary);
		$worksheet->write($line,3,  "", $sumary);
		$worksheet->write($line,4,  "", $sumary);	
		$worksheet->write($line,5,  "", $sumary);
		$worksheet->write($line,6,  "", $sumary);
		$worksheet->write($line,7,  "", $sumary);	
		$worksheet->write($line,8,  number_format($order["OXTOTALNETSUM"], 2, ",", "."),$sumary);
		$worksheet->write($line,9,  number_format($order["OXTOTALBRUTSUM"], 2, ",", "."), $sumary);
		
		$line++;
		foreach($order["articles"] as $art) 
		{
			
			$worksheet->write($line,2, $art["OXARTNUM"]);
			$worksheet->write($line,3, $art["OXTITLE"]);
			$worksheet->write($line,4, $art["OXSELVARIANT"]);
			$worksheet->write($line,5, number_format($art["OXNETPRICE"], 2, ",", "."));	
			$worksheet->write($line,6, number_format($art["OXBRUTPRICE"], 2, ",", "."));
			$worksheet->write($line,7, number_format($art["OXVATPRICE"], 2, ",", "."));
		
			$line++;
		}
		$line++;
	}
	
	echo "Der Export wurde im Dateimanager bereit gestellt<br />Dateiname: ".$myfile;
	
	
	$workbook->close();
	
?>