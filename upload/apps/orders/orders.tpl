<div class="tabs2" style="border:0; margin:0; min-height:450px;">
  <ul>
    <li><a href="#tabs-all">Alle Bestellungen</a></li>
    <li><a href="#tabs-pack">Packlisten Export</a></li>
    <li><a href="#tabs-vers">Versandetiketten</a></li>
    <li><a href="#tabs-steuer">Steuer Bibiliothek</a></li>
  </ul>
  <div id="tabs-all">
     <div style="margin: 5px auto; width:500px; text-align:center;">Bestellungen anzeigen für:
      <input name="oodate" type="text" id="oodate" size="10" maxlength="10" readonly="readonly" value="{$smarty.now|date_format:"%Y"}-{$smarty.now|date_format:"%m"}-{$smarty.now|date_format:"%d"}" >
      <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="speichern" onClick="orderList();" style="height:20px; margin:0; padding:0;">Bestellungen anzeigen</button>
    </div>
    <div id="myorders">
      <center>
        Lade Daten...<br/>
        <img src="/images/system/load.gif" />
      </center>
    </div>

    <p align="center"><a  href="#" onclick="javascript:parent.spawngeneric('/?app=orders&subview=manuell', '', 500, 920);">Bestellung manuell eingeben</a> | <a href="javascript:location.reload()">Ansicht neu laden</a></p>
  </div>
  <div id="tabs-pack">
    <div class="ui-widget">
      <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Hinweis:</strong> Der Packlisten Export exportiert Ihnen die am ausgewählten Tag zum Versand markeierten Aritkel.</p>
      </div>
    </div>
    <div style="margin: 20px auto; width:500px; text-align:center;">Packlisten erstellen für
      <input name="pdate" type="text" id="pdate" size="10" maxlength="10" readonly="readonly">
      <br />
      <em>(Datum an dem der Artikel als Versandfertig markiert wurde)</em>
<p style="margin-bottom:20px;">
        <center>
          <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="speichern" onClick="packList();">Jetzt exportieren</button>
        </center>
        <br />
      </p>
    </div>
  </div>
  <div id="tabs-vers">
    <div class="ui-widget">
      <div class="ui-state-highlight ui-corner-all" style="padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Hinweis:</strong> Der Etiketten Export exportiert Ihnen die am ausgewählten Tag zum Versand markierten Aritkel. Für einen täglichen Export der gesamten Bestellungen steht Ihnen weiterhin der generische Export zur Verfügung.</p>
      </div>
    </div>
    <div style="margin: 20px auto; width:500px; text-align:center;">Versandetiketten CSV erstellen für
      <input name="vdate" type="text" id="vdate" size="10" maxlength="10" readonly="readonly">
      <br />
      <em>(Datum an dem der Artikel als Versandfertig markiert wurde)</em>
<p style="margin-bottom:20px;">
		<div id="vdown"></div>
        <center>
          <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="speichern" onClick="verset();">Jetzt exportieren</button>
        </center>
        <br />
      </p>
    </div>
  </div>
  <div id="tabs-steuer">
    <div style="margin: 0 auto; width:300px; text-align:center;">
      <p>Export XLS File der Bestellungen</p>
      von
      <input name="mindate" type="text" id="mindate" size="10" maxlength="10" readonly="readonly">
      bis
      <input name="maxdate" type="text" id="maxdate" size="10" maxlength="10" readonly="readonly">
    </div>
    <div class="xlsexport"></div>
    <p>&nbsp; </p>
    <p style="margin-bottom:20px;">
      <center>
        <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="speichern" onClick="xlsExport();">Jetzt exportieren</button>
      </center>
      <br />
    </p>
  </div>
</div>
