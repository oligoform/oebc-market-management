<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../../tmp/';
	$smarty->template_dir = dirname(__FILE__).'/../';

	if($_REQUEST["action"] == "settings") {

		$files->setOpt("shopname",$db->clean($_REQUEST["shopname"]));
		//$files->setOpt("url",$db->clean($_REQUEST["s3"]));	

		//$files->setOpt("title",$db->clean($_REQUEST["s1"]));
		//$files->setOpt("publisher",$db->clean($_REQUEST["s2"]));
		$files->setOpt("url",$db->clean($_REQUEST["s3"]));

		$files->setOpt("iconzoom",$db->clean($_REQUEST["iconzoom"]));
		
		$files->setOpt("shopftp",$db->clean($_REQUEST["shopftp"]));
		$files->setOpt("shopftpuser",$db->clean($_REQUEST["shopftpuser"]));
		$files->setOpt("shopftppw",$db->clean($_REQUEST["shopftppw"]));	
		$files->setOpt("shopftpverz",$db->clean($_REQUEST["shopftpverz"]));

		header("Location: http://". $_SERVER['SERVER_NAME']."/?app=settings");
		exit(0);	
	}

	$smarty->assign("shopurl", $shopurl);
	$smarty->assign("thisserver", $_SERVER['SERVER_NAME']);

	$ch = @curl_init("http://oebc.de/newsfeed.xml"); 
	curl_setopt($ch,CURLOPT_HEADER,0);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
	curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$news = @curl_exec($ch);
	
	$responseDoc = new DomDocument();
	$responseDoc->loadXML($news);
	
	$newsitems = $responseDoc->getElementsByTagName('news');
		
	$pubnews = array();
		
	foreach($newsitems as $newsitem) {
			$item = array();
			$item["topic"] = $newsitem->getElementsByTagName('topic')->item(0)->nodeValue;
			$item["message"] = $newsitem->getElementsByTagName('message')->item(0)->nodeValue;	
			$item["link"] = $newsitem->getElementsByTagName('link')->item(0)->nodeValue;	
			array_push($pubnews, $item);			
		}	
	
	$smarty->assign("news", $pubnews);

	// Dashboard
	$smarty->assign("mysqlversion", $db->version());
	$smarty->assign("phpversion", phpversion());

	$loadresult = @exec('/usr/bin/uptime');
	preg_match("/averages?: ([0-9\.]+),[\s]+([0-9\.]+),[\s]+([0-9\.]+)/",$loadresult,$avgs);

	$smarty->assign("serverload", $avgs[2]);
	
	/* desktop parameter */
	
	$shopurl = $files->getOpt("url");
	$smarty->assign("shopname", $files->getOpt("shopname"));
	
	$smarty->assign("shopurl", $shopurl);
	$smarty->assign("s3", $files->getOpt("url"));

	$smarty->assign("iconzoom", $files->getOpt("iconzoom"));
	
	$smarty->assign("shopftp", $files->getOpt("shopftp"));
	$smarty->assign("shopftpuser", $files->getOpt("shopftpuser"));
	$smarty->assign("shopftppw", $files->getOpt("shopftppw"));
	$smarty->assign("shopftpverz", $files->getOpt("shopftpverz"));
	
	$template->assign("bgkind", $files->getOpt("bgkind"));
	$smarty->assign("flickr", $files->getOpt("flickr"));
	$smarty->assign("bgimage", $files->getOpt("bgimage"));
	$smarty->assign("bgcolor", $files->getOpt("bgcolor"));
    
	$content = $smarty->fetch('settings/settings.tpl');
	$template->assign("content",$content);
?>