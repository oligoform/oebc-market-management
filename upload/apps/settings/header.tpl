{literal}
<style>
html {
	overflow:auto !important;
	overflow-x:hidden !important;
	overflow-y:visible !important;
}
html, body {
	height:100%;
	width:100%;
	padding:0;
	margin:0;
}
</style>

<script language="javascript" src="/javascript/color.js"></script>
<link href="/css/snippets/colorpicker.css" rel="stylesheet" type="text/css" />

<script language="javascript">

function saveDeskSet() {
	
	var iconzoom = $('#iconzoom:checked').val();
	
	if(iconzoom == "undefined") {
		iconzoom = "";	
	}
	
	$.post("/components/ajax.php", {	
					'mode'	   			: 	'savedeskset',
					'thisapp'			: 	'settings',
					'bgkind'			: 	$("#bgkind").val(),
					'flickr'			: 	$("#flickr").val(),	
					'bgcolor'			: 	$("#swatch").css("background-color"),
					'bgimage'			:	$("#bgimage").val(),
					'iconzoom'			:	iconzoom
	},
			function(resdata){
				$("#desksetres").html(resdata);
 			});
			
	return true;	
}

$(function() {
	{/literal}	
	$("#bgkind").val("{$bgkind}");
	{literal}
	$(".tabs2").tabs();	
});
</script>
{/literal}
