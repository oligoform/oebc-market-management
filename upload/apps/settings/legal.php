<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #999;
}
</style>
<p><strong>OEBC und alle von PBT Media entwickelten Applikationen</strong></p>
<p>PBT Media/Alexander Pick (Lizenzgeber) gewährt dem Kunden (Lizenznehmer) ein entgeltliches, zeitlich unbegrenztes, nicht ausschließliches Nutzungsrecht (Lizenz) and der Software OEBC (Software). Einsicht in den Quellcode gehört nicht zum Umfang der Lizenz.</p>
<p>Die Lizenz berechtigt den Lizenznehmer zur Einzelnutzung der Software im Rahmen des normalen Gebrauchs. Dies umfasst eine Installation auf einem dafür vorgesehenen Server, dass anfertigen einer Sicherheitskopie, sowie das Laden der Software in den Arbeitsspeicher. Andere Arten der Nutzung sind über diese Lizenz nicht gedeckt. Der Lizenznehmer darf insbesondere keine weitere Vervielfältigung, auch nicht teilweise oder vorübergehend, der Software vornehmen, gleich welcher Art oder mit welchen Mitteln. Modifikationen, zu denen der Lizenzgeber nach Treu und Glauben die Zustimmung nicht verweigern kann, sind statthaft (§39.2 UrhG). </p>
<p>Rückübersetzungen des überlassenen Programmcodes in andere Codeformen (Dekompilierung) und sonstige Arten der Rückerschließung der verschiedenen Herstellungsstufen der Software (Reverse-Engineering) ist nicht gestattet. Sollten Schnittstelleninformationen für die Herstellung der Interoperabilität eines unabhängig geschaffenen Computerprogramms erforderlich sein, so können diese - soweit es dem Lizenzgeber technisch möglich ist - gegen Erstattung eines geringen Kostenbeitrags bei dem Lizenzgeber oder einem von ihm zu benennenden Dritten angefordert werden. </p>
<p>Die zur Nutzung überlassene Software darf als einzelne Installation auf einem Server, im Netzwerk von mehreren Rechnern als Client genutzt werden. </p>
<p>Der Lizensnehmer darf die Software weder vermieten noch verleihen.&nbsp; Der Lizenznehmer darf - vorbehaltlich abweichender Bestimmungen in dieses Software-Lizenzvertrages - einzelne Rechte aus diesem Vertrag sowie den Vertrag im Ganzen nicht auf Dritte übertragen, es sei denn der Lizenzgeber erteilt hierzu ausdrücklich seine schriftliche Zustimmung. Der Lizenznehmer ist nicht berechtigt, eine Kopien der Software, egal welcher Art, zurückzubehalten. Die § 69d und §69e UrhG bleiben unberührt.</p>
<p>Das Eigentum und das Urheberrecht an dem Softwareprodukt (einschließlich irgendwelcher Bilder, Photographien, Animationen, Video, Musik, Text und &ldquo;Widgets&rdquo;, die im Softwareprodukt enthalten sind), dem Begleitmaterial und sämtliche Kopien liegen beim Lizensgeber oder dessen Lieferanten. </p>
<p>Das Softwareprodukt ist durch Urheberrechtsgesetze, Bestimmungen internationaler Verträge und Rechtsvorschriften gegen Kopieren geschützt.</p>
<p><em>Gewährleistung</em></p>
<p>Der Lizenzgeber gewährleistet das die Software mit der Spezifikation in der dazugehörigen Dokumentation übereinstimmt (§ 434ff BGB) und mit der gebotenen Sorgfalt und Fachkenntnis erstellt wurde. Dennoch ist nach dem derzeitigen Stand der Technik der völlige Ausschluss von Fehlern nicht möglich.</p>
<p>Fehler die die Nutzung gemäß der Spezifikation nicht unerheblich beeinflussen, wird der Lizenzgeber berichtigen. Dies kann entweder durch eine neue Software Version oder einen Hinweis zur Beseitigung bzw. Umgehung des Fehlers geschehen. Der Lizenznehmer ist verpflichtet eine im Rahmen der Korrektur angebotene neue Version der Software zu übernehmen, es sei denn dies führt zu unzumutbaren Problemen.</p>
<p>Der Lizenznehmer hat das Recht, bei Fehlschlagen der Fehlerkorrektur eine Herabsetzung der Lizenzgebühr zu verlangen oder vom Vertrag zurückzutreten. Bei einem Rücktritt vom Vertrag wird der Lizenznehmer alle Kopien der Software und der Dokumentation vernichten.</p>
<p>Die Gewährleistungsfrist beträgt 12 Monate ab Lieferung.</p>
<p><em>Haftung</em></p>
<p>Der Lizenzgeber haftet unbeschränkt nur für Vorsatz und grobe Fahrlässigkeit. Der Lizenzgeber haftet für einfache Fahrlässigkeit dem Grunde nach nur, sofern eine Pflicht verletzt wird, deren Einhaltung für die Erreichung des Vertragszwecks von besonderer Bedeutung ist (&quot;Kardinalpflicht&quot;). Für einfache Fahrlässigkeit haftet der Lizenzgeber in der Höhe begrenzt auf den vertragstypisch vorhersehbaren Schaden. Für die Fälle der anfänglichen Unmöglichkeit haftet der Lizenzgeber nur, wenn ihm das Leistungshindernis bekannt oder infolge grober Fahrlässigkeit unbekannt geblieben war.</p>
<p>Der Lizenzgeber haftet nicht für die Wiederbeschaffung von Daten, sofern er deren Vernichtung nicht grob fahrlässig oder vorsätzlich verursacht hat und der Lizenznehmer sichergestellt hat, dass diese mit wenig Aufwand wieder rekonstruiert werden können.</p>
<p>Die Verjährungsfrist für Ansprüche auf Schadenersatz gegen den Lizenzgeber beträgt 6 Monate gerechnet ab dem gesetzlichen Verjährungsbeginn. Die vorstehenden Haftungsbegrenzungen gelten nicht für Ansprüche nach dem Produkthaftungsgesetz sowie bei Schäden aus der Verletzung des Lebens, des Körpers oder der Gesundheit. Die Haftung des Lizenzgebers im Falle einer vertragswidrigen Nutzung durch den Lizenznehmer wird ausgeschlossen.</p>
<p>Dem Lizenznehmer ist bekannt, dass er im Rahmen seiner Pflicht zur Schadensminderung eine regelmäßige Sicherung (Backup) durchzuführen hat, und im Fall eines vermuteten Softwarefehlers alle zumutbaren, möglichen Sicherheitsmaßnahmen zu treffen hat.<br />
  Sonstiges</p>
<p><em>Sonstiges</em></p>
<p> Dieser Lizenzvertrag unterliegt dem Recht der Bundesrepublik Deutschland. Die Anwendung des &quot;Einheitlichen Gesetzes über den internationalen Kauf beweglicher Sachen&quot; und des &quot;Einheitlichen Gesetzes über den Abschluss internationaler Kaufverträge&quot; sowie des &quot;Übereinkommens der Vereinten Nationen über Verträge über den internationalen Warenkauf&quot; werden ausgeschlossen. </p>
<p>Gerichtsstand für alle sich im kaufmännischen Verkehr aus dem Vertragsverhältnis ergebenden Streitigkeiten, einschließlich Scheck-, Wechsel- und Urkundenprozesse, ist Bayreuth. Der Lizenzgeber kann den Lizenznehmer auch an dessen Sitz gerichtlich in Anspruch nehmen.<br />
  Nebenabreden sind nicht getroffen. Änderungen dieses Software-Lizenzvertrages bedürfen der Textform. Gleiches gilt für die Aufhebung der Textformklausel.</p>
<p>Für den Fall, dass Bestimmungen dieses Lizenzvertrages ganz oder teilweise unwirksam sind oder werden, berührt dies die Wirksamkeit des Lizenzvertrages im übrigen nicht.</p>
<p><strong>JQuery</strong></p>
<p>Copyright (c) 2010 John Resig, http://jquery.com/</p>
<p>Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  &quot;Software&quot;), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </p>
<p><strong>JQuery UI</strong></p>
<p>Copyright (c) 2010 Paul Bakaus, http://jqueryui.com/  </p>
<p>This software consists of voluntary contributions made by many individuals (AUTHORS.txt, http://jqueryui.com/about) For exact contribution history, see the revision history and logs, available at http://jquery-ui.googlecode.com/svn/  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the &quot;Software&quot;), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  </p>
<p>THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </p>
<p><strong>JQuery Backstretch</strong></p>
<p>Copyright (c) 2010 Scott Robbin, http://jquery.com/</p>
<p>Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  &quot;Software&quot;), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.</p>
<p>THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. </p>
<p><strong>Smarty</strong></p>
<p>GNU LESSER GENERAL PUBLIC LICENSE</p>
<p>Version 3, 29 June 2007</p>
<p>Copyright © 2007 Free Software Foundation, Inc. &lt;http://fsf.org/&gt;</p>
<p>Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.</p>
<p>This version of the GNU Lesser General Public License incorporates the terms and conditions of version 3 of the GNU General Public License, supplemented by the additional permissions listed below.<br />
  0. Additional Definitions.</p>
<p>As used herein, &quot;this License&quot; refers to version 3 of the GNU Lesser General Public License, and the &quot;GNU GPL&quot; refers to version 3 of the GNU General Public License.</p>
<p>&quot;The Library&quot; refers to a covered work governed by this License, other than an Application or a Combined Work as defined below.</p>
<p>An &quot;Application&quot; is any work that makes use of an interface provided by the Library, but which is not otherwise based on the Library. Defining a subclass of a class defined by the Library is deemed a mode of using an interface provided by the Library.</p>
<p>A &quot;Combined Work&quot; is a work produced by combining or linking an Application with the Library. The particular version of the Library with which the Combined Work was made is also called the &quot;Linked Version&quot;.</p>
<p>The &quot;Minimal Corresponding Source&quot; for a Combined Work means the Corresponding Source for the Combined Work, excluding any source code for portions of the Combined Work that, considered in isolation, are based on the Application, and not on the Linked Version.</p>
<p>The &quot;Corresponding Application Code&quot; for a Combined Work means the object code and/or source code for the Application, including any data and utility programs needed for reproducing the Combined Work from the Application, but excluding the System Libraries of the Combined Work.<br />
  1. Exception to Section 3 of the GNU GPL.</p>
<p>You may convey a covered work under sections 3 and 4 of this License without being bound by section 3 of the GNU GPL.<br />
  2. Conveying Modified Versions.</p>
<p>If you modify a copy of the Library, and, in your modifications, a facility refers to a function or data to be supplied by an Application that uses the facility (other than as an argument passed when the facility is invoked), then you may convey a copy of the modified version:</p>
<p> * a) under this License, provided that you make a good faith effort to ensure that, in the event an Application does not supply the function or data, the facility still operates, and performs whatever part of its purpose remains meaningful, or<br />
  * b) under the GNU GPL, with none of the additional permissions of this License applicable to that copy.</p>
<p>3. Object Code Incorporating Material from Library Header Files.</p>
<p>The object code form of an Application may incorporate material from a header file that is part of the Library. You may convey such object code under terms of your choice, provided that, if the incorporated material is not limited to numerical parameters, data structure layouts and accessors, or small macros, inline functions and templates (ten or fewer lines in length), you do both of the following:</p>
<p> * a) Give prominent notice with each copy of the object code that the Library is used in it and that the Library and its use are covered by this License.<br />
  * b) Accompany the object code with a copy of the GNU GPL and this license document.</p>
<p>4. Combined Works.</p>
<p>You may convey a Combined Work under terms of your choice that, taken together, effectively do not restrict modification of the portions of the Library contained in the Combined Work and reverse engineering for debugging such modifications, if you also do each of the following:</p>
<p> * a) Give prominent notice with each copy of the Combined Work that the Library is used in it and that the Library and its use are covered by this License.<br />
  * b) Accompany the Combined Work with a copy of the GNU GPL and this license document.<br />
  * c) For a Combined Work that displays copyright notices during execution, include the copyright notice for the Library among these notices, as well as a reference directing the user to the copies of the GNU GPL and this license document.<br />
  * d) Do one of the following:<br />
  o 0) Convey the Minimal Corresponding Source under the terms of this License, and the Corresponding Application Code in a form suitable for, and under terms that permit, the user to recombine or relink the Application with a modified version of the Linked Version to produce a modified Combined Work, in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.<br />
  o 1) Use a suitable shared library mechanism for linking with the Library. A suitable mechanism is one that (a) uses at run time a copy of the Library already present on the user's computer system, and (b) will operate properly with a modified version of the Library that is interface-compatible with the Linked Version.<br />
  * e) Provide Installation Information, but only if you would otherwise be required to provide such information under section 6 of the GNU GPL, and only to the extent that such information is necessary to install and execute a modified version of the Combined Work produced by recombining or relinking the Application with a modified version of the Linked Version. (If you use option 4d0, the Installation Information must accompany the Minimal Corresponding Source and Corresponding Application Code. If you use option 4d1, you must provide the Installation Information in the manner specified by section 6 of the GNU GPL for conveying Corresponding Source.)</p>
<p>5. Combined Libraries.</p>
<p>You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice, if you do both of the following:</p>
<p> * a) Accompany the combined library with a copy of the same work based on the Library, uncombined with any other library facilities, conveyed under the terms of this License.<br />
  * b) Give prominent notice with the combined library that part of it is a work based on the Library, and explaining where to find the accompanying uncombined form of the same work.</p>
<p>6. Revised Versions of the GNU Lesser General Public License.</p>
<p>The Free Software Foundation may publish revised and/or new versions of the GNU Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.</p>
<p>Each version is given a distinguishing version number. If the Library as you received it specifies that a certain numbered version of the GNU Lesser General Public License &quot;or any later version&quot; applies to it, you have the option of following the terms and conditions either of that published version or of any later version published by the Free Software Foundation. If the Library as you received it does not specify a version number of the GNU Lesser General Public License, you may choose any version of the GNU Lesser General Public License ever published by the Free Software Foundation.</p>
<p>If the Library as you received it specifies that a proxy can decide whether future versions of the GNU Lesser General Public License shall apply, that proxy's public statement of acceptance of any version is permanent authorization for you to choose that version for the Library.</p>
