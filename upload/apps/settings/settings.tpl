<div class="tabs2"  style="border:0; margin:0; min-height:450px;">
  <ul>
    <li><a href="#tabs-EB">Shop</a></li>
    <li><a href="#tabs-DK">Desktop</a></li>
    <li><a href="#tabs-AB">Über OEBC</a></li>
  </ul>
  <div id="tabs-EB">
    <h4>Grundeinstellungen</h4>
    <form id="settings" name="settings" method="post" action="index.php">
      <input type="hidden" name="mode" value="adm" />
      <input type="hidden" name="action" value="settings" />
      <table width="880" border="0" cellpadding="0" cellspacing="10">
        <tr>
          <td width="138" align="left" valign="top">Shop Name</td>
          <td width="632"><input name="shopname" type="text" id="shopname" style="width:630px" value="{$shopname}" /></td>
        </tr>
        <tr>
          <td width="138" align="left" valign="top">Shop URL</td>
          <td width="632"><input name="s3" type="text" id="s3" style="width:630px" value="{$s3}" /></td>
        </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="top">Shop FTP Host</td>
            <td><input name="shopftp" type="text" id="shopftp" style="width:600px" value="{$shopftp}" /></td>
          </tr>
          <tr>
            <td align="left" valign="top">Shop FTP User</td>
            <td><input name="shopftpuser" type="text" id="shopftpuser" style="width:600px" value="{$shopftpuser}" /></td>
          </tr>
          <tr>
            <td align="left" valign="top">Shop FTP Passwort</td>
            <td><input name="shopftppw" type="password" id="shopftppw" style="width:600px" value="{$shopftppw}" /></td>
          </tr>
          <tr>
            <td align="left" valign="top">Shop htdocs Verzeichnis</td>
            <td><input name="shopftpverz" type="text" id="shopftpverz" style="width:600px" value="{$shopftpverz}" /></td>
          </tr>
      </table>
      <p style="margin-bottom:20px;">
        <center>
          <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="anbieten">Einstellungen speichern</button>
        </center>
      </p>
    </form>
  </div>
  <div id="tabs-DK"> <span id="desksetres">&nbsp;</span>
    <table width="100%" border="0" cellspacing="5" cellpadding="0">
      <tr>
        <td width="150" align="left" valign="top">Hintergrund Bild</td>
        <td><select name="bgkind" id="bgkind" style="width:630px">
            <option value="1">Hintergrundbild von URL</option>
            <option value="2">Flickr RSS 2.0 Feed</option>
            <option value="3">Solid Color</option>
          </select></td>
      </tr>
      <tr>
        <td width="150" align="left" valign="top">Flickr RSS 2.0</td>
        <td><input name="flickr" type="text" id="flickr" value="{$flickr}" size="42" style="width:630px"></td>
      </tr>
      <tr>
        <td width="150" align="left" valign="top">Hintergundbild</td>
        <td><input name="bgimage" type="text" id="bgimage" value="{$bgimage}" size="42" style="width:630px"></td>
      </tr>
      <tr>
        <td width="150" align="left" valign="top">Hintergrund Farbe</td>
        <td><div id="red"></div>
          <div id="green"></div>
          <div id="blue"></div>
          <div id="swatch" class="ui-widget-content ui-corner-all"></div></td>
      </tr>
      <tr>
      <td>Icon Zoom</td>
      <td><input type="checkbox" name="iconzoom" id="iconzoom" {if $iconzoom}checked{/if} /></td>
      </tr>
    </table>
    <p align="center">
      <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="speichern" onclick="saveDeskSet()";>Speichern</button>
    </p>
  </div>
  <div id="tabs-AB">
    <div style="padding:10px;"> {section name=news loop=$news}
      <table width="880" border="0" cellspacing="5" cellpadding="3" style="border:1px solid #DDD;" >
        <tr>
          <td><strong>{$news[news].topic}</strong></td>
        </tr>
        <tr>
          <td>{$news[news].message}</td>
        </tr>
        <tr>
          <td><a href="{$news[news]}.link" target="_blank">mehr...</a></td>
        </tr>
      </table>
      <br />
      {/section}
      </p>
      <p> <strong>Systemeigenschaften</strong>
      <table width="880" border="0">
        <tr>
          <td><strong>OEBC Version</strong></td>
          <td>0.5</td>
          <td><strong>SQL Engine</strong></td>
          <td>{$mysqlversion}</td>
        </tr>
        <tr>
          <td><strong>PHP Version</strong></td>
          <td>{$phpversion} </td>
          <td><strong>Server Load</strong></td>
          <td>{$serverload}</td>
        </tr>
      </table>
      <br  />
      </p>
    </div>
  </div>
</div>
</div>
