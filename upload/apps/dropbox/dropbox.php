<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	/*
		Dropbox
	*/

	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}

	if($_REQUEST["dpasswd"] && $_REQUEST["duser"]) {
		$files->setOpt("dpasswd", $db->clean($_REQUEST["dpasswd"]));
		$files->setOpt("duser", $db->clean($_REQUEST["duser"]));	
	}

	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../../tmp/';
	$smarty->template_dir = dirname(__FILE__).'/../';
	
	$smarty->assign("dpasswd", $dpasswd);
	$smarty->assign("duser", $duser); 
	
	include(dirname(__FILE__).'/classes/Dropbox/autoload.php');
	
	$consumerKey = 'gjesweand917xq0';
	$consumerSecret = 'klsfw00rdkv0bnk';

	$dpasswd = $files->getOpt("dpasswd");
	$duser = $files->getOpt("duser");
	
	$oauth = new Dropbox_OAuth_PEAR($consumerKey, $consumerSecret);
	
	$dropbox = new Dropbox_API($oauth);

	$tokens = $dropbox->getToken($duser, $dpasswd); 
	
	if(empty($tokens['token'])) {
		$smarty->assign("needconfig", "1"); 
	}
	
	//print_r($rfiles);
	
	//var_dump($cms);

	//Content erzeugen und bereitstellen
	$content = $smarty->fetch('dropbox/dropbox.tpl');
	$template->assign("content",$content);

?>