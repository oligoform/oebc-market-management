<?	
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}

	$consumerKey = 'gjesweand917xq0';
	$consumerSecret = 'klsfw00rdkv0bnk';

	$dpasswd = $files->getOpt("dpasswd");
	$duser = $files->getOpt("duser");

	include(dirname(__FILE__).'/../classes/Dropbox/autoload.php');

	$oauth = new Dropbox_OAuth_PEAR($consumerKey, $consumerSecret);
	
	$dropbox = new Dropbox_API($oauth);

	$tokens = $dropbox->getToken($duser, $dpasswd); 

	//Note that it's wise to save these tokens for re-use.
	$oauth->setToken($tokens);	
	
	$file = trim(strip_tags($_REQUEST["file"]));
	
	// sanatize path
	if($_REQUEST["path"] != "/") {
		$path = $_REQUEST["path"]."/";
	} else {
		$path = $_REQUEST["path"];
	}
	
	$lfile = dirname(__FILE__).'/../../../filesystem/'.$_REQUEST["lpath"]."/".$file;
	
	$rfiles  = $dropbox->getMetaData($path);
	
	// check if exists
	$set = 1;
	$i = 0;
	
	$ufile = $file;
		
	while($set) {
	
		if(!empty($rfiles["contents"])) {
	
		foreach($rfiles["contents"] as $cfile) {
					
			if($cfile["path"] == $path.$file) {
				$i++;
				$file = $i."_".$ufile;
				$set = 1;				
				break;	
			}
			
			echo $cfile["path"];
			$set = 0;
		}	
		} else {
			$set = 0;	
		}
	}
	// is lfile a directory?
	if(!is_dir($lfile)) {
		$rfiles  = $dropbox->putFile($path.$file, $lfile);
	} else {
		$dropbox->createFolder($path.$file);	
	}
	
	if($_REQUEST["do"] == "move") {
		unlink(dirname(__FILE__).'/../../../filesystem'.$_REQUEST["lpath"]."/".$file);	
	}

?>