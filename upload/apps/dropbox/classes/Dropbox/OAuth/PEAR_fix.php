<?

/*
	This fixes a issue with PEAR OAuth corrupting binary
	28.9.2010 - a.pick
*/

class HTTP_OAuth_Consumer_Request_Fix extends HTTP_OAuth_Consumer_Request {
    
	public function send()
    {
        $this->buildRequest();

        // Hack for the OAuth's spec + => %20 and HTTP_Request2
        // HTTP_Request2 uses http_build_query() which does spaces
        // as '+' and not '%20'
        if ($this->getMethod() == 'POST') {
            $body = $this->getHTTPRequest2()->getBody();
			
			// Content-Type -> multipart/form-data, skip encode
				
			$headers =  $this->getHTTPRequest2()->getHeaders();	
			if(!strstr($headers["content-type"], "multipart/form-data")) {					
            	$body = str_replace('+', '%20', $body);
			}
			
			$this->getHTTPRequest2()->setBody($body);
        }

        try {
            $response = $this->getHTTPRequest2()->send();
        } catch (Exception $e) {
            throw new HTTP_OAuth_Exception($e->getMessage(), $e->getCode());
        }

        return new HTTP_OAuth_Consumer_Response($response);
    }	
}

?>