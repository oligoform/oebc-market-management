{literal}
<style>
html {
	overflow:hidden !important;
	overflow-x:hidden !important;
	overflow-y:hidden !important;
}
html, body {
	height:100%;
	width:100%;
	padding:0;
	margin:0;

}
.pathview {
	width: 49%;
	height: 25px;
	margin-top:5px;
	margin-bottom:5px;	
}
.local {
	float: left;	
}
.remote {
	float: right;
}
.options {
	width: 49%;
	height:18px;
	margin-top:5px;	
}
.options img {
	margin-right:10px;	
}
.options select {
	float:right;	
}
.fileview {
	width:49%; 
	height:100%;
	background-color:#FFF;
	border:#999 1px solid;
	height:350px !important;
	overflow:auto;	
}
.myfile {
	margin:3px;
	width:250px;
}
.remotefile {
	margin:3px;
	width:250px;
}
.tabs2 {
	min-height: 95%;	
}

</style>
<script>

var cfile = 0;

function getLocalFolder(folder) {
		$.post("/components/ajax.php", {	
					'mode'	   			: 	'opendirl',
					'thisapp'			: 	'dropbox',
					'path'				: 	folder
		},
		function(resdata){
				$(".localview").html($(resdata).find('html').text());
				$(".pathlocal").html($(resdata).find('thispath').text());
				$('.myfile').draggable({helper: 'clone'}); 
 		}, "xml");
}

function getRemoteFolder(folder) {
		$.post("/components/ajax.php", {	
					'mode'	   			: 	'opendirr',
					'thisapp'			: 	'dropbox',
					'path'				: 	folder
		},
		function(resdata){
				$(".remoteview").html($(resdata).find('html').text());
				$(".pathremote").html($(resdata).find('thispath').text());
				$('.remotefile').draggable({helper: 'clone'}); 
 		}, "xml");
}

function createRemoteFolder() {
	
	var folder = prompt("Ordnername:",$(".pathremote").html().trim());	
	
	$.post("/components/ajax.php", {	
					'mode'	   			: 	'createrfolder',
					'thisapp'			: 	'dropbox',
					'path'				: 	folder
		},
		function(){
			getRemoteFolder($(".pathremote").html().trim());
 		});
	
}

function createLocalFolder() {
	
	var folder = prompt("Ordnername:",$(".pathlocal").html().trim());	
	
	$.post("/components/ajax.php", {	
					'mode'	   			: 	'createfolder',
					'path'				: 	folder
		},
		function(){
			getLocalFolder($(".pathlocal").html().trim());
 		});
	
}

function putRemoteFile(file) {
		
		$.post("/components/ajax.php", {	
					'mode'	   			: 	'putfile',
					'thisapp'			: 	'dropbox',
					'path'				: 	$(".pathremote").html().trim(),
					'lpath'				:	$(".pathlocal").html().trim(),
					'file'				:	file,
					'do'				:	$("#dropmoderemote").val()
		},
		function(){
			getLocalFolder($(".pathlocal").html().trim());
			getRemoteFolder($(".pathremote").html().trim());
 		});
	
	
}

function putLocalFile(file) {
		
		$.post("/components/ajax.php", {	
					'mode'	   			: 	'getfile',
					'thisapp'			: 	'dropbox',
					'path'				: 	$(".pathremote").html().trim(),
					'lpath'				:	$(".pathlocal").html().trim(),
					'file'				:	file,
					'do'				:	$("#dropmodelocal").val()
		},
		function(){
			getLocalFolder($(".pathlocal").html().trim());
			getRemoteFolder($(".pathremote").html().trim());
 		});
	
	
}

$(function() {
	
$(".tabs2").tabs();	

getLocalFolder("/");
getRemoteFolder("/");

$('.myfile').draggable({ opacity: 0.7, helper: 'clone' }); 
$('.remotedrop').droppable({ accept: ".myfile", 
                       activeClass: 'droppable-active', 
                       hoverClass: 'droppable-hover', 
                       drop: function(e, ui) { 
						   putRemoteFile($(ui.draggable).html().trim()) 
						   $('.remotedrop').append(ui.draggable.clone().addClass("cf"+cfile));
						   cfile++;
                       }
});
$('.remotefile').draggable({helper: 'clone'}); 
$('.localdrop').droppable({ accept: ".remotefile", 
                       activeClass: 'droppable-active', 
                       hoverClass: 'droppable-hover', 
                       drop: function(e, ui) { 
						   putLocalFile($(ui.draggable).html().trim()) 
   						   $('.localdrop').append(ui.draggable.addClass("cf"+cfile));
						   cfile++;
                       }
});
});
</script>
{/literal}
