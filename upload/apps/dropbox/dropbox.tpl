<div class="tabs2">
  <ul>
    <li><a href="#tabs-files">Dateien</a></li>
    <li><a href="#tabs-settings">Einstellungen</a></li>
  </ul>
  <div id="tabs-files">
    <div>
      <div style="clear:both"></div>
      <div class="options local"><a href="javascript:createLocalFolder();"><img src="../../images/icons/folder_add.png" width="16" height="16" /></a><a href='javascript:getLocalFolder("/");'><img src="../../images/icons/house_go.png" width="16" height="16" border="0" /></a>
        <select name="dropmodelocal" id="dropmodelocal">
          <option value="copy">Dateien hierher kopieren</option>
          <option value="move">Dateien hierher verschieben</option>
        </select>
      </div>
      {if !$needconfig}
      <div class="options remote"><a href="javascript:createRemoteFolder();"><img src="../../images/icons/folder_add.png" width="16" height="16" border="0" /></a><a href='javascript:getRemoteFolder("/");'><img src="../../images/icons/house_go.png" width="16" height="16" border="0" /></a>
        <select name="dropmoderemote" id="dropmoderemote">
          <option value="copy">Dateien hierher kopieren</option>
          <option value="move">Dateien hierher verschieben</option>
        </select>
      </div>
      {/if}
      <div style="clear:both"></div>
      <div class="pathview local pathlocal"></div>
      {if !$needconfig}
      <div class="pathview remote pathremote"></div>
      {/if}
      <div style="clear:both"></div>
      <div class="localview fileview localdrop" style="float:left;"> </div>
      {if !$needconfig}
      <div class="remoteview fileview remotedrop" style="float:right"> </div>
      {/if}
      <div style="clear:both"></div>
    </div>
  </div>
  <div id="tabs-settings">
    <form action="index.php" method="post">
      <input type="hidden" name="app" value="dropbox">
      <table width="500" border="0" align="center" cellpadding="10" cellspacing="0">
        <tr>
          <td>Dropbox Benutzername</td>
          <td width="300"><input name="duser" type="text" id="duser" style="width:100%;" value="{$duser}" /></td>
        </tr>
        <tr>
          <td>Dropbox Passwort</td>
          <td width="300"><input name="dpasswd" type="password" id="dpasswd" style="width:100%;" value="{$dpasswd}" /></td>
        </tr>
      </table>
      <p style="margin-bottom:20px;">
        <center>
          <button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="anbieten">Einstellungen speichern</button>
        </center>
      </p>
    </form>
  </div>
</div>
