<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

	/*
		Fileman
	*/

	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	//smarty Element für Darstellung erzeugen
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../../tmp/';
	$smarty->template_dir = dirname(__FILE__);
	
	if(empty($_REQUEST["path"]))
	{
		$_REQUEST["path"] = "/";
	}
	
	$smarty->assign("currentfs", $_REQUEST["path"]);
	
	//Content erzeugen und bereitstellen
	$content = $smarty->fetch('fileman.tpl');
	$template->assign("content",$content);

?>