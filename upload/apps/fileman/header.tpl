{literal}
<style>
html, body {
	background-color:#ccc;
	height:100%;
	width:100%;
	outline:none;
}
.file {
	color:#333 !important;	
}
.fileman {
 	height:100%;
}
.selected {
	background-color:#FFF;	
}
.sidebar {
	width:200px;
	min-height:100%;
	height:100%;
	background-color:#ccc;	
	float:left;
	background-image:url(/images/system/fileman_sidebar.png);
	background-repeat:no-repeat;
	background-position:top left;
}
.sidebar img {
	margin: 10px;	
}
#copyup {
	width:80%;
	height:70px;
	border: 1px dashed #CCC;
	padding:5%;
	margin: 5px auto;
	background-color:#FFF;
}
#fileinfo {
	width:80%;
	height:200px;
	border: 1px dashed #CCC;
	padding:5%;
	margin: 5px auto;
	background-color:#FFF;
}
</style>
<script language="javascript" src="/javascript/jquery.backstretch.min.js" type="text/javascript" ></script>
<script language="javascript" src="/javascript/adm.js"></script>
<script>

function goBack() {
	$.post("/components/ajax.php", {	
					'mode'	   		: 	'listfile',
					'path'			: 	"/"
					
			},
	function(resdata){
		$(".desktop").html(resdata);
		fileman();
 	});
}

function createFolder() {

	var curDir = $("body").find(".currentfs").val();
	
	var folder = prompt("Ordnername:",curDir);	
	
	$.post("/components/ajax.php", {	
					'mode'	   			: 	'createfolder',
					'path'				: 	folder
		},
		function(){
		$.post("/components/ajax.php", {	
					'mode'	   		: 	'listfile',
					'path'			: 	$(".desktop").find(".currentfs").val()
					
		},
		function(resdata){
			$(".desktop").html(resdata);
			fileman();
 		});
	});
	
}

function fileman() {
	
	$('.file').draggable({ opacity: 0.7, stack: ".filestack", refreshPositions: true });
			
	$(".file").bind({
		click: function() {
			selectItem(this);
			$("#myfilesize").html($(this).find(".filesize").val());
			$("#myfiledate").html($(this).find(".filedate").val());
			$("#myfile").html($(this).find(".filename").val());
			$("#mypath").html($(this).find(".path").val());
			$("#fileinfo").show();
		},
		mouseenter: function() {
			$(this).addClass("focus");	
		},
		mouseleave: function() {
			$(this).removeClass("focus");	
		}						
	});
	
	$(".file").dblclick(function() {
		var file = $(this).find(".path").val();
		if($(this).find(".is_dir").val() != "") {
			//window.location = "/?app=fileman&path=" + file;
			$.post("/components/ajax.php", {	
					'mode'	   		: 	'listfile',
					'path'			: 	file
					
			},
			function(resdata){
				$(".desktop").html(resdata);
				fileman();
 			});
		}
	});

	/**********************************************************/
		
	$(".desktop").bind('click', function() {
			unselectAll();
	});
				
	/**********************************************************/

	$('.folder').droppable({ accept: ".file", 
    	activeClass: 'droppable-active', 
    	hoverClass: 'droppable-hover', 
    	drop: function(e, ui) { 
		
			folder = $(this).find(".path").val();
			file = $(ui.draggable).find(".filename").val();
			src = $(ui.draggable).find(".path").val();
			if(src != "/desktop") {
			$.post("/components/ajax.php", {	
					'mode'	   			: 	'movefile',
					'folder'			: 	folder,
					'file'				:	file,
					'src'				:	src
					
			},
			function(){
				$(ui.draggable).remove();
				if(folder == "/desktop") {
					parent.deskRefresh();	
				}
 			});
			} else {
				alert("Sie können das Desktop nicht verschieben!");	
			}
    	}
	});
	
	$('#copyup').droppable({ accept: ".file", 
    	activeClass: 'droppable-active', 
    	hoverClass: 'droppable-hover', 
    	drop: function(e, ui) { 
		
			folder = $(".desktop").find(".currentfs").val();
			folder = folder + "/..";
			
			file = $(ui.draggable).find(".filename").val();
			src = $(ui.draggable).find(".path").val();
			if(src != "/desktop") {
			$.post("/components/ajax.php", {	
					'mode'	   			: 	'movefile',
					'folder'			: 	folder,
					'file'				:	file,
					'src'				:	src
					
			},
			function(){
				$(ui.draggable).remove();
				if(folder == "/desktop/..") {
					parent.deskRefresh();	
				}
 			});
			} else {
				alert("Sie können das Desktop nicht verschieben!");	
			}
    	}
	});		
	
	$("#fileinfo").hide();
	
	if($(".desktop").find(".currentfs").val() == "/") {
		$("#copyup").hide();	
	} else {
		$("#copyup").show();	
	}
	
	loadContext();
	
}

function goUp() {

	folder = $(".desktop").find(".currentfs").val();
	
	if(folder != "/") { 
		folder = folder + "/..";
	}
	$.post("/components/ajax.php", {	
					'mode'	   		: 	'listfile',
					'path'			: 	folder
					
			},
	function(resdata){
		$(".desktop").html(resdata);
		fileman();
 	});	

}

$(function() {
	
	$("#fileinfo").hide();
		
	if($(".desktop").find(".currentfs").val() == "/") {
		$("#copyup").hide();	
	}
	
	$.post("/components/ajax.php", {	
					'mode'	   		: 	'listfile',
					'path'			: 	$(".desktop").find(".currentfs").val()
					
			},
	function(resdata){
		$(".desktop").html(resdata);
		fileman();
 	});
	
});	
</script>
{/literal}