<div class="cat_page">

<div class="cat_workbench"></div>

<div class="cat_elements cat_window">
<div class="bar ui-widget-header" align="absmiddle"> &nbsp;</div>
  
  <div class="cat_element image">Product Image</div>
  <div class="cat_element desc">Product Text</div>
  <div class="cat_element price">Price</div>
  <div class="cat_element ean">EAN</div>
  <div class="cat_element frame">Box</div>
  <div class="cat_element gimage">Image</div>
  <div class="cat_element gtext">Text</div>
</div>  

<div class="cat_status cat_window">
X:<span id="pagex">&nbsp;</span><br />
Y:<span id="pagey">&nbsp;</span><br />
<hr />
X:<br />
Y:<br />
W:<br />
H:<br />
</div>

<div class="cat_tools cat_window">
<div class="bar ui-widget-header" align="absmiddle">&nbsp;</div>
  <a href="#" onclick='$(".cat_fontmenu").toggle();bringtofront(".cat_fontmenu");'><img src="/apps/kdesigner/images/font.png" /></a>
  <a href="#" onclick='$(".cat_background").toggle();bringtofront(".cat_background");'><img src="/apps/kdesigner/images/background.png" /></a>
  <a href="#" onclick='$(".cat_frame").toggle();bringtofront(".cat_frame");'><img src="/apps/kdesigner/images/frame.png" /></a>
  <a href="#" onclick='$(".cat_color").toggle();bringtofront(".cat_color");'><img src="/apps/kdesigner/images/color.png" /></a>
  <a href="#" onclick='$(".cat_opacity").toggle();bringtofront(".cat_opacity");'><img src="/apps/kdesigner/images/opacity.png" /></a>
<hr />
  <a href="#" onclick="clone();"><img src="/apps/kdesigner/images/clone.png" /></a>
  <a href="#" onclick="deleteChild();"><img src="/apps/kdesigner/images/delete.png" /></a>
</div>

<div class="cat_file cat_window">
<div class="bar ui-widget-header" align="absmiddle">&nbsp;</div>
<a href="#" onclick='$(".cat_export_pdf").toggle();bringtofront(".cat_export_pdf");'><img src="/apps/kdesigner/images/export.png" /></a>
  
  <a href="#" onclick='$(".cat_export_html").toggle();bringtofront(".cat_export_html");'><img src="/apps/kdesigner/images/export_html.png" /></a>
  
  <a href="#" onclick='$(".cat_save").toggle();bringtofront(".cat_save");'><img src="/apps/kdesigner/images/save.png" /></a>
</div>  

{include file="kdesigner/asserts/bg.tpl"}
  
{include file="kdesigner/asserts/font.tpl"}

{include file="kdesigner/asserts/color.tpl"}

{include file="kdesigner/asserts/frame.tpl"}

{include file="kdesigner/asserts/export.tpl"}

{include file="kdesigner/asserts/opacity.tpl"}

<iframe width="1" height="1" frameborder="0" id="download" style="position: absolute; left:0; bottom:0;";></iframe>
