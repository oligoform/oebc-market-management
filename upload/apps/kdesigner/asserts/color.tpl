<div class="cat_color cat_window cat_mytools">
<div class="bar ui-widget-header" align="absmiddle">Colorize</div>
{include file="kdesigner/asserts/colorpicker.tpl"}
<input name="colorhex" type="text" id="colorhex" size="6" style="float:right; margin-right:8px;" />
<p align="center" style="margin-top:30px;"><button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="bgcolor" onclick="javascript:setBGColor();">Background Color</button> 

<button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="fontcolor" onclick="javascript:setFontColor();">Font Color</button> 

<button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="frmcolor" onclick="javascript:setFrmColor();">Frame Color</button>
</p>
</div>