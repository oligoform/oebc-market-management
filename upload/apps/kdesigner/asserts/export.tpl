<div class="cat_export_html cat_export cat_window cat_mytools">
<div class="bar ui-widget-header" align="absmiddle">Export HTML</div>
<div style="width:100%; text-align:center;">
<button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="frmcolor" onclick="javascript:exportPDF(1);">Export HTML Now!</button>
</div>
</div>


<div class="cat_export_pdf cat_export cat_window cat_mytools">
<div class="bar ui-widget-header" align="absmiddle">Export PDF</div>
<div style="width:100%; text-align:center;">
<br />
<div id="pdfoutput">&nbsp;</div>
<div style="color:#FFF; width:150px; text-align:left; margin:10px;"><input name="singleOnly" type="checkbox" id="singleOnly" value="1" /> Single Page</div>
<div style="color:#FFF; width:150px; text-align:left; margin:10px;"><input name="fitImg" type="checkbox" id="fitImg" value="1" /> Fit Product Images</div>

<button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="button" id="frmcolor" onclick="javascript:exportPDF();">Export PDF Now!</button>
</div>
</div>



<div class="cat_save cat_export cat_window cat_mytools">
<div class="bar ui-widget-header" align="absmiddle">Save / Load</div>
<p></p>
</div>