<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}

	require_once dirname(__FILE__)."/../classes/fpdf/fpdf.php";
	require_once dirname(__FILE__)."/../classes/fpdf/textbox.php";
	
	if(empty($_REQUEST['elements'])) {
		echo "error";
		exit(0);
	}
	
	function splitRgb($rgb) {
		
		$rgba = explode(", ", $rgb);
		
		$rgba[0] = str_replace("rgb(", "", $rgba[0]);
		$rgba[2] = str_replace("rgb(", "", $rgba[2]);
		
		return $rgba;
	}
	
	
	$content = "";
	
	/*
	
		elements[i][0] = "element";			// typ
		elements[i][1] = p.left;			// x
		elements[i][2] = p.top;				// y
		elements[i][3] = $(this).height();	// height
		elements[i][4] = $(this).width();	// width
		
		elements[i][9] = $(this).css("color");
		elements[i][10] = $(this).css("background-color");
		elements[i][11] = $(this).css("border-color");
		elements[i][12] = $(this).css("border-width");
		
		Umrechnungsfaktor px -> pt = 1.3333
	
	*/
		
	foreach($_REQUEST['elements'] as $element) {
		$content .= '<div style="position: absolute; left: '.$element[1].'px ;top: '.$element[2].'px ; height: '.$element[3].'px ; width: '.$element[4].'px ; background-color:#ff9900;"></div>';
	}
	
	$html = '<html><body>'.$content.'</body></html>';
	
	if($_REQUEST["preview"]) {
		file_put_contents( dirname(__FILE__)."/../../tmp/cat_preview.htm", $html);
		exit(1);
	}
		
	$pdf=new FPDFadv('P', 'pt', 'A4');
	
	$pdf->SetAuthor('OEBC - http://www.oebc.de');
	$pdf->SetTitle('PDF Catalog'); 
	
	// place articles 
	
	$ox = new oxid($db);
	
	$pageelements = $ox->getArticles();
	
	foreach($pageelements as $pageelement) {
	
	//echo $pageelement["OXLONGDESC"];
	
	$pdf->AddPage();
	
	// Set BAckground image if given:
	//$pdf->Image('../images/'.$image, 0, 0, $size[0], $size[1]); 
	

	foreach($_REQUEST['elements'] as $element) {
		
		// TODO: switch/case
		
		//reset
		$pdf->SetFont('Arial','',9);
		$pdf->SetLineWidth(0.5);	
		
		if(($element[7] != "undefined") && ($element[8] != "undefined")) {
			$pdf->SetFont($element[7],'',$element[8]);
		}

		
		// set alpha
		$pdf->SetAlpha($element[13]);
				
		$bbool = 1;		
				
		if($element[12] != "none") {
			$pdf->SetLineWidth($element[12]/1.3333);
		} else {
			$pdf->SetLineWidth(0);
		}

		$type = "";
		
		if($element[10] != "transparent") {
			$myrgb = splitRgb($element[10]);
	
			$pdf->SetFillColor($myrgb[0], $myrgb[1] , $myrgb[2]);

			$type = "FD";			
		}
		
		$myrgb = splitRgb($element[11]);
		
		$pdf->SetDrawColor($myrgb[0], $myrgb[1] , $myrgb[2]); 

		
		//echo "Verarbeite: ".$element[0]."<br />";
		
		//$pdf->Cell($element[1],$element[2],'Hello World!');
		
		if($element[12] == 0) {
			$bbool = 0;	
			$type = "F";
		}
		
		
		if($element[0] == "frame") {
		
			/* draw a square */
			if(($element[10] != "transparent") || ($element[12] != 0) ) {
			$pdf->Rect(	($element[1]/1.3333+(($element[12]/1.3333)/2)),
						($element[2]/1.3333+(($element[12]/1.3333)/2)), 
						($element[4]/1.3333),
						($element[3]/1.3333), $type
					);
			}
		} elseif($element[0] == "desc") {
						
			if($utf8) {
				$desc = html_entity_decode(strip_tags(preg_replace('#<br\s*?/?>#i', "\n", utf8_decode($pageelement["OXLONGDESC"]))));
			} else {
				$desc = html_entity_decode(strip_tags(preg_replace('#<br\s*?/?>#i', "\n", $pageelement["OXLONGDESC"])));	
			}
			
			$pdf->SetFillColor(125,125,0);
			$pdf->SetXY(($element[1]/1.3333),($element[2]/1.3333));
			$pdf->drawTextBox( $desc, ($element[4]/1.3333),($element[3]/1.3333), "L", "T",  false);
			$pdf->SetXY(0,0);		
			
		} elseif($element[0] == "image") {
			
			$url = $files->getOpt("url").'/out/pictures/master/1/'.$pageelement["OXPIC1"];
							
			require(dirname(__FILE__)."/../../../../classes/oxidimg.class.php");
	
			$res = $oximg->getImg($url);
	
			$nurl = $res[1];
	
			$pdf->SetFillColor(0,0,255);
			$pdf->Image($nurl, ($element[1]/1.3333),($element[2]/1.3333), ($element[4]/1.3333), ($element[3]/1.3333));		
			
		} elseif($element[0] == "ean") {
						
			if($utf8) {
				$OXARTNUM = html_entity_decode(strip_tags(preg_replace('#<br\s*?/?>#i', "\n", utf8_decode($pageelement["OXARTNUM"]))));
			} else {
				$OXARTNUM = html_entity_decode(strip_tags(preg_replace('#<br\s*?/?>#i', "\n", $pageelement["OXARTNUM"])));	
			}
			
			$pdf->SetFillColor(125,125,0);
			$pdf->SetXY(($element[1]/1.3333),($element[2]/1.3333));
			$pdf->drawTextBox( $OXARTNUM, ($element[4]/1.3333),($element[3]/1.3333), "L", "T",  $bbool);
			$pdf->SetXY(0,0);		
			
		} elseif($element[0] == "price") {
						
			if($utf8) {
				$price = html_entity_decode(strip_tags(preg_replace('#<br\s*?/?>#i', "\n", utf8_decode($pageelement["OXPRICE"]))));
			} else {
				$price = html_entity_decode(strip_tags(preg_replace('#<br\s*?/?>#i', "\n", $pageelement["OXPRICE"])));	
			}
			
			$pdf->SetFillColor(125,125,0);
			$pdf->SetXY(($element[1]/1.3333),($element[2]/1.3333));
			$pdf->drawTextBox( $price, ($element[4]/1.3333),($element[3]/1.3333), "L", "T",  $bbool);
			$pdf->SetXY(0,0);			
			
		} elseif($element[0] == "gtext") {
								
			$pdf->SetFillColor(125,125,0);
			$pdf->SetXY(($element[1]/1.3333),($element[2]/1.3333));
			$pdf->drawTextBox( $element[6], ($element[4]/1.3333),($element[3]/1.3333), "L", "T",  $bbool);
			$pdf->SetXY(0,0);	
			
		} elseif($element[0] == "gimage") {
			
			$pdf->SetFillColor(0,0,255);
			$pdf->Image($element[5] , ($element[1]/1.3333),($element[2]/1.3333), ($element[4]/1.3333), ($element[3]/1.3333));		
			
		} 
	}
	 if($_REQUEST["singlepage"] == 1) {
		break; 
	 }
	
	}
	
	$exfile = "/filesystem/KD_".time().".pdf";
	
	$pdf->Output(dirname(__FILE__)."/../../../".$exfile);
	
	echo $exfile;

	//echo date("G:i:s")."PDF Export erfolgreich!"; 

?>