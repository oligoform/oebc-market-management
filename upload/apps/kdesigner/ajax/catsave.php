<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}

	// Great help:
	// http://articles.techrepublic.com.com/5100-10878_11-6141415.html

	$dom = new DOMDocument("1.0");
	
	$root = $dom->createElement("kd");
	$dom->appendChild($root);

	/*************/

	// create child element
	$item = $dom->createElement("item");
	$root->appendChild($item);

	// create text node
	$text = $dom->createTextNode("image");
	$item->appendChild($text);
	
	// create attribute node
	$cords = $dom->createAttribute("cords");
	$item->appendChild($cords);
	
	/*****************/
	
	// do next element:
	
	// create child element
	$item = $dom->createElement("item");
	$root->appendChild($item);

	// create another text node
	$text = $dom->createTextNode("desc");
	$item->appendChild($text);
	
	$dom->save(dirname(__FILE__).'/../templates/catalog/'.$_REQUEST["filename"]);
	
?>