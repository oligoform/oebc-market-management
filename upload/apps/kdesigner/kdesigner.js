// JavaScript Document
/*
	(C) PBT Media 2010 all rights reserved
	
	This file is belongs to a commercial product.
	It MUST not be distributed or used in whole or in parts 
	without written permission of PBT Media or it's 
	author Alexander Pick.
	
	jQuery was used under the terms of MIT License.

*/

var el = 0;
var background = "";

var X = 0;
var Y = 0;

var sX = 0;
var sY = 0;
var startSelect = 1;
 

function deleteChild() {
	
	$(".cat_workbench").find(".selected").resizable( "destroy" );
	
	$(".cat_workbench").find(".selected").remove();

}

/* Set Opacity */

function setOpacity() {
	
	var newOpa = $("#myOpacity").val();
	
	$(".cat_workbench").find(".selected").resizable( "destroy" );
	
	$(".cat_workbench").find(".selected").css("opacity", newOpa);
	
	$(".cat_workbench").find(".selected").resizable({ containment: '.cat_workbench' });	
		
	//$(".cat_workbench").find(".selected").css("border-width");

}

/* Set Frame Size */

function setFrmSize() {
	
	var newSize = $("#myFrameSize").val();
	
	$(".cat_workbench").find(".selected").resizable( "destroy" );
	
	$(".cat_workbench").find(".selected").css("border-width", newSize);
	
	if($(".cat_workbench").find(".selected").css("borderLeftWidth") != "0px") {
		
		$(".cat_workbench").find(".selected").css("border-style", "solid");
	
	} else {
		
		$(".cat_workbench").find(".selected").css("border-width", "1px");
		
		$(".cat_workbench").find(".selected").css("border-style", "dashed");
	}
	
	$(".cat_workbench").find(".selected").resizable({ containment: '.cat_workbench' });	

	//$(".cat_workbench").find(".selected").css("border-width");

}

/* Set BG Color */

function setBGColor() {
	
	var newColor = $("#colorhex").val();
	
	$(".cat_workbench").find(".selected").resizable( "destroy" );
	
	if($(".cat_workbench").find(".selected").hasClass("gtext") && $(".cat_workbench").find(".selected").children(".textspace").size() > 0) {
				
		$(".cat_workbench").find(".selected").css("background-color", newColor);
		$(".cat_workbench").find(".selected").children(".textspace").css("background-color", newColor);
	} else {
		
		$(".cat_workbench").find(".selected").css("background-color", newColor);

	} 
		
	$(".cat_workbench").find(".selected").resizable({ containment: '.cat_workbench' });	
	
	
	//$(".cat_workbench").find(".selected").css("background-color");
	
}

/* Set Frame Color */

function setFrmColor() {
	var newColor = $("#colorhex").val();
		
	$(".cat_workbench").find(".selected").resizable( "destroy" );
	
	if($(".cat_workbench").find(".selected").hasClass("gtext") && $(".cat_workbench").find(".selected").children(".textspace").size() > 0) {
				
		$(".cat_workbench").find(".selected").css("border-color", newColor);
		$(".cat_workbench").find(".selected").children(".textspace").css("border-color", newColor);
	
	} else {
		
		$(".cat_workbench").find(".selected").css("border-color", newColor);

	} 
		
	$(".cat_workbench").find(".selected").resizable({ containment: '.cat_workbench' });		
}

function setFontSize() {
	var newSize = $("#myFontSize").val();
	
	var saveText = "";
	
	$(".cat_workbench").find(".selected").resizable( "destroy" );
	
	if($(".cat_workbench").find(".selected").hasClass("gtext") && $(".cat_workbench").find(".selected").children(".textspace").size() > 0) {
		
		//saveText = $(".cat_workbench").find(".selected").children(".textspace").val();
		
		$(".cat_workbench").find(".selected").css("font-size", newSize);
		$(".cat_workbench").find(".selected").children(".textspace").css("font-size", newSize);
	} else {
		
		saveText = $(".cat_workbench").find(".selected").html();
		$(".cat_workbench").find(".selected").css("font-size", newSize);

	} 
		
	$(".cat_workbench").find(".selected").resizable({ containment: '.cat_workbench' });		

}

function setFontFamily() {
	var newFamily = $("#myFont").val();
	
	var saveText = "";
	
	$(".cat_workbench").find(".selected").resizable( "destroy" );
	
	if($(".cat_workbench").find(".selected").hasClass("gtext") && $(".cat_workbench").find(".selected").children(".textspace").size() > 0) {
		
		//saveText = $(".cat_workbench").find(".selected").children(".textspace").val();
		
		$(".cat_workbench").find(".selected").css("font-family", newFamily);
		$(".cat_workbench").find(".selected").children(".textspace").css("font-family", newFamily);
		
	} else /*if($(".cat_workbench").find(".selected").hasClass("gtext"))*/ {
		
		saveText = $(".cat_workbench").find(".selected").html();
		$(".cat_workbench").find(".selected").css("font-family", newFamily);

	} 
	if($(".cat_workbench").find(".selected").hasClass("Times-Roman")) {
		$(".cat_workbench").find(".selected").removeClass("Times-Roman");
	}
	if($(".cat_workbench").find(".selected").hasClass("Courier")) {
		$(".cat_workbench").find(".selected").removeClass("Courier");
	} 
	if($(".cat_workbench").find(".selected").hasClass("Helvetica")) {
		$(".cat_workbench").find(".selected").removeClass("Helvetica");
	} 
	if($(".cat_workbench").find(".selected").hasClass("Arial")) {
		$(".cat_workbench").find(".selected").removeClass("Arial");
	}
	$(".cat_workbench").find(".selected").addClass(newFamily);
		
	$(".cat_workbench").find(".selected").resizable({ containment: '.cat_workbench' });		
}

function setBackground() {
	$(".cat_workbench").css("background-image", "url(/images/background/" + $("#bgimg").val());
	background = bg;
}

function clone() {
	
	var warning = 0;
	var biggest = 0;
	
	$('.cat_workbench').children().each(
    
	function(){
		
		var height = $(this).height();	
		var curpos = $(this).aPosition().top; 
		var xpt	   = height + curpos;
	
		if(xpt > biggest) {
			biggest = xpt;
		}
		
	});
	$('.cat_workbench').children().each(
    function(){
		
		var eln;
		
		if($(this).hasClass("frame")) {
			eln = "frame";			// typ
		}
		if($(this).hasClass("desc")) {
			eln = "desc";			// typ
		}
		if($(this).hasClass("image")) {
			eln = "image";			// typ
		}	
		if($(this).hasClass("ean")) {
			eln = "ean";			// typ
		}	
		if($(this).hasClass("price")) {
			eln = "price";			// typ
		}
		
		var wbench = $(".cat_workbench").aPosition();
		
		var mypos = $(this).aPosition();
		
		//alert(wbench.left + " " + mypos.left);
		
		var newx = mypos.top + biggest; // - wbench.top;
		var newy = mypos.left; // - wbench.left;
			
		if(newx > 1103) {
			if(!warning) {
				alert("Cloning outside of workspace is not possible!");
				warning++;
			} 
			// do nothing	
		} else {
		
		$(".cat_workbench").append($(this).clone().removeClass().addClass("item"+el));
		
		//alert(mypos.left + " " + newx);
		
		// Workaround -> resizeable messes position to absolute and makes offset go mad on clone
		// http://dev.jqueryui.com/ticket/5335
		
		$(".item"+el).css({ position: "relative" });
		
		$(".item"+el).position( newy, newx );
				
		$(".item"+el).addClass(eln);
		$(".item"+el).addClass("welement");
		$(".item"+el).addClass("pagestack");

		$(".item"+el).draggable({
            containment: 'parent',
			opacity: 0.7, 
			stack: ".pagestack", 
			refreshPositions: true
        });
		$(".item"+el).resizable();
		bringtofront(".item"+el);
	
		el++;
		}
	});
	
}

function exportPDF(preview) {
	
	elements = new Array();
	
	var i = 0;
	
	$('.cat_workbench').children().each(
    function(){
			
		var p = $(this).aPosition()
		
		elements[i] = new Array();
		
		if($(this).hasClass("frame")) {
			elements[i][0] = "frame";			// typ
		}
		if($(this).hasClass("desc")) {
			elements[i][0] = "desc";			// typ
		}
		if($(this).hasClass("image")) {
			elements[i][0] = "image";			// typ
		}	
		if($(this).hasClass("ean")) {
			elements[i][0] = "ean";			// typ
		}	
		if($(this).hasClass("price")) {
			elements[i][0] = "price";			// typ
		}
		if($(this).hasClass("gimage")) {
			elements[i][0] = "gimage";			// typ
			elements[i][5] = $(this).children(".gimg").attr("src");
		}	
		if($(this).hasClass("gtext")) {
			elements[i][0] = "gtext";			// typ
			elements[i][6] = $(this).children(".textspace").val();
		}
		
		elements[i][1] = p.left;			// x
		elements[i][2] = p.top;				// y
		elements[i][3] = $(this).height();	// height
		elements[i][4] = $(this).width();	// width
		
		if($(this).hasClass("Times-Roman")) {
			elements[i][7] = "Times";
		}
		if($(this).hasClass("Courier")) {
			elements[i][7] = "Courier";
		} 
		if($(this).hasClass("Helvetica")) {
			elements[i][7] = "Helvetica";
		} 
		if($(this).hasClass("Arial")) {
			elements[i][7] = "Arial";
		}	
		
		elements[i][8] = $(this).css("font-size");
		elements[i][9] = $(this).css("color");
		elements[i][10] = $(this).css("backgroundColor");
		elements[i][11] = $(this).css("borderColor");
		
		// workaround for computed styles:
		if($(this).css("borderStyle") != "dashed") {	
			elements[i][12] = $(this).css("borderLeftWidth");
		} else {
			elements[i][12] = 0;	
		}
		
		elements[i][13] = $(this).css("opacity");
			
		i++;
		
	});
	
	$('#pdfoutput').html("Generating PDF .. Please wait!");
	
	$.post("/components/ajax.php", {	
					'mode'	   			: 	'catexportpdf',
					'thisapp'				:	'kdesigner',
					'elements'			:	elements,
					'preview'			: 	preview,
					'singlepage'		: 	$('#singleOnly:checked').val()
			},
			function(resdata){
				if(resdata != "error") {
					
					$('#pdfoutput').html(resdata);

				} else {
					
					alert("An Error occured: No element on workspace!");	
				
				}
			});
			
	if(preview == 1) {
		parent.spawngeneric('/tmp/cat_preview.htm', "600");
	}
}

function unselectAll() {

	var inFocus = 0;

	$('.cat_workbench').children().each(
    
	function(){
		
		if($(this).hasClass("focus")) {
			inFocus = 1;
		}
		
	});
	
	if(!inFocus) {
		
		$('.cat_workbench').children().each(
    
		function(){
		
			if($(this).hasClass("selected")) {
				$(this).removeClass("selected");			// typ
				if( $(this).hasClass("gtext") ) 
				{
					var myval = $(this).children(".textspace").val();
					$(this).resizable( "destroy" )
					$(this).resizable({ containment: '.cat_workbench' });
					//$(this).append(myval);	
					$(this).css("text-align", "left");		
				} else if ($(this).hasClass("gimage") ) {
					$(this).resizable( "destroy" )
					$(this).resizable({ containment: '.cat_workbench' });				
				} else {
					$(this).resizable( "destroy" )
					$(this).html("");
					$(this).resizable({ containment: '.cat_workbench' });
				}
				//$(this).css("border-style", "solid");
			}

		});
		
		
		$("#myFontSize").val("");
		$("#myFrameSize").val("");
		$("#myOpacity").val("");

		$("#myFont").attr('disabled', 'disabled');
		$("#myFontSize").attr('disabled', 'disabled');
		
		$("#frmcolor").attr('disabled', 'disabled');
		$("#bgcolor").attr('disabled', 'disabled');
		$("#fontcolor").attr('disabled', 'disabled');
		$("#myFrameSize").attr('disabled', 'disabled');
		
		$("#myOpacity").attr('disabled', 'disabled');

	
	}
	
	//alert(selectMe);
}

function selectItem(selectMe) {

	$('.cat_workbench').children().each(
    
	function(){
		
		if($(this).hasClass("selected")) {
			
			$(this).removeClass("selected");			// typ
			if( !$(this).hasClass("gtext") && !$(this).hasClass("gimage")   ) 
			{
					//alert("clean");
					$(this).resizable( "destroy" )
					$(this).html("");
					$(this).resizable({ containment: '.cat_workbench' });
			}
			
			//$(this).css("border-style", "solid");
	
			$("#myFontSize").val("");
			$("#myFrameSize").val("");
			$("#myOpacity").val("");

			$("#myFont").attr('disabled', 'disabled');
			$("#myFontSize").attr('disabled', 'disabled');
			
			$("#frmcolor").attr('disabled', 'disabled');
			$("#bgcolor").attr('disabled', 'disabled');
			$("#fontcolor").attr('disabled', 'disabled');
			$("#myFrameSize").attr('disabled', 'disabled');		
			
			$("#myOpacity").attr('disabled', 'disabled');
			
		}
		if($(this).hasClass("focus")) {	
			
			$(selectMe).addClass("selected");
			//$('<a href="#" style="position: absolute; bottom:-13px; left: 0;" onclick="$(this).parent().remove();"><strong>X</strong></a>').appendTo(selectMe);
			if($(this).hasClass("gtext") && !$(this).children(".textspace").size() > 0) {
				$(this).resizable( "destroy" )
				var con = $(this).html();
				$(this).html('<textarea class="textspace" style="color: black; background: transparent;">'+con+'</textarea>');
				var fsz = $(this).css("font-size");
				$(this).children(".textspace").css("font-size", fsz);
				$(this).resizable({ containment: '.cat_workbench' });				
			}
			if($(this).hasClass("gimage") && !$(this).children(".gimg").size() > 0) {
			//if($(this).css("background-image") == "none") {
					//var bgimg = "sdfgdgf"; //$(this).css("background-image");
					
					var gbgimage = prompt("Picture URL", ""); 
					
					//$(this).css("background-image", "url(" + gbgimage + ")");
					$(this).resizable( "destroy" )
					$(this).html('<img src="' + gbgimage + '" alt="" style="width:100%; height:100%;" class="gimg" />');	
					$(this).resizable({ containment: '.cat_workbench' });
				
			}
			
			//$(this).css("border-style", "dashed");
			
			$("#myFontSize").val($(this).css("font-size"));
			$("#myFrameSize").val($(this).css("borderLeftWidth"));
			$("#myOpacity").val($(this).css("opacity"));
			
			$("#myFont").removeAttr('disabled');
			$("#myFontSize").removeAttr('disabled');
			
			$("#frmcolor").removeAttr('disabled');
			$("#bgcolor").removeAttr('disabled');
			$("#fontcolor").removeAttr('disabled');
			$("#myFrameSize").removeAttr('disabled');
			
			$("#myOpacity").removeAttr('disabled');

		}
	});
	//alert(selectMe);
}

function bringtofront(div) {
	
	var zmax = 0;
	
	$(div).siblings( '.pagestack' ).each(function() {
		var cur = $(div).css( 'zIndex');
		zmax = cur > zmax ? $(div).css( 'zIndex') : zmax;
	});
	
	$(div).css( 'zIndex', zmax+1 );

}

function getDiv(myElements) {
    
	var div = [];
	var i = 0;
	var offset;
	var myElement;
    
    while ((myElement = myElements[i++])) 
	{
		
		// aPostition messes here
		
        offset = $(myElement).offset();
		poffset = $(".cat_workbench").offset();
        
		div.push([
            offset.top - poffset.top,
            offset.left - poffset.left,
            $(myElement).width(),
            $(myElement).height() 
        ]);
    }
    
    return div;
}

function checkOverlap(div1, div2) {
    var x1 = div1[0], y1 = div1[1],
        w1 = div1[2], h1 = div1[3],
        x2 = div2[0], y2 = div2[1],
        w2 = div2[2], h2 = div2[3];
    return !(y2 + h2 < y1 || y1 + h1 < y2 || x2 + w2 < x1 || x1 + w1 < x2);
}

function testOverlap(arg1, arg2) {
	
    var div1   = getDiv(arg1);
    var div2   = getDiv(arg2);
    var index1  = 0;
    var index2  = 0;
    var length1 = div1.length;
    var length2 = div2.length;

    for (; index1 < length1; index1++) {
		
        for (index2 = 0; index2 < length2; index2++) {
        
		    if (arg1[index1] === arg2[index2]) {
                continue;
            }
        
		    if (checkOverlap(div1[index1], div2[index2])) {
                return true;
            }
        }
		
    }
    
    return false;
}

$(function() {
	
	$(".cat_workbench").bind('click', function() {
							unselectAll();
						});
	
	$.fn.aPosition = function() {
		thisLeft = this.offset().left;
		thisTop = this.offset().top;
		thisParent = this.parent();
		parentLeft = thisParent.offset().left;
		parentTop = thisParent.offset().top;
		return {
			left: thisLeft-parentLeft,
			top: thisTop-parentTop
		}
	} 
	
	var offset = {
		left: 100,
		top: 100	
	}; 
	 
	// Draggable win: 
	 
	$('.cat_tools').draggable({ opacity: 0.7, stack: ".winstack", refreshPositions: true });
	$('.cat_file').draggable({ opacity: 0.7, stack: ".winstack", refreshPositions: true });
	$('.cat_mytools').draggable({ opacity: 0.7, stack: ".winstack", refreshPositions: true });

	
	// Toggle some...
	
	$('.cat_mytools').toggle();
	
	// init
	
	$('.cat_elements').draggable({ opacity: 0.7, stack: ".winstack", refreshPositions: true });
	 
	$(".cat_element").draggable({
    	helper: 'clone',
		appendTo: '.cat_workbench',
		refreshPositions: true,
		stop: function(event, ui) {
			offset = $(this).position();
			
		}
	});

	$(".cat_workbench").droppable({
  			drop: function(event, ui) {
							
				if($(ui.draggable).hasClass("cat_element")) {	
				
					var pos = ui.helper.aPosition();
    				//alert('top: ' + pos.top+ ', left: ' + pos.left); 				
													
					$(".cat_workbench").append($(ui.draggable).clone());
					$(".cat_workbench .cat_element").addClass("item"+el);
	
					$(".item"+el).addClass("welement");
                	$(".item"+el).removeClass("ui-draggable");
                	$(".item"+el).removeClass("cat_element");
					$(".item"+el).addClass("pagestack");

					$(".item"+el).draggable({
                        containment: '.cat_workbench',
						opacity: 0.7, 
						stack: ".pagestack", 
						refreshPositions: true
                	});
					//$(".cat_workbench").html(offset.left);
					$(".item"+el).css("left", pos.left);
					$(".item"+el).css("top", pos.top);
					
					$(".item"+el).html("");
					
					if($(".item"+el).hasClass("gimage")) {
						//elements[i][0] = "gimage";			// typ
					}	
					if($(".item"+el).hasClass("gtext")) {
						//elements[i][0] = "gtext";			// typ
						$('<textarea class="textspace"></textarea>').appendTo(".item"+el);	
						
						// set defaut font size & face
						var fsz = $(".item"+el).css("font-size");
						$(".item"+el).children(".textspace").css("font-size", fsz);
					}

					//bringtofront(".item"+el);
					$(".item"+el).resizable({ containment: '.cat_workbench' });

					$(".item"+el).bind({
						click: function() {
							selectItem(this);
						},
						mouseenter: function() {
							$(this).addClass("focus");	
						},
						mouseleave: function() {
							$(this).removeClass("focus");	
						}

						
					});
					
					el++;
					
					
				}
			}
	});
	
	$(".cat_workbench").mouseup(function(e){
			//1. find all div inside selection
			//2. mark them  
	
			$('.cat_workbench').children().each(
    			function(){
										
					if( testOverlap($(".cat_workbench").find(".selector"), $(this)) ) {
						$(this).addClass("selected");
					}
					
			});
				
			//reset
			
			sX = 0;
			sY = 0;
			cX = 0;
			cY = 0;
			$(".cat_workbench").find(".selector").remove();
			startSelect = 0;		
	}).mousedown(function(e){
		if($(".cat_workbench").children(".focus").length == 0) {
			sX = e.pageX - this.offsetLeft;
			sY = e.pageY - this.offsetTop;
			$('<div class="selector"></div>').appendTo(".cat_workbench");
			startSelect = 1;	
		}
			
	}).mousemove(function(e){
		
      $('#pagex').html(e.pageX - this.offsetLeft);
	  $('#pagey').html(e.pageY - this.offsetTop);
	  if(startSelect == 1) {
		
		var cX = e.pageX - this.offsetLeft;
		var cY = e.pageY - this.offsetTop;
		
		if(sX >	cX) {
			$(".cat_workbench").find(".selector").css("left", cX); 
			$(".cat_workbench").find(".selector").css("width", sX - cX);
		} else {
			$(".cat_workbench").find(".selector").css("left", sX);
			$(".cat_workbench").find(".selector").css("width", cX - sX);
		}
		if(sY > cY) {
			$(".cat_workbench").find(".selector").css("top", cY);
			$(".cat_workbench").find(".selector").css("height", sY - cY);
		} else {
			$(".cat_workbench").find(".selector").css("top", sY); 
			$(".cat_workbench").find(".selector").css("height", cY - sY);
		}
	  } 
 	}); 

	
});