<?
 /*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

date_default_timezone_set(date_default_timezone_get());

ini_set("error_reporting", "E_ALL  & ~E_NOTICE");
ini_set("display_errors", "on");

$sql = "U0VUIFNRTF9NT0RFPSJOT19BVVRPX1ZBTFVFX09OX1pFUk8iOw0KDQpDUkVBVEUgVEFCTEUgSUYgTk9UIEVYSVNUUyBgb2ViY19jYXRlZ29yaWVzYCAoDQogIGBJRGAgaW50KDExKSBOT1QgTlVMTCBhdXRvX2luY3JlbWVudCwNCiAgYGNpZGAgaW50KDExKSBOT1QgTlVMTCwNCiAgYG5hbWVgIHZhcmNoYXIoMjU2KSBjb2xsYXRlIGxhdGluMV9nZW5lcmFsX2NpIE5PVCBOVUxMLA0KICBgcGFyZW50YCBpbnQoMTEpIE5PVCBOVUxMLA0KICBgbGV2ZWxgIGludCgxMSkgTk9UIE5VTEwsDQogIGBiZXN0b2ZmZXJgIGludCgxMSkgTk9UIE5VTEwsDQogIGBsZWFmYCBpbnQoMTEpIE5PVCBOVUxMLA0KICBgZXhwaXJlZGAgaW50KDExKSBOT1QgTlVMTCwNCiAgYHZpcnR1YWxgIGludCgxMSkgTk9UIE5VTEwsDQogIGB2YXJpYW50c2AgaW50KDExKSBOT1QgTlVMTCwNCiAgUFJJTUFSWSBLRVkgIChgSURgKSwNCiAgS0VZIGBuYW1lYCAoYG5hbWVgKSwNCiAgS0VZIGBjaWRgIChgY2lkYCksDQogIEtFWSBgcGFyZW50YCAoYHBhcmVudGApLA0KICBLRVkgYGxldmVsYCAoYGxldmVsYCksDQogIEtFWSBgYmVzdG9mZmVyYCAoYGJlc3RvZmZlcmApDQopIEVOR0lORT1NeUlTQU0gREVGQVVMVCBDSEFSU0VUPWxhdGluMSBDT0xMQVRFPWxhdGluMV9nZW5lcmFsX2NpIEFVVE9fSU5DUkVNRU5UPTEgOw0KDQpDUkVBVEUgVEFCTEUgSUYgTk9UIEVYSVNUUyBgb2ViY19leHBvcnRzYCAoDQogIGBpZGAgaW50KDExKSBOT1QgTlVMTCBhdXRvX2luY3JlbWVudCwNCiAgYHRlbXBsYXRlYCBsb25ndGV4dCBjb2xsYXRlIGxhdGluMV9nZW5lcmFsX2NpIE5PVCBOVUxMLA0KICBgbmFtZWAgdmFyY2hhcigyNTYpIGNvbGxhdGUgbGF0aW4xX2dlbmVyYWxfY2kgTk9UIE5VTEwsDQogIGBhY3RpdmVgIGludCgxMSkgTk9UIE5VTEwsDQogIGBpbnRlcnZhbGAgaW50KDExKSBOT1QgTlVMTCwNCiAgYGxhdGVzdGAgaW50KDExKSBOT1QgTlVMTCwNCiAgYGZpbGVuYW1lYCB2YXJjaGFyKDI1NikgY29sbGF0ZSBsYXRpbjFfZ2VuZXJhbF9jaSBOT1QgTlVMTCwNCiAgUFJJTUFSWSBLRVkgIChgaWRgKQ0KKSBFTkdJTkU9TXlJU0FNICBERUZBVUxUIENIQVJTRVQ9bGF0aW4xIENPTExBVEU9bGF0aW4xX2dlbmVyYWxfY2kgQVVUT19JTkNSRU1FTlQ9MyA7DQoNCkNSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIGBvZWJjX29mZmVyc2AgKA0KICBgYWlkYCBpbnQoMTEpIE5PVCBOVUxMIGF1dG9faW5jcmVtZW50LA0KICBgZWlkYCBiaWdpbnQoMTEpIE5PVCBOVUxMLA0KICBgb2lkYCB2YXJjaGFyKDY0KSBjb2xsYXRlIGxhdGluMV9nZW5lcmFsX2NpIE5PVCBOVUxMLA0KICBgZmVlc2AgdmFyY2hhcig2NCkgY29sbGF0ZSBsYXRpbjFfZ2VuZXJhbF9jaSBOT1QgTlVMTCwNCiAgYHN0b2NrYCBpbnQoMTEpIE5PVCBOVUxMLA0KICBgc29sZGAgaW50KDExKSBOT1QgTlVMTCwNCiAgYGxhc3RgIGRhdGV0aW1lIE5PVCBOVUxMLA0KICBQUklNQVJZIEtFWSAgKGBhaWRgKSwNCiAgS0VZIGBlaWRgIChgZWlkYCkNCikgRU5HSU5FPU15SVNBTSBERUZBVUxUIENIQVJTRVQ9bGF0aW4xIENPTExBVEU9bGF0aW4xX2dlbmVyYWxfY2kgQVVUT19JTkNSRU1FTlQ9MSA7DQoNCkNSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIGBvZWJjX3NldHRpbmdzYCAoDQogIGBvcHRpb25gIHZhcmNoYXIoMjU2KSBOT1QgTlVMTCwNCiAgYHZhbHVlYCBtZWRpdW10ZXh0IE5PVCBOVUxMLA0KICBQUklNQVJZIEtFWSAgKGBvcHRpb25gKQ0KKSBFTkdJTkU9TXlJU0FNIERFRkFVTFQgQ0hBUlNFVD11dGY4Ow0KDQpJTlNFUlQgSU5UTyBgb2ViY19zZXR0aW5nc2AgKGBvcHRpb25gLCBgdmFsdWVgKSBWQUxVRVMNCignY2F0X3ZlcnNpb24nLCAnJyksDQooJ2NhdF94bWwnLCAnJyksDQooJ3RpdGxlJywgJ09FQkMgMC41JyksDQooJ3B1Ymxpc2hlcicsICdPRUJDIChjKSAyMDEwLTIwMTIgQWxleGFuZGVyIFBpY2sgKGFwQHBidC1tZWRpYS5jb20pJyksDQooJ3VybCcsICcnKSwNCignY3VyJywgJycpLA0KKCdvcnQnLCAnRGV1dHNjaGxhbmQnKSwNCigncF9wcCcsICcxJyksDQooJ3BfdWViJywgJycpLA0KKCdwX25uJywgJycpLA0KKCdwX290JywgJzEnKSwNCigndGVtcGxhdGUnLCAneyRhdWN0aW9udGl0bGV9XHJcbjxpbWcgc3JjPSJ7JGFydGljbGVwaWN0dXJlfSIgYWx0PSJQcm9kdWt0YmlsZCI+XHJcbnskYXVjdGlvbmRldGFpbHN9JyksDQooJ0dhbGxlcnlUeXBlJywgJ0dhbGxlcnknKSwNCignTGlzdGluZ0R1cmF0aW9uJywgJ0dUQycpLA0KKCdzaGlwcGluZycsICcyLjkwJyksDQooJ3NoaXBwaW5nMicsICcwJyksDQooJ2ZyZWVTaGlwcGluZycsICcnKSwNCignZGVsaXZlcnknLCAnREVfU29uc3RpZ2VEb21lc3RpYycpLA0KKCdkdXNlcicsICdiaXR0ZUBhdXNmdWVsbGVuLmRlJyksDQooJ2ljb256b29tJywgJ29uJyksDQooJ25vdGVzJywgJ1RoaXMgaXMgYSBub3RlJyksDQooJ2xhbmQnLCAnNzcnKSwNCignRGlzcGF0Y2hUaW1lTWF4JywgJzInKSwNCigndG9rZW4nLCAnJyksDQooJ2xvY2F0aW9uJywgJ0RldXRzY2hsYW5kJyksDQooJ3Bfbm8nLCAnJyksDQooJ3NpZCcsICcnKSwNCigndmFsaWQnLCAnJyksDQooJ2VzRGVzJywgJ2VCYXkgU3RvcmUgQmVzY2hyZWlidW5nJyksDQooJ2VzSG9tZScsICdodHRwOi8vc3RvcmVzLmViYXkuZGUvaWhyU3RvcmUnKSwNCignZXNMb2dvJywgJ2h0dHA6Ly93d3cubG9nby51cmwnKSwNCignZXNOYW1lJywgJ0lociBlQmF5IFN0b3JlJyksDQooJ3BwX2VtYWlsJywgJ2locmVAcGF5cGFsZW1haWwuZGUnKSwNCignUmVmdW5kT3B0aW9uJywgJzEnKSwNCignUmV0dXJuc1dpdGhpbk9wdGlvbicsICdEYXlzXzE0JyksDQooJ1JldHVybkRlc2NyaXB0aW9uJywgJ1dpZGVycnVmc2JlbGVocnVuZ1xyXG4tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbldpZGVycnVmc3JlY2h0XHJcbi0tLS0tLS0tLS0tLS0tJyksDQooJ1NoaXBwaW5nQ29zdFBhaWRCeU9wdGlvbicsICdCdXllcicpLA0KKCdlbGFuZCcsICdERScpLA0KKCdiZXN0YW5kJywgJzInKSwNCignY3Jvbl8xJywgJzEzMjkzMTQ3MDEnKSwNCignY3Jvbl82JywgJzEzMjkzMTk4MDInKSwNCignY3Jvbl8xMicsICcxMzI5MzQyOTAyJyksDQooJ2Nyb25fMjQnLCAnMTMyOTM4NDYwMicpLA0KKCdiZ2ltYWdlJywgJy9pbWFnZXMvYmFja2dyb3VuZC9iZy5qcGcnKSwNCignYmdraW5kJywgJzEnKSwNCignYmdjb2xvcicsICdyZ2IoMTQwLCAxNDAsIDE0MCknKSwNCignZ3AnLCAnJyksDQooJ2diJywgJycpLA0KKCdzaG9wZnRwYWN0aXZlJywgJ29uJyksDQooJ3Nob3BmdHAnLCAnJyksDQooJ3Nob3BmdHB1c2VyJywgJycpLA0KKCdzaG9wZnRwcHcnLCAnJyksDQooJ3Nob3BmdHB2ZXJ6JywgJycpLA0KKCdzaG9wbmFtZScsICdpaHJTaG9wJyksDQooJ2ZsaWNrcicsICdodHRwOi8vYXBpLmZsaWNrci5jb20vc2VydmljZXMvZmVlZHMvcGhvdG9zX2ZhdmVzLmduZT9uc2lkPTU0MTE1MjkxQE4wMyZmb3JtYXQ9cnNzXzIwMCcpLA0KKCdjcm9uX3BwJywgJzEnKSwNCignY3Jvbl8wMDUnLCAnMTMyOTMxMjAwMScpLA0KKCdjcm9uXzAxNScsICcxMzI5MzEyMzAxJyksDQooJ2Nyb25fMDMwJywgJzEzMjkzMTM1MDInKSwNCignZ3NhY3RpdmUnLCAnb24nKSwNCignZ21hY3RpdmUnLCAnb24nKSwNCignZ3NhJywgJ29uJyksDQooJ2dzYXInLCAnMC45JyksDQooJ2dzYWMnLCAnZGFpbHknKSwNCignZ3NrJywgJ29uJyksDQooJ2dza2MnLCAnJyksDQooJ2dza3InLCAnMC42JyksDQooJ2dzYycsICdvbicpLA0KKCdnc2NyJywgJzAuMycpLA0KKCdnc2NjJywgJ2RhaWx5JyksDQooJ2dtdScsICdYWFhYREVNT1hYWFgnKTsNCg0KQ1JFQVRFIFRBQkxFIElGIE5PVCBFWElTVFMgYG9lYmNfc2hvcGNhdHNgICgNCiAgYGlkYCBpbnQoMTEpIE5PVCBOVUxMIGF1dG9faW5jcmVtZW50LA0KICBgZWlkYCBpbnQoMTEpIE5PVCBOVUxMLA0KICBgbmFtZWAgdmFyY2hhcigyNTYpIGNvbGxhdGUgbGF0aW4xX2dlbmVyYWxfY2kgTk9UIE5VTEwsDQogIGBwYXJlbnRgIGludCgxMSkgTk9UIE5VTEwsDQogIFBSSU1BUlkgS0VZICAoYGlkYCkNCikgRU5HSU5FPU15SVNBTSBERUZBVUxUIENIQVJTRVQ9bGF0aW4xIENPTExBVEU9bGF0aW4xX2dlbmVyYWxfY2kgQVVUT19JTkNSRU1FTlQ9MSA7DQoNCkNSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIGBvZWJjX3RyYWRlc2AgKA0KICBgaWRgIGludCgxMSkgTk9UIE5VTEwgYXV0b19pbmNyZW1lbnQsDQogIGBlaWRgIGJpZ2ludCgyMCkgTk9UIE5VTEwsDQogIGBveG9yZGVyYCB2YXJjaGFyKDI1NikgY29sbGF0ZSBsYXRpbjFfZ2VuZXJhbF9jaSBOT1QgTlVMTCwNCiAgYGNyZWF0ZWRhdGVgIHZhcmNoYXIoMjU2KSBjb2xsYXRlIGxhdGluMV9nZW5lcmFsX2NpIE5PVCBOVUxMLA0KICBgdGltZWAgZGF0ZXRpbWUgTk9UIE5VTEwsDQogIFBSSU1BUlkgS0VZICAoYGlkYCkNCikgRU5HSU5FPU15SVNBTSBERUZBVUxUIENIQVJTRVQ9bGF0aW4xIENPTExBVEU9bGF0aW4xX2dlbmVyYWxfY2kgQVVUT19JTkNSRU1FTlQ9MSA7DQoNCkNSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIGBvZWJjX3VzZXJgICgNCiAgYHVpZGAgaW50KDExKSBOT1QgTlVMTCBhdXRvX2luY3JlbWVudCwNCiAgYG5hbWVgIHZhcmNoYXIoMTI4KSBOT1QgTlVMTCwNCiAgYHBhc3N3ZGAgdmFyY2hhcigzMikgTk9UIE5VTEwsDQogIGBzZWVuYCB0aW1lc3RhbXAgTk9UIE5VTEwgZGVmYXVsdCBDVVJSRU5UX1RJTUVTVEFNUCwNCiAgYGVkaXRfdHlwZWAgaW50KDExKSBOT1QgTlVMTCwNCiAgYGVtYWlsYCB2YXJjaGFyKDEyOCkgTk9UIE5VTEwgZGVmYXVsdCAnMCcsDQogIGBzZXNzaW9uYCB2YXJjaGFyKDMyKSBOT1QgTlVMTCwNCiAgUFJJTUFSWSBLRVkgIChgdWlkYCksDQogIEtFWSBgTGV2ZWxgIChgZW1haWxgKQ0KKSBFTkdJTkU9TXlJU0FNICBERUZBVUxUIENIQVJTRVQ9dXRmOCBBVVRPX0lOQ1JFTUVOVD0yOSA7";

$htaccess = "
AddDefaultCharset OFF
RewriteEngine On

RewriteRule ^admin/(.*) /index.php?mode=adm

ErrorDocument 404 /
"

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>OEBC Installer</title>
<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	color: #000;
}
body {
	background-color: #333;
}
#installer {
	margin: 10px auto;
	width: 600px;	
	padding:10px;
	border: 1px solid #ccc;
	background-color: #e1e0e0 !important;
    background: -moz-linear-gradient(top,  #e1e0e0, #FFFFFF);
    background: -webkit-gradient(linear, left top, left bottom, from(#e1e0e0), to(#FFFFFF));
    -pie-background: linear-gradient(90deg, #FFFFFF, #e1e0e0);	
	background: -o-linear-gradient(top, #e1e0e0, #FFFFFF);
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    -khtml-border-radius: 4px;
    border-radius: 4px;
	-moz-box-shadow: 2px 2px 3px #000;
	-webkit-box-shadow: 2px 2px 3px #000;
	box-shadow: 2px 2px 3px #000;
	-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=135, Color='#000000')";
	filter: progid:DXImageTransform.Microsoft.Shadow(Strength=2, Direction=135, Color='#000000');	
}
#installer #copyright {
	font-size: 9px;
	text-align: center;
}
#installer em {
	color: #F00;
}
table tr td input {
	width: 90%;
}
</style>
</head>

<body>
<div id="installer">
<img src="images/system/logo.png" width="280" height="96" />
<h1>Installation</h1>
<?
	if(	$_REQUEST["database"] && $_REQUEST["user"] && $_REQUEST["host"] && $_REQUEST["password"] &&
		$_REQUEST["oebcusername"] && $_REQUEST["oebcpassword"] && $_REQUEST["shopurl"] && $_REQUEST["shopftphost"] &&
		$_REQUEST["shopftpuser"] && $_REQUEST["shopftppasswd"] && $_REQUEST["shopftpverz"] && $_REQUEST["shopname"]
		) {
	
		// create config file
		
		echo "Creating Config file...<br />";
		
		$configfile = file_get_contents(dirname(__FILE__)."/classes/config.inc.sample.php");
		
		$configfile = str_replace('{$name}', $_REQUEST["user"], $configfile);
		$configfile = str_replace('{$passwd}', $_REQUEST["password"], $configfile);
		$configfile = str_replace('{$host}', $_REQUEST["host"], $configfile);
		$configfile = str_replace('{$db}', $_REQUEST["database"], $configfile);
		
		if(file_exists(dirname(__FILE__)."/classes/config.inc.php")) {
			
			echo "ERROR: config file already exists!<br />";	
		
		} else {
		
			if(!file_put_contents(dirname(__FILE__)."/classes/config.inc.php", $configfile)) {
				echo "Unable to generate the config file";			
			}
		
		}
		
		// install sql files
		
		$sql = base64_decode($sql);
		
		//echo $sql;
		
		echo "Connecting DB ...<br />";
		
		require(dirname(__FILE__)."/classes/db.class.php");
		require(dirname(__FILE__)."/classes/subsystem.class.php");
				
		$db 	= new dbal;
		$files 	= new subsystem($db);
		
		echo mysql_error();
		
		echo "Installing SQL tables...<br />";	

		
		//echo "Testing SQL ... ".$db->version()."<br />";
		
		$sqlQs = explode(";", $sql); 
		
		foreach($sqlQs as $sql) {
			$db->query_exec_noerr($sql);
		}
				
		// Set Options
		echo "Setting Options...<br />";
		
		$files->setOpt("shopname",$db->clean($_REQUEST["shopname"]));
		$files->setOpt("url",$db->clean($_REQUEST["shopurl"]));		
		$files->setOpt("shopftp",$db->clean($_REQUEST["shopftphost"]));
		$files->setOpt("shopftpuser",$db->clean($_REQUEST["shopftpuser"]));
		$files->setOpt("shopftppw",$db->clean($_REQUEST["shopftppasswd"]));	
		$files->setOpt("shopftpverz",$db->clean($_REQUEST["shopftpverz"]));
		
		// create htaccess file
		
		echo "Writing .htaccess file...<br />";
		
		if(!file_put_contents(dirname(__FILE__)."/.htaccess", $htaccess)) {
			echo "Unable to generate the htaccess file, please install it manually from the /misc folder in the install package";	
		}
		
		// create user
		
		echo "Adding User...<br />";
		
		$sql = "INSERT INTO oebc_user (name, passwd, edit_type, email) VALUES ('".$_REQUEST["oebcusername"]."','".md5($_REQUEST["oebcpassword"])."', 0, 0)";
				
		$db->query($sql);
				
?>	
Installation durchgeführt, bitte <em>löschen Sie die Datei install.php</em> und klicken anschliessen <a href="/">hier</a>.
<?	
	} else {
?>
<ul>
  <li>Dieser Installer legt eine .htaccess Datei an, stellen Sie sicher, das sich keine andere .htaccess Datei in diesem Hosting befindet </li>
  <li>Installieren Sie OEBC nicht in einem Unterordner, legen Sie unbedingt eine Subdomain dazu an</li>
  <li>Löschen Sie bitte den Installer nach erfolgreicher Installation aus dem Verzeichnis, das System wird sonst nicht starten</li>
</ul>
<form action="" method="post" name="form1" target="_self" id="form1">
  <table width="600" border="0">
  <tr>
    <td>Datenbank</td>
    <td><label for="database"></label>
      <input type="text" name="database" id="database" /></td>
    </tr>
  <tr>
    <td>Host</td>
    <td><input name="host" type="text" id="host" value="localhost" /></td>
    </tr>
  <tr>
    <td>Benutzer</td>
    <td><input type="text" name="user" id="user" /></td>
    </tr>
  <tr>
    <td>Passwort</td>
    <td><input type="text" name="password" id="password" /></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
      <tr>
    <td>OEBC Benutzer</td>
    <td><input name="oebcusername" type="text" id="oebcusername" value="admin" /></td>
    </tr>
      <tr>
        <td>OEBC Passwort</td>
        <td><input type="text" name="oebcpassword" id="oebcpassword" /></td>
      </tr>
      <tr>
        <td>Shop Name</td>
        <td><input type="text" name="shopname" id="shopname" /></td>
      </tr>
      <tr>
        <td>Shop URL</td>
        <td><input type="text" name="shopurl" id="shopurl" /></td>
      </tr>
      <tr>
        <td>Shop FTP Host</td>
        <td><input name="shopftphost" type="text" id="shopftphost" value="localhost" /></td>
      </tr>
      <tr>
        <td>Shop FTP User</td>
        <td><input type="text" name="shopftpuser" id="shopftpuser" /></td>
      </tr>
      <tr>
        <td>Shop FTP Passwort</td>
        <td><input type="text" name="shopftppasswd" id="shopftppasswd" /></td>
      </tr>
      <tr>
        <td>Shop FTP Verzeichnis</td>
        <td><input name="shopftpverz" type="text" id="shopftpverz" value="./httpdocs" /></td>
      </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="button" id="button" value="Jetzt Installieren!" /></td>
  </tr>
</table>
</form>
<? } ?>
<p id="copyright">OEBC (c) 2010-2012 Alexander Pick (ap@pbt-media.com), veröffentlicht unter GPL<br />
  Mehr Informationen unter <a href="http://www.oebc.de">www.oebc.de</a></p></div>
</body>
</html>