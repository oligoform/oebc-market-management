<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

require dirname(__FILE__)."./../../classes/subsystem.class.php";
require dirname(__FILE__)."/../../classes/db.class.php";
require dirname(__FILE__)."/../../classes/oxid.class.php";

$db 	= new dbal;
$files 	= new subsystem($db);

require(realpath(dirname(__FILE__)."/../../apps/ebaymng/classes/eBay.inc.php"));
require(realpath(dirname(__FILE__)."/../../apps/ebaymng/classes/eBay.class.php"));

// Get orders form eBay
// check for id in db, check if marked, if not mark and substract and add order to shop
	

$page = 0;	

do {

$page++;
	
$sl = new eBayGetSellerTransactions();
$sl->_page = $page;
				
$eres = $sl->callEbay();
		
$responseDoc = new DomDocument();
$responseDoc->loadXML($eres);
	
//var_dump($responseDoc->toArray());
	
//echo $eres;
	
$responses = $responseDoc->getElementsByTagName("GetSellerTransactionsResponse");
	
$a_items = array();
	
foreach ($responses as $response) 
{
		$acks = $response->getElementsByTagName("Ack");
		$ack   = $acks->item(0)->nodeValue;
		//echo "Ack = $ack <BR />\n";   // Success if successful
				
		if($ack != "Success") {
			//prevent high load
			echo "ebay reported an error, quitting!";
			echo $eres;
			exit(0);
		}

		$items  = $response->getElementsByTagName("Transaction");

		for($i=0; $i<$items->length; $i++) 
		{
			//echo $items->item($i)->getElementsByTagName('ItemID')->item(0)->nodeValue."\n";
			
			$eid = $items->item($i)->getElementsByTagName('ItemID')->item(0)->nodeValue;
			
			//3echo $eid."<br />";

			$sql = "SELECT * FROM oebc_offers WHERE eid = $eid";
			$res = $db->query($sql);	 		
		
			if(!empty($res)) {
				
				echo $eid."<br />";
				//echo $files->dom_dump($items->item($i));
				
				$cd = $items->item($i)->getElementsByTagName('CreatedDate')->item(0)->nodeValue;
				$sql = "SELECT count(*) FROM oebc_trades WHERE createdate = '$cd'";
				$res2 = $db->query($sql);
				
				//echo $res2[0]."\n";
				
				if(!$res2[0]) {
					
					echo "ItemID: ".$items->item($i)->getElementsByTagName('ItemID')->item(0)->nodeValue."<br />";
					
					
					// RegistrationAddres -> Bill
					
					if($items->item($i)->getElementsByTagName('RegistrationAddress')->item(0)) {
						//echo $files->dom_dump($items->item($i)->getElementsByTagName('RegistrationAddress')->item(0));
					
						$bname			=	$items->item($i)->getElementsByTagName('RegistrationAddress')->item(0)->getElementsByTagName('Name')->item(0)->nodeValue;
						$bstreet 		=	$items->item($i)->getElementsByTagName('RegistrationAddress')->item(0)->getElementsByTagName('Street')->item(0)->nodeValue;
					
						$bcity			=	$items->item($i)->getElementsByTagName('RegistrationAddress')->item(0)->getElementsByTagName('CityName')->item(0)->nodeValue;
						$bplz 			= 	$items->item($i)->getElementsByTagName('RegistrationAddress')->item(0)->getElementsByTagName('PostalCode')->item(0)->nodeValue;
						$bcountry		=	$items->item($i)->getElementsByTagName('RegistrationAddress')->item(0)->getElementsByTagName('Country')->item(0)->nodeValue;
					} else {
						$bname			=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('Name')->item(0)->nodeValue;
						$bstreet 		=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('Street1')->item(0)->nodeValue;
						$bcity			=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('CityName')->item(0)->nodeValue;
						$bplz 			= 	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('PostalCode')->item(0)->nodeValue;
						$bcountry		=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('Country')->item(0)->nodeValue;
					}
					
					/* Hier mus smöglciheriwes enoch ein Fallback hin bzgl. keienr vorhandener Shipping Adresse, das der Kauf erstmal geskipt wird bis die Kaufabwicklung erledigt ist. */
					
	
					/*---------------*/
					
					//ShippingAdress -> Del
					
					$sname			=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('Name')->item(0)->nodeValue;
					$sstreet 		=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('Street1')->item(0)->nodeValue;
					
					$scity			=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('CityName')->item(0)->nodeValue;
					$splz 			= 	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('PostalCode')->item(0)->nodeValue;
					$scountry		=	$items->item($i)->getElementsByTagName('ShippingAddress')->item(0)->getElementsByTagName('Country')->item(0)->nodeValue;
					
					/*---------------*/
					
					
					$tid 			=	$items->item($i)->getElementsByTagName('TransactionID')->item(0)->nodeValue;
					
					$email 			=	$items->item($i)->getElementsByTagName('Email')->item(0)->nodeValue;
					
					$note			=	$items->item($i)->getElementsByTagName('ItemID')->item(0)->nodeValue;
			
					$cur			=	$items->item($i)->getElementsByTagName('Currency')->item(0)->nodeValue;
					
					$price 			=	$items->item($i)->getElementsByTagName('TransactionPrice')->item(0)->nodeValue;
					
					$quanity 		=	$items->item($i)->getElementsByTagName('QuantityPurchased')->item(0)->nodeValue;
										
					$variant 		= 	$items->item($i)->getElementsByTagName('Variation');
					
					$variants = array();	
					
					if($variant->length != 0) {
					
						// Variante suchen	
						
						$nameval = $variant->item(0)->getElementsByTagName('NameValueList'); 
						
						foreach($nameval as $nv) {
							$v = $nv->getElementsByTagName('Value')->item(0)->nodeValue;
							array_push($variants, $v);
						}
						
					}
					
					/*
			<Status>
      			<eBayPaymentStatus>NoPaymentFailure</eBayPaymentStatus>
      			<CheckoutStatus>CheckoutComplete</CheckoutStatus>
      			<LastTimeModified>2010-09-09T11:20:12.000Z</LastTimeModified>
      			<PaymentMethodUsed>PayPal</PaymentMethodUsed>
      			<CompleteStatus>Complete</CompleteStatus>
      			<BuyerSelectedShipping>true</BuyerSelectedShipping>
      			<PaymentHoldStatus>None</PaymentHoldStatus>
      			<IntegratedMerchantCreditCardEnabled>false</IntegratedMerchantCreditCardEnabled>
    		</Status>
					*/
				
					$p_status		= 	$items->item($i)->getElementsByTagName('CompleteStatus')->item(0)->nodeValue;		
					$p_method		= 	$items->item($i)->getElementsByTagName('PaymentMethodUsed')->item(0)->nodeValue;
					
					
					//$title		= 	$items->item($i)->getElementsByTagName('Title')->item(0)->nodeValue;	//OXTITLE
					
					$ox = new oxid($db);
					$oxorder = $ox->createSingleOrderFromExternal(	$bname, $bstreet, $bcity, $bplz, $bcountry,
																	$sname, $sstreet, $scity, $splz, $scountry,	
																	$email, $note, $cur, $price, $quanity, $variants,
																	$eid, $tid, $p_status, $p_method
																);
					
					$sql = "INSERT INTO `oebc_trades` (`eid` ,`oxorder` ,`createdate` ,`time`)
							VALUES ('".$db->clean($eid)."', '".$db->clean($oxorder)."', '".$db->clean($cd)."', NOW( ))";
										
					$db->query_exec($sql);		
					
					$bes = $files->getOpt("bestand");
					
					if($bes == "2") 
					{
					
						if(empty($variants)){
							
							$sql 	= "SELECT * FROM oxarticles WHERE OXID = '".$res["oid"]."'";
							
							
						} else {
							
							$sql 	= "SELECT * FROM oxarticles WHERE OXPARENTID = '".$res["oid"]."'";
						
							foreach($variants as $var) {
								$sql .= " AND OXVARSELECT LIKE '%".$db->clean($var)."%'";						
							}
						}
						
						$rs 	= $db->query($sql);
												
						if(!empty($rs)) {
							$ox->correctStock($rs["OXID"], $quanity, $rs["OXPARENTID"]);
						}
					}
							
				}
			}
		
		}	
		
}
	
} while($responseDoc->getElementsByTagName('TotalNumberOfPages')->item(0)->nodeValue != $page);

// revise listings if order in store

?>
