<?
 /*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	
if(!$uid || !defined('isOEBC'))
{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
}

require "../classes/subsystem.class.php";
require "../classes/db.class.php";
require "../classes/oxid.class.php";

require "../classes/smarty/Smarty.class.php";

if(!$_REQUEST["time"]) {
		echo "no time!";
		exit(1);
}

$db 	= new dbal;
$files 	= new subsystem($db);
$smarty = new smarty;

$smarty->compile_dir  = dirname(__FILE__).'/../tmp/';

$mytemplate = "";

$sql = "SELECT * FROM `oebc_exports` WHERE `active` = '1' AND `interval` = '".$db->clean($_REQUEST["time"])."'";
$result = $db->query_array($sql);

if($_REQUEST["verbose"]) {
	echo "<strong>Ergebnis</strong><br />";
	echo "Führe Interval ".$_REQUEST["time"]." aus<br />";
}

function var_template($tpl_name, &$tpl_source, &$smarty_obj)
{
	global $files;
	global $mytemplate;
   
	$tpl_source = $mytemplate;
		
	return empty($tpl_source) ? false : true;
}

function var_timestamp($tpl_name, &$tpl_timestamp, &$smarty_obj)
{
    //echo "timestamp called!";
			
	$time = $smarty_obj->get_template_vars($tpl_name.'_time');
	return !empty($time) ? $time : time();
}

function var_secure($tpl_name, &$smarty_obj)
{
	return true;
}

function var_trusted($tpl_name, &$smarty_obj)
{
} 

foreach($result as $res) {
	
	$mytemplate = $res["template"];

	//echo $res["template"];

	$tpl = new smarty;
	$tpl->compile_dir  = dirname(__FILE__).'/../tmp/';
		
	$rname = rand();	
		
		// register the resource name "var"
	$tpl->register_resource($rname, array("var_template",
                                     "var_timestamp",
                                     "var_secure",
                                     "var_trusted"));

	//$tpl->clear_cache("var:map");
	
	$tpl->template_dir = dirname(__FILE__).'/../templates/';
	$tpl->compile_dir  = dirname(__FILE__).'/../tmp/';

	$host = $files->getOpt("url");
	$tpl->assign("host", $host);

	$ox = new oxid($db);
	$a = $ox->getSeoUrls();

	$pparentlist = array();

	foreach($a as $aa) {
	if($aa["OXPARENTID"] == "")
	{
		$parentlist[$aa["OXID"]] = $aa;
	}
	}

	$articles = array();

	foreach($a as $aa) {
	
	
	
	if($aa["OXPARENTID"] != "")
	{
		$aa["OXTITLE"] 	= $parentlist[$aa["OXPARENTID"]]["OXTITLE"];	
		$aa["OXLONGDESC"] = $parentlist[$aa["OXPARENTID"]]["OXLONGDESC"];	
		$aa["OXPIC1"] 	= $parentlist[$aa["OXPARENTID"]]["OXPIC1"];
		$aa["OXTAGS"] 	= $parentlist[$aa["OXPARENTID"]]["OXTAGS"];
		
		$aa["OXLONGDESC"] = filter_var($aa["OXLONGDESC"], FILTER_SANITIZE_STRING);
		
		$isd = strstr($aa["OXSTDURL"], 'cl=details');
		if($isd) {
			array_push($articles, $aa);
		}
	} 
		elseif($files->getOpt("gp")) 
	{	
		$isd = strstr($aa["OXSTDURL"], 'cl=details');
		if($isd && ($aa["OXTITLE"] != "")) {
			array_push($articles, $aa);
		}
	}
	
	}
	
	$tpl->assign("articles", $articles);
	
	if($result["latest"]) {
		$latest = 1;
	} else {
		$latest = 0;	
	}
	
	// neuste selector einfügen;
	
	$orders = $ox->getOrders($latest);
	
	//var_dump($orders);
	
	$tpl->assign("orders", $orders);

	$sitemap = $tpldisplay = $tpl->fetch($rname.":map");
	
	//echo $sitemap;
	
	$outfile = str_replace("###date###", date("-d-m-Y") , $res["filename"]);

	$fp = fopen(dirname(__FILE__).'/../filesystem/'.$outfile, 'w');
	fwrite($fp, $sitemap);
	fclose($fp);
	
	if($_REQUEST["verbose"]) {
		echo "Export ausgeführt, Ausgabe in Datei: ".$outfile."<br />";
	}
}

?>
