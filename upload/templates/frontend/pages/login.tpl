{if $fail}
{literal}
<script type="text/javascript">
$(function() {
	if (window != window.top) 
	top.location.href = document.location.href;
	$('.login').effect('shake', { times:3 }, 100); 
});
</script>
{/literal}
{/if}
<div class="ui-widget-content ui-corner-all login" style="min-height:200px;">
<div class="bar ui-widget-header" align="absmiddle"> <span style="float:left;"><img src="/images/system/oebc_mini.png"  align="absmiddle" /> Login</span></div>
<div style="padding:10px;">
<p align="center"><img src="/images/system/logo.png" width="280" height="96" alt="OEBC" /></p>
<center>
  {if $fail}<b style="color:#F00">Login fehlgeschlagen!</b>
  {/if}
</center>
<form method="post" action="/index.php" target="_top">
  <table width="260" border="0" align="center">
  <tr>
    <td>Benutzername</td>
    <td width="260">
        <input type="text" name="name" id="name"  style="width:240px" />
    </td>
  </tr>
  <tr>
    <td>Kennwort</td>
    <td width="260"><input type="password" name="passwd" id="passwd" style="width:240px" /></td>
  </tr>
</table>

<div align="center">
<button class="fg-button ui-corner-all ui-widget-header ui-state-active st-button" type="submit" id="anbieten">Anmelden</button><input value="login" name="mode" id="mode" type="hidden" /><input value="{$target}" name="target" id="target" type="hidden" /><br />
        <div style="position:absolute; right:10px; bottom:5px;"><a href="#" onClick="fullwin()" >Fullscreen Mode</a></div>
</div>
</form>
</div>
</div>
</div>


