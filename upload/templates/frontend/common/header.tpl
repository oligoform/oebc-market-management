<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>{$title}</title>

<meta http-equiv="content-Language" content="de" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />

<meta name="description" content='' />

<meta name="language" content="de" />

<meta name="robots" content="noindex, nofollow">

<meta name="publisher" content="{$publisher}" />
<meta name="copyright" content="Copyright 2009-2012 by {$publisher}" />
<meta name="author" content="Alexander Pick - PBT Media - http://www.pbt-media.de" />

<!--

 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
	
-->

<link rel="shortcut icon" href="favicon.ico" /> 
<link href="/css/style.css" rel="stylesheet" type="text/css" />

<style>
{literal}
body {
	{/literal}
	background-color: {$bgcolor};	
	{literal}
}
{/literal}
</style>

<script language="javascript" src="/javascript/jquery.js" type="text/javascript" ></script>
<script language="javascript" src="/javascript/jquery.ui.js" type="text/javascript" ></script>

<script type="text/javascript">
{literal}
$(function() {
		/* draggables */	
	    $(".login").draggable({ opacity: 0.7 });
	    $(".window").draggable({ opacity: 0.7, stack: ".winstack", iframefix: true });
		$(".resizeable").resizable({ minWidth: 920, minHeight:200 });
		$( "#mainmenu" ).draggable({ snapMode: 'outer', grid: [ 20,20 ] });
	});

function fullwin(){
	window.open("index.php","bfs","fullscreen,scrollbars")
}

{/literal}	
</script>

{include file="frontend/module_header/$mode.tpl"}

</head>
<body>
<!--Content area -->
<div id="content">


