<!-- adm header -->
{literal}

<script language="javascript" src="/javascript/jquery.ui.js"></script>
<script language="javascript" src="/javascript/adm.js"></script>
<script language="javascript" src="/javascript/jquery.backstretch.min.js" type="text/javascript" ></script>

<script type="text/javascript">

function desktop() {
	
	$('.file').draggable({ opacity: 0.7, stack: ".filestack", refreshPositions: true });
			
	$(".file").bind({
		click: function() {
			selectItem(this);
		},
			mouseenter: function() {
			$(this).addClass("focus");	
		},
			mouseleave: function() {
			$(this).removeClass("focus");	
	}

						
	});
	
	$(".file").dblclick(function() {
		var file = $(this).find(".path").val();
		if($(this).find(".is_dir").val() != "") {
			spawngeneric("/?app=fileman&path=" + file, '', 500, 940);
		}
	});
		
	$(".desktop").bind('click', function() {
			unselectAll();
	});
				
	/**********************************************************/
	
	$('.folder').droppable({ accept: ".file", 
    	activeClass: 'droppable-active', 
    	hoverClass: 'droppable-hover', 
    	drop: function(e, ui) { 
		
			folder = $(this).find(".path").val();
			file = $(ui.draggable).find(".filename").val();
			src = $(ui.draggable).find(".path").val();
		
			$.post("/components/ajax.php", {	
					'mode'	   			: 	'movefile',
					'folder'			: 	folder,
					'file'				:	file,
					'src'				:	src
					
			},
			function(){
				$(ui.draggable).remove();
 			});
    	}
	});
		
	loadContext();
}

function deskRefresh() {
	/**********************************************************/	
	$.post("/components/ajax.php", {	
					'mode'	   		: 	'listfile',
					'path'			: 	'/desktop'
					
			},
	function(resdata){
		$(".desktop").html(resdata);
		desktop();
 	});	
}

$(function() {
	
	/*** Widgets ****/	
	{/literal}
		{if $bgimage}
	$.backstretch("{$bgimage}");
		{/if}
	{literal}
	$('.widget').draggable({ opacity: 0.7, stack: ".winstack", refreshPositions: true });
	deskRefresh();
	
	{/literal}
	{if $iconzoom}
	{literal}
	$('.icon').mouseover(function() {
		$(this).animate({"width":"50px", "height":"50px"}, "fast");
	}).mouseout(function(e, ui) {
		$(this).animate({"height":"30px","width":"30px"}, "fast");
	});;
	{/literal}
	{/if}
	{literal}	
				
});
</script>
{/literal}

<!-- adm header end -->

<!-- widget -->
<link href="/javascript/widgets/clock/clock.css" rel="stylesheet" type="text/css" />
<link href="/javascript/widgets/shopstats/stats.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/javascript/widgets/clock/clock.js" type="text/javascript" ></script>
<!-- end widget -->