<? 
 /*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

session_start();

if(file_exists(dirname(__FILE__)."/install.php")) {
	die("Please remove the installer! If you haven't installed the system yet, click <a href='/install.php'>here</a>");
}

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Datum in der Vergangenheit

//disable notcies about timezones on OSX
date_default_timezone_set(date_default_timezone_get());

ini_set("error_reporting", "E_ALL  & ~E_NOTICE");
ini_set("display_errors", "on");

define('isOEBC', TRUE);

require(dirname(__FILE__)."/classes/smarty/Smarty.class.php");
require(dirname(__FILE__)."/classes/db.class.php");
require(dirname(__FILE__)."/classes/auth.class.php");
require(dirname(__FILE__)."/classes/subsystem.class.php");
require(dirname(__FILE__)."/classes/oxid.class.php");


$method = $_SERVER['REQUEST_METHOD'];
$request = split("/", substr(@$_SERVER['PATH_INFO'], 1));

// Init
$db 	= new dbal;
$auth 	= new auth($db);
$files 	= new subsystem($db);
$template = new smarty;

$template->template_dir = dirname(__FILE__).'/templates/';
$template->compile_dir  = dirname(__FILE__).'/tmp/';

// Check Login

$uid = $auth->get_uid();

if(!empty($_REQUEST["app"])) {

	//smarty class laden und die template pfaede festlegen

	$template->template_dir = dirname(__FILE__).'/apps/';

	// Module laden/checken
	$applist 	= $files->getApps(dirname(__FILE__).'/apps/');
	$app 	 	= $_REQUEST["app"];

	$app = ereg_replace("[^A-Za-z0-9]", "", $app);

	$valid_mode = in_array($app, $applist);

	// Grundeinstellungen:
	$publisher = "OEBC";

	$template->assign("publisher", $publisher);
	$template->assign("title", $files->getOpt("title"));

	if($valid_mode) {
	
		$template->assign("app", $app);
		
		if(!empty($_REQUEST["subview"])) {
			$svlist 	= $files->dirList(dirname(__FILE__).'/apps/'.$app.'/subviews/');
			$sv = ereg_replace("[^A-Za-z0-9]", "", $_REQUEST["subview"]);
			
			$valid_sv = in_array($sv, $svlist);

			if($valid_sv) {
				$template->assign("sv", $sv);
				include(dirname(__FILE__).'/apps/'.$app.'/subviews/'.$sv.'.php');	
			} else {
				echo "subview does not exist!";
				exit(0);
			}
			
		} else {
			$sv = $app;
			include(dirname(__FILE__).'/apps/'.$app.'/'.$sv.'.php');	
		}
	
	} else {
		echo "application does not exist!";
		exit(0);
	}
	
	$template->display('common/app.tpl');
	
} else {
	
	if($uid)
	{
		$sql = "SELECT edit_type, name FROM oebc_user WHERE uid = '".$uid."'";
		$res = $db->query($sql);
	
		if($res["edit_type"] == 1) {
			$isAdmin = 1;
			$template->assign("isAdmin", 1);
		} else {
			$isAdmin = 0;
		}
	
		$template->assign("uname", $res["name"]);
		
	}
	
	// apps checken & laden:
	
	$applist 	= $files->getApps(dirname(__FILE__).'/apps/');
	
	//smarty class laden und die template pfaede festlegen

	// Module laden/checken
	$modlist 	= $files->dirList(dirname(__FILE__).'/modules/');
	$mode 	 	= $_REQUEST["mode"];

	$mode = ereg_replace("[^A-Za-z0-9]", "", $mode);

	$valid_mode = in_array($mode, $modlist);

	// Grundeinstellungen:
	$publisher = "OE";

	$template->assign("publisher", $publisher);
	$template->assign("title", $files->getOpt("title"));

	if($valid_mode) {
		$template->assign("mode", $mode);
		include(dirname(__FILE__).'/modules/'.$mode.'.php');	
	} else {
		$template->assign("mode", "startpage");
		include(dirname(__FILE__).'/modules/login.php');	
		//$template->assign("error","true");
	}
	
	// Ausgabe
	if(empty($template->titel)){
		$template->titel = "OEBC";
	}


	$template->assign("bgcolor", $files->getOpt("bgcolor"));
	
	$bgkind =$files->getOpt("bgkind");
	
	if($bgkind == 1) {
		
		$template->assign("bgimage", $files->getOpt("bgimage"));

		
	} elseif($bgkind == 2) {
				
		$ch = @curl_init($files->getOpt("flickr")); 
		curl_setopt($ch,CURLOPT_HEADER,0);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$flickr = @curl_exec($ch);
		
		$responseDoc = new DomDocument();
		$responseDoc->loadXML($flickr);
		
		$bgimages = @$responseDoc->getElementsByTagName('item');
		
		$cbgimages = @$bgimages->length;
		
		$nimage = rand(1,$cbgimages); 
		
		$image = @$bgimages->item($nimage)->getElementsByTagNameNS('http://search.yahoo.com/mrss/', 'content')->item(0)->getAttribute("url");
		
		
		$template->assign("bgimage", $image);	
			
			
	} else {
	
		$template->assign("bgcolor", $files->getOpt("bgcolor"));
		
	}
	
	$template->display('frontend/common/index.tpl'); 

}
?>