<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

class subsystem 
{

	var $dbcon;

	public function __construct($db) {

		global $dbcon;
		$dbcon = $db;

	}

	public function getApps($directory) 
	{
    	$results = array();
	    $handler = opendir($directory);
	
		while ($file = readdir($handler)) {
        if ($file != '.' && $file != '..' && $file != "common") {
				//$file = substr_replace($file, "", -4);
            	$results[] = $file;
			}
	    }

    	closedir($handler);
	
	    return($results);
	}
	
	public function recursiveDel($str)
	{
        
		if(is_file($str)){
            return @unlink($str);
        }
        elseif(is_dir($str)){
            $scan = glob(rtrim($str,'/').'/*');
            foreach($scan as $index=>$path){
                $this->recursiveDel($path);
            }
            return @rmdir($str);
        }
    
	}
	
	public function recursiveCopy($src,$dst) {
    	
		if(is_dir($src)) {
		$dir = opendir($src);
    	@mkdir($dst);
    	while(false !== ( $file = readdir($dir)) ) {
        
			if (( $file != '.' ) && ( $file != '..' )) {
        	    if ( is_dir($src . '/' . $file) ) {
        	        $this->recursiveCopy($src . '/' . $file,$dst . '/' . $file);
        	    }
        	    else {
        	        copy($src . '/' . $file,$dst . '/' . $file);
        	    }
        	}
    	}
    	closedir($dir);
		} else {
			echo $src."\n";
			echo $dst;
			copy($src , $dst);	
		}
	} 
	
	public function shortenText($text) {

        $chars = 8;
		
		$ext = substr($text, -3, 3);
		
		if(strlen($text) >= $chars+3) {
        	$text = substr($text,0,$chars);
	        $text = $text."...".$ext;
		}

        return $text;

    }
	
	public function format_bytes($bytes) {
		
  		if ($bytes < 1024) return $bytes.' Byte';
   	
		elseif ($bytes < 1048576) return round($bytes / 1024, 2).' KByte';
   	
		elseif ($bytes < 1073741824) return round($bytes / 1048576, 2).' MByte';
   	
		elseif ($bytes < 1099511627776) return round($bytes / 1073741824, 2).' GByte';
   	
		else return round($bytes / 1099511627776, 2).' TByte';
	
	}
	
	/*
	
		0 = filename
		1 = shortend filename
		2 = extension
		3 = is_dir	
		4 = icon
	
	*/
	
	public function checkPath($path) {
		
		$rp = realpath($path);
		
		$fsp = realpath(dirname(__FILE__).'/../filesystem//');
		
		if(!strstr($rp, $fsp)) 
		{
			return $fsp;	
		}
		
		return $rp;
		
	}
	
	public function fspath($relativepath) {
   	   
	   $realpath	=	realpath($relativepath);
	   
	   $p = $_SERVER['DOCUMENT_ROOT'];
	   
	   if(substr($_SERVER['DOCUMENT_ROOT'],-1)!='/') {
		   $p .= "/";
	   }
	   
	   $fspath		=	str_replace($p."filesystem",'',$realpath);
	   	   
	   return $fspath;
	}
	
	
	public function dirFS($directory) {
		
		// prevent hacking
		// to be added!
		
		if($directory == "/") {
			$directory = "";
		} 
				
		$rdir = $this->checkPath(dirname(__FILE__).'/../filesystem/'.$directory."/");	
		
				
		$fsfiles = $this->dirListExt($rdir);
		
		$fsfilesnames = array();
		$myFiles = array();
	
		$i = 0;
	
		foreach($fsfiles as $ffile) 
		{
 			 $myFiles[$i][0] 		= $this->fspath($rdir."/")."/".$ffile; 
 			 $myFiles[$i]["path"] 	= $this->fspath($rdir."/")."/".$ffile; 
			 
			 $myFiles[$i][1] = $this->shortenText($ffile);
			 
			 $myFiles[$i][2] 		= substr($ffile, -3, 3);
			 $myFiles[$i]["ext"]	= $myFiles[$i][2];
			 
			 $myFiles[$i][3] 		= is_dir($rdir."/".$ffile);
			 $myFiles[$i]["is_dir"] = $myFiles[$i][3];	
			 
			 if($myFiles[$i]["is_dir"] != "") {
			 	$myFiles[$i][4] 		= "folder";
			 	$myFiles[$i]["icon"] 	= "folder";
				
				$chkFol = dirname(__FILE__).'/../filesystem/'.$directory."/".$ffile;
				$res = $this->dirList($chkFol);
				
			 	if(empty($res)) {
					$myFiles[$i][8] 	= ""; 
				} else {
					$myFiles[$i][8] 	= "1";
				}
			 
			 } elseif($myFiles[$i]["ext"] == "pdf") {
			 	$myFiles[$i][4] 		= "page_white_acrobat";
			 	$myFiles[$i]["icon"] 	= "page_white_acrobat";				 
			 } else {
			 	$myFiles[$i][4] 		= "page_white";
			 	$myFiles[$i]["icon"] 	= "page_white";				 				 
			 }
			 
			 $myFiles[$i][5] 		  = $this->format_bytes(filesize(dirname(__FILE__).'/../filesystem'.$directory.'/'.$ffile));
			 $myFiles[$i]["filesize"] = $myFiles[$i][5];	
			 
			 $myFiles[$i][6] 			= date ("F d Y H:i:s.", filemtime(dirname(__FILE__).'/../filesystem'.$directory.'/'.$ffile));
			 $myFiles[$i]["filedate"] = $myFiles[$i][6];
			 
 			 $myFiles[$i][7] 			= $ffile;
			 $myFiles[$i]["filename"]	= $myFiles[$i][7];	
			 
	
			 $i++; 
		}

		$myFS["path"] = $this->fspath($rdir)."/";

		$myFS["contents"] = $myFiles;	
		
		return $myFS;
	}

	public function dirList($directory) 
	{
    	$results = array();
	    $handler = opendir($directory);
	
		while ($file = readdir($handler)) {
        if ($file != '.' && $file != '..') {
				$file = substr_replace($file, "", -4);
            	$results[] = $file;
			}
	    }

    	closedir($handler);

	    return($results);
	}
	
	public function dirListExt($directory) 
	{
    	$results = array();
	    $handler = opendir($directory);
	
		while ($file = readdir($handler)) {
        if ($file != '.' && $file != '..' ) {
				//$file = substr_replace($file, "", -4);
            	$results[] = $file;
			}
	    }

    	closedir($handler);

	    return($results);
	}
	
	public function getOpt($opt)
	{
		global $dbcon;
		
		$sql 	= "SELECT * FROM `oebc_settings` WHERE `option` = '".$opt."'";
		$res 	= $dbcon->query($sql);
		
		return(	$res["value"] );
	}
	
	public function setOpt($opt, $val)
	{
		global $dbcon;
		
		$sql = "SELECT count(*) FROM oebc_settings WHERE `option` = '".$opt."'";
		$res 	= $dbcon->query($sql);
		if($res[0] != 0) {						
			$sql 	= "UPDATE `oebc_settings` SET `value` = '".$val."' WHERE `option` = '".$opt."' LIMIT 1";
			$dbcon->query_exec($sql);
		} else {
			$sql = "INSERT INTO `oebc_settings` (`option` ,`value`) VALUES ('".$opt."', '".$val."')";
			$dbcon->query_exec($sql);
		}
	}

}

?>