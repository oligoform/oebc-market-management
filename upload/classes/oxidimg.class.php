<?

/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

class oxidimg {

	public function getImg($url) {
		
	$multiPartImageData = file_get_contents($url);

		//look for original
	if($multiPartImageData === false) {
				for($i = 0; $i != 20; $i++) {
			
				$nurl = $url;
			
				$nurl = preg_replace("/\/[1-9]\//", '//'.$i.'/', $nurl);
		    
				$multiPartImageData = file_get_contents($nurl);
				if($multiPartImageData !== false)
				{
					break;	
				}
				}
			} else {
				$nurl = $url;	
			}
			
			// without attached crap
	if($multiPartImageData === false) {
		
		$nurl = preg_replace( '/_p\d+(\.jpg|\.gif|\.png)$/i', '\\1', $url);
		
		for($i = 0; $i != 20; $i++) {
	
			$nurl = preg_replace("/\/[1-9]\//", '//'.$i.'/', $nurl);

		    $multiPartImageData = file_get_contents($nurl);

			if($multiPartImageData !== false)
			{
				break;	
			}
		}
		
		//return $url;
	}
	
	// scan normal image folder if no master is found
	if($multiPartImageData === false) {
		for($i = 0; $i != 20; $i++) {
			
			$nurl = $url;

			$nurl = preg_replace("/\/[1-9]\//", '//'.$i.'/', $nurl);
		    
			$nurl = str_replace("/master/", "", $nurl);
			
			$multiPartImageData = file_get_contents($nurl);
	
			if($multiPartImageData !== false)
			{
				break;	
			}
		}
	}
	// try with common filename
	if($multiPartImageData === false) {
		
		$nurl = preg_replace( '/_p\d+(\.jpg|\.gif|\.png)$/i', '\\1', $url);
		
		for($i = 0; $i != 20; $i++) {
					
			$nurl = preg_replace("/\/[1-9]\//", '//'.$i.'/', $nurl);
			
			$nurl = str_replace("/master/", "", $nurl);
			
		    $multiPartImageData = file_get_contents($nurl);

			if($multiPartImageData !== false)
			{
				break;	
			}
		}
		
	}	
	
	return array($multiPartImageData, $nurl);
		
	}
	
}

?>