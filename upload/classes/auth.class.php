<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

class auth 
{

var $dbcon;

public function __construct($db) {

	global $dbcon;
	
	$dbcon = $db;

}

public function check_user( $name, $pass ) {

	global $dbcon;

	$pass = md5($pass);
	
    if ( get_magic_quotes_gpc() ) {
        $name = stripslashes($name);
        //$pass = stripslashes($pass);
    }
    $name = mysql_real_escape_string($name);
    $name = str_replace('%', '\%', $name);
    $name = str_replace('_', '\_', $name);

    $sql = 'SELECT UID, edit_type FROM oebc_user WHERE name = \'' . $name . '\' AND passwd =\'' . $pass . '\'';

	//echo $sql;
	$result = $dbcon->query_raw($sql);
	//echo mysql_num_rows($result);
	//var_dump(mysql_fetch_assoc($result));

    if ( @mysql_num_rows($result) == 1 ) {
        
		$user = mysql_fetch_assoc($result);		
		return ( $user['UID'] );
		
    } else {
        return ( false );
    }
}

public function login( $userid ) {
    
	global $dbcon;
	
	$sql = 'UPDATE oebc_user SET session = \'' . session_id() . '\' WHERE UID = ' . ((int)$userid);
	
	$dbcon->query_raw($sql);
}


public function logged_in() {

	global $dbcon;

	$sql = 'SELECT UID FROM oebc_user WHERE session = \'' . session_id() . '\'';
	
	$result = $dbcon->query_raw($sql);
	
	$x = @mysql_num_rows($result);
	
    return ($x);
}

public function logoutuser() {

	global $dbcon;
	
    $sql = 'UPDATE oebc_user SET session = NULL WHERE session = \'' . session_id() . '\'';
		
	$result = $dbcon->query_exec($sql);
	
	header('Location: http://'.$_SERVER['SERVER_NAME'].'/');
}

public function get_username() {
	
	global $dbcon;
	
    $sql = 'SELECT name FROM oebc_user WHERE session = \'' . session_id() . '\'';

	$result = $dbcon->query_raw($sql);

	$row = mysql_fetch_object($result);
   
   	return ($row->name);
}

public function get_uid() {

	global $dbcon;

	$sql = 'SELECT UID FROM oebc_user WHERE session = \'' . session_id() . '\'';
	
	$result = $dbcon->query_raw($sql);

	$row = mysql_fetch_object($result);
    
	return ($row->UID);
}

public function check_login($name, $passwd) {
	if ($passwd)
	{
  	  	$id=$this->check_user($name, $passwd);
  	  	if ($id!=false) {
			//echo "ID:".$id;
   	    	$this->login($id);
			return(true);
		}	
	}
	return(false);
}

public function vertify() {
	if (!self::logged_in()) {
		return(false);
	}
	return(true);
}
}

?>
