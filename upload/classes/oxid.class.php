<? 

/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

class oxid 
{

	var $dbcon;

	public function __construct($db) 
	{
		global $dbcon;
		$dbcon = $db;
	}
	
	public function getOXOpt($oxconfvar) {
	
		global $dbcon;
		
		include(dirname(__FILE__)."/config.inc.php");
		
		$sql = "SELECT DECODE(OXVARVALUE, '".$oxidkey."') FROM oxconfig WHERE OXVARNAME = '".$oxconfvar."'";
			
		$var = $dbcon->query($sql);
		
		return $var[0];
	}
	
	public function getPayments() 
	{
		
		global $dbcon;
		
		$sql = "SELECT * FROM oxpayments WHERE OXACTIVE = '1'";

		$res = $dbcon->query_array($sql); 
		
		return $res;
	}
	
	public function getCountries() 
	{
		
		global $dbcon;
		
		$sql = "SELECT * FROM oxcountry";

		$res = $dbcon->query_array($sql); 
		
		return $res;
	}
	
	public function getDelivery() 
	{
		
		global $dbcon;
		
		$sql = "SELECT * FROM oxdelivery WHERE OXACTIVE = '1'";

		$res = $dbcon->query_array($sql); 
		
		return $res;
	}
	
	public function getArtPics($artnr) 
	{
		
		global $dbcon;
		
		$sql = "SELECT OXPIC1,OXPIC2,OXPIC3,OXPIC4,OXPIC5,OXPIC6,OXPIC7,OXPIC8,
				OXPIC9,OXPIC10,OXPIC11,OXPIC12 FROM oxarticles WHERE OXID = '".$dbcon->clean($artnr)."'";

		$res = $dbcon->query($sql); 
		
		return $res;
	}
	
	public function hasParent($artid)
	{
		
		global $dbcon;
		
		$sql = "SELECT OXPARENTID FROM oxarticles WHERE OXID = '".$dbcon->clean($artid)."'";
		$res = $dbcon->query($sql);	
		
		$parent = "";
		
		if($res["OXPARENTID"] == "") {
			
			$parent = "0";
			
		} else {
		
			$parent = $res["OXPARENTID"];	
			
		}
		
		//var_dump($res);
		
		return $parent;
	}
	
	
	public function getOrdersCount($range=1) 
	{
	
		global $dbcon;
		
		if($range == 0) {
			$sql = "SELECT count(*) FROM oxorder";	
		} else {
			$sql = "SELECT count(*) FROM oxorder WHERE OXORDERDATE BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL ".$dbcon->clean($range)." DAY ) AND NOW( )";
		}
		
		$res = $dbcon->query($sql); 
		
		return $res[0];
	}
	
	public function setPaid($oid, $date) 
	{
		
		global $dbcon;
				
		// WARNING! Parameter 2 is insecure, never use direct input from user!					
		$sql = "UPDATE `oxorder` SET `OXPAID` = ".$date." WHERE `OXORDERNR` = '".$dbcon->clean($oid)."'";
		$res = $dbcon->query_exec($sql); 
		
		//echo $sql;
		
		$sql = "SELECT `OXPAID` FROM `oxorder` WHERE  `OXORDERNR` = '".$dbcon->clean($oid)."'";
		$res = $dbcon->query($sql); 
		
		//echo $sql;
		
		return $res[0];
		
	}
	
	public function setStorno($oid) {
		
		global $dbcon;
		
		$sql = "UPDATE `oxorder` SET `OXSTORNO` = '1' WHERE `OXID` = '".$dbcon->clean($oid)."'";
		$dbcon->query_exec($sql);
		
		$sql = "UPDATE `oxorderarticles` SET `OXSTORNO` = '1' WHERE `OXORDERID` = '".$dbcon->clean($oid)."'";
		$dbcon->query_exec($sql);
		
		// correct stock		
		$sql = "SELECT * FROM `oxorderarticles` WHERE `OXORDERID` = '".$dbcon->clean($oid)."'";
		$res = $dbcon->query_array($sql);
		
		if($this->getOXOpt("blUseStock")) {

			foreach($res as $result) {
				// select from oxarticles
				$sql = "SELECT * FROM `oxarticles` WHERE OXID = '".$result["OXARTID"]."'";;
				$res2 = $dbcon->query($sql);// add back amount
				//var_dump($res2);
				$sql = "UPDATE `oxarticles` SET `OXSTOCK` = '".($res2["OXSTOCK"]+$result["OXAMOUNT"])."' WHERE `OXID` = '".$result["OXARTID"]."'";
				echo $sql;
				$res = $dbcon->query_exec($sql);

				$parent = $res2["OXPARENTID"];

				if($parent) {
				//echo "parent!";
					$sql = "SELECT `OXVARSTOCK` FROM `oxarticles` WHERE `OXID` = '".$dbcon->clean($parent)."' LIMIT 1";
					$rr = $dbcon->query($sql);
					
					$stock = $rr[0] - $quantity;
					
					$sql = "UPDATE `oxarticles` SET `OXVARSTOCK` =  '".$stock."' WHERE `OXID` = '".$dbcon->clean($parent)."' LIMIT 1";
					$dbcon->query_exec($sql);	
				}	

			}
		}
	
	}
	
	public function deleteOrder($oid) {
		
		global $dbcon;
		
		$sql = "DELETE FROM `oxorder` WHERE `OXID` = '".$dbcon->clean($oid)."'";
		$dbcon->query_exec($sql);
		
		$sql = "SELECT * FROM `oxorderarticles` WHERE `OXORDERID` = '".$dbcon->clean($oid)."'";
		$res = $dbcon->query_array($sql);
		
		if($this->getOXOpt("blUseStock")) {
		
			foreach($res as $result) {
				// select from oxarticles
				$sql = "SELECT * FROM `oxarticles` WHERE OXID = '".$result["OXARTID"]."'";;
				$res2 = $dbcon->query($sql);// add back amount
				//var_dump($res2);
				$sql = "UPDATE `oxarticles` SET `OXSTOCK` = '".($res2["OXSTOCK"]+$result["OXAMOUNT"])."' WHERE `OXID` = '".$result["OXARTID"]."'";
				//echo $sql;
				$res = $dbcon->query_exec($sql);

				$parent = $res2["OXPARENTID"];

				if($parent) {
					//echo "parent!";
					$sql = "SELECT `OXVARSTOCK` FROM `oxarticles` WHERE `OXID` = '".$dbcon->clean($parent)."' LIMIT 1";
					$rr = $dbcon->query($sql);
					
					$stock = $rr[0] - $quantity;
					
					$sql = "UPDATE `oxarticles` SET `OXVARSTOCK` =  '".$stock."' WHERE `OXID` = '".$dbcon->clean($parent)."' LIMIT 1";
					$dbcon->query_exec($sql);	
				}	

			}
		}
		
	}

	public function setPacked($oid, $date) 
	{
		
		global $dbcon;

		// WARNING! Parameter 2 is insecure, never use direct input from user!		
		$sql = "UPDATE `oxorder` SET `OXSENDDATE` = ".$date." WHERE `OXORDERNR` = '".$dbcon->clean($oid)."'";
		$res = $dbcon->query_exec($sql); 
		
		$sql = "SELECT `OXSENDDATE` FROM `oxorder` WHERE  `OXORDERNR` = '".$dbcon->clean($oid)."'";
		$res = $dbcon->query($sql); 
		
		return $res[0];
		
	}	

	public function getArticles() 
	{
		
		global $dbcon;
		
		$sql = "SELECT oxarticles.*, oxartextends.* FROM oxarticles".
			   " LEFT JOIN oxartextends ON oxarticles.OXID = oxartextends.OXID WHERE oxarticles.OXPARENTID = '' GROUP BY oxarticles.OXID ORDER BY oxarticles.OXID".$limit;

		$res = $dbcon->query_array($sql);
		
		for($i = 0; $i < sizeof($res); $i++) {
		
			$sql = "SELECT count(*) FROM oxarticles WHERE OXSTOCK > 0 AND OXPARENTID = '".$res[$i][0]."'";

			$res2 = $dbcon->query($sql);
	
			$res[$i]["hc"] = $res2["count(*)"];	

			if($res2["count(*)"] != 0) {
					$sql = "SELECT * FROM oxarticles WHERE OXSTOCK > 0 AND OXPARENTID = '".$res[$i][0]."'";
					$res2 = $dbcon->query_array($sql);
					$res[$i]["mc"] = $res2;
			}
		}
				
		return $res;
	}
	
	
	
	public function getArticle($artnr) {
		
		global $dbcon;
		
		
		$sql =  "SELECT oxarticles.*, oxartextends.* FROM oxarticles ".
				"LEFT JOIN oxartextends ON oxarticles.OXID = oxartextends.OXID WHERE oxarticles.OXID = '".$dbcon->clean($artnr)."'";
							
		$res = $dbcon->query($sql);
		
		if($res["OXPARENTID"] != "") { 
					
				$sql =  "SELECT oxarticles.*, oxartextends.* FROM oxarticles ".
				"LEFT JOIN oxartextends ON oxarticles.OXID = oxartextends.OXID WHERE oxarticles.OXID = '".$res["OXPARENTID"]."'";
				
				$res2 = $dbcon->query($sql);
				
				if($res["OXTITLE"] == "") {
					$res["OXTITLE"] = $res2["OXTITLE"];
				}
				if($res["OXLONGDESC"] == "") {
					$res["OXLONGDESC"] = $res2["OXLONGDESC"];
				}
				if($res["OXSHORTDESC"] == "") {
					$res["OXSHORTDESC"] = $res2["OXSHORTDESC"];
				}
				if($res["OXPIC1"] == "") {
					$res["OXPIC1"] = $res2["OXPIC1"];
					$res["OXPIC2"] = $res2["OXPIC2"];
					$res["OXPIC3"] = $res2["OXPIC3"];
					$res["OXPIC4"] = $res2["OXPIC4"];
					$res["OXPIC5"] = $res2["OXPIC5"];
					$res["OXPIC6"] = $res2["OXPIC6"];
					$res["OXPIC7"] = $res2["OXPIC7"];
					$res["OXPIC8"] = $res2["OXPIC8"];
					$res["OXPIC9"] = $res2["OXPIC9"];
					$res["OXPIC10"] = $res2["OXPIC10"];
					$res["OXPIC11"] = $res2["OXPIC11"];
					$res["OXPIC12"] = $res2["OXPIC12"];
				}
		
		}
				
		return $res;
	}
	
	public function getArticleChildren($artnr, $stock="0") {
		
		global $dbcon;
		
		$sql =  "SELECT oxarticles.*, oxartextends.* FROM oxarticles ".
				"LEFT JOIN oxartextends ON oxarticles.OXID = oxartextends.OXID WHERE oxarticles.OXPARENTID = '".$dbcon->clean($artnr)."'";
				
		if($stock == "1") {
			$sql .= " AND oxarticles.OXSTOCK > 0"; 	
		}
				
		$sql .= " ORDER BY oxarticles.OXVARSELECT";
				
				
							
		$res = $dbcon->query_array($sql);
		
		return $res;
	}
	
	public function getSeoUrls() 
	{
		
		global $dbcon;
		
		$sql =  "SELECT oxseo.*, oxarticles.*, oxartextends.* FROM oxseo 
				LEFT JOIN oxarticles ON oxseo.OXOBJECTID = oxarticles.OXID
				LEFT JOIN oxartextends ON oxseo.OXOBJECTID = oxartextends.OXID
				WHERE 1";
							
		$res = $dbcon->query_array($sql);
		
		return $res;
	}
	
	public function getOrdersSent($mindate, $maxdate) 
	{
		
		global $dbcon;

		$sql = "SELECT oxorder.*, oxcountry.*, oxorder.OXID as OXID FROM oxorder 
				LEFT JOIN oxcountry ON oxorder.OXBILLCOUNTRYID = oxcountry.OXID
				WHERE oxorder.OXSENDDATE BETWEEN '".$dbcon->clean($mindate)." 00:00:00' AND '".$dbcon->clean($maxdate)." 23:59:59'
	    		GROUP BY oxorder.OXID";
		
		$res = $dbcon->query_array($sql);
	
		$retAr = array();
		
		foreach($res as $result) {
			
			$sql = "SELECT * FROM `oxorderarticles` WHERE OXORDERID = '".$result["OXID"]."'";
						
			$res2 = $dbcon->query_array($sql);
				
			$result["articles"]	= $res2;
			
			array_push($retAr, $result);
		}
		
		return $retAr;
			
	}

	public function getOrdersPaid($mindate, $maxdate) 
	{
		
		global $dbcon;

		$sql = "SELECT oxorder.*, oxcountry.*, oxorder.OXID as OXID FROM oxorder 
				LEFT JOIN oxcountry ON oxorder.OXBILLCOUNTRYID = oxcountry.OXID
				WHERE oxorder.OXPAID BETWEEN '".$dbcon->clean($mindate)." 00:00:00' AND '".$dbcon->clean($maxdate)." 23:59:59'
	    		GROUP BY oxorder.OXID";
		
		$res = $dbcon->query_array($sql);
	
		$retAr = array();
		
		foreach($res as $result) {
			
			$sql = "SELECT * FROM `oxorderarticles` WHERE OXORDERID = '".$result["OXID"]."'";
						
			$res2 = $dbcon->query_array($sql);
				
			$result["articles"]	= $res2;
			
			array_push($retAr, $result);
		}
		
		return $retAr;
			
	}
	
	public function getOrders($new=0, $mindate=0, $maxdate=0) 
	{
		
		global $dbcon;
				
		// > DATE_ADD(CURDATE(), INTERVAL -1 DAY)
		
		$sql = "SELECT oxorder.*, oxcountry.*, oxorder.OXID as OXID FROM oxorder 
				LEFT JOIN oxcountry ON oxorder.OXBILLCOUNTRYID = oxcountry.OXID";
		
		//OXBILLCOUNTRY ID - Possible bug if BILL/DEL Country are different -> fix some time later
		
		if($mindate) {
			
			$sql .= " WHERE oxorder.OXORDERDATE BETWEEN '".$dbcon->clean($mindate)." 00:00:00' AND '".$dbcon->clean($maxdate)." 23:59:59'";
			
		} else {
			
			if($new == 0) {		
				$sql .=	" WHERE 1";	
			} else {
				$sql .= " WHERE oxorder.OXORDERDATE BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL ".$dbcon->clean($new)." DAY ) AND NOW( )";	
			}
			
		}
		
		$sql .= " GROUP BY oxorder.OXID";
		
		//echo $sql;
		
		$res = $dbcon->query_array($sql);
		
		$retAr = array();
		
		foreach($res as $result) {
			
			$sql = "SELECT * FROM `oxorderarticles` WHERE OXORDERID = '".$result["OXID"]."'";
						
			$res2 = $dbcon->query_array($sql);
				
			$result["articles"]	= $res2;
			
			array_push($retAr, $result);
		}
		
		return $retAr;
		
	}
	
	public function getOrder($oid) 
	{
		
		global $dbcon;
						
		$sql = "SELECT oxorder.*, oxcountry.*, oxorder.OXID as OXID FROM oxorder 
				LEFT JOIN oxcountry ON oxorder.OXBILLCOUNTRYID = oxcountry.OXID
				WHERE oxorder.OXID = '".$dbcon->clean($oid)."'";
						
		$res = $dbcon->query($sql);
		
		$sql = "SELECT * FROM `oxorderarticles` WHERE OXORDERID = '".$res["OXID"]."'";
						
		$res2 = $dbcon->query_array($sql);
				
		$res["articles"]	= $res2;
		
		return $res;
		
	}
	
	public function correctStock($artnr, $quantity, $parent=0) {
		
		global $dbcon;
		
		if($this->getOXOpt("blUseStock")) {

		$sql = "SELECT `OXSTOCK` FROM `oxarticles` WHERE `OXID` = '".$dbcon->clean($artnr)."' LIMIT 1";
		$rr = $dbcon->query($sql);
					
		$stock = $rr[0] - $quantity;
				
		$sql = "UPDATE `oxarticles` SET `OXSTOCK` =  '$stock' WHERE `OXID` = '".$dbcon->clean($artnr)."' LIMIT 1";
		$dbcon->query_exec($sql);	
					
		if($parent) {
			//echo "parent!";
			$sql = "SELECT `OXVARSTOCK` FROM `oxarticles` WHERE `OXID` = '".$dbcon->clean($parent)."' LIMIT 1";
			$rr = $dbcon->query($sql);
					
			$stock = $rr[0] - $quantity;
					
			$sql = "UPDATE `oxarticles` SET `OXVARSTOCK` =  '$stock' WHERE `OXID` = '".$dbcon->clean($parent)."' LIMIT 1";
			$dbcon->query_exec($sql);	
		}	
		
		}
	}
	
	/*
		$artnr = OXID of article
		$v = variant count
		$iq = array
		$ia = array
	*/
	
	public function correctStockMv($artnr, $v, $iq, $ia) {
	
		global $dbcon;

		if($this->getOXOpt("blUseStock")) {
		
			$sql = "SELECT `OXVARSTOCK` FROM `oxarticles` WHERE `OXID` = '".$dbcon->clean($artnr)."' LIMIT 1";
			$rr = $dbcon->query($sql);
					
			$minus = 0;
					
			for($i = 1; $i <= $v;$i++) {
						
				$minus += $iq[$i];
						
			}
					
			$stock = $rr["OXVARSTOCK"] - $minus;
												
			$sql = "UPDATE `oxarticles` SET `OXVARSTOCK` =  '$stock' WHERE `OXID` = '".$dbcon->clean($artnr)."' LIMIT 1";
			$dbcon->query_exec($sql);

			// children minus 
					
			for($i = 0; $i < $v;$i++) {
						
				$sql = "SELECT `OXSTOCK` FROM `oxarticles` WHERE `OXID` = '".$dbcon->clean($ia[$i])."' LIMIT 1";
				$rr = $dbcon->query($sql);
					
				$stock = $rr[0] - $iq[$i];
					
				$sql = "UPDATE `oxarticles` SET `OXSTOCK` =  '$stock' WHERE `OXID` = '".$dbcon->clean($ia[$i])."' LIMIT 1";
				$dbcon->query_exec($sql);						
			
			}
		
		}
		
	}
	
	
	public function createSingleOrderFromExternal(	$bname, $bstreet, $bcity, $bplz, $bcountry,
												$sname, $sstreet, $scity, $splz, $scountry,	
												$email, $note, $cur, $price, $quanity, $variants,
												$eid, $tid, $p_status, $p_method 
												)
	{
		
			global $dbcon;
					
			$sql = "SELECT * FROM oebc_offers WHERE eid = $eid";
			$res = $dbcon->query($sql);								
			
			if(empty($variants)) {	
			
				// no variant article
					
				$sql = "SELECT * FROM oxarticles WHERE OXID = '".$res["oid"]."'";	
				$res2 = $dbcon->query($sql);					
						
			} else {
				
				// is a child
				
				$sql = "SELECT OXID FROM oxarticles WHERE OXPARENTID = '".$res["oid"]."'";
				foreach($variants as $var) {
					$sql .= " AND OXVARSELECT LIKE '%".$dbcon->clean($var)."%'";						
				}
				$res2 = $dbcon->query($sql);	
				
				$res2 = $this->getArticle($res2["OXID"]);		
			}
						
			include(dirname(__FILE__)."/config.inc.php");
			
			$files 	= new ahc($dbcon);
			
			$paiddate = "'0000-00-00 00:00:00'";
			
			if($files->getOpt("cron_pp")) {
				if(($p_status == "Complete") && ($p_method == "PayPal")) 
				{
					$paiddate = "NOW( )";
				}
			}
			
			
			
			$sql = "SELECT DECODE(OXVARVALUE, '".$oxidkey."') FROM oxconfig WHERE OXVARNAME = 'dDefaultVAT'";			
			
			$vatblob = $dbcon->query($sql);
			$vat = $vatblob[0];
			
			// get OXID for country ID 
			
			$sql = "SELECT OXID FROM oxcountry WHERE OXISOALPHA2 = '".$bcountry."'"; 
			$res3 = $dbcon->query($sql);
			
			$bcountry = $res3["OXID"]; 
			
			$sql = "SELECT OXID FROM oxcountry WHERE OXISOALPHA2 = '".$scountry."'"; 
			$res3 = $dbcon->query($sql);
			
			$scountry = $res3["OXID"]; 
			
			// split name if possible 
			
			$bname2 = array();
			$sname2 = array();
			
			$bname2 = explode(" ", $bname, 2);
			$sname2 = explode(" ", $sname, 2);
			
			// find max orderid
			
			$sql = "SELECT MAX(OXORDERNR) FROM oxorder"; 
			$res3 = $dbcon->query($sql);
			
			$oid = $res3[0]+1;		
			
			
				
			$sql 	= "INSERT INTO `oxorderarticles` (	`OXID` ,
														`OXORDERID` ,
														`OXAMOUNT` ,
														`OXARTID` ,
														`OXARTNUM` ,
														`OXTITLE` ,
														`OXSHORTDESC` ,
														`OXSELVARIANT` ,
														`OXNETPRICE` ,
														`OXBRUTPRICE` ,
														`OXVATPRICE` ,
														`OXVAT` ,
														`OXPERSPARAM` ,
														`OXPRICE` ,
														`OXBPRICE` ,
														`OXNPRICE` ,
														`OXWRAPID` ,
														`OXEXTURL` ,
														`OXURLDESC` ,
														`OXURLIMG` ,
														`OXTHUMB` ,
														`OXPIC1` ,
														`OXPIC2` ,
														`OXPIC3` ,
														`OXPIC4` ,
														`OXPIC5` ,
														`OXWEIGHT` ,
														`OXSTOCK` ,
														`OXDELIVERY` ,
														`OXINSERT` ,
														`OXTIMESTAMP` ,
														`OXLENGTH` ,
														`OXWIDTH` ,
														`OXHEIGHT` ,
														`OXFILE` ,
														`OXSEARCHKEYS` ,
														`OXTEMPLATE` ,
														`OXQUESTIONEMAIL` ,
														`OXISSEARCH` ,
														`OXFOLDER` ,
														`OXSUBCLASS` ,
														`OXSTORNO` ,
														`OXORDERSHOPID`
														)
								VALUES (				'".$dbcon->clean($tid)."' ,
														'".$dbcon->clean($tid)."' ,
														'".$dbcon->clean($quanity)."' ,
														'".$dbcon->clean($res2["OXID"])."' ,
														'".$dbcon->clean($res2["OXARTNUM"])."' ,
														'".$dbcon->clean($res2["OXTITLE"])."' ,
														'".$dbcon->clean($res2["OXSHORTDESC"])."' ,
														'".$dbcon->clean($res2["OXVARSELECT"])."' ,
														'".$dbcon->clean(((double)$price/(100+$vat))*100)."', 
														'".$dbcon->clean((double)$price)."' ,
														'".$dbcon->clean($price-((double)$price/(100+$vat))*100)."', 
														'".$dbcon->clean($vat)."' ,
														'' ,
														'".$dbcon->clean($price)."' ,
														'".$dbcon->clean($price)."' ,
														'".$dbcon->clean(((double)$price/(100+$vat))*100)."' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'0000-00-00' ,
														'0000-00-00' ,
														'0000-00-00 00:00:00' ,
														'0' ,
														'0' ,
														'0' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'0' ,
														'' ,
														'oxarticle' ,
														'0' ,
														'oxbaseshop'
										)";
				
				/*
					Todo: add more fields from article listing 
				*/
				
										
				$dbcon->query_exec($sql);
				
				$sql = "INSERT INTO `oxorder` (	`OXID`, 
												`OXSHOPID`, 
												`OXUSERID`, 
												`OXORDERDATE`, 
												`OXORDERNR`, 
												`OXBILLCOMPANY`, 
												`OXBILLEMAIL`, 
												`OXBILLFNAME`, 
												`OXBILLLNAME`, 
												`OXBILLSTREET`, 
												`OXBILLSTREETNR`, 
												`OXBILLADDINFO`, 
												`OXBILLUSTID`, 
												`OXBILLCITY`, 
												`OXBILLCOUNTRYID`, 
												`OXBILLSTATEID`, 
												`OXBILLZIP`, 
												`OXBILLFON`, 
												`OXBILLFAX`, 
												`OXBILLSAL`, 
												`OXDELCOMPANY`, 
												`OXDELFNAME`, 
												`OXDELLNAME`, 
												`OXDELSTREET`, 
												`OXDELSTREETNR`, 
												`OXDELADDINFO`, 
												`OXDELCITY`, 
												`OXDELCOUNTRYID`, 
												`OXDELSTATEID`, 
												`OXDELZIP`, 
												`OXDELFON`, 
												`OXDELFAX`, 
												`OXDELSAL`, 
												`OXPAYMENTID`, 
												`OXPAYMENTTYPE`, 
												`OXTOTALNETSUM`, 
												`OXTOTALBRUTSUM`, 
												`OXTOTALORDERSUM`, 
												`OXARTVAT1`, 
												`OXARTVATPRICE1`, 
												`OXARTVAT2`, 
												`OXARTVATPRICE2`, 
												`OXDELCOST`, 
												`OXDELVAT`, 
												`OXPAYCOST`, 
												`OXPAYVAT`, 
												`OXWRAPCOST`, 
												`OXWRAPVAT`, 
												`OXCARDID`, 
												`OXCARDTEXT`, 
												`OXDISCOUNT`, 
												`OXEXPORT`, 
												`OXBILLNR`, 
												`OXTRACKCODE`, 
												`OXSENDDATE`, 
												`OXREMARK`, 
												`OXVOUCHERDISCOUNT`, 
												`OXCURRENCY`, 
												`OXCURRATE`, 
												`OXFOLDER`, 
												`OXPIDENT`, 
												`OXTRANSID`, 
												`OXPAYID`, 
												`OXXID`, 
												`OXPAID`, 
												`OXSTORNO`, 
												`OXIP`, 
												`OXTRANSSTATUS`, 
												`OXLANG`, 
												`OXINVOICENR`, 
												`OXDELTYPE`, 
												`OXTSPROTECTID`, 
												`OXTSPROTECTCOSTS`
							) VALUES (			
												'".$dbcon->clean($tid)."', 
												'oxbaseshop', 
												'', 
												NOW( ), 
												'".$dbcon->clean($oid)."', 
												'', 
												'".$dbcon->clean($email)."', 
												'".$dbcon->clean($bname2[0])."', 
												'".$dbcon->clean($bname2[1])."', 
												'".$dbcon->clean($bstreet)."', 
												'', 
												'', 
												'',
												'".$dbcon->clean($bcity)."', 
												'".$dbcon->clean($bcountry)."', 
												'', 
												'".$dbcon->clean($bplz)."', 
												'', 
												'', 
												'', 
												'', 
												'".$dbcon->clean($sname2[0])."', 
												'".$dbcon->clean($sname2[1])."', 
												'".$dbcon->clean($sstreet)."', 
												'', 
												'', 
												'".$dbcon->clean($scity)."', 
												'".$dbcon->clean($scountry)."', 
												'', 
												'".$dbcon->clean($splz)."', 
												'', 
												'', 
												'', 
												'', 
												'oxidpayadvance', 
												'".$dbcon->clean(((double)$price/(100+$vat))*100)."', 
												'".$dbcon->clean((double)$price)."', 
												'".$dbcon->clean((double)$price + $paycost +$delcost)."', 
												'".$dbcon->clean((double)$vat)."', 
												'".$dbcon->clean($price-(((double)$price/(100+$vat))*100))."',
												'0', 
												'0', 
												'0', 
												'0', 
												'0',  
												'0', 
												'0', 
												'0', 
												'', 
												'', 
												'0', 
												'0', 
												'1', 
												'', 
												'0000-00-00 00:00:00', 
												'".$dbcon->clean($note)."', 
												'0', 
												'".$dbcon->clean($cur)."', 
												'1', 
												'ORDERFOLDER_NEW', 
												'', 
												'', 
												'', 
												'', 
												$paiddate, 
												'0', 
												'', 
												'OK', 
												'0', 
												'0', 
												'', 
												'', 
												'0'
										)"; 
			
			//echo $sql;
			$dbcon->query_exec($sql);

										
			return $tid;							
			
	}
	

	public function createOrder(	$badd, $bfname, $bname, $bstreet, $bnr, $bcity, $bplz, $bcountry, $bcorp, $bfon, $bfax,
									$sadd, $sfname, $sname, $sstreet, $snr, $scity, $splz, $scountry, $scorp, $sfon, $sfax,
									$email, $note, $cur, $p_status, $isBill, $delivery, $payments, $vatid,
									$arts
								)
	{
		
			global $dbcon;
						
			include(dirname(__FILE__)."/config.inc.php");
			
			$files 	= new ahc($dbcon);			
			
			$paiddate = "'0000-00-00 00:00:00'";
			
			if(($p_status == "1")) 
			{
					$paiddate = "NOW( )";
			}
		
			
			$sql = "SELECT DECODE(OXVARVALUE, '".$oxidkey."') FROM oxconfig WHERE OXVARNAME = 'dDefaultVAT'";
			
			$vatblob = $dbcon->query($sql);
			$vat = $vatblob[0];
			
			// get OXID for country ID 
			
			$sql = "SELECT OXID FROM oxcountry WHERE OXISOALPHA2 = '".$bcountry."'"; 
			$res3 = $dbcon->query($sql);
			
			$bcountry = $res3["OXID"]; 
			
			$sql = "SELECT OXID FROM oxcountry WHERE OXISOALPHA2 = '".$scountry."'"; 
			$res3 = $dbcon->query($sql);
			
			$scountry = $res3["OXID"]; 
			
			
			// find max orderid
			
			$sql = "SELECT MAX(OXORDERNR) FROM oxorder"; 
			$res3 = $dbcon->query($sql);
			
			$oid = $res3[0]+1;		
			
			// set unique ID based on time()
			
			$tid = time();
			
			$sum = 0;
			
			//var_dump($arts);
					
			foreach($arts as $art) {
							
			$res2 = $this->getArticle($art[0]);	
			
			$price = $res2["OXPRICE"];
			
			$multi = $art[1];	
			
			$sum = $sum + ($price * $multi);	
			
			//echo $sum."\n";
			
			$sql 	= "INSERT INTO `oxorderarticles` (	`OXID` ,
														`OXORDERID` ,
														`OXAMOUNT` ,
														`OXARTID` ,
														`OXARTNUM` ,
														`OXTITLE` ,
														`OXSHORTDESC` ,
														`OXSELVARIANT` ,
														`OXNETPRICE` ,
														`OXBRUTPRICE` ,
														`OXVATPRICE` ,
														`OXVAT` ,
														`OXPERSPARAM` ,
														`OXPRICE` ,
														`OXBPRICE` ,
														`OXNPRICE` ,
														`OXWRAPID` ,
														`OXEXTURL` ,
														`OXURLDESC` ,
														`OXURLIMG` ,
														`OXTHUMB` ,
														`OXPIC1` ,
														`OXPIC2` ,
														`OXPIC3` ,
														`OXPIC4` ,
														`OXPIC5` ,
														`OXWEIGHT` ,
														`OXSTOCK` ,
														`OXDELIVERY` ,
														`OXINSERT` ,
														`OXTIMESTAMP` ,
														`OXLENGTH` ,
														`OXWIDTH` ,
														`OXHEIGHT` ,
														`OXFILE` ,
														`OXSEARCHKEYS` ,
														`OXTEMPLATE` ,
														`OXQUESTIONEMAIL` ,
														`OXISSEARCH` ,
														`OXFOLDER` ,
														`OXSUBCLASS` ,
														`OXSTORNO` ,
														`OXORDERSHOPID`
														)
								VALUES (				'oebc_".substr(md5(rand()+time()), 0, 12)."' ,
														'".$tid."' ,
														'".$dbcon->clean($art[1])."' ,
														'".$dbcon->clean($res2["OXID"])."' ,
														'".$dbcon->clean($res2["OXARTNUM"])."' ,
														'".$dbcon->clean($res2["OXTITLE"])."' ,
														'".$dbcon->clean($res2["OXSHORTDESC"])."' ,
														'".$dbcon->clean($res2["OXVARSELECT"])."' ,
														'".$dbcon->clean(((double)$price/(100+$vat))*100)."', 
														'".$dbcon->clean((double)$price)."' ,
														'".$dbcon->clean($price-((double)$price/(100+$vat))*100)."', 
														'".$dbcon->clean($vat)."' ,
														'' ,
														'".$dbcon->clean($price)."' ,
														'".$dbcon->clean($price)."' ,
														'".$dbcon->clean(((double)$price/(100+$vat))*100)."' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'0000-00-00' ,
														'0000-00-00' ,
														'0000-00-00 00:00:00' ,
														'0' ,
														'0' ,
														'0' ,
														'' ,
														'' ,
														'' ,
														'' ,
														'0' ,
														'' ,
														'oxarticle' ,
														'0' ,
														'oxbaseshop'
										)";			
										
				$dbcon->query_exec($sql);
				
				}
				
				//echo $sum."\n";
				
				if($delivery) {
					$sql 	= "SELECT * FROM oxdelivery WHERE OXID = '".$dbcon->clean($delivery)."'"; 
					$dres 	= $dbcon->query($sql);
				
					if($dres["OXADDSUMTYPE"] == "abs") 
					{
						$delcost = $dres["OXADDSUM"];
					} else {
						$delcost = ($sum/100)	* $dres["OXADDSUM"];
					}
				} else {
					$delcost = 0;
				}
				
				//echo "delcost: ".$delcost;		
				
				if($payments) {
					$sql 	= "SELECT * FROM oxpayments WHERE OXID = '".$dbcon->clean($payments)."'"; 
					$pres 	= $dbcon->query($sql);				
				
					if($pres["OXADDSUMTYPE"] == "abs") 
					{
						$paycost = $pres["OXADDSUM"];
					} else {
						$paycost = (($sum+$delcost)/100)	* $pres["OXADDSUM"];
					}
				} else {
					$paycost = 0;	
				}
						
				//echo "paycost: ".$paycost;		
								
				$sql = "INSERT INTO `oxorder` (	`OXID`, 
												`OXSHOPID`, 
												`OXUSERID`, 
												`OXORDERDATE`, 
												`OXORDERNR`, 
												`OXBILLCOMPANY`, 
												`OXBILLEMAIL`, 
												`OXBILLFNAME`, 
												`OXBILLLNAME`, 
												`OXBILLSTREET`, 
												`OXBILLSTREETNR`, 
												`OXBILLADDINFO`, 
												`OXBILLUSTID`, 
												`OXBILLCITY`, 
												`OXBILLCOUNTRYID`, 
												`OXBILLSTATEID`, 
												`OXBILLZIP`, 
												`OXBILLFON`, 
												`OXBILLFAX`, 
												`OXBILLSAL`, 
												`OXDELCOMPANY`, 
												`OXDELFNAME`, 
												`OXDELLNAME`, 
												`OXDELSTREET`, 
												`OXDELSTREETNR`, 
												`OXDELADDINFO`, 
												`OXDELCITY`, 
												`OXDELCOUNTRYID`, 
												`OXDELSTATEID`, 
												`OXDELZIP`, 
												`OXDELFON`, 
												`OXDELFAX`, 
												`OXDELSAL`, 
												`OXPAYMENTID`, 
												`OXPAYMENTTYPE`, 
												`OXTOTALNETSUM`, 
												`OXTOTALBRUTSUM`, 
												`OXTOTALORDERSUM`, 
												`OXARTVAT1`, 
												`OXARTVATPRICE1`, 
												`OXARTVAT2`, 
												`OXARTVATPRICE2`, 
												`OXDELCOST`, 
												`OXDELVAT`, 
												`OXPAYCOST`, 
												`OXPAYVAT`, 
												`OXWRAPCOST`, 
												`OXWRAPVAT`, 
												`OXCARDID`, 
												`OXCARDTEXT`, 
												`OXDISCOUNT`, 
												`OXEXPORT`, 
												`OXBILLNR`, 
												`OXTRACKCODE`, 
												`OXSENDDATE`, 
												`OXREMARK`, 
												`OXVOUCHERDISCOUNT`, 
												`OXCURRENCY`, 
												`OXCURRATE`, 
												`OXFOLDER`, 
												`OXPIDENT`, 
												`OXTRANSID`, 
												`OXPAYID`, 
												`OXXID`, 
												`OXPAID`, 
												`OXSTORNO`, 
												`OXIP`, 
												`OXTRANSSTATUS`, 
												`OXLANG`, 
												`OXINVOICENR`, 
												`OXDELTYPE`, 
												`OXTSPROTECTID`, 
												`OXTSPROTECTCOSTS`
							) VALUES (			
												'".$tid."', 
												'oxbaseshop', 
												'', 
												NOW( ), 
												'".$dbcon->clean($oid)."', 
												'".$dbcon->clean($bcorp)."', 
												'".$dbcon->clean($email)."', 
												'".$dbcon->clean($bfname)."', 
												'".$dbcon->clean($bname)."', 
												'".$dbcon->clean($bstreet)."', 
												'".$dbcon->clean($bnr)."', 
												'', 
												'".$dbcon->clean($bvat)."',
												'".$dbcon->clean($bcity)."', 
												'".$dbcon->clean($bcountry)."', 
												'', 
												'".$dbcon->clean($bplz)."', 
												'".$dbcon->clean($btel)."', 
												'".$dbcon->clean($bfax)."', 
												'".$dbcon->clean($badd)."', 
												'".$dbcon->clean($scorp)."', 
												'".$dbcon->clean($sfname)."', 
												'".$dbcon->clean($sname)."', 
												'".$dbcon->clean($sstreet)."', 
												'".$dbcon->clean($snr)."', 
												'', 
												'".$dbcon->clean($scity)."', 
												'".$dbcon->clean($scountry)."', 
												'', 
												'".$dbcon->clean($splz)."', 
												'".$dbcon->clean($stel)."',
												'".$dbcon->clean($sfax)."', 
												'".$dbcon->clean($sadd)."', 
												'', 
												'oxidpayadvance', 
												'".$dbcon->clean(((double)$sum/(100+$vat))*100)."', 
												'".$dbcon->clean((double)$sum)."', 
												'".$dbcon->clean((double)$sum + $paycost +$delcost)."', 
												'".$dbcon->clean((double)$vat)."', 
												'".$dbcon->clean($sum-(((double)$sum/(100+$vat))*100))."', 
												'0', 
												'0', 
												'".$dbcon->clean($delcost)."', 
												'0', 
												'".$dbcon->clean($paycost)."', 
												'0', 
												'0', 
												'0', 
												'', 
												'', 
												'0', 
												'0', 
												'1', 
												'', 
												'0000-00-00 00:00:00', 
												'".$dbcon->clean($note)."', 
												'0', 
												'".$dbcon->clean($cur)."', 
												'1', 
												'ORDERFOLDER_NEW', 
												'', 
												'', 
												'', 
												'', 
												".$paiddate.", 
												'0', 
												'', 
												'OK', 
												'0', 
												'0', 
												'', 
												'', 
												'0'
										)"; 
			
			//echo $sql;
			$dbcon->query_exec($sql);
										
			return $tid;							
			
	}
	
}
?>