<?
/*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/

class dbal 
{

	// constructor: connect to DB server

	public function __construct() {
	
		require(dirname(__FILE__)."/config.inc.php");

		$dbcon = mysql_connect($host, $name, $passwd);
		mysql_query("SET NAMES 'utf8'");
		mysql_select_db($db);
	}

	// Clean string for security

	public function clean($string)
	{
  		if(get_magic_quotes_gpc())
  		{
   		 $string = stripslashes($string);
  		}
		
		//$badWords = "(union)|(drop)|(--)";
		//$string = eregi_replace($badWords, "", $string);

	    $string = mysql_real_escape_string($string);

		return($string);
	}

	// Query and return single result
	// result[column]

	public function query($sql) {
		if ( !$result = mysql_query($sql) ) {
        	exit(mysql_error());
		}	
		return(@mysql_fetch_array($result));
	}
	
	public function query_noerr($sql) {
		if ( !$result = mysql_query($sql) ) {
        	return 0;
		}	
		return(@mysql_fetch_array($result));
	}
	
	public function query_raw($sql) {
		if ( !$result = mysql_query($sql) ) {
        	exit(mysql_error());
		}	
		return($result);
	}

	// Query and return array of results
	// result[row][column]
	
	public function query_array($sql) {
	    if ( !$result = mysql_query($sql) ) {
			 exit(mysql_error());
   		 }
		
		$resrow = array();
		
		while ($row = mysql_fetch_array($result)) {
			array_push($resrow, $row);  
		}
		 
    	return($resrow);
	}
	
	public function query_array_append($sql) {
	    if ( !$result = mysql_query($sql) ) {
			 exit(mysql_error());
   		 }
		
		$resrow = array();
		
		while ($row = mysql_fetch_array($result)) {
			//array_push($resrow, $row);  
			$resrow[] = $row[0];
		}
		 
    	return($resrow);
	}
	
	// Execute query (for INSERT, UPDATE etc.)
	
	public function query_exec($sql) {
	    if ( !$result = mysql_query($sql) ) {
   		     exit(mysql_error());
   		 }
    	return 0;
	}
	
	public function query_exec_noerr($sql) {
	    if ( !$result = mysql_query($sql) ) {
			return 1;	
		}
    	return 0;
	}
	
	public function get_lastid() {
		return mysql_insert_id();	
	}
	
	public function version() {
		return mysql_get_server_info();
	}

}

?>