<?
 /*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/



	if(!$uid || !defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
		die('Direct access not premitted');
	}
	
	/*******************************************/
	
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../tmp/';
	
	$smarty->assign("applist", $applist);
	
	$smarty->assign("thisserver", $_SERVER['SERVER_NAME']);
	
	/*********** GET OX ATRICLES ****************************/
	
	$ox = new oxid($db);
	$arts = $ox->getArticles();
	
	$smarty->assign("oxitems", $arts);
	

	// Benutzerzahlen
	
	$sql = "SELECT eid FROM oebc_offers ORDER BY eid";
	$result = $db->query_array_append($sql);
	$smarty->assign("bysystem", $result); 
	
		
	/*****************************************************************/	
	
	/* provide order count */
	
	$smarty->assign("orders_today", $ox->getOrdersCount());
	$smarty->assign("orders_week", $ox->getOrdersCount(7));
	$smarty->assign("orders_all", $ox->getOrdersCount(0));
	
	/* provide date */
	
	$template->assign("iconzoom", $files->getOpt("iconzoom"));
	$template->assign("date", date("d.m.Y"));
	
	//Content erzeugen und bereitstellen
	$content = $smarty->fetch('frontend/pages/adm.tpl');
	$template->assign("content",$content);
    
?>