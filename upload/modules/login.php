<?
 /*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


	if(!defined('isOEBC'))
	{
		header("Location: http://". $_SERVER['SERVER_NAME']."/");
		exit(0);
	}
	
	/*
		Login Modul
	*/
	//smarty Element für Darstellung erzeugen
	if($_REQUEST["action"] == "logout") {
		$auth->logoutuser();
		exit(0);
	}
	
	$smarty = new smarty;
	$smarty->compile_dir  = dirname(__FILE__).'/../tmp/';

	if($_REQUEST["name"] && $_REQUEST["passwd"]) {
		$loggedin = $auth->check_login($_REQUEST["name"], $_REQUEST["passwd"]);
		if(!$loggedin) {
			$smarty->assign("fail", "1");
		}
	}

	if($loggedin || $uid) {
	
		$sql= "SELECT edit_type FROM oebc_user WHERE uid = '$uid'";
		$l = $db->query($sql);
		if($l[0] == "1") {
			$auth->logoutuser();
			header("Location: http://". $_SERVER['SERVER_NAME']."/");
			exit(0);
		}

	
		header("Location: http://". $_SERVER['SERVER_NAME']."/admin/");
		exit(0);
	}


	$smarty->assign("target", $_REQUEST["target"]);

	//Content erzeugen und bereitstellen
	$content = $smarty->fetch('frontend/pages/login.tpl');
	$template->assign("content",$content);

?>