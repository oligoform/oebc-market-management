<?xml version="1.0" encoding="UTF-8"?>
<catalogue lang="DE" date="{$smarty.now|date_format:"%Y-%m-%d %H:%M"}" GMT="+1">
{section name=articles loop=$articles}
<product place="{$smarty.section.articles.iteration}">
	<category><![CDATA[Mode & Accessoire]]></category>
	<offer_id><![CDATA[{$articles[articles].OXID}]]></offer_id>
	<name><![CDATA[{$articles[articles].OXTITLE}]]></name>
	<price currency="EUR">{$articles[articles].OXPRICE}</price>
	<product_url></product_url>
	{if $articles[articles].OXPIC1}
	<image_url><![CDATA[{$host}/out/pictures/1/{$articles[articles].OXPIC1}]]></image_url>
	{else}
	<image_url><![CDATA[]]></image_url>	
	{/if}
	<description><![CDATA[{$articles[articles].OXLONGDESC}]]></description>
	<shipping>2,90</shipping>
	<availability>2</availability>
	<ean>{$articles[articles].OXARTNUM}</ean>
	<guarantee>2</guarantee>
	<prix_barre currency="EUR"></prix_barre>
</product>		
{/section}
</catalogue>


