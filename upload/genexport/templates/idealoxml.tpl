<?xml version="1.0" encoding="UTF-8"?>
<LISTE>
{section name=articles loop=$articles}
    <ARTIKEL>
        <ARTIKELNUMMER>{$articles[articles].OXID}</ARTIKELNUMMER>
        <EAN>{$articles[articles].OXARTNUM}</EAN>
        <HERSTELLERARTIKELNUMMER>{$articles[articles].OXID}</HERSTELLERARTIKELNUMMER>
        <HERSTELLERNAME></HERSTELLERNAME>
        <PRODUKTNAME><![CDATA[{$articles[articles].OXTITLE}]]></PRODUKTNAME>
        <PRODUKTGRUPPE><![CDATA[Software & Spiele]]></PRODUKTGRUPPE>
        <PREIS>{$articles[articles].OXPRICE}</PREIS>
		<LIEFERZEIT>sofort lieferbar</LIEFERZEIT>
        <PRODUKTURL><![CDATA[{$host}/{$articles[articles].OXSEOURL}]]></PRODUKTURL>
        <BILDURL><![CDATA[{$host}/out/pictures/1/{$articles[articles].OXPIC1}]]></BILDURL>
        <VERSANDKOSTEN>
			<VORKASSE>2,90</VORKASSE>
			<PAYPAL>2,90</PAYPAL>
		</VERSANDKOSTEN>
        <VERSANDKOMMENTAR>Versand innerhalb 24 Stunden nach Geldeingang!</VERSANDKOMMENTAR>
    </ARTIKEL>
{/section}	
</LISTE>