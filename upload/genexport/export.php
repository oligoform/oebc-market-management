<?
 /*
 	Copyright 2010-2012 Alexander Pick (ap@pbt-media.com)

    This file is part of OEBC.

    OEBC is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    OEBC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with OEBC. If not, see http://www.gnu.org/licenses/.
*/


header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Datum in der Vergangenheit

//disable notcies about timezones on OSX
date_default_timezone_set(date_default_timezone_get());

ini_set("error_reporting", "E_ALL"); //  & ~E_NOTICE");
ini_set("display_errors", "On");

set_time_limit(0);
ini_set('memory_limit','-1');

require "../classes/subsystem.class.php";
require "../classes/db.class.php";
require "../classes/oxid.class.php";
require "../classes/smarty/Smarty.class.php";

$db 	= new dbal;
$files 	= new subsystem($db);

$modlist 	= $files->dirList(dirname(__FILE__).'/templates/');
$mode 	 	= $_REQUEST["mode"];

$mode = ereg_replace("[^A-Za-z0-9]", "", $mode);
$valid_mode = in_array($mode, $modlist);

//yes, we have that 
if($valid_mode) {

$smarty = new smarty;

$smarty->compile_dir  = dirname(__FILE__).'/../tmp/';
$smarty->template_dir = dirname(__FILE__).'/templates/';

$host = $files->getOpt("url");
$smarty->assign("host", $host);

$smarty->assign("shopname", $files->getOpt("shopname"));

$ox = new oxid($db);
$a = $ox->getSeoUrls();

// Build Pricelist

$pparentlist = array();

foreach($a as $aa) {
	if($aa["OXPARENTID"] == "")
	{
		$parentlist[$aa["OXID"]] = $aa;
	}
}

$articles = array();

foreach($a as $aa) {
		
	if($aa["OXPARENTID"] != "")
	{
		$aa["OXTITLE"] 	= $parentlist[$aa["OXPARENTID"]]["OXTITLE"];	
		$aa["OXLONGDESC"] = $parentlist[$aa["OXPARENTID"]]["OXLONGDESC"];	
		$aa["OXPIC1"] 	= $parentlist[$aa["OXPARENTID"]]["OXPIC1"];
		$aa["OXTAGS"] 	= $parentlist[$aa["OXPARENTID"]]["OXTAGS"];
		
		$aa["OXLONGDESC"] = filter_var($aa["OXLONGDESC"], FILTER_SANITIZE_STRING);
		$aa["OXLONGDESC"] = str_replace("!", ".", $aa["OXLONGDESC"]);
		
		$isd = strstr($aa["OXSTDURL"], 'cl=details');
		if($isd) {
			array_push($articles, $aa);
		}
	} 
		elseif(1) 
	{	
		$isd = strstr($aa["OXSTDURL"], 'cl=details');
		if($isd && ($aa["OXTITLE"] != "")) {
			array_push($articles, $aa);
		}
	}

}
	
$smarty->assign("articles", $articles);

$smarty->display($mode.".tpl");

} else {
	echo "ERROR!";
	exit(0);
}

?>

